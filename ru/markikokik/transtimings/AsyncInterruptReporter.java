package ru.markikokik.transtimings;

public interface AsyncInterruptReporter {
    boolean interrupted();
}
