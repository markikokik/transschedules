package ru.markikokik.transtimings;

import com.google.common.collect.Table;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ru.markikokik.transtimings.db.DataBase;
import ru.markikokik.transtimings.db.connectors.Connector;
import ru.markikokik.transtimings.entities.MonitoringLogEntry;
import ru.markikokik.transtimings.entities.Period;
import ru.markikokik.transtimings.entities.PositionRelToRoutes;
import ru.markikokik.transtimings.entities.RouteInfo;
import ru.markikokik.transtimings.entities.StopDistance;
import ru.markikokik.transtimings.entities.TransportPosition;
import ru.markikokik.transtimings.entities.ids.KR_ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;
import ru.markikokik.transtimings.entities.ids.RouteIDs;
import ru.markikokik.transtimings.entities.ids.MR_ID;
import ru.markikokik.transtimings.entities.ids.VehicleTN;

import static ru.markikokik.transtimings.Calculator.NEW_RACE_TIME_TRESHOLD;
import static ru.markikokik.transtimings.Utils.getDistBetween;
import static ru.markikokik.transtimings.Utils.getSpeedBetweenLatLng;


/**
 * Исправляем ошибки навигации и СА
 */
public class Corrector {
    public static final int ROUTE_LEAVE_TRESHOLD = 100;
    public static final int ROUTE_MOVE_TRESHOLD = 30;
    public static final double MAX_SPEED = 16.7;
    public static final int ROUTE_LEAVE_TRESHOLD_SQR = ROUTE_LEAVE_TRESHOLD * ROUTE_LEAVE_TRESHOLD;
    private static final int WINDOW_SIZE = 5;

    /**
     * найденные косяки в привязках контрольных точек
     */
    private static final int[][] cpRebinding = new int[][]{
            {86, 10230, 85},
            {86, 10243, 85},
            {86, 10103, 85},
            {86, 10264, 85},
            {86, 10557, 85},
            {86, 10363, 85},
            {86, 10360, 85},
            {86, 10358, 85},
            {86, 10355, 85},
            };


    /**
     * удаляем промежутки, когда датчик шлёт одинаковые координаты
     * @param vehicleMonitoring
     * @param routeInfo
     */
    private static void removeSensorFreezesForVehicle(List<? extends PositionRelToRoutes> vehicleMonitoring,
                                                      RouteInfo routeInfo) {
        double dist = 0;
        int indexStart, indexFreeze = 0, winIndex = 0, size = vehicleMonitoring.size();
        PositionRelToRoutes prevPos, pos;

        double[] window = new double[WINDOW_SIZE];
        boolean vehiclePrinted = false;
        for (indexStart = 1; indexStart < size; ) {
            prevPos = vehicleMonitoring.get(indexStart - 1);
            pos = vehicleMonitoring.get(indexStart);
            window[winIndex] = getDistBetween(prevPos, pos, routeInfo);
            if (indexFreeze < indexStart - 1 && (getSpeedBetweenLatLng(pos, prevPos) > MAX_SPEED ||
                                                 (pos.routeID == null || prevPos.routeID == null ||
                                                  pos.routeID.id != prevPos.routeID.id) &&
                                                 Math.abs(window[winIndex]) > 500 ||
                                                 pos.timestamp - prevPos.timestamp > 180)
                    ) {
//                if (!vehiclePrinted) {
//                    System.out.println("\n");
//                    System.out.println(((MonitoringLogEntry) pos).boardNum);
//                    vehiclePrinted = true;
//                }
//                System.out.print("\nDeleting ");
//                System.out.print(
//                        Printer.getFormattedDate(Printer.hmsDateFormat, vehicleMonitoring.get(indexFreeze).timestamp));
//                System.out.print(" - ");
//                System.out.print(Printer.getFormattedDate(Printer.hmsDateFormat,
//                                                          vehicleMonitoring.get(indexStart - 1).timestamp));
                vehicleMonitoring.subList(indexFreeze, indexStart).clear();
                size = vehicleMonitoring.size();
                indexStart = indexFreeze > 1 ? indexFreeze - 1 : 1;
                prevPos = vehicleMonitoring.get(indexStart - 1);
                if (indexStart < -size) {
                    pos = vehicleMonitoring.get(indexStart);
                    window[winIndex] = getDistBetween(prevPos, pos, routeInfo);
                }
            } else {
                indexStart++;
            }
            dist += window[winIndex];
            winIndex = (winIndex + 1) % WINDOW_SIZE;
            if (indexStart > WINDOW_SIZE) {
                dist -= window[winIndex];
            }
            if (dist > ROUTE_LEAVE_TRESHOLD) {
                indexFreeze = indexStart;
            }
        }
    }

    /**
     * вырезаем случайные выбросы, когда одна точка вдруг прыгает очень далеко
     * @param vehicleMonitoring
     * @param routeInfo
     */
    private static void removeFarCoordsFreezesForVehicle(List<? extends PositionRelToRoutes> vehicleMonitoring,
                                                         RouteInfo routeInfo) {
        int size = vehicleMonitoring.size(), indexStart, indexEnd;
        PositionRelToRoutes prevPos, pos;
        double dist;

        for (indexStart = 1; indexStart < size; ) {
            prevPos = vehicleMonitoring.get(indexStart - 1);
            pos = vehicleMonitoring.get(indexStart);
            if (getSpeedBetweenLatLng(pos, prevPos) > MAX_SPEED) {
                dist = 0;
                for (indexEnd = indexStart + 1;

                     indexEnd < size &&
                     getSpeedBetweenLatLng(prevPos = pos, pos = vehicleMonitoring.get(indexEnd)) <= MAX_SPEED &&
                     dist < ROUTE_LEAVE_TRESHOLD;

                     indexEnd++) {
                    dist += getDistBetween(prevPos, pos, routeInfo);
                }
                if (dist == 0) {
                    vehicleMonitoring.subList(indexStart, indexEnd).clear();
                    size = vehicleMonitoring.size();
                } else {
                    indexStart = indexEnd - 1;
                }
            } else {
                indexStart++;
            }
        }
    }


    /**
     * вырезаем одиночные точки, которые почему-то привязались к другому направлению
     * @param vehicleMonitoring
     */
    private static void filterDirectionNoiseForVehicle(List<? extends PositionRelToRoutes> vehicleMonitoring) {
        //фильтрация спонтанно привязавшихся нескольких точек с маленьким суммарным пробегом

        double dist;
        int indexStart = -1, i = 0;
        PositionRelToRoutes prevPos = null, pos, pos1;
        Iterator<? extends PositionRelToRoutes> iterator = vehicleMonitoring.iterator(),
                iterator1;
        List<? extends PositionRelToRoutes> interval;
        while (iterator.hasNext()) {
            pos = iterator.next();
            if (prevPos != null) {
                if (pos.routeID != prevPos.routeID) {
                    if (indexStart >= 0 && vehicleMonitoring.get(indexStart).routeID != null) {
                        interval = vehicleMonitoring.subList(indexStart, i);
                        dist = 0;

                        iterator1 = interval.iterator();
                        if (iterator.hasNext()) {
                            pos1 = iterator1.next();
                            while (iterator1.hasNext()) {
                                dist += getDistBetween(pos1, pos1 = iterator1.next());
                            }
                        }
                        if (dist < ROUTE_LEAVE_TRESHOLD) {
                            for (PositionRelToRoutes position : interval) {
                                position.routeID = null;
                                position.dist = -1;
                                position.nextStopDist = 0;
                                position.nextStopID = null;
                            }

                        }

                    }
                    indexStart = i;
                }
            }
            prevPos = pos;
            i++;
        }
    }

    /**
     * привязать мониторинг ТС к направлениям
     * @param vehicleMonitoring
     * @param routeInfo
     */
    private static void calcDirectionsForVehicle(List<? extends PositionRelToRoutes> vehicleMonitoring,
                                                 RouteInfo routeInfo) {
        if (routeInfo.geometries[0].length == 0) {
            return;
        }
        double dist;
        boolean reverse, jump;
        double[] mileagesFirst, mileagesLast;
        int KRindex, lastKRindex, idsCount = routeInfo.kr_ids.size(),
                candidatesCount;
        PositionRelToRoutes prevPos, prevPrevPos;
        StopDistance[] stopMileage = null;

        removeFarCoordsFreezesForVehicle(vehicleMonitoring, routeInfo);

        lastKRindex = -1;
        prevPos = null;
        prevPrevPos = null;
        dist = 0;
        for (PositionRelToRoutes position : vehicleMonitoring) {
            position.mileages = new double[idsCount];
            candidatesCount = 0;

            for (int i = 0; i < idsCount; i++) {
                if (Utils.getPointDistSqToRoute(routeInfo.geometries[i], position) <= ROUTE_LEAVE_TRESHOLD_SQR) {
                    candidatesCount++;
                    position.mileages[i] = Utils.getAnyPointMileage(routeInfo.geometries[i], position);
                } else {
                    position.mileages[i] = -1;
                }
            }

            jump = false;
            if (prevPos != null) {
                jump = position.timestamp - prevPos.timestamp > NEW_RACE_TIME_TRESHOLD;
                if (jump) {
                    lastKRindex = -1;
                }
            }

            if (candidatesCount > 0 && !jump) {
                if (prevPos != null) {
//                                    if (getSpeedBetweenLatLng(position, prevPos) <= MAX_SPEED) {
                    KRindex = -1;
                    mileagesFirst = prevPos.mileages;
                    mileagesLast = position.mileages;
                    reverse = false;

                    for (int i = 0; i < idsCount; i++) {
                        if (mileagesFirst[i] >= 0 && mileagesLast[i] >= 0) {
                            if (mileagesLast[i] - mileagesFirst[i] >= ROUTE_MOVE_TRESHOLD) {
                                KRindex = i;
                                break;
                            } else if (mileagesLast[i] - mileagesFirst[i] <=
                                       -ROUTE_MOVE_TRESHOLD && candidatesCount == 1) {
                                reverse = true;
                            }
                        }
                    }

                    if (KRindex >= 0) {
                        if (lastKRindex < 0) {
                            lastKRindex = KRindex;
                            stopMileage = routeInfo.stopMileages[lastKRindex];
                        }
                        if (prevPrevPos != null) {
                            lastKRindex = KRindex;
                            stopMileage = routeInfo.stopMileages[lastKRindex];
                            prevPrevPos.dist = prevPrevPos.mileages[lastKRindex];
                            determineNextStop(prevPrevPos,
                                              routeInfo.kr_ids.get(lastKRindex),
                                              routeInfo.stopMileages[lastKRindex]);

                            prevPrevPos = null;
                        }

                        if (KRindex != lastKRindex) {
                            prevPrevPos = position;
                        }

                    } else if (reverse) {
                        if (prevPrevPos == null) {
                            prevPrevPos = position;
                        } else {
                            lastKRindex = -1;
                            prevPrevPos = null;
                        }
                    }

                    if (lastKRindex >= 0) {
                        position.dist = position.mileages[lastKRindex];
                        determineNextStop(position, routeInfo.kr_ids.get(lastKRindex),
                                          routeInfo.stopMileages[lastKRindex]);

                        if (prevPos.routeID == null) {
                            prevPos.dist = prevPos.mileages[lastKRindex];
                            determineNextStop(prevPos, routeInfo.kr_ids.get(lastKRindex),
                                              routeInfo.stopMileages[lastKRindex]);
                        }

                        if (prevPos.nextStopID == stopMileage[stopMileage.length - 1].ks_id) {
                            if (prevPos.nextStopDist >= dist || prevPos.nextStopDist < 1) {
                                dist = Double.MAX_VALUE;
                                lastKRindex = -1;//idsCount - lastKRindex - 1;}
                            } else {
                                dist = prevPos.nextStopDist;
                            }
                        } else if (dist < Double.MAX_VALUE) {
                            dist = Double.MAX_VALUE;
                            lastKRindex = -1;//idsCount - lastKRindex - 1;}
                        }
                    }

                }

            }
            prevPos = position;
        }

        filterDirectionNoiseForVehicle(vehicleMonitoring);
        removeSensorFreezesForVehicle(vehicleMonitoring, routeInfo);
    }

    /**
     * посчитать расстояния от начала маршрута для всех отсчётов
     * @param vehicleMonitoring
     * @param routeInfo
     * @param idsCount
     */
    private static void calcMileages(List<? extends PositionRelToRoutes> vehicleMonitoring,
                                     RouteInfo routeInfo, int idsCount) {
        for (PositionRelToRoutes position : vehicleMonitoring) {
            position.mileages = new double[idsCount];

            for (int i = 0; i < idsCount; i++) {
                if (Utils.getPointDistSqToRoute(routeInfo.geometries[i], position) <= ROUTE_LEAVE_TRESHOLD_SQR) {
                    position.mileages[i] = Utils.getAnyPointMileage(routeInfo.geometries[i], position);
                } else {
                    position.mileages[i] = -1;
                }
            }
        }
    }

    /**
     * эксперименты по улучшению старого метода
     * @param vehicleMonitoring
     * @param routeInfo
     */
    private static void calcDirectionsForVehicleNew(List<? extends PositionRelToRoutes> vehicleMonitoring,
                                                    RouteInfo routeInfo) {
        if (routeInfo.geometries[0].length == 0) {
            return;
        }
        int KRindex = -1, idsCount = routeInfo.kr_ids.size(), winStart = 0, newWinStart;
        PositionRelToRoutes pos, prevPos, winPrevPos, winPos;

        removeFarCoordsFreezesForVehicle(vehicleMonitoring, routeInfo);

        calcMileages(vehicleMonitoring, routeInfo, idsCount);

        int firstCount, secondCount;
        double mileage, speed;
        Iterator<? extends PositionRelToRoutes> iterator = vehicleMonitoring.iterator();
        prevPos = iterator.next();
        for (int posIndex = 1; iterator.hasNext(); posIndex++) {
            pos = iterator.next();

            if (pos.timestamp - prevPos.timestamp > NEW_RACE_TIME_TRESHOLD) {
                winStart = posIndex;
                KRindex = -1;
            } else {
                newWinStart = posIndex - WINDOW_SIZE;
                if (newWinStart >= winStart) {
                    winStart = newWinStart;
                } else if (newWinStart < 0) {
                    winStart = 0;
                }

            }

            firstCount = 0;
            secondCount = 0;
            winPrevPos = vehicleMonitoring.get(winStart);
            for (int w = winStart + 1, i = 0; w <= posIndex; w++, i++) {
                winPos = vehicleMonitoring.get(w);

                speed = idsCount == 1 ?
                        Math.abs(Utils.getSpeedBetweenSamples(winPos, winPrevPos)) :
                        Utils.getSpeedBetweenLatLng(winPos, winPrevPos);
                if (speed > MAX_SPEED) {
                    continue;
                }

                if (winPos.mileages[0] >= 0 && winPrevPos.mileages[0] >= 0) {
                    mileage = winPos.mileages[0] - winPrevPos.mileages[0];

                    if (mileage > ROUTE_MOVE_TRESHOLD) {
                        firstCount++;
                    } else if (mileage < -ROUTE_MOVE_TRESHOLD) {
                        firstCount--;
                    }
                }

                if (idsCount > 1 && winPos.mileages[1] >= 0 && winPrevPos.mileages[1] >= 0) {
                    mileage = winPos.mileages[1] - winPrevPos.mileages[1];
                    if (mileage > ROUTE_MOVE_TRESHOLD) {
                        secondCount++;
                    } else if (mileage < -ROUTE_MOVE_TRESHOLD) {
                        secondCount--;
                    }
                }

                winPrevPos = winPos;
            }

//            KRindex = -1;
            if (firstCount > secondCount) {
                KRindex = 0;
            } else if (firstCount < secondCount) {
                if (idsCount > 1) {
                    KRindex = 1;
                } else {
                    KRindex = -1;
                }
            }
            if (KRindex >= 0 && pos.mileages[KRindex] >= 0) {
                pos.routeID = routeInfo.kr_ids.get(KRindex);
                pos.dist = pos.mileages[KRindex];
                determineNextStop(pos, routeInfo.stopMileages[KRindex]);
            } else {
                pos.routeID = null;
                pos.dist = -1;
                pos.nextStopDist = -1;
                pos.nextStopID = null;
            }
            prevPos = pos;
        }

        filterDirectionNoiseForVehicle(vehicleMonitoring);
//        removeSensorFreezesForVehicle(vehicleMonitoring, routeInfo);
    }

    /**
     * пересчитать направления для всего массива мониторинга
     * @param db
     * @param positions
     * @param routeIds
     * @param periods
     * @param routeInfos
     * @throws SQLException
     */
    public static void correctDirections(DataBase db,
                                         Table<MR_ID, VehicleTN, ? extends List<? extends PositionRelToRoutes>> positions,
                                         Table<MR_ID, KR_ID, Object> routeIds,
                                         Map<KR_ID, List<Period>> periods,
                                         Table<KR_ID, Period, RouteInfo> routeInfos) throws SQLException {
        RouteIDs currentRouteIDs;
        RouteInfo relatedRoutesInfo;
        List<? extends PositionRelToRoutes> vehicleMonitoring;
        int i, dayBound, time;
        List<Period> routePeriods;
        KR_ID firstKR_ID, nullID = new KR_ID(0);


        Collection<KR_ID> disabled = new ArrayList<>();
//        disabled.add(new KR_ID(116));
//        disabled.add(new KR_ID(107));
        boolean found;

        Period period;
        Iterator<? extends PositionRelToRoutes> iterator;

        for (Map.Entry<MR_ID, ? extends Map<VehicleTN, ? extends List<? extends PositionRelToRoutes>>> routeMonitoringEntry : positions
                .rowMap().entrySet()) {
            if (routeMonitoringEntry.getKey().id != 44) { //37: 20; 12: 4; 44,45: 13
                continue;
            }

            if (routeMonitoringEntry.getKey().id == 0) {
                continue;
            }

//            System.out.print(routeMonitoringEntry.getKey() + ": ");
            currentRouteIDs = new RouteIDs();
            currentRouteIDs.kr_ids.addAll(routeIds.row(routeMonitoringEntry.getKey()).keySet());

            found = false;
            for (KR_ID kr_id : disabled) {
                if (currentRouteIDs.kr_ids.contains(kr_id)) {
                    found = true;
                    break;
                }
            }
            if (found) {
                continue;
            }

            currentRouteIDs.kr_ids.remove(nullID);
            currentRouteIDs.transType = routeMonitoringEntry.getKey().transType;
            currentRouteIDs.tn_ids.clear();
            for (KR_ID kr_id : currentRouteIDs.kr_ids) {
                currentRouteIDs.tn_ids.addAll(routeIds.column(kr_id).keySet());
            }
            if (currentRouteIDs.tn_ids.size() == 0) {
                continue;
            }

            routePeriods = periods.get(firstKR_ID = currentRouteIDs.kr_ids.get(0));
            if (routePeriods == null) {
                periods.put(firstKR_ID, routePeriods = db.getAllRoutePeriods(firstKR_ID));
            }

            for (Map.Entry<VehicleTN, ? extends List<? extends PositionRelToRoutes>> vehicleMonitoringEntry : routeMonitoringEntry
                    .getValue().entrySet()) {
//                if (routeMonitoringEntry.getKey().id == 10034) {
//                    System.out.println(vehicleMonitoringEntry.getKey());
//                }
                vehicleMonitoring = vehicleMonitoringEntry.getValue();
                i = 1;
                iterator = vehicleMonitoring.iterator();
                dayBound = Utils.getDayEnd(iterator.next().timestamp);
                period = Utils.getPeriod(routePeriods, dayBound - 60);
                relatedRoutesInfo = routeInfos.get(firstKR_ID, period);
                if (relatedRoutesInfo == null) {
                    routeInfos.put(firstKR_ID, period,
                                   relatedRoutesInfo = db.getRelatedRoutesInfo(currentRouteIDs, period.id));
                    //Utils.getMidday(dayBound - 60));)
                }
                relatedRoutesInfo.updateIds(currentRouteIDs);
                while (iterator.hasNext()) {
                    time = iterator.next().timestamp;
//                    if (routeMonitoringEntry.getKey().id == 10034) {
//                        System.out.println(time);
//                    }
                    if (time >= dayBound) {
                        calcDirectionsForVehicle(vehicleMonitoring = vehicleMonitoring.subList(0, i),
                                                 relatedRoutesInfo);
                        iterator = vehicleMonitoring.iterator();
                        i = 1;
                        dayBound = Utils.getDayEnd(time);
                        if (period.endTime < dayBound) {
                            period = Utils.getPeriod(routePeriods, dayBound - 60);
                            relatedRoutesInfo = routeInfos.get(firstKR_ID, period);
                            if (relatedRoutesInfo == null) {
                                routeInfos.put(firstKR_ID, period,
                                               relatedRoutesInfo =
                                                       db.getRelatedRoutesInfo(currentRouteIDs, period.id));
                                //Utils.getMidday(dayBound - 60));)
                            }
                            relatedRoutesInfo.updateIds(currentRouteIDs);
                        }
                    } else {
                        i++;
                    }
                }
                if (i > 1) {
                    calcDirectionsForVehicleNew(vehicleMonitoring, relatedRoutesInfo);
                }
            }
//            Printer.printVehicleDirections(out, db, vehicleMonitoringEntry.getValue(),
//                                           db.getVehicle(vehicleMonitoringEntry.getKey()));
//            if (n++ % 10 == 0) {
//                System.out.println(n + "/" + count);
//            }

//            System.out.println(routeMonitoringEntry.getValue().size() + " vehicles");


        }
    }


    /**
     * какая следующая остановка для данной точки. Дёргает следующий метод
     * @param pos1
     * @param kr_id
     * @param stopMileages
     */
    private static void determineNextStop(PositionRelToRoutes pos1, KR_ID kr_id,
                                          StopDistance[] stopMileages) {
        determineNextStop(pos1, stopMileages);

        if (pos1.dist < 0) {
            return;
        }
        pos1.routeID = kr_id;
    }

    /**
     * какая следующая остановка для данной точки
     * @param pos1
     * @param stopMileages
     */
    public static void determineNextStop(TransportPosition pos1, StopDistance[] stopMileages) {
        int length = stopMileages.length;

        double dist = pos1.dist;
        if (dist < 0) {
            return;
        }

        if (dist > stopMileages[length - 1].dist && stopMileages[length - 1].ks_id.id != stopMileages[0].ks_id.id) {
            pos1.nextStopID = stopMileages[length - 1].ks_id;
            pos1.nextStopDist = 0;
        } else if (dist < stopMileages[0].dist) {
            pos1.nextStopID = stopMileages[0].ks_id;
            pos1.nextStopDist = stopMileages[0].dist - dist;
        } else {
            pos1.nextStopID = new KS_ID(-1);
            pos1.nextStopDist = -1;
            for (int i = 1; i < length; i++) {
                if (dist <= stopMileages[i].dist && dist >= stopMileages[i - 1].dist) {
                    pos1.nextStopID = stopMileages[i].ks_id;
                    pos1.nextStopDist = stopMileages[i].dist - dist;
                }
            }
        }
    }

    /**
     * удалить дубликаты контрольных точек. На самом деле они не совсем одно и то же, но привязались к одной остановке
     * @param connector
     */
    public static void removeCPduplicates(Connector connector) {
        try (DataBase db = new DataBase(connector)) {
            db.removeCPduplicates();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * пока не догадался о смысле KT_ID, удалял его дубликаты из базы
     * @param connector
     */
    public static void removeTransportDuplicates(Connector connector) {
        try (DataBase db = new DataBase(connector)) {
            db.removeVehicleDuplicates();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    public static void correctDirections(DataBase db,
                                         Map<Integer, Table<MR_ID, VehicleTN, List<MonitoringLogEntry>>> positions,
                                         Map<Integer, Table<MR_ID, KR_ID, Object>> routeIds,
                                         Map<KR_ID, List<Period>> periods,
                                         Table<KR_ID, Period, RouteInfo> routeInfos) throws SQLException {
        Integer transType;
        Table<MR_ID, KR_ID, Object> transTypeIds;
        RouteIDs currentRouteIDs = new RouteIDs();
        RouteInfo relatedRoutesInfo;
        List<MonitoringLogEntry> vehicleMonitoring;
        int i, dayBound, time;
        List<Period> routePeriods;
        Set<MR_ID> tn_ids = new HashSet<>();
        KR_ID firstKR_ID;
        Period period;
        Iterator<MonitoringLogEntry> iterator;

        for (Map.Entry<Integer, Table<MR_ID, VehicleTN, List<MonitoringLogEntry>>> transTypeMonitoringEntry :
                positions.entrySet()) {
            transType = transTypeMonitoringEntry.getKey();
            if (transType == 0) {
                continue;
            }
            transTypeIds = routeIds.get(transType);
            for (Map.Entry<MR_ID, Map<VehicleTN, List<MonitoringLogEntry>>> routeMonitoringEntry :
                    transTypeMonitoringEntry.getValue().rowMap().entrySet()) {
                System.out.print(routeMonitoringEntry.getKey() + ": ");
                currentRouteIDs.kr_ids.clear();
                currentRouteIDs.kr_ids.addAll(transTypeIds.row(routeMonitoringEntry.getKey()).keySet());
                currentRouteIDs.tn_ids.clear();
                tn_ids.clear();
                for (KR_ID kr_id : currentRouteIDs.kr_ids) {
                    if (kr_id.id == 0) {
                        continue;
                    }

                    tn_ids.addAll(transTypeIds.column(kr_id).keySet());
                }
                currentRouteIDs.tn_ids.addAll(tn_ids);
                if (currentRouteIDs.tn_ids.size() == 0) {
                    continue;
                }

                routePeriods = periods.get(firstKR_ID = currentRouteIDs.kr_ids.get(0));
                if (routePeriods == null) {
                    periods.put(firstKR_ID, routePeriods = db.getAllRoutePeriods(firstKR_ID));
                }

                for (Map.Entry<VehicleTN, List<MonitoringLogEntry>> vehicleMonitoringEntry :
                        routeMonitoringEntry.getValue().entrySet()) {
//                    System.out.println(vehicleMonitoringEntry.getKey());
                    vehicleMonitoring = vehicleMonitoringEntry.getValue();
                    i = 1;
                    iterator = vehicleMonitoring.iterator();
                    dayBound = Utils.getDayEnd(iterator.next().timestamp);
                    period = Utils.getPeriod(routePeriods, dayBound - 60);
                    relatedRoutesInfo = routeInfos.get(firstKR_ID, period);
                    if (relatedRoutesInfo == null) {
                        routeInfos.put(firstKR_ID, period,
                                       relatedRoutesInfo = db.getRelatedRoutesInfo(currentRouteIDs, period.id));
                        //Utils.getMidday(dayBound - 60));)
                    }
                    relatedRoutesInfo.updateIds(currentRouteIDs);
                    while (iterator.hasNext()) {
                        time = iterator.next().timestamp;
                        if (time >= dayBound) {
//                            calcDirectionsForVehicle(vehicleMonitoring = vehicleMonitoring.subList(0, i),
//                                                     relatedRoutesInfo);
                            iterator = vehicleMonitoring.iterator();
                            i = 1;
                            dayBound = Utils.getDayEnd(time);
                            if (period.endTime < dayBound) {
                                period = Utils.getPeriod(routePeriods, dayBound - 60);
                                relatedRoutesInfo = routeInfos.get(firstKR_ID, period);
                                if (relatedRoutesInfo == null) {
                                    routeInfos.put(firstKR_ID, period,
                                                   relatedRoutesInfo =
                                                           db.getRelatedRoutesInfo(currentRouteIDs, period.id));
                                    //Utils.getMidday(dayBound - 60));)
                                }
                                relatedRoutesInfo.updateIds(currentRouteIDs);
                            }
                        } else {
                            i++;
                        }
                    }
                    if (i > 1) {
//                        calcDirectionsForVehicle(vehicleMonitoring, relatedRoutesInfo);
                    }

//                    Printer.printVehicleDirections(out, db, vehicleMonitoringEntry.getValue(),
//                                                   db.getVehicle(vehicleMonitoringEntry.getKey()));
                }
                System.out.println(routeMonitoringEntry.getValue().entrySet().size() + " vehicles");
            }
        }
    }
}
