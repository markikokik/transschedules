package ru.markikokik.transtimings;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import ru.markikokik.transtimings.db.Complementator;
import ru.markikokik.transtimings.db.DataBase;
import ru.markikokik.transtimings.db.connectors.Connector;
import ru.markikokik.transtimings.db.connectors.MysqlConnector;
import ru.markikokik.transtimings.entities.ControlPoint;
import ru.markikokik.transtimings.entities.PlanTimingRecord;
import ru.markikokik.transtimings.entities.Route;
import ru.markikokik.transtimings.entities.TransType;
import ru.markikokik.transtimings.entities.ids.CP_ID;
import ru.markikokik.transtimings.entities.ids.KR_ID;
import ru.markikokik.transtimings.entities.ids.KT_ID;
import ru.markikokik.transtimings.entities.ids.MR_ID;
import ru.markikokik.transtimings.entities.ids.PeriodID;
import ru.markikokik.transtimings.entities.ids.RaceID;

import static ru.markikokik.transtimings.Calculator.calcScheduleDeviation;
import static ru.markikokik.transtimings.Corrector.removeTransportDuplicates;

public class Main {


    //2016.03.21 23:59:06
    public static void main(String[] args) {
        String logFile = null, timingsFile = null, outPath = null, url = null;
        KR_ID kr_id = null;
        boolean bindControlPoints = false, parseMonitoring = false, parseTimings = false,
                stopsXY = false, routesXY = false, calc = false, measureStopsDist = false,
                calcRealSchedule = false,
                printRealSchedule = false, calcDeviations = false, removeDuplicates = false,
                correctDirections = false, removeCPduplicates = false, detectAccidents = false,
                printDeviations = false,
                printDirections = false, printAccidents = false, accidentsToHtml = false,
                stays = false, calc20_3_frunze = false, logsToBin = false, planTimings = false,
                update = false, graph = false;
        int[] timeLimits = Utils.allTime.clone();
        String[] arg;
        for (int i = 0; i < args.length; i++) {
            arg = args[i].split("=");
            switch (arg[0]) {
                case "graph":
                    graph = true;
                    break;
                case "update":
                    update = true;
                    break;
                case "url":
                    url = arg[1];
                    break;
                case "planTimings":
                    planTimings = true;
                    break;
                case "logsToBin":
                    logsToBin = true;
                    break;
                case "out":
                    logsToBin = true;
                    outPath = arg[1];
                    break;
                case "calc20_3_frunze":
                    calc20_3_frunze = true;
                    break;
                case "log":
                    logFile = arg[1];
                    break;
                case "date":
                    timeLimits = Utils.getDayLimits(arg[1]);
                    break;
                case "times":
                    timingsFile = arg[1];
                    break;
                case "bind":
                    bindControlPoints = true;
                    break;
                case "monitoring":
                    parseMonitoring = true;
                    break;
                case "detectAccidents":
                    detectAccidents = true;
                    break;
                case "timings":
                    parseTimings = true;
                    break;
                case "printDirections":
                    printDirections = true;
                    break;
                case "printDeviations":
                    printDeviations = true;
                    break;
                case "stops_xy":
                    stopsXY = true;
                    break;
                case "routes_xy":
                    routesXY = true;
                    break;
                case "measureStopsDist":
                    measureStopsDist = true;
                    break;
                case "calcRealSchedule":
                    calcRealSchedule = true;
                    break;
                case "calc":
                    calc = true;
                    break;
                case "printRealSchedule":
                    printRealSchedule = true;
                    break;
                case "calcDeviations":
                    calcDeviations = true;
                    break;
                case "removeDuplicates":
                    removeDuplicates = true;
                    break;
                case "correctDirections":
                    correctDirections = true;
                    break;
                case "removeCPduplicates":
                    removeCPduplicates = true;
                    break;
                case "accidentsToHtml":
                    accidentsToHtml = true;
                    break;
                case "printAccidents":
                    printAccidents = true;
                    break;
                case "KR_ID":
                    kr_id = new KR_ID(Integer.parseInt(arg[1]));
                    break;
                case "stays":
                    stays = true;
                    break;
            }
        }


//        Connector connector = new SqliteConnector(dbFile);
        Connector connector = getLocalConnector();
//        Connector connector = new SqlServerConnector();

        if (graph) {
            Complementator.buildTramGraph(connector);
        }
        if (update && url != null) {
            try {
                File file = new File("classifiers.zip");
                FileOutputStream out = new FileOutputStream(file);
                NetworkUtils.downloadUrlToStream(url, out, 60000, null, null);
                out.close();
                new DataBase(connector).updateFromZip(file);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (logsToBin && outPath != null && logFile != null) {
            Parser.convertLogsToBin(logFile, outPath);
        }

        if (calc20_3_frunze) {
            Printer.printStatsTram3_20(connector, null);
        }
        if (stays) {
            Calculator.stationStats(connector, 0);
        }
        if (printAccidents) {
            Printer.printAccidents(connector, Utils.getDayLimits(2016, 07, 11));
        }
        if (removeDuplicates) {
            removeTransportDuplicates(connector);
        }
        if (printDirections && kr_id != null) {
            Printer.printDirections(connector, kr_id, Utils.getDayLimits(2016, 07, 11));
        }
        if (accidentsToHtml) {
            Printer.visualizeAccidents(connector, timeLimits);
        }
        if (printDeviations && kr_id != null) {
            Printer.printDeviations(connector, kr_id, timeLimits);
        }
        if (detectAccidents) {
            Calculator.detectAccidentsByVehicle(connector, timeLimits);
        }
        if (removeCPduplicates) {
            Corrector.removeCPduplicates(connector);
        }
//        if (correctDirections ) {
//            Corrector.correctDirections(connector, timeLimits);
//        }
        if (calcDeviations) {
            calcScheduleDeviation(connector, timeLimits);
        }
        if (printRealSchedule && kr_id != null) {
            Printer.printRealSchedule(connector, kr_id, timeLimits);
        }
        if (calcRealSchedule) {
            Calculator.calcRealSchedule(connector, timeLimits);
        }
        if (measureStopsDist) {
            Complementator.measureStopsDist(connector, null);
        }

        if (stopsXY) {
            Complementator.stopsXY(connector);
        }
        if (routesXY) {
            Complementator.routesXY(connector, null);
        }

        if (bindControlPoints) {
            Complementator.bindControlPoints(connector);
        }

        if (parseMonitoring && logFile != null) {
            Parser.parseMonitoring(connector, logFile);
        }
        if (parseTimings && timingsFile != null) {
            Parser.parseTimings(connector, timingsFile);
        }

        if (planTimings && timingsFile != null) {
            try {
                List<PlanTimingRecord> planTimingRecords = Parser.parsePlanRaces(
                        timingsFile,
                        TransType.TRAM
                );
                Table<MR_ID, KT_ID, Map<RaceID, List<PlanTimingRecord>>> schedule
                        = Utils.splitPlanRacesByRaces(planTimingRecords);

//                Table<MR_ID, Character, List<PlanTimingRecord>> schByDirections =
//                        HashBasedTable.create();
//                for (Map.Entry<MR_ID, Map<KT_ID, Map<RaceID, List<PlanTimingRecord>>>> routeSch :
//                        schedule.rowMap().entrySet()) {
//                    MR_ID mr_id = routeSch.getKey();
//                    Map<Character, List<PlanTimingRecord>> routeDirections = schByDirections.row(mr_id);
//
//                    for (Map<RaceID, List<PlanTimingRecord>> vehicleRaces : routeSch.getValue()
//                                                                                    .values()) {
//                        for (List<PlanTimingRecord> race : vehicleRaces.values()) {
//                            char directionChar = race.get(0).direction;
//                            if (directionChar == '-') {
//                                continue;
//                            }
//                            List<PlanTimingRecord> existing =
//                                    routeDirections.get(directionChar);
//                            if (existing == null) {
//                                routeDirections.put(directionChar, race);
//                            } else {
//                                boolean matching = existing.size() == race.size();
//                                if (matching) {
//                                    for (int i = 0; i < race.size() && matching; i++) {
//                                        PlanTimingRecord record1 = race.get(i);
//                                        PlanTimingRecord record2 = existing.get(i);
//                                        matching &= record1.id == record2.id;
//                                    }
//                                }
//                                if (!matching) {
//                                    System.out.print(mr_id);
//                                    System.out.print(" ");
//                                    System.out.println(directionChar);
//                                }
//                            }
//
//                        }
//
//                    }
//
//
//                }


                DataBase db = new DataBase(connector);


//                Table<CP_ID, KR_ID, Integer> routesThruCPs = HashBasedTable.create();
                Table<CP_ID, KR_ID, Integer> routesThruCPs = db.getAllControlPointsOrdered(
                        new PeriodID(0)
                );
                List<PlanTimingRecord> records;
                Map<KR_ID, List<ControlPoint>> controlPoints = new HashMap<>();
                KR_ID detectedKr_id;
//                int lastStopForCP;
                Map<KR_ID, Route> routes = new HashMap<>();


                MR_ID mr_id;
                List<KR_ID> routeIDs;

//                mr_id = new MR_ID(21, 2);
//                routeIDs = db.getRelatedRouteIDs(mr_id, 0).kr_ids;
//                records = schedule.get(mr_id, new KT_ID(4953054)).get(new RaceID(15));
//
//                int[] ids = new int[]{10342, 10334};
//                Collection<KR_ID> allowed = new HashSet<>();
//                for (int id : ids) {
//                    kr_id = new KR_ID(id);
//                    allowed.add(kr_id);
//                }
//
//
//                detectedKr_id = Calculator.detectRaceKR_IDs(
//                        records,
//                        routeIDs,
//                        routesThruCPs
//                );

                FileOutputStream out =
                        new FileOutputStream("/media/Data/Documents/detected_routes.txt");

                System.setOut(new PrintStream(out));

                Table<MR_ID, KR_ID, List<List<PlanTimingRecord>>> matching = HashBasedTable.create();
                for (Map.Entry<MR_ID, Map<KT_ID, Map<RaceID, List<PlanTimingRecord>>>> routeSchedule :
                        schedule.rowMap().entrySet()) {
                    mr_id = routeSchedule.getKey();
                    Map<KR_ID, List<List<PlanTimingRecord>>> routeMatching = matching.row(mr_id);
                    routeIDs = db.getRelatedRouteIDs(mr_id, 0).kr_ids;
                    for (Map.Entry<KT_ID, Map<RaceID, List<PlanTimingRecord>>> vehicleRaces :
                            routeSchedule.getValue().entrySet()) {
                        for (Map.Entry<RaceID, List<PlanTimingRecord>> race :
                                vehicleRaces.getValue().entrySet()) {
                            records = race.getValue();

                            detectedKr_id = Calculator.detectRaceKR_IDs(
                                    records,
                                    routeIDs,
                                    routesThruCPs
                            );


                            List<List<PlanTimingRecord>> races = routeMatching.get(detectedKr_id);
                            if (races == null) {
                                routeMatching.put(detectedKr_id, races = new ArrayList<>());
                                races.add(records);
                            } else {
                                boolean found = false;
                                Iterator<List<PlanTimingRecord>> iterator = races.iterator();
                                while (!found && iterator.hasNext()) {
                                    List<PlanTimingRecord> exRace = iterator.next();
                                    boolean match = records.size() == exRace.size();
                                    if (match) {
                                        Iterator<PlanTimingRecord> iterator1 = records.iterator(),
                                                iterator2 = exRace.iterator();
                                        while (match && iterator1.hasNext()) {
                                            PlanTimingRecord record1 = iterator1.next();
                                            PlanTimingRecord record2 = iterator2.next();
                                            match &= record1.id == record2.id;
                                        }
                                    }
                                    if (match) {
                                        found = true;
                                    }
                                }
                                if (!found) {
                                    races.add(records);
                                }
                            }


//                            System.out.print(routeSchedule.getKey());
//                            System.out.print(" and ");
//                            System.out.print(vehicleRaces.getKey());
//                            System.out.print(" race ");
//                            System.out.println(race.getKey());
//
//
//                            if (detectedKr_id.id > 0) {
//                                Route route = routes.get(detectedKr_id);
//                                if (route == null) {
//                                    routes.put(detectedKr_id, route = db.getRoute(detectedKr_id));
//                                }
//                                System.out.print("detected ");
//                                System.out.print(route.kr_id);
//                                System.out.print(" ");
//                                System.out.print(route.getNumber());
//                                System.out.print(" >> ");
//                                System.out.println(route.direction);
//                                List<ControlPoint> points = controlPoints.get(detectedKr_id);
//                                if (points == null) {
//                                    controlPoints.put(detectedKr_id, points =
//                                            db.getRouteControlPoints(detectedKr_id, new PeriodID(0)));
//                                }
//                                int len = Math.min(points.size(), records.size());
//                                for (int i = 0; i < len; i++) {
//                                    System.out.print(records.get(i).id);
//                                    System.out.print(" ");
//                                    System.out.print(records.get(i).cpShortName);
//                                    System.out.print("\t");
//                                    System.out.print(points.get(i).cp_id.id);
//                                    System.out.print(" ");
//                                    System.out.println(points.get(i).name);
//                                }
//                                for (int i = len; i < records.size(); i++) {
//                                    System.out.print(records.get(i).id);
//                                    System.out.print(" ");
//                                    System.out.println(records.get(i).cpShortName);
//                                }
//                                for (int i = len; i < points.size(); i++) {
//                                    System.out.print("\t\t");
//                                    System.out.print(points.get(i).cp_id.id);
//                                    System.out.print(" ");
//                                    System.out.println(points.get(i).name);
//                                }
//                            } else {
//                                System.out.println("No route detected");
//                                for (PlanTimingRecord record : records) {
//                                    System.out.print(record.id);
//                                    System.out.print(" ");
//                                    System.out.println(record.cpShortName);
//                                }
//                            }
//                            System.out.println();
//                            System.out.println();
                        }
                    }
                }

//                Set<KR_ID> uniqKr_ids = new HashSet<>();
//                for (Map.Entry<MR_ID, Map<Character, List<KR_ID>>> routeDirections : directions.rowMap()
//                                                                                               .entrySet()) {
//                    System.out.println(routeDirections.getKey());
//                    Map<Character, List<List<PlanTimingRecord>>> routeRacesPoints =
//                            factRaces.row(routeDirections.getKey());
//                    for (Map.Entry<Character, List<KR_ID>> direction : routeDirections.getValue()
//                                                                                      .entrySet()) {
//                        List<KR_ID> kr_ids = direction.getValue();
//                        List<List<PlanTimingRecord>> directionsSchedule =
//                                routeRacesPoints.get(direction.getKey());
//                        Iterator<KR_ID> kr_idIterator = kr_ids.iterator();
//                        Iterator<List<PlanTimingRecord>> scheduleIterator =
//                                directionsSchedule.iterator();
//                        uniqKr_ids.clear();
//                        for (;
//                             kr_idIterator.hasNext(); ) {
//                            kr_id = kr_idIterator.next();
//                            records = scheduleIterator.next();
//
//                            if (uniqKr_ids.contains(kr_id)) {
//                                continue;
//                            }
//                            uniqKr_ids.add(kr_id);
//
//                            List<ControlPoint> routeControlPoints = controlPoints.get(kr_id);
//                            if (routeControlPoints == null) {
//                                controlPoints.put(kr_id, routeControlPoints =
//                                        db.getRouteControlPoints(kr_id, new PeriodID(0)));
//                            }
//
//                            System.out.print(records.get(0).kt_id);
//                            System.out.print(" ");
//                            System.out.println(records.get(0).raceNumber);
//                            System.out.print(direction.getKey());
//                            if (kr_id.id > 0) {
//                                Route route = routes.get(kr_id);
//                                if (route == null) {
//                                    routes.put(kr_id, route = db.getRoute(kr_id));
//                                }
//                                System.out.print(": ");
//                                System.out.print(route.kr_id);
//                                System.out.print(" ");
//                                System.out.print(route.getNumber());
//                                System.out.print(" >> ");
//                                System.out.println(route.direction);
//
//                                int len = Math.min(routeControlPoints.size(), records.size());
//                                for (int i = 0; i < len; i++) {
//                                    System.out.print(records.get(i).id);
//                                    System.out.print(" ");
//                                    System.out.print(records.get(i).cpShortName);
//                                    System.out.print("\t");
//                                    System.out.print(routeControlPoints.get(i).cp_id.id);
//                                    System.out.print(" ");
//                                    System.out.println(routeControlPoints.get(i).name);
//                                }
//                                for (int i = len; i < records.size(); i++) {
//                                    System.out.print(records.get(i).id);
//                                    System.out.print(" ");
//                                    System.out.println(records.get(i).cpShortName);
//                                }
//                                for (int i = len; i < routeControlPoints.size(); i++) {
//                                    System.out.print("\t\t");
//                                    System.out.print(routeControlPoints.get(i).cp_id.id);
//                                    System.out.print(" ");
//                                    System.out.println(routeControlPoints.get(i).name);
//                                }
//                            } else {
//                                System.out.println("No route detected");
//                                for (PlanTimingRecord record : records) {
//                                    System.out.print(record.id);
//                                    System.out.print(" ");
//                                    System.out.println(record.cpShortName);
//                                }
//                            }
//                        }
//                        System.out.println();
//                        System.out.println();
//                    }
//                    System.out.println();
//                    System.out.println();
//                }


                /////////////////////////
                /////////////////////////
                /////////////////////////
                for (Map.Entry<MR_ID, Map<KR_ID, List<List<PlanTimingRecord>>>> routeMatching :
                        matching.rowMap().entrySet()) {
                    mr_id = routeMatching.getKey();
                    System.out.println(mr_id);
                    for (Map.Entry<KR_ID, List<List<PlanTimingRecord>>> directionMatching :
                            routeMatching.getValue().entrySet()) {
                        kr_id = directionMatching.getKey();
                        List<ControlPoint> routeControlPoints = controlPoints.get(kr_id);
                        if (routeControlPoints == null) {
                            controlPoints.put(
                                    kr_id,
                                    routeControlPoints = kr_id.id > 0 ?
                                                         db.getRouteControlPoints(
                                                                 kr_id,
                                                                 new PeriodID(0)
                                                         ) :
                                                         new ArrayList<ControlPoint>()
                            );
                        }

                        Route route = routes.get(kr_id);
                        if (route == null) {
                            routes.put(kr_id, route = db.getRoute(kr_id));
                        }
                        if (route == null) {
                            route = new Route();
                        }
                        System.out.print(route.kr_id);
                        System.out.print(" ");
                        System.out.print(route.getNumber());
                        System.out.print(" >> ");
                        System.out.println(route.direction);

                        System.out.println("ROUTE");
                        for (ControlPoint point : routeControlPoints) {
                            System.out.print(point.cp_id.id);
                            System.out.print(" ");
                            System.out.println(point.name);
                        }
                        System.out.println();

                        System.out.println("CP LISTS");
                        for (List<PlanTimingRecord> recordList : directionMatching.getValue()) {
                            for (PlanTimingRecord record : recordList) {
                                System.out.print(record.id);
                                System.out.print(" ");
                                System.out.println(record.cpShortName);
                            }
                            System.out.println();
                        }

                        System.out.println();
                    }


                    System.out.println();
                    System.out.println();
                }


                planTimingRecords.size();

                out.close();
            } catch (IOException | ClassNotFoundException | SQLException e) {
                e.printStackTrace();
            }
        }

    }

    public static MysqlConnector getLocalConnector() {
        return new MysqlConnector("localhost", "samaratrans", "markikokik", "111111");
    }
}
