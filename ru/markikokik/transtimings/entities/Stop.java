package ru.markikokik.transtimings.entities;

import java.io.Serializable;

import ru.markikokik.transtimings.entities.ids.KS_ID;

/**
 * Created by markikokik on 31.03.16.
 */
public class Stop extends Point implements Serializable, Comparable<Stop> {
    public KS_ID ks_id;
    public int geoportalID;
    public String title;
    public String adjacentStreet;
    public String direction;
    public String busesMunicipal;
    public String busesCommercial;
    public String busesPrigorod;
    public String busesSeason;
    public String busesSpecial;
    public String trams;
    public String trolleybuses;

    public Stop() {
    }

    public Stop(KS_ID ks_id, int geoportalID, String title, String adjacentStreet, String direction,
                String busesMunicipal, String busesCommercial, String busesPrigorod, String busesSeason,
                String busesSpecial, String trams, String trolleybuses, double latitude, double longitude, double x,
                double y) {
        this.ks_id = ks_id;
        this.geoportalID = geoportalID;
        this.title = title;
        this.adjacentStreet = adjacentStreet;
        this.direction = direction;
        this.busesMunicipal = busesMunicipal;
        this.busesCommercial = busesCommercial;
        this.busesPrigorod = busesPrigorod;
        this.busesSeason = busesSeason;
        this.busesSpecial = busesSpecial;
        this.trams = trams;
        this.trolleybuses = trolleybuses;
        this.lat = latitude;
        this.lng = longitude;
        this.x = x;
        this.y = y;
    }

    //    @Override
//    public int compareTo(Stop another) {
//        return title.compareToIgnoreCase(another.title);
//    }
    @Override
    public int compareTo(Stop another) {
        int main = adjacentStreet.compareToIgnoreCase(another.adjacentStreet);
        int slave = title.compareToIgnoreCase(another.title);
        return main != 0 ? main : slave;
    }

    @Override
    public int hashCode() {
        return ks_id.id;
    }

    @Override
    public String toString() {
        return title + " " + adjacentStreet + " " + direction + " {" + ks_id.id + "}";
    }
}
