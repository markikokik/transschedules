package ru.markikokik.transtimings.entities;

import ru.markikokik.transtimings.Printer;
import ru.markikokik.transtimings.entities.ids.ID;

/**
 * Информация о простое одного ТС
 */
public class Accident<RouteIdType extends ID, VehicleIdType extends ID>
        extends Interval<RouteIdType, VehicleIdType> {

    public Accident() {
        super();
    }

    public Accident(TransportPosition<VehicleIdType> start, TransportPosition<VehicleIdType> end, RouteIdType routeID) {
        super(start, end, routeID);
    }

    @Override
    public String toString() {
        return (routeID != null ? routeID.toString() + ": " : "") + Printer.getFormattedDate(start.timestamp) + " " +
               Math.round(start.nextStopDist) + " м до " +
               (start.nextStopID != null ? start.nextStopID.toString() : "?") + " - " +
               Printer.getFormattedDate(end.timestamp) + " " + Math.round(end.nextStopDist) + " м до " +
               (end.nextStopID != null ? end.nextStopID.toString() : "?");
    }
}
