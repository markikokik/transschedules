package ru.markikokik.transtimings.entities;

import java.util.HashSet;
import java.util.Set;

import ru.markikokik.transtimings.entities.ids.KR_ID;

/**
 * Соответствие KR_ID с Транснави ID
 */
public class RouteBinding {
    public int transType;
    public KR_ID kr_id;
    public final Set<Integer> TN_IDs = new HashSet<>();

    public RouteBinding() {
    }

    public RouteBinding(KR_ID kr_id, int TN_ID, int transType) {
        this.TN_IDs.add(TN_ID);
        this.kr_id = kr_id;
        this.transType = transType;
    }
}
