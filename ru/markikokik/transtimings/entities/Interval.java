package ru.markikokik.transtimings.entities;

import ru.markikokik.transtimings.entities.ids.ID;

/**
 * Created by markikokik on 13.03.17.
 */
public class Interval<RouteIdType extends ID, VehicleIdType extends ID> {
    public TransportPosition<VehicleIdType> start, end;
    public RouteIdType routeID;

    public Interval() {
    }

    public Interval(TransportPosition<VehicleIdType> start, TransportPosition<VehicleIdType> end, RouteIdType routeID) {
        this.start = start;
        this.end = end;
        this.routeID = routeID;
    }
}
