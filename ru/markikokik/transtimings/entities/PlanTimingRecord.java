package ru.markikokik.transtimings.entities;

import ru.markikokik.transtimings.entities.ids.CP_ID;

public class PlanTimingRecord extends CP_ID {
    public int kt_id, mr_id, raceNumber, time,transType;
    public String cpShortName;
    public char direction;

    public PlanTimingRecord(int kt_id, int mr_id, int raceNumber, char direction, int time, int cp_id,
                            String cpShortName) {
        this.kt_id = kt_id;
        this.mr_id = mr_id;
        this.raceNumber = raceNumber;
        this.time = time;
        this.id = cp_id;
        this.direction = direction;
        this.cpShortName = cpShortName;
    }

    public PlanTimingRecord() {
    }
}
