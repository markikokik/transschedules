package ru.markikokik.transtimings.entities;

import ru.markikokik.transtimings.entities.ids.CP_ID;
import ru.markikokik.transtimings.entities.ids.KR_ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;

/**
 * Соответствие контрольной точки остановке на маршруте
 */
public class ControlPointBinding {
    public KS_ID ks_id;
    public KR_ID kr_id;
    public CP_ID cp_id;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ControlPointBinding that = (ControlPointBinding) o;

        if (!kr_id.equals(that.kr_id)) {
            return false;
        }
        return cp_id.equals(that.cp_id);

    }

    @Override
    public int hashCode() {
        return 31 * cp_id.id + kr_id.id;
    }

    public ControlPointBinding() {
    }


    public ControlPointBinding(int id, int KS_ID, int KR_ID) {
        this.cp_id = new CP_ID(id);
        this.ks_id = new KS_ID(KS_ID);
        this.kr_id = new KR_ID(KR_ID);
    }


    public ControlPointBinding(int id, int KS_ID, KR_ID kr_id) {
        this.cp_id = new CP_ID(id);
        this.ks_id = new KS_ID(KS_ID);
        this.kr_id = kr_id;
    }


    public ControlPointBinding(CP_ID cp_id, KS_ID ks_id, KR_ID kr_id) {
        this.ks_id = ks_id;
        this.kr_id = kr_id;
        this.cp_id = cp_id;
    }
}
