package ru.markikokik.transtimings.entities;

import ru.markikokik.transtimings.entities.ids.KT_ID;

/**
 * Плановый рейс
 */
public class ScheduleTime {
    public KT_ID kt_id;
    public int time, raceNumber;
    public String direction; //A, B...

    public ScheduleTime(KT_ID kt_id, int time) {
        this.kt_id = kt_id;
        this.time = time;
    }

    public ScheduleTime(int kt_id, int time) {
        this.kt_id = new KT_ID(kt_id);
        this.time = time;
    }

    public ScheduleTime(int time) {
        this.time = time;
    }

    public ScheduleTime(KT_ID kt_id, int time, String direction) {
        this.kt_id = kt_id;
        this.time = time;
        this.direction = direction;
    }

    public ScheduleTime(int kt_id, int time, String direction) {
        this.kt_id = new KT_ID(kt_id);
        this.time = time;
        this.direction = direction;
    }

    public ScheduleTime() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ScheduleTime that = (ScheduleTime) o;

        return time == that.time;

    }

    @Override
    public int hashCode() {
        return time;
    }
}
