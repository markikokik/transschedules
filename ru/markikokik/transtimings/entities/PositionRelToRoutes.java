package ru.markikokik.transtimings.entities;

import java.io.Serializable;

import ru.markikokik.transtimings.entities.ids.ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;

/**
 * Привязка позиции к направлениям маршрута
 */
public class PositionRelToRoutes<VehicleIdType extends ID, RouteIdType extends ID>
        extends TransportPosition<VehicleIdType> implements Serializable {
    public transient double[] mileages; //[0] - пробег от начала до точки в первом направлении, [1] - во втором
    public RouteIdType routeID;

    public PositionRelToRoutes(VehicleIdType vehicleID, int timestamp, double x, double y) {
        super(vehicleID, timestamp, x, y);
    }


    public PositionRelToRoutes(VehicleIdType vehicleID, int timestamp, double x, double y, double lat, double lng) {
        super(vehicleID, timestamp, x, y, lat, lng);
    }

    public PositionRelToRoutes(RouteIdType routeID, VehicleIdType vehicleID, KS_ID nextStopID, double nextStopDist,
                               int timestamp, double x,
                               double y, double lat,
                               double lng) {
        super(vehicleID, timestamp, x, y, lat, lng);
        this.routeID = routeID;
        this.nextStopID = nextStopID;
        this.nextStopDist = nextStopDist;
    }


    public PositionRelToRoutes(VehicleIdType vehicleID, KS_ID nextStopID, double nextStopDist, int timestamp, double x,
                               double y,
                               double lat, double lng) {
        super(vehicleID, nextStopID, y, timestamp, nextStopDist, x, lat, lng);
    }

    public PositionRelToRoutes() {
    }
}
