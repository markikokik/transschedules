package ru.markikokik.transtimings.entities;

import ru.markikokik.transtimings.entities.ids.CP_ID;
import ru.markikokik.transtimings.entities.ids.KR_ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;

/**
 * Контрольная точка расписания
 */
public class ControlPoint extends ControlPointBinding implements Comparable<ControlPoint> {
    public String name;
    public double x, y, dist; //дистанция от начала направления маршрута

    @Override
    public String toString() {
        return "ControlPoint{" + name + "}";
    }

    public ControlPoint() {
    }


    public ControlPoint(int id, int KS_ID, int KR_ID, String name, double x, double y, double dist) {
        super(id, KS_ID, KR_ID);
        this.name = name;
        this.x = x;
        this.y = y;
        this.dist = dist;
    }

    public ControlPoint(int id, int KS_ID, KR_ID kr_id, String name, double x, double y, double dist) {
        super(id, KS_ID, kr_id);
        this.name = name;
        this.x = x;
        this.y = y;
        this.dist = dist;
    }

    public ControlPoint(int id, int KS_ID, int KR_ID, String name) {
        super(id, KS_ID, KR_ID);
        this.name = name;
    }

    public ControlPoint(int id, int KS_ID, KR_ID kr_id, String name) {
        super(id, KS_ID, kr_id);
        this.name = name;
    }

    public ControlPoint(CP_ID cp_id, KS_ID ks_id, KR_ID kr_id, String name) {
        super(cp_id, ks_id, kr_id);
        this.name = name;
    }

    public ControlPoint(CP_ID cp_id, KS_ID ks_id, KR_ID kr_id, String name, double x, double y, double dist) {
        super(cp_id, ks_id, kr_id);
        this.name = name;
        this.x = x;
        this.y = y;
        this.dist = dist;
    }

    @Override
    public int compareTo(ControlPoint o) {
        return Double.compare(dist, o.dist);
    }


}
