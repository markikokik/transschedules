package ru.markikokik.transtimings.entities;

import ru.markikokik.transtimings.entities.ids.KT_ID;
import ru.markikokik.transtimings.entities.ids.VehicleTN;

/**
 * Рейс
 */
public class Race extends RaceTimings {
    public KT_ID kt_id;
    public VehicleTN vehicleTN;

    public Race(int startTime, int endTime, KT_ID kt_id) {
        super(startTime, endTime);
        this.kt_id = kt_id;
    }

    public Race(int startTime, int endTime, int kt_id) {
        this(startTime, endTime, new KT_ID(kt_id));
    }

    public Race(int startTime, int endTime, VehicleTN vehicleTN) {
        super(startTime, endTime);
        this.vehicleTN = vehicleTN;
    }

    public Race(int startTime, int endTime, int vehicleTN, int transType) {
        this(startTime, endTime, new VehicleTN(vehicleTN, transType));
    }

    @Override
    public int hashCode() {
        return startTime;
    }
}
