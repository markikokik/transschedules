package ru.markikokik.transtimings.entities;

import java.io.Serializable;

import ru.markikokik.transtimings.entities.ids.KR_ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;
import ru.markikokik.transtimings.entities.ids.VehicleTN;

/**
 * Представление строки из лога
 */
public class MonitoringLogEntry extends PositionRelToRoutes<VehicleTN, KR_ID> implements Serializable {
    public int kt_id, routeTN;
    public String boardNum;

    public MonitoringLogEntry() {
        super();
    }

    public MonitoringLogEntry(KR_ID routeID, VehicleTN vehicleID, KS_ID nextStopID,
                              double nextStopDist, int timestamp, double x, double y, double lat, double lng, int kt_id,
                              int routeTN, String boardNum) {
        super(routeID, vehicleID, nextStopID, nextStopDist, timestamp, x, y, lat, lng);
        this.kt_id = kt_id;
        this.routeTN = routeTN;
        this.boardNum = boardNum;
    }
}
