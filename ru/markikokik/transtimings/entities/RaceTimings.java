package ru.markikokik.transtimings.entities;

/**
 * Временной промежуток для рейса
 */
public class RaceTimings {
    public int startTime, endTime;

    public RaceTimings() {
    }

    public RaceTimings(int startTime, int endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    @Override
    public int hashCode() {
        return startTime;
    }
}
