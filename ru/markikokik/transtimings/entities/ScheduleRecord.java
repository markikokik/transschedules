package ru.markikokik.transtimings.entities;

import ru.markikokik.transtimings.entities.ids.KR_ID;

/**
 * Строка из файла расписаний
 */
public class ScheduleRecord extends ScheduleTime {
//    public final static int DIRECTION_A = 1, DIRECTION_B = 2, DIRECTION_C = 3, DIRECTION_D = 4, DIRECTION_E = 5;

//    public void setDirection(String direction) {
//        switch (direction) {
//            case "A":
//                this.direction = DIRECTION_A;
//                break;
//            case "B":
//                this.direction = DIRECTION_B;
//                break;
//            case "C":
//                this.direction = DIRECTION_C;
//                break;
//            case "D":
//                this.direction = DIRECTION_D;
//                break;
//            case "E":
//                this.direction = DIRECTION_E;
//                break;
//            case "":
//                break;
//            default:
//                System.out.println(direction);
//
//        }
//    }
//
//    public void setDirectionTN(String direction) {
//        switch (direction) {
//            case "A":
//                this.directionTN = DIRECTION_A;
//                break;
//            case "B":
//                this.directionTN = DIRECTION_B;
//                break;
//            case "C":
//                this.directionTN = DIRECTION_C;
//                break;
//            case "D":
//                this.direction = DIRECTION_D;
//                break;
//            case "E":
//                this.direction = DIRECTION_E;
//                break;
//            case "":
//                break;
//            default:
//                System.out.println(direction);
////                throw new IllegalArgumentException();
//        }
//    }

    public KR_ID kr_id;
    public int raceNumber;
    public int controlPointId;
    public String direction;
    public String directionTN;
    public int routeIdTN;
    public int transportTypeIdTN;
    //    public long dateStamp;
    public String controlPointName, modelTitle;
}
