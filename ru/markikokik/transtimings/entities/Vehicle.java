package ru.markikokik.transtimings.entities;

import ru.markikokik.transtimings.entities.ids.KT_ID;
import ru.markikokik.transtimings.entities.ids.VehicleTN;

/**
 * Транспортное средство
 */
public class Vehicle {
    public int transType;
    public KT_ID kt_id;
    public VehicleTN vehicleTN;
    public String boardNum;

    public Vehicle() {
    }

    public Vehicle(int TN_ID, KT_ID kt_id, int transType, String boardNum) {
        this.vehicleTN = new VehicleTN(TN_ID, transType);
        this.kt_id = kt_id;
        this.transType = transType;
        this.boardNum = boardNum;
    }

    public Vehicle(VehicleTN vehicleTN, KT_ID kt_id, int transType, String boardNum) {
        this.vehicleTN = vehicleTN;
        this.kt_id = kt_id;
        this.transType = transType;
        this.boardNum = boardNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Vehicle vehicle = (Vehicle) o;

        if (transType != vehicle.transType) {
            return false;
        }
        if (kt_id != null ? !kt_id.equals(vehicle.kt_id) : vehicle.kt_id != null) {
            return false;
        }
        return vehicleTN != null ? vehicleTN.equals(vehicle.vehicleTN) : vehicle.vehicleTN == null;
    }


    @Override
    public int hashCode() {
        return kt_id != null ? kt_id.id : vehicleTN != null ? vehicleTN.id * 10 + transType : 0;
    }

    @Override
    public String toString() {
        switch (transType) {
            case 1:
                return "Авт " + boardNum + " {" + kt_id.id + "}";
            case 3:
                return "Т " + boardNum + " {" + kt_id.id + "}";
            case 4:
                return "Тр " + boardNum + " {" + kt_id.id + "}";
        }
        return boardNum;
    }
}
