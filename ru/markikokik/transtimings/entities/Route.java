package ru.markikokik.transtimings.entities;

import java.io.Serializable;

import ru.markikokik.transtimings.entities.ids.KR_ID;

/**
 * Created by markikokik on 31.03.16.
 */
public class Route implements Serializable, Comparable<Route> {
    public int transportTypeID;
    public int affiliationID;
    public String direction; //A, B...
    public int geoportalId;
    public String layerName;
    public KR_ID kr_id;
    protected String number;
    private int parsedNumber = 0; // для быстрого сравнения

    public Route() {
    }

    public Route(int transportTypeID, int affiliationID, String direction, int geoportalId, String layerName,
                 KR_ID kr_id,
                 String number) {
        this.transportTypeID = transportTypeID;
        this.affiliationID = affiliationID;
        this.direction = direction;
        this.geoportalId = geoportalId;
        this.layerName = layerName;
        this.kr_id = kr_id;
        setNumber(number);
    }

    public void setNumber(String number) {
        this.number = number;
        try {
            parsedNumber = parse(number);
        } catch (NumberFormatException ignored) {
        }
    }

    public String getNumber() {
        return number;
    }

    @Override
    public int compareTo(Route another) {
        if (another == null) {
            return 1;
        }
        if (this.parsedNumber == another.parsedNumber) {
            return number.compareTo(another.number);
        }
        return parsedNumber - another.parsedNumber;
    }

    @Override
    public int hashCode() {
        return kr_id.id;
    }

    private static Integer parse(String src) {
        int result = 0;
        try {
            for (int i = 0; i < src.length(); i++) {
                result = result * 10 + Integer.parseInt(src.substring(i, i + 1));
            }
        } catch (NumberFormatException ignored) {
        }
        return result;
    }

    @Override
    public String toString() {
        return TransType.getTransTypeName(transportTypeID, affiliationID) + " " + number + ": >> " + direction;
    }
}
