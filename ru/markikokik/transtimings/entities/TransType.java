package ru.markikokik.transtimings.entities;

import java.util.Collection;
import java.util.HashSet;

/**
 * Маскирование видов транспорта и типов обслуживания, из Прибывалки
 */
public class TransType {
    private TransType() {
    }

    public static final int BUS = 0b000001;
    public static final int TRAM = 0b000010;
    public static final int TROLLEYBUS = 0b000100;
    public static final int METRO = 0b001000;
    public static final int RAILWAY = 0b010000;
    public static final int RIVERFLEET = 0b100000;
//    public static final int SHUTTLE = 0b1000000;

    private static final int TYPES_COUNT = 6;
    private static final int AFFILIATIONS_COUNT = 6;

    public static final int MUNICIPAL = 0b000001 << TYPES_COUNT;
    public static final int COMMERCIAL = 0b000010 << TYPES_COUNT;
    public static final int SPECIAL = 0b000100 << TYPES_COUNT;
    public static final int SEASON = 0b001000 << TYPES_COUNT;
    public static final int SUBURBAN = 0b010000 << TYPES_COUNT;
    public static final int INTERCITY = 0b100000 << TYPES_COUNT;

    private static final int[] affToMaskMapping =
            new int[]{0, MUNICIPAL, COMMERCIAL, SUBURBAN, SEASON, SPECIAL, INTERCITY};
    private static final int[] typeToMaskMapping =
            new int[]{0, BUS, METRO, TRAM, TROLLEYBUS, RAILWAY, RIVERFLEET};

    public static final int MUN_BUS_INDEX = BUS + MUNICIPAL;
    public static final int COMM_BUS_INDEX = BUS + COMMERCIAL;
    public static final int SUBURBAN_BUS_INDEX = BUS + SUBURBAN;
    public static final int SEASON_BUS_INDEX = BUS + SEASON;
    public static final int SPECIAL_BUS_INDEX = BUS + SPECIAL;
    public static final int INTER_BUS_INDEX = BUS + INTERCITY;
    public static final int TROLL_INDEX = TROLLEYBUS + MUNICIPAL;
    public static final int TRAM_INDEX = TRAM + MUNICIPAL;
    public static final int METRO_INDEX = METRO + MUNICIPAL;
    public static final int FLEET_INDEX = RIVERFLEET + MUNICIPAL;
    public static final int RAIL_INDEX = RAILWAY + SPECIAL;

    public static boolean isScheduled(int typeId) {
        return (typeId & TransType.RAILWAY) == TransType.RAILWAY ||
               (typeId & TransType.METRO) == TransType.METRO ||
               (typeId & TransType.INTER_BUS_INDEX) == TransType.INTER_BUS_INDEX
               || (typeId & TransType.RIVERFLEET) == TransType.RIVERFLEET;
    }

    public static int getTransTypeMask(int transTypeId) {
        return transTypeId <= TYPES_COUNT ? typeToMaskMapping[transTypeId] : 0;
    }

    public static int getAffiliationMask(int affiliationId) {
        return affiliationId <= AFFILIATIONS_COUNT ? affToMaskMapping[affiliationId] : 0;
    }

    /**
     * @param transTypeMask transport type ID
     * @param affTypeMask   affiliation ID
     * @return
     */
    public static int getTransIndex(int transTypeMask, int affTypeMask) {
        return transTypeMask + affTypeMask;
    }

    public static Collection<Integer> getAllBusIndexes() {
        Collection<Integer> result = getNonCommBusIndexes();
        result.add(TransType.getTransIndex(BUS, COMMERCIAL));
        return result;
    }

    public static Collection<Integer> getNonCommBusIndexes() {
        Collection<Integer> result = new HashSet<>(4);
        result.add(TransType.getTransIndex(BUS, MUNICIPAL));
        result.add(TransType.getTransIndex(BUS, SPECIAL));
        result.add(TransType.getTransIndex(BUS, SUBURBAN));
        result.add(TransType.getTransIndex(BUS, SEASON));
        return result;
    }


    public static String getTransTypeName(Route route) {
        return getTransTypeName(route.transportTypeID, route.affiliationID);
    }

    public static String getTransTypeName(int transType, int affType) {
        switch (transType) {
            case BUS:
                if (affType == COMMERCIAL) {
                    return "Комм. автобус";
                }
                return "автобус";
            case TRAM:
                return "трамвай";
            case TROLLEYBUS:
                return "троллейбус";
            case METRO:
                return "метро";
        }
        return "";
    }

    public static String getTransTypeName(int transTypeMask) {
        if ((transTypeMask & BUS) > 0) {
            if ((transTypeMask & COMMERCIAL) > 0) {
                return "Комм. автобус";
            }
            return "автобус";
        }
        if ((transTypeMask & TRAM) > 0) {
            return "трамвай";
        }
        if ((transTypeMask & TROLLEYBUS) > 0) {
            return "троллейбус";
        }
        if ((transTypeMask & METRO) > 0) {
            return "метро";
        }

        return "";
    }

    public static int getTransIndex(Route route) {
        return getTransIndex(route.transportTypeID, route.affiliationID);
    }
}