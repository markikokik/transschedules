package ru.markikokik.transtimings.entities;

/**
 * Надо было использовать Map.Entry
 */
public class Matching<F, S> {
    public F first;
    public S second;

    public Matching(F first, S second) {
        this.first = first;
        this.second = second;
    }

    public Matching() {
    }
}
