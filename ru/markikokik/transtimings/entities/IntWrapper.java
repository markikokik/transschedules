package ru.markikokik.transtimings.entities;

/**
 * Вместо Integer для многократного переиспользования
 */
public class IntWrapper {
    public int value;

    public IntWrapper() {
    }

    public IntWrapper(int value) {

        this.value = value;
    }
}
