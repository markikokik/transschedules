package ru.markikokik.transtimings.entities;

import ru.markikokik.transtimings.entities.ids.RouteIDs;

/**
 * геоинформация про маршрут
 */
public class RouteInfo extends RouteIDs {
    public double[][] geometries;   //[0] - для первого направления, [1] - для второго
    //формат записи: {x, y, x, y, ...}

    public StopDistance[][] stopMileages; //аналогично дистанции для каждой остановки по направлениям

    public RouteInfo(RouteIDs ids) {
        this.kr_ids = ids.kr_ids;
        this.tn_ids = ids.tn_ids;
        this.transType = ids.transType;
    }

    public void updateIds(RouteIDs ids) {
        this.kr_ids = ids.kr_ids;
        this.tn_ids = ids.tn_ids;
        this.transType = ids.transType;
    }
}
