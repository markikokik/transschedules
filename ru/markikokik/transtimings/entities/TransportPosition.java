package ru.markikokik.transtimings.entities;

import java.io.Serializable;

import ru.markikokik.transtimings.entities.ids.ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;

/**
 * Положение ТС на маршруте
 */
public class TransportPosition<VehicleIdType extends ID> extends Point
        implements Comparable<TransportPosition>, Serializable {
    public VehicleIdType vehicleID;
    public int timestamp;
    public double dist;
    public KS_ID nextStopID;
    public double nextStopDist;


    public TransportPosition(VehicleIdType vehicleID, int timestamp, double x, double y) {
        this.vehicleID = vehicleID;
        this.timestamp = timestamp;
        this.x = x;
        this.y = y;
    }

    public TransportPosition(VehicleIdType vehicleID, int timestamp, double x, double y, double lat, double lng) {
        this(vehicleID, timestamp, x, y);
        this.lat = lat;
        this.lng = lng;
    }


    public TransportPosition() {

    }

    public TransportPosition(VehicleIdType vehicleID, KS_ID nextStopID, double nextStopDist, int timestamp, double x,
                             double y,
                             double lat, double lng) {
        this(vehicleID, nextStopID, nextStopDist, timestamp, x, y);
        this.lat = lat;
        this.lng = lng;
    }

    public TransportPosition(VehicleIdType vehicleID, KS_ID nextStopID, double nextStopDist, int timestamp, double x,
                             double y) {
        this(vehicleID, timestamp, x, y);
        this.nextStopID = nextStopID;
        this.nextStopDist = nextStopDist;
    }

    @Override
    public int compareTo(TransportPosition o) {
        return Integer.compare(timestamp, o.timestamp);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TransportPosition position = (TransportPosition) o;

        if (timestamp != position.timestamp) {
            return false;
        }
        return vehicleID != null ? vehicleID.equals(position.vehicleID) : position.vehicleID == null;

    }

    @Override
    public int hashCode() {
        int result = vehicleID != null ? vehicleID.hashCode() : 0;
        result = 31 * result + timestamp;
        return result;
    }
}
