package ru.markikokik.transtimings.entities;

/**
 * Created by markikokik on 27.04.16.
 */
public class Point {
    public double x;
    public double y;
    public double lat;
    public double lng;
}
