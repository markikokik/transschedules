package ru.markikokik.transtimings.entities.ids;

/**
 * ID рейса
 */
public class RaceID extends ID {
    public RaceID(int id) {
        super(id);
    }

    @Override
    public String toString() {
        return "RaceID{" + id + "}";
    }

    public RaceID() {
    }
}
