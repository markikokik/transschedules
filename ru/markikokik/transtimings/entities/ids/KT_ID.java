package ru.markikokik.transtimings.entities.ids;

/**
 * Не понял, что это за ID. Возможно, наряда, ибо соответствие с
 * TN_ID транспорта для каждого дня разное
 */
public class KT_ID extends ID {
    public KT_ID(int id) {
        super(id);
    }

    @Override
    public String toString() {
        return "KT_ID{" + id + "}";
    }

    public KT_ID() {
    }
}
