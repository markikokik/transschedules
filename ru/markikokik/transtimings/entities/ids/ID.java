package ru.markikokik.transtimings.entities.ids;

/**
 * Базовый класс для всех айдишников
 */
public abstract class ID implements Comparable<ID> {
    public int id = 0;

    public ID(int id) {
        this.id = id;
    }

    public ID() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null ) {
            return false;
        }

        ID _id = (ID) o;

        return id == _id.id;

    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public int compareTo(ID o) {
        return Integer.compare(id, o.id);
    }

    @Override
    public String toString() {
        return "ID{" + id + "}";
    }
}
