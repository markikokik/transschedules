package ru.markikokik.transtimings.entities.ids;

/**
 * Базовый Транснави ID
 */
public abstract class TN_ID extends ID {
    public int transType;

    public TN_ID(int id) {
        super(id);
    }

    public TN_ID() {
    }

    public TN_ID(int id, int transType) {
        super(id);
        this.transType = transType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        TN_ID tn_id = (TN_ID) o;

        return transType == tn_id.transType;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + transType;
        return result;
    }
}
