package ru.markikokik.transtimings.entities.ids;

import java.io.Serializable;

/**
 * Транснави ID транспортного средства
 */
public class VehicleTN extends TN_ID implements Serializable {

    public VehicleTN(int id, int transType) {
        super(id, transType);
    }

    @Override
    public String toString() {
        return "VehicleTN{" + id + "}";
    }

    public VehicleTN() {
    }
}
