package ru.markikokik.transtimings.entities.ids;

/**
 * Для учёта изменений трасс маршрутов.
 */
public class PeriodID extends ID {
    public PeriodID(int id) {
        super(id);
    }

    public PeriodID() {
    }
}
