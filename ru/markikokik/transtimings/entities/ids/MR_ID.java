package ru.markikokik.transtimings.entities.ids;

import java.io.Serializable;

/**
 * Транснави ID маршрута
 */
public class MR_ID extends TN_ID implements Serializable {
    public MR_ID(int id) {
        super(id);
    }

    public MR_ID(int id, int transType) {
        super(id, transType);
    }

    @Override
    public String toString() {
        return "RouteTN{" + id + "," + transType + "}";
    }

    public MR_ID() {
    }
}
