package ru.markikokik.transtimings.entities.ids;

/**
 * ID контрольной точки на маршруте
 */
public class CP_ID extends ID {
    public CP_ID(int id) {
        super(id);
    }

    @Override
    public String toString() {
        return "CP_ID{" + id + "}";
    }

    public CP_ID() {
    }
}
