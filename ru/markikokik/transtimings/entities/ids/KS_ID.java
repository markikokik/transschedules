package ru.markikokik.transtimings.entities.ids;

import java.io.Serializable;

/**
 * ID остановки в смысле ТОС
 */
public class KS_ID extends ID implements Serializable {
    public KS_ID(int id) {
        super(id);
    }

    @Override
    public String toString() {
        return "KS_ID{" + id + "}";
    }

    public KS_ID() {
    }
}
