package ru.markikokik.transtimings.entities.ids;

import java.io.Serializable;

/**
 * ID маршрута в смысле ТОС
 *
 */
public class KR_ID extends ID implements Serializable {
    public KR_ID(int id) {
        super(id);
    }

    @Override
    public String toString() {
        return "KR_ID{" + id + "}";
    }

    public KR_ID() {
    }
}
