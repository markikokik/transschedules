package ru.markikokik.transtimings.entities.ids;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Соответствие всех TN_ID маршрута всем его KR_ID
 */
public class RouteIDs {
    public List<KR_ID> kr_ids = new ArrayList<>();
    public Set<MR_ID> tn_ids = new HashSet<>();
    public int transType;

}
