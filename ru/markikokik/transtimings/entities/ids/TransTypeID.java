package ru.markikokik.transtimings.entities.ids;

/**
 * ID типа транспорта
 */
public class TransTypeID extends ID {
    public static final TransTypeID
            BUS = new TransTypeID(1),
            TRAM = new TransTypeID(3),
            TROLLEYBUS = new TransTypeID(4);

    public TransTypeID(int id) {
        super(id);
    }

    @Override
    public String toString() {
        switch (id) {
            case 1:
                return "Автобус";
            case 3:
                return "Трамвай";
            case 4:
                return "Троллейбус";
            default:
                return String.valueOf(id);
        }
    }

    public TransTypeID() {
    }
}
