package ru.markikokik.transtimings.entities;

import java.util.HashMap;
import java.util.Map;

import ru.markikokik.transtimings.entities.ids.ID;

public class AccidentsCluster<RouteId extends ID, VehicleIDkey extends ID, VehicleIDentry extends ID> {
    private static int CURRENT_ID = 0;
    public Map<VehicleIDkey, Accident<RouteId, VehicleIDentry>> accidents = new HashMap<>();
    public int id, movingCount = 0, movingTimestamp = 0;
    public boolean changed, informedStop = false, informedGo = false;

    public AccidentsCluster() {
        id = CURRENT_ID++;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof AccidentsCluster) {
            return id == ((AccidentsCluster) obj).id;
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return id;
    }
}
