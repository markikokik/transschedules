package ru.markikokik.transtimings.entities;

import ru.markikokik.transtimings.entities.ids.KS_ID;

/**
 * Расстояние от начала маршрута для остановки
 */
public class StopDistance extends Point {
    public KS_ID ks_id;
    public double dist;

    public StopDistance(KS_ID ks_id, double dist) {
        this.ks_id = ks_id;
        this.dist = dist;
    }

    public StopDistance(int KS_ID, double dist) {
        this.ks_id = new KS_ID(KS_ID);
        this.dist = dist;
    }

    public StopDistance(KS_ID ks_id, double dist, double x, double y) {
        this.ks_id = ks_id;
        this.dist = dist;
        this.x = x;
        this.y = y;
    }

    public StopDistance(int KS_ID, double dist, double x, double y) {
        this.ks_id = new KS_ID(KS_ID);
        this.dist = dist;
        this.x = x;
        this.y = y;
    }

    public StopDistance() {
    }
}
