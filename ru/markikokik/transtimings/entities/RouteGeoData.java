package ru.markikokik.transtimings.entities;

import java.util.ArrayList;
import java.util.List;

import ru.markikokik.transtimings.entities.ids.KR_ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;

public class RouteGeoData {
    public KR_ID kr_id;
    public double[] geometry;
    private List<KS_ID> circuit = new ArrayList<>();

    public void addStop(KS_ID ks_id) {
        circuit.add(ks_id);
    }

    public List<KS_ID> getCircuit() {
        return circuit;
    }
}
