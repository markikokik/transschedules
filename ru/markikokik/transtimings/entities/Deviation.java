package ru.markikokik.transtimings.entities;

import java.util.Comparator;

import ru.markikokik.transtimings.entities.ids.KT_ID;

/**
 * Отклонение от графика
 */
public class Deviation implements Comparable<Deviation> {
    public int planTime, deviation; //плановое время и отклонение в секундах
    public String direction; //A, B...
    public KT_ID planVehicle, realVehicle; //какое ТС должно было выполнить рейс и какое фактически вышло

    public static Comparator<Deviation> planTimeComparator = new Comparator<Deviation>() {
        @Override
        public int compare(Deviation o1, Deviation o2) {
            return Integer.compare(o1.planTime, o2.planTime);
        }
    };

    public Deviation() {
    }

    public Deviation(int deviation) {
        this.deviation = deviation;
    }

    public Deviation(int deviation, String direction) {
        this.deviation = deviation;
        this.direction = direction;
    }

    public Deviation(int planTime, int deviation) {
        this.planTime = planTime;
        this.deviation = deviation;
    }

    public Deviation(int planTime, int deviation, KT_ID planVehicle) {
        this.planTime = planTime;
        this.deviation = deviation;
        this.planVehicle = planVehicle;
    }

    public Deviation(int planTime, int deviation, KT_ID planVehicle, String direction) {
        this.planTime = planTime;
        this.deviation = deviation;
        this.planVehicle = planVehicle;
        this.direction = direction;
    }

    public Deviation(int planTime, int deviation, String direction, KT_ID planVehicle,
                     KT_ID realVehicle) {
        this.planTime = planTime;
        this.deviation = deviation;
        this.direction = direction;
        this.planVehicle = planVehicle;
        this.realVehicle = realVehicle;
    }

    public Deviation(int planTime, int deviation, String direction, int planVehicle,
                     int realVehicle) {
        this.planTime = planTime;
        this.deviation = deviation;
        this.direction = direction;
        this.planVehicle = new KT_ID(planVehicle);
        this.realVehicle = new KT_ID(realVehicle);
    }

    @Override
    public int compareTo(Deviation o) {
        return deviation - o.deviation;
    }


}
