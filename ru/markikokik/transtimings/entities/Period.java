package ru.markikokik.transtimings.entities;

import ru.markikokik.transtimings.entities.ids.PeriodID;

/**
 * Период времени для временного изменения трассы маршрута
 */
public class Period {
    public int startTime, endTime;
    public PeriodID id;

    public Period(int id, int startTime, int endTime) {
        this.id = new PeriodID(id);
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Period(PeriodID id, int startTime, int endTime) {
        this.id = id;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Period period = (Period) o;

        if (id != period.id) {
            return false;
        }
        if (startTime != period.startTime) {
            return false;
        }
        return endTime == period.endTime;

    }

    @Override
    public int hashCode() {
        return id.id;
    }
}
