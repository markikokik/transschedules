package ru.markikokik.transtimings;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import ru.markikokik.transtimings.db.DataBase;
import ru.markikokik.transtimings.db.connectors.Connector;
import ru.markikokik.transtimings.entities.MonitoringLogEntry;
import ru.markikokik.transtimings.entities.Period;
import ru.markikokik.transtimings.entities.PlanTimingRecord;
import ru.markikokik.transtimings.entities.RouteInfo;
import ru.markikokik.transtimings.entities.ScheduleRecord;
import ru.markikokik.transtimings.entities.Vehicle;
import ru.markikokik.transtimings.entities.ids.CP_ID;
import ru.markikokik.transtimings.entities.ids.KR_ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;
import ru.markikokik.transtimings.entities.ids.KT_ID;
import ru.markikokik.transtimings.entities.ids.MR_ID;
import ru.markikokik.transtimings.entities.ids.VehicleTN;

/**
 *
 */
public class Parser {
    private static final SimpleDateFormat parserTimestamp = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    private static final SimpleDateFormat parserDate = new SimpleDateFormat("yyyy.MM.dd");
    private static final SimpleDateFormat parserSDFschedule =
            new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.000");

    /**
     * распарсить файл лога
     *
     * @param connector
     * @param logFile
     */
    public static void parseMonitoring(Connector connector, String logFile) {
        try (DataBase db = new DataBase(connector)) {
            db.createMonitoringTables();
            db.createAccidentsTable();
//            List<MonitoringLogEntry> positions = parseLog(logFile);
            Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positionsToday;
            Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positionsTomorrow =
                    HashBasedTable.create();
            Table<MR_ID, KR_ID, Integer> routeIds = HashBasedTable.create();
            Table<MR_ID, KR_ID, Integer> routeIdsTomorrow = HashBasedTable.create();
            Table<VehicleTN, KT_ID, Vehicle> vehicles = HashBasedTable.create();
            Map<KR_ID, List<Period>> periods = new HashMap<>();
//            Table<KR_ID, Period, RouteInfo> routeInfos = HashBasedTable.create();


            File file = new File(logFile);
            String[] names;
            String path;
            if (file.isFile()) {
                names = new String[]{file.getName()};
                path = file.getParent() + File.separator;
            } else {
                names = file.list();
                Arrays.sort(names, new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        return o1.compareTo(o2);
                    }
                });
                path = file.getPath() + File.separator;
            }
//            Arrays.sort(names, new Comparator<String>() {
//                @Override
//                public int compare(String o1, String o2) {
//                    return o2.compareTo(o1);
//                }
//            });


            Map<Integer, VehicleTN>[] vehicleTNs = new Map[5];
            Map<Integer, MR_ID>[] routeTNs = new Map[5];
            for (int i = 0; i < 5; i++) {
                vehicleTNs[i] = new HashMap<>();
                routeTNs[i] = new HashMap<>();
            }
            Map<Integer, KT_ID> kt_ids = new HashMap<>();
            Map<Integer, KS_ID> ks_ids = new HashMap<>();
            Map<Integer, KR_ID> kr_ids = new HashMap<>();
//
//            long time = -System.nanoTime();
//            positionsToday = readBinaryLog(
//                    "/media/Data/Documents/TransportStateLogs/logs_tmp/log_2015.02.01.bin.gz");
//            time += System.nanoTime();
//            System.out.println("read " + positionsToday.size() + " records, " + time / 1e6 + " ms");
//
//            Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positionsBin = positionsToday;
            positionsToday = HashBasedTable.create();

            for (String name : names) {
                System.out.println("Parsing " + name);
                vehicles.clear();
                positionsTomorrow = HashBasedTable.create();
                if (name.endsWith("zip")) {
                    int dayDelim = getDayDelimFromArchiveName(name);
                    Parser.parseLogToTable(path + name, positionsToday, positionsTomorrow,
                                           routeIds, routeIdsTomorrow, vehicles, vehicleTNs, routeTNs,
                                           kt_ids, kr_ids,
                                           ks_ids, dayDelim);
                } else if (name.endsWith("bin.gz")) {
                    positionsToday = Parser.readBinaryLog(path + name);
                }

                int count = 0;
                for (List<MonitoringLogEntry> list : positionsToday.values()) {
                    count += list.size();
                }

                System.out.println("size=" + count);

                processLogData(db, positionsToday);

                positionsToday = positionsTomorrow;
                routeIds = routeIdsTomorrow;
                routeIdsTomorrow = HashBasedTable.create();
                System.gc();
            }
//            processLogData(db, positionsTomorrow);

//            db.insertRouteBindingsTable(routeIds);
//            db.insertVehiclesTable(vehicles);
//            db.dropMonitoring();

        } catch (IOException | SQLException | ClassNotFoundException /*| ParseException*/ e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int getDayDelimFromArchiveName(String name) {
        String[] dateComponents = name.substring(15, 25).split("-");
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT+4:00"));
        calendar.set(
                Integer.parseInt(dateComponents[0]),
                Integer.parseInt(dateComponents[1]) - 1,
                Integer.parseInt(dateComponents[2]),
                3, 0, 0
        );
        return (int) (calendar.getTimeInMillis() / 1000);
    }

    /**
     * текстовые логи в архивах перегнать в бинарники
     *
     * @param logDir
     * @param outDir
     */
    public static void convertLogsToBin(String logDir, String outDir) {
        Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positionsYesterday =
                HashBasedTable.create();
        Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positionsToday =
                HashBasedTable.create();
        Table<MR_ID, KR_ID, Integer> routeIds = HashBasedTable.create();
        Table<MR_ID, KR_ID, Integer> routeIdsTomorrow = HashBasedTable.create();
        Table<VehicleTN, KT_ID, Vehicle> vehicles = HashBasedTable.create();

        File file = new File(logDir);
        String[] names;
        String path;
        if (file.isFile()) {
            names = new String[]{file.getName()};
            path = file.getParent() + File.separator;
        } else {
            names = file.list();
            Arrays.sort(names, new Comparator<String>() {
                @Override
                public int compare(String o1, String o2) {
                    return o1.compareTo(o2);
                }
            });
            path = file.getPath() + File.separator;
        }

        Map<Integer, VehicleTN>[] vehicleTNs = new Map[5];
        Map<Integer, MR_ID>[] routeTNs = new Map[5];
        for (int i = 0; i < 5; i++) {
            vehicleTNs[i] = new HashMap<>();
            routeTNs[i] = new HashMap<>();
        }
        Map<Integer, KT_ID> kt_ids = new HashMap<>();
        Map<Integer, KS_ID> ks_ids = new HashMap<>();
        Map<Integer, KR_ID> kr_ids = new HashMap<>();

        int prevDelim = 0;
        if (!outDir.endsWith(File.separator)) {
            outDir += File.separator;
        }
        try {
            for (String name : names) {
                if (!name.endsWith(".zip") || !name.startsWith("transports.log.")) {
                    continue;
                }
                System.out.println("Parsing " + name);

                int dayDelim = getDayDelimFromArchiveName(name);
                if (prevDelim > 0 && dayDelim - prevDelim > Utils.DAY_SECS) {
                    writeBinaryLog(outDir + "log_" +
                                   Printer.getFormattedDate(Printer.dateDateFormat,
                                                            prevDelim) + ".bin.gz",
                                   positionsYesterday);
                    positionsYesterday.clear();
                }

                vehicles.clear();

                Parser.parseLogToTable(path + name, positionsYesterday, positionsToday,
                                       routeIds, routeIdsTomorrow, vehicles, vehicleTNs, routeTNs,
                                       kt_ids, kr_ids,
                                       ks_ids, dayDelim);

                writeBinaryLog(outDir + "log_" +
                               Printer.getFormattedDate(Printer.dateDateFormat,
                                                        dayDelim -
                                                        Utils.DAY_SECS) + ".bin.gz",
                               positionsYesterday);

                prevDelim = dayDelim;
                positionsYesterday = positionsToday;
                positionsToday = HashBasedTable.create();
                routeIds = routeIdsTomorrow;
                routeIdsTomorrow = HashBasedTable.create();
                System.gc();
            }

            writeBinaryLog(outDir + "log_" +
                           Printer.getFormattedDate(Printer.dateDateFormat,
                                                    prevDelim) + ".bin.gz",
                           positionsYesterday);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    /**
     * обработка логов, где я творю всякие эксперименты, постоянно меняется
     *
     * @param db
     * @param positionsToday
     * @throws SQLException
     */
    private static void processLogData(DataBase db,
                                       Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positionsToday)
            throws SQLException {
        Table<KR_ID, Period, RouteInfo> routeInfos = HashBasedTable.create();


        long time = -System.nanoTime();

        Map<KR_ID, List<Period>> periods = new HashMap<>();
        Table<MR_ID, KR_ID, Object> routeIds;
        routeIds = Calculator.getRoutesBindings(positionsToday);

        time = -System.nanoTime();
        Corrector.correctDirections(db, positionsToday, routeIds, periods, routeInfos);
        time += System.nanoTime();
        System.out.println("Correction " + time / 1e6);

        List<KR_ID[]> incedentalRoutes = new ArrayList<>();
        incedentalRoutes.add(new KR_ID[]{new KR_ID(114), new KR_ID(94)});
        incedentalRoutes.add(new KR_ID[]{new KR_ID(115), new KR_ID(93)});
        Map<KR_ID, CP_ID[]> cps = new HashMap<>();
        cps.put(new KR_ID(114), new CP_ID[]{new CP_ID(10498), new CP_ID(10206)});//aquarium, arts
        cps.put(new KR_ID(94), new CP_ID[]{new CP_ID(10499), new CP_ID(10206)});//aquarium, arts
        cps.put(new KR_ID(115), new CP_ID[]{new CP_ID(20000), new CP_ID(10499)});//square mich,
        cps.put(new KR_ID(93), new CP_ID[]{new CP_ID(20000), new CP_ID(10499)});//square mich,
        // aquarium

        Calculator.TimeBetween[][] times = Calculator.calcTimesBetweenCPs(db, positionsToday,
                                                                          incedentalRoutes,
                                                                          cps
        );
        Printer.printTimeBetweenCPs(
                "/media/Data/Documents/TransportStateLogs/", incedentalRoutes, times
        );
    }

    /**
     * запись лога в бинарник
     *
     * @param filename
     * @param positions
     * @throws IOException
     */
    public static void writeBinaryLog(String filename,
                                      Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positions)
            throws IOException {
        DataOutputStream out = new DataOutputStream(new BufferedOutputStream(
                new GZIPOutputStream(new FileOutputStream(filename))));
        Iterator<MonitoringLogEntry> iterator;
        MonitoringLogEntry monitoringLogEntry;
        List<MonitoringLogEntry> monitoring;
        out.writeInt(positions.rowMap().size());
        for (Map.Entry<MR_ID, Map<VehicleTN, List<MonitoringLogEntry>>> routeEntry : positions.rowMap()
                                                                                              .entrySet()) {
            MR_ID mr_id = routeEntry.getKey();
            out.writeByte(mr_id.transType);
            out.writeInt(mr_id.id);

            Map<VehicleTN, List<MonitoringLogEntry>> routeMonitoring = routeEntry.getValue();
            out.writeInt(routeMonitoring.size());
            for (Map.Entry<VehicleTN, List<MonitoringLogEntry>> vehicleEntry : routeMonitoring
                    .entrySet()) {
                out.writeInt(vehicleEntry.getKey().id);
                monitoring = vehicleEntry.getValue();

                iterator = monitoring.iterator();
                monitoringLogEntry = iterator.next();
                out.writeUTF(monitoringLogEntry.boardNum == null ? "" : monitoringLogEntry.boardNum);
                out.writeInt(monitoringLogEntry.kt_id);
                iterator = monitoring.iterator();
                out.writeInt(monitoring.size());
                while (iterator.hasNext()) {
                    monitoringLogEntry = iterator.next();
                    out.writeShort(
                            monitoringLogEntry.routeID != null ? monitoringLogEntry.routeID.id : 0);
                    out.writeShort(
                            monitoringLogEntry.nextStopID != null ? monitoringLogEntry.nextStopID.id :
                            0);
                    out.writeFloat((float) monitoringLogEntry.nextStopDist);
                    out.writeInt(monitoringLogEntry.timestamp);
                    out.writeFloat((float) monitoringLogEntry.x);
                    out.writeFloat((float) monitoringLogEntry.y);
                    out.writeFloat((float) (monitoringLogEntry.lat - 53d));
                    out.writeFloat((float) (monitoringLogEntry.lng - 50d));
                }
            }
        }

        out.close();
    }

    /**
     * чтение бинарника
     *
     * @param filename
     * @return
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Table<MR_ID, VehicleTN, List<MonitoringLogEntry>>
    readBinaryLog(String filename)
            throws IOException, ClassNotFoundException {
        long time = -System.nanoTime();

        DataInputStream in = new DataInputStream(
                new BufferedInputStream(
                        new GZIPInputStream(
                                new FileInputStream(filename)
                        )
                )
        );

        List<MonitoringLogEntry> monitoring;
        int routesCount, vehiclesCount, size, transType, kt_id_int;
        Integer tmpID;
        KT_ID kt_id;
        short tmpKR_ID, tmpKS_ID;
        String boardNum;
        MR_ID mr_id;
        VehicleTN vehicleTN;
        KR_ID kr_id;
        KS_ID ks_id;
        Map<Short, KR_ID> krIdsPool = new HashMap<>();
        Map<Short, KS_ID> ksIdsPool = new HashMap<>();
        Map<Integer, KT_ID> ktIdsPool = new HashMap<>();
        MonitoringLogEntry sample;
        routesCount = in.readInt();
        Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positionsToday =
                HashBasedTable.create(routesCount, 0);
        int count = 0;
        for (int r = 0; r < routesCount; r++) {
            transType = in.readUnsignedByte();
            tmpID = in.readInt();
//            routeTN = routeTNs[transType].get(tmpID);
//            if (routeTN == null) {
//                routeTNs[transType].put(tmpID, routeTN = new RouteTN(tmpID, transType));
//            }
            mr_id = new MR_ID(tmpID, transType);

//            Map<KR_ID, Integer> kr_ids = routeIds.row(routeTN);

            vehiclesCount = in.readInt();

            for (int v = 0; v < vehiclesCount; v++) {
                tmpID = in.readInt();
//                vehicleTN = vehicleTNs[transType].get(tmpID);
//                if (vehicleTN == null) {
//                    vehicleTNs[transType].put(tmpID, vehicleTN = new VehicleTN(tmpID, transType));
//                }
                vehicleTN = new VehicleTN(tmpID, transType);

                boardNum = in.readUTF();
                kt_id_int = in.readInt();

//                kt_id = ktIdsPool.get(kt_id_int);
//                if (kt_id == null) {
//                    ktIdsPool.put(kt_id_int, kt_id = new KT_ID(kt_id_int));
//                }

//                if (!vehicles.contains(vehicleTN, kt_id)) {
//                    vehicles.put(vehicleTN, kt_id,
//                                 new Vehicle(vehicleTN, kt_id, transType, boardNum));
//                }

                positionsToday.put(mr_id, vehicleTN, monitoring = new ArrayList<>(size = in.readInt()));
                count += size;
//                monitoring.add(null);
                for (int i = 0; i < size; i++) {
                    tmpKR_ID = in.readShort();
                    kr_id = krIdsPool.get(tmpKR_ID);
                    if (kr_id == null) {
                        krIdsPool.put(tmpKR_ID, kr_id = tmpKR_ID > 0 ? new KR_ID(tmpKR_ID) : null);
                    }
                    tmpKS_ID = in.readShort();
                    ks_id = ksIdsPool.get(tmpKS_ID);
                    if (ks_id == null) {
                        ksIdsPool.put(tmpKS_ID, ks_id = tmpKS_ID > 0 ? new KS_ID(tmpKS_ID) : null);
                    }
                    sample = new MonitoringLogEntry(
                            kr_id,
                            vehicleTN,
                            ks_id,
                            in.readFloat(),
                            in.readInt(),
                            in.readFloat(),
                            in.readFloat(),
                            in.readFloat() + 53f,
                            in.readFloat() + 50f,
                            kt_id_int,
                            mr_id.id,
                            boardNum
                    );
//                    if (i == size - 1) {
//                        monitoring.set(0, sample);
//                    } else {
                    monitoring.add(sample);
//                    }

//                    if (sample.routeID.id > 0 && !kr_ids.containsKey(sample.routeID)) {
//                        kr_ids.put(sample.routeID, Utils.getDayBeginning(sample.timestamp));
//                    }
                }

//                if (size > 0 && kr_ids.size() == 0) {
//                    kr_ids.put(nullKR_ID, Utils.getDayBeginning(sample.timestamp));
//                }
            }

        }

        in.close();
        time += System.nanoTime();
        System.out.println("time " + Math.round(time / 1e6) + ", count " + count);
        return positionsToday;
    }


    /**
     * парсинг файла графиков и запись в БД
     *
     * @param connector
     * @param timingsFile
     */
    public static void parseTimings(Connector connector, String timingsFile) {
        try (DataBase db = new DataBase(connector)) {
            List<ScheduleRecord> timings = parseSchedule(timingsFile);
            db.dropTimings();
            db.createTimingsTables();
            db.insertScheduleData(timings);
        } catch (IOException | ParseException | ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * распарсить файл графиков
     *
     * @param inputFile
     * @return
     * @throws IOException
     * @throws ParseException
     */
    public static List<ScheduleRecord> parseSchedule(String inputFile)
            throws IOException, ParseException {
        File file = new File(inputFile);
//        FileInputStream fileInputStream=new FileInputStream(file);
//        Reader reader=new InputStreamReader(fileInputStream, )
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line;
//        int i = 0;
        List<ScheduleRecord> races = new ArrayList<>(80000);
        ScheduleRecord scheduleRecord;
        parserDate.setTimeZone(TimeZone.getTimeZone("GMT+4:00"));
        parserSDFschedule.setTimeZone(TimeZone.getTimeZone("GMT+4:00"));

        reader.readLine();
        while ((line = reader.readLine()) != null) {
            scheduleRecord = lineToSchedule(line, null);
            races.add(scheduleRecord);
//            if (++i % 10000 == 0) {
//                System.out.println(i);
//            }
        }
        fileReader.close();
        System.out.println(races.size() + " records processed");
        return races;
    }

    public static List<PlanTimingRecord> parsePlanRaces(String inputFile, int transType) throws
                                                                                         IOException {
        File file = new File(inputFile);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line;
//        int i = 0;
        List<PlanTimingRecord> races = new ArrayList<>(25000);
        PlanTimingRecord record;

        reader.readLine();
        while ((line = reader.readLine()) != null) {
            record = lineToPlanTiming(line, null);
            record.transType = transType;
            races.add(record);
//            if (++i % 10000 == 0) {
//                System.out.println(i);
//            }
        }
        fileReader.close();
        System.out.println(races.size() + " records processed");
        return races;
    }

    @Deprecated
    public static List<MonitoringLogEntry> parseLog(String inputFile)
            throws IOException, ParseException {
        File file = new File(inputFile);
        FileReader fileReader = new FileReader(file);
        BufferedReader reader = new BufferedReader(fileReader);
        String line;
        int i = 0;
        List<MonitoringLogEntry> positions = new ArrayList<>(2300000);
        MonitoringLogEntry position;
        parserTimestamp.setTimeZone(TimeZone.getTimeZone("GMT+4:00"));

        while ((line = reader.readLine()) != null) {
            position = lineToTransportPosition(line, null);
            if (position != null) {
                positions.add(position);
            }
            if (++i % 100000 == 0) {
                System.out.println(i);
            }
        }
        fileReader.close();
        System.out.println(positions.size() + " records processed");
        return positions;
    }

    /**
     * распарсить лог в память. Лог делится в полночь, а граница суток - 03:00. Поэтому все, что до этого,
     * пишется в positionsToday, более позднее в positionsTomorrow. В остальных коллекциях накапливаются
     * айдишники
     *
     * @param inputFile
     * @param positionsToday
     * @param positionsTomorrow
     * @param routeIds
     * @param routeIdsTomorrow
     * @param vehicles
     * @param vehicleTNs
     * @param routeTNs
     * @param kt_ids
     * @param kr_ids
     * @param ks_ids
     * @throws IOException
     * @throws ParseException
     */
    public static void parseLogToTable(String inputFile,
                                       Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positionsToday,
                                       Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positionsTomorrow,
                                       Table<MR_ID, KR_ID, Integer> routeIds,
                                       Table<MR_ID, KR_ID, Integer> routeIdsTomorrow,
                                       Table<VehicleTN, KT_ID, Vehicle> vehicles,
                                       Map<Integer, VehicleTN>[] vehicleTNs,
                                       Map<Integer, MR_ID>[] routeTNs, Map<Integer, KT_ID> kt_ids,
                                       Map<Integer, KR_ID> kr_ids, Map<Integer, KS_ID> ks_ids,
                                       int dayDelim)
            throws IOException, ParseException {
        try (FileInputStream in = new FileInputStream(inputFile);
             ZipInputStream zip = new ZipInputStream(in);
             InputStreamReader entryReader = new InputStreamReader(zip);
             BufferedReader reader = new BufferedReader(entryReader)) {


            for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
                String line;
                int i = 0;

                parserTimestamp.setTimeZone(TimeZone.getTimeZone("GMT+4:00"));
                long time = -System.nanoTime();
                while ((line = reader.readLine()) != null) {
                    parseLine(line, positionsToday, positionsTomorrow, routeIds, routeIdsTomorrow,
                              vehicles,
                              vehicleTNs, routeTNs, kt_ids, kr_ids, ks_ids, dayDelim);

                    if (++i % 100000 == 0) {
                        System.out.println(i);
                    }
                }
                time += System.nanoTime();
                System.out.println(
                        Math.round(time / 1e9) + "s, " + Math.round(1e9 * i / time) + " records/s");
                System.out.println(i + " records processed");
            }
        }
    }

    private static KR_ID nullKR_ID = new KR_ID(0);

    /**
     * парсим одну строку лога
     *
     * @param line
     * @param yesterdayPositions monitoring by trans type / route / vehicle
     * @param todayPositions
     * @param routeIdsYesterday  mapping by trans type tn_id <-> routeID
     * @param vehicles           vehicles by trans type and tn_id / routeID   @throws ParseException
     * @param dayDelim
     */
    public static void parseLine(String line,
                                 Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> yesterdayPositions,
                                 Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> todayPositions,
                                 Table<MR_ID, KR_ID, Integer> routeIdsYesterday,
                                 Table<MR_ID, KR_ID, Integer> routeIdsToday,
                                 Table<VehicleTN, KT_ID, Vehicle> vehicles,
                                 Map<Integer, VehicleTN>[] vehicleTNs,
                                 Map<Integer, MR_ID>[] routeTNs, //id - type - object
                                 Map<Integer, KT_ID> kt_ids,
                                 Map<Integer, KR_ID> kr_ids,
                                 Map<Integer, KS_ID> ks_ids,
                                 int dayDelim)
            throws ParseException {
        String[] tokens = line.split("\t");
        if (tokens.length < 16) {
            return;
        }
//        System.out.println("tokens count: " + tokens.length);

        MonitoringLogEntry position = new MonitoringLogEntry();
        position.lat = Double.parseDouble(tokens[3]);
        position.lng = Double.parseDouble(tokens[4]);
        if (position.lat < 53 && position.lng < 50) {
            return;
        }

        List<MonitoringLogEntry> vehicleMonitoring;
        position.timestamp = (int) (parserTimestamp.parse(tokens[2].trim()).getTime() / 1000);

        if (position.timestamp >= dayDelim) {
            yesterdayPositions = todayPositions;
            routeIdsYesterday = routeIdsToday;
        } else {
            Iterator<List<MonitoringLogEntry>> iterator = yesterdayPositions.values().iterator();
            if (iterator.hasNext()) {
                List<MonitoringLogEntry> list;
                do {
                    list = iterator.next();
                } while (list.size() == 0);
//                if (position.timestamp - list.get(0).timestamp > Utils.DAY_SECS) {
//                    return;
//                }
            }
        }

        int transType;
        if (tokens[14].length() > 0) {
            transType = 0;
        } else {
            transType = Integer.parseInt(tokens[6]);
        }

        Integer tmpID = Integer.parseInt(tokens[0].trim());
        position.vehicleID = vehicleTNs[transType].get(tmpID);
        if (position.vehicleID == null) {
            vehicleTNs[transType].put(tmpID, position.vehicleID = new VehicleTN(tmpID, transType));
        }

        tmpID = Integer.parseInt(tokens[1]);
        MR_ID mr_id = routeTNs[transType].get(tmpID);
        if (mr_id == null) {
            routeTNs[transType].put(tmpID, mr_id = new MR_ID(tmpID, transType));
        }
        position.routeTN = tmpID;


        vehicleMonitoring = yesterdayPositions.get(mr_id, position.vehicleID);
        if (vehicleMonitoring == null) {
            yesterdayPositions.put(mr_id, position.vehicleID, vehicleMonitoring = new ArrayList<>());
        }
        int i = vehicleMonitoring.size() - 1;
        while (i >= 0 && vehicleMonitoring.get(i).timestamp > position.timestamp) {
            System.out.println("Непоследовательные записи " + tokens[7]);
            i--;
        }
        vehicleMonitoring.add(i + 1, position);

        if (transType == 0) {
            return;
        }

        tmpID = Integer.parseInt(tokens[5]);
        position.kt_id = tmpID;
        position.boardNum = tokens[7];
        position.x = Double.parseDouble(tokens[8]);
        position.y = Double.parseDouble(tokens[9]);


        KT_ID kt_id = kt_ids.get(tmpID);
        if (kt_id == null) {
            kt_ids.put(tmpID, kt_id = new KT_ID(tmpID));
        }
        if (!vehicles.contains(position.vehicleID, kt_id)) {
            vehicles.put(position.vehicleID, kt_id,
                         new Vehicle(position.vehicleID, kt_id, transType, position.boardNum));
        }

        Map<KR_ID, Integer> row = routeIdsYesterday.row(mr_id);
        if (tokens[13].charAt(0) == 'M') {
            tmpID = Integer.parseInt(tokens[10]);
            KR_ID kr_id = kr_ids.get(tmpID);
            if (kr_id == null) {
                kr_ids.put(tmpID, kr_id = new KR_ID(tmpID));
            }
            position.routeID = kr_id;

            tmpID = Integer.parseInt(tokens[11]);
            KS_ID ks_id = ks_ids.get(tmpID);
            if (ks_id == null) {
                ks_ids.put(tmpID, ks_id = new KS_ID(tmpID));
            }
            position.nextStopID = ks_id;
            position.nextStopDist = Double.parseDouble(tokens[12]);
            if (position.routeID.id > 0 && !row.containsKey(position.routeID)) {
                row.put(position.routeID, Utils.getDayBeginning(position.timestamp));
                row.remove(nullKR_ID);
            }
        }

        if (row.size() == 0) {
            row.put(nullKR_ID, Utils.getDayBeginning(position.timestamp));
        }
    }


//    [12:40:25] Александр Сергеев: Структура лога:
//            [12:40:26] Александр Сергеев: - ID транспорта в ТранНави
//    - ID маршрута в ТранНави
//    - TimeNav
//    - Latitude
//    - Longitude
//    - ID транспорта в СА
//    - ID типа транспорта в СА
//    - Гос номер
//    - X
//    - Y
//    - KR_ID маршрута
//    - KS_ID ближайшей остановки
//    - Оставшийся путь до остановки
//    - Состояние
//    - Время записи в лог

    @Deprecated
    public static MonitoringLogEntry lineToTransportPosition(String line, MonitoringLogEntry reuse)
            throws ParseException {
        if (reuse == null) {
            reuse = new MonitoringLogEntry();
        }


        String[] tokens = line.split("\t");
        if (tokens.length < 16) {
            return null;
        }
//        System.out.println("tokens count: " + tokens.length);

        reuse.lat = Double.parseDouble(tokens[3].trim());
        reuse.lng = Double.parseDouble(tokens[4].trim());
        if (reuse.lat < 53 && reuse.lng < 50) {
            return null;
        }
        reuse.routeID = new KR_ID();
        reuse.nextStopID = new KS_ID();
        reuse.vehicleID =
                new VehicleTN(Integer.parseInt(tokens[0].trim()), Integer.parseInt(tokens[6].trim()));
        reuse.routeTN = Integer.parseInt(tokens[1].trim());
        reuse.timestamp = (int) (parserTimestamp.parse(tokens[2].trim()).getTime() / 1000);
        if (!"".equals(tokens[14])) {
            reuse.kt_id = -1;
            return reuse;
        }

        reuse.kt_id = Integer.parseInt(tokens[5].trim());
        reuse.boardNum = tokens[7].trim();
        reuse.x = Double.parseDouble(tokens[8].trim());
        reuse.y = Double.parseDouble(tokens[9].trim());
        String s = tokens[13].trim();
        if ("MOVING".equals(s)) {
            reuse.routeID.id = Integer.parseInt(tokens[10].trim());
            reuse.nextStopID.id = Integer.parseInt(tokens[11].trim());
            reuse.nextStopDist = Double.parseDouble(tokens[12].trim());
        } else if (!"UNKNOWN".equals(s)) {
            System.out.println("UNDEFINED STATUS");
        }

        return reuse;
    }


    /**
     * парсим одну строку файла графиков
     *
     * @param line
     * @param reuse
     * @return
     * @throws ParseException
     */
    public static ScheduleRecord lineToSchedule(String line, ScheduleRecord reuse)
            throws ParseException {
        if (reuse == null) {
            reuse = new ScheduleRecord();
        }


        String[] tokens = line.split("\t");
        if (tokens.length < 12) {
            return null;
        }
//        System.out.println("tokens count: " + tokens.length);

        reuse.kt_id = new KT_ID(Integer.parseInt(tokens[0].trim()));
        reuse.kr_id = new KR_ID(Integer.parseInt(tokens[1].trim()));
        reuse.raceNumber = Integer.parseInt(tokens[2].trim());
        reuse.controlPointId = Integer.parseInt(tokens[3].trim());
        reuse.controlPointName = tokens[4].trim();
        reuse.time = (int) (parserSDFschedule.parse(tokens[5].trim()).getTime() / 1000);
        reuse.modelTitle = tokens[6].trim();
//        reuse.setDirection(tokens[7].trim());
//        reuse.setDirectionTN(tokens[8].trim());
        reuse.direction = tokens[7].trim();
        reuse.directionTN = tokens[8].trim();
        reuse.routeIdTN = Integer.parseInt(tokens[9].trim());
        reuse.transportTypeIdTN = Integer.parseInt(tokens[10].trim());
//        reuse.dateStamp = parserDate.parse(tokens[11].trim()).getTime();

        return reuse;
    }


    public static PlanTimingRecord lineToPlanTiming(String line, PlanTimingRecord reuse) {
        if (reuse == null) {
            reuse = new PlanTimingRecord();
        }

        String[] tokens = line.split("\t");
        if (tokens.length < 7) {
            return null;
        }

        reuse.kt_id = Integer.parseInt(tokens[0]);
        reuse.mr_id = Integer.parseInt(tokens[1]);
        reuse.raceNumber = Integer.parseInt(tokens[2]);
        reuse.time = Integer.parseInt(tokens[4]);
        reuse.id = Integer.parseInt(tokens[5]); //cp_id
        reuse.direction = tokens[3].length() > 0 ? tokens[3].charAt(0) : '-';
        reuse.cpShortName = tokens[6];

        return reuse;
    }
}









