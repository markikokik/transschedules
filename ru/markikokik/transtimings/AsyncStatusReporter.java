package ru.markikokik.transtimings;

public interface AsyncStatusReporter {
    void onError(Exception e);

    void onSuccess();

    void onProgressChanged(int read, int length);
}
