package ru.markikokik.transtimings.db;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import java.io.ByteArrayInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ru.markikokik.transtimings.entities.Accident;
import ru.markikokik.transtimings.entities.ControlPoint;
import ru.markikokik.transtimings.entities.Deviation;
import ru.markikokik.transtimings.entities.Interval;
import ru.markikokik.transtimings.entities.MonitoringLogEntry;
import ru.markikokik.transtimings.entities.PositionRelToRoutes;
import ru.markikokik.transtimings.entities.Race;
import ru.markikokik.transtimings.entities.RaceTimings;
import ru.markikokik.transtimings.entities.Route;
import ru.markikokik.transtimings.entities.ScheduleTime;
import ru.markikokik.transtimings.entities.ScheduleRecord;
import ru.markikokik.transtimings.entities.Stop;
import ru.markikokik.transtimings.entities.TransportPosition;
import ru.markikokik.transtimings.entities.Vehicle;
import ru.markikokik.transtimings.entities.ids.CP_ID;
import ru.markikokik.transtimings.entities.ids.ID;
import ru.markikokik.transtimings.entities.ids.KR_ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;
import ru.markikokik.transtimings.entities.ids.KT_ID;
import ru.markikokik.transtimings.entities.ids.PeriodID;
import ru.markikokik.transtimings.entities.ids.RaceID;
import ru.markikokik.transtimings.entities.ids.MR_ID;
import ru.markikokik.transtimings.entities.ids.VehicleTN;

import static ru.markikokik.transtimings.db.DbContract.CircuitEntry;
import static ru.markikokik.transtimings.db.DbContract.ControlPointEntry;
import static ru.markikokik.transtimings.db.DbContract.MonitoringEntry;
import static ru.markikokik.transtimings.db.DbContract.RouteEntry;
import static ru.markikokik.transtimings.db.DbContract.StopEntry;
import static ru.markikokik.transtimings.db.DbContract.VehicleEntry;

/**
 * Created by markikokik on 01.04.16.
 */
public class DbUtils {
    public static final int newRaceTresholdSecs = 600;

    public static String constructStaysRequest(int stationID, Set<Integer> routeTNs, int timeFrom,
                                               int timeTo) {
        String result = null;
        if (stationID >= 0) {
            result = DbContract.StationStayEntry.STATION_ID + "=" + stationID;
        }
        if (routeTNs != null) {
            result = result != null ? result + " AND " : "";
            result += DbContract.StationStayEntry.ROUTE_TN + " in (";
            for (Integer routeTN : routeTNs) {
                result += routeTN + ",";
            }
            result = result.substring(0, result.length() - 1) + ")";
        }
        if (timeFrom > 0) {
            result = result != null ? result + " AND " : "";
            result += DbContract.StationStayEntry.ARRIVE_TIME + ">=" + timeFrom;
        }
        if (timeTo > 0) {
            result = result != null ? result + " AND " : "";
            result += DbContract.StationStayEntry.DEPART_TIME + "<=" + timeTo;
        }

        if (result == null) {
            result = DbContract.StationStayEntry.SELECT_STAYS_SQL.replace("?", "");
        } else {
            result = DbContract.StationStayEntry.SELECT_STAYS_SQL.replace("?", " where " + result);
        }

        return result;
    }

    public static Table<MR_ID, VehicleTN, Deque<Interval<MR_ID, VehicleTN>>>
    resultSetToStays(ResultSet resultSet) throws SQLException {
        Table<MR_ID, VehicleTN, Deque<Interval<MR_ID, VehicleTN>>> table =
                HashBasedTable.create();
        int arrIndex = resultSet.findColumn(DbContract.StationStayEntry.ARRIVE_TIME);
        int depIndex = resultSet.findColumn(DbContract.StationStayEntry.DEPART_TIME);
        int routeIndex = resultSet.findColumn(DbContract.StationStayEntry.ROUTE_TN);
//        int stationIndex = resultSet.findColumn(DbContract.StationStayEntry.STATION_ID);
        int trTypeIndex = resultSet.findColumn(DbContract.StationStayEntry.TRANS_TYPE);
        int vehicleIndex = resultSet.findColumn(DbContract.StationStayEntry.VEHICLE_TN);

        int route, transType, vehicle;
        MR_ID mr_id = new MR_ID();
        VehicleTN vehicleTN = new VehicleTN();
        Map<VehicleTN, Deque<Interval<MR_ID, VehicleTN>>> routeStays = null;
        ArrayDeque<Interval<MR_ID, VehicleTN>> vehicleStays = null;
        while (resultSet.next()) {
            route = resultSet.getInt(routeIndex);
            transType = resultSet.getInt(trTypeIndex);

            if (route != mr_id.id || transType != mr_id.transType) {
                mr_id = new MR_ID(route, transType);
                routeStays = table.row(mr_id);
            }

            vehicle = resultSet.getInt(vehicleIndex);
            if (vehicle != vehicleTN.id || transType != vehicleTN.transType) {
                vehicleTN = new VehicleTN(vehicle, transType);
                routeStays.put(vehicleTN, vehicleStays = new ArrayDeque<>());
            }
            vehicleStays.add(new Interval<>(
                                     new TransportPosition<>(
                                             vehicleTN,
                                             resultSet.getInt(arrIndex),
                                             0, 0
                                     ),
                                     new TransportPosition<>(
                                             vehicleTN,
                                             resultSet.getInt(depIndex),
                                             0, 0
                                     ),
                                     mr_id
                             )
            );

        }

        return table;
    }

    public static Table<KT_ID, RaceID, List<TransportPosition<KT_ID>>> resultSetToRouteMonitoringByRace(
            ResultSet resultSet)
            throws SQLException {
        Table<KT_ID, RaceID, List<TransportPosition<KT_ID>>> result = HashBasedTable.create();
        int transIdIndex = resultSet.findColumn(VehicleEntry.KT_ID);
        int nextStopIndex = resultSet.findColumn(MonitoringEntry.NEXT_STOP);
        int distIndex = resultSet.findColumn(MonitoringEntry.STOP_DIST);
        int xIndex = resultSet.findColumn(MonitoringEntry.X);
        int yIndex = resultSet.findColumn(MonitoringEntry.Y);
        int timeIndex = resultSet.findColumn(MonitoringEntry.TIMESTAMP);

        KT_ID kt_id = new KT_ID();
        int tmpID, raceNumber = 0, time, lastTime = 0;
        List<TransportPosition<KT_ID>> positions = null;
        while (resultSet.next()) {
            tmpID = resultSet.getInt(transIdIndex);
            if (kt_id.id != tmpID) {
                kt_id = new KT_ID(tmpID);
                raceNumber = 0;
                result.put(kt_id, new RaceID(0), positions = new ArrayList<>());
            }
            time = resultSet.getInt(timeIndex);
            if (time - lastTime > newRaceTresholdSecs) {
                result.put(kt_id, new RaceID(++raceNumber), positions = new ArrayList<>());
            }
            positions.add(new TransportPosition<>(kt_id,
                                                  new KS_ID(resultSet.getInt(nextStopIndex)),
                                                  resultSet.getDouble(distIndex),
                                                  lastTime = time,
                                                  resultSet.getDouble(xIndex),
                                                  resultSet.getDouble(yIndex)
            ));

        }
        return result;
    }

    public static Table<VehicleTN, CP_ID, List<RaceTimings>> resultSetToRouteRealScheduleByVehicle(
            ResultSet resultSet)
            throws SQLException {
        Table<VehicleTN, CP_ID, List<RaceTimings>> result = HashBasedTable.create();
        int vehicleIndex = resultSet.findColumn(DbContract.RealScheduleEntry.VEHICLE_TN);
        int typeIndex = resultSet.findColumn(DbContract.RealScheduleEntry.TRANS_TYPE);
        int cpIndex = resultSet.findColumn(DbContract.RealScheduleEntry.CP_ID);
        int arrIndex = resultSet.findColumn(DbContract.RealScheduleEntry.TIME_ARR);
        int depIndex = resultSet.findColumn(DbContract.RealScheduleEntry.TIME_DEP);

        VehicleTN vehicleTN = new VehicleTN(-1, -1);
        CP_ID cp_id = new CP_ID(-1);
        int currentVehicle, currentCP_ID, currentType;
        List<RaceTimings> timings = null;
        boolean changed;
        while (resultSet.next()) {
            currentVehicle = resultSet.getInt(vehicleIndex);
            currentType = resultSet.getInt(typeIndex);
            currentCP_ID = resultSet.getInt(cpIndex);
            changed = false;
            if (currentVehicle != vehicleTN.id || currentType != vehicleTN.transType) {
                vehicleTN = new VehicleTN(currentVehicle, currentType);
                changed = true;
            }
            if (currentCP_ID != cp_id.id) {
                cp_id = new CP_ID(currentCP_ID);
                changed = true;
            }
            if (changed) {
                result.put(vehicleTN, cp_id, timings = new ArrayList<>());
            }
            timings.add(new RaceTimings(resultSet.getInt(arrIndex), resultSet.getInt(depIndex)));
        }

        return result;
    }

    public static Map<CP_ID, List<Race>> resultSetToRouteRealSchedule(ResultSet resultSet)
            throws SQLException {
        Map<CP_ID, List<Race>> result = new HashMap<>();
        int vehicleIndex = resultSet.findColumn(DbContract.RealScheduleEntry.VEHICLE_TN);
        int typeIndex = resultSet.findColumn(DbContract.RealScheduleEntry.TRANS_TYPE);
        int cpIndex = resultSet.findColumn(DbContract.RealScheduleEntry.CP_ID);
        int arrIndex = resultSet.findColumn(DbContract.RealScheduleEntry.TIME_ARR);
        int depIndex = resultSet.findColumn(DbContract.RealScheduleEntry.TIME_DEP);

        CP_ID cp_id = new CP_ID(-1);
        int currentCP_ID;
        List<Race> timings = null;
        while (resultSet.next()) {
            currentCP_ID = resultSet.getInt(cpIndex);
            if (currentCP_ID != cp_id.id) {
                cp_id = new CP_ID(currentCP_ID);
                result.put(cp_id, timings = new ArrayList<>());
            }
            timings.add(new Race(
                                resultSet.getInt(arrIndex),
                                resultSet.getInt(depIndex),
                                resultSet.getInt(vehicleIndex),
                                resultSet.getInt(typeIndex)
                        )
            );
        }

        return result;
    }

    public static Map<KT_ID, List<TransportPosition<KT_ID>>> resultSetToRouteMonitoringByKTid(
            ResultSet resultSet)
            throws SQLException {
        Map<KT_ID, List<TransportPosition<KT_ID>>> result = new HashMap<>();
        if (resultSet.isClosed()) {
            System.out.println("monitoring result closed");
            return result;
        }
        int transIdIndex = resultSet.findColumn(VehicleEntry.KT_ID);
        int nextStopIndex = resultSet.findColumn(MonitoringEntry.NEXT_STOP);
        int distIndex = resultSet.findColumn(MonitoringEntry.STOP_DIST);
        int xIndex = resultSet.findColumn(MonitoringEntry.X);
        int yIndex = resultSet.findColumn(MonitoringEntry.Y);
        int latIndex = resultSet.findColumn(MonitoringEntry.LAT);
        int lngIndex = resultSet.findColumn(MonitoringEntry.LNG);
        int timeIndex = resultSet.findColumn(MonitoringEntry.TIMESTAMP);

        KT_ID KT_ID = new KT_ID();
        int tmpID;
        List<TransportPosition<KT_ID>> positions = null;
        while (resultSet.next()) {
            tmpID = resultSet.getInt(transIdIndex);
            if (KT_ID.id != tmpID) {
                KT_ID = new KT_ID(tmpID);
                result.put(KT_ID, positions = new ArrayList<>());
            }
            positions.add(new TransportPosition<KT_ID>(KT_ID,
                                                       new KS_ID(resultSet.getInt(nextStopIndex)),
                                                       resultSet.getDouble(distIndex),
                                                       resultSet.getInt(timeIndex),
                                                       resultSet.getDouble(xIndex),
                                                       resultSet.getDouble(yIndex),
                                                       resultSet.getDouble(latIndex),
                                                       resultSet.getDouble(lngIndex)
            ));
        }
        return result;
    }

    /**
     * @param resultSet
     * @param kr_id
     * @return map of lists of transport potitions WITH NULL {@link KT_ID}, splitted by {@link VehicleTN},
     * @throws SQLException
     */
    public static Map<VehicleTN, List<PositionRelToRoutes<VehicleTN, KR_ID>>> resultSetToRouteMonitoringByTNid(
            ResultSet resultSet, KR_ID kr_id)
            throws SQLException {
        Map<VehicleTN, List<PositionRelToRoutes<VehicleTN, KR_ID>>> result = new HashMap<>();
        if (resultSet.isClosed()) {
            System.out.println("monitoring result closed");
            return result;
        }
        int transIdIndex = resultSet.findColumn(MonitoringEntry.VEHICLE_TN);
        int transTypeIndex = resultSet.findColumn(MonitoringEntry.TRANS_TYPE);
        int nextStopIndex = resultSet.findColumn(MonitoringEntry.NEXT_STOP);
        int distIndex = resultSet.findColumn(MonitoringEntry.STOP_DIST);
        int xIndex = resultSet.findColumn(MonitoringEntry.X);
        int yIndex = resultSet.findColumn(MonitoringEntry.Y);
        int latIndex = resultSet.findColumn(MonitoringEntry.LAT);
        int lngIndex = resultSet.findColumn(MonitoringEntry.LNG);
        int timeIndex = resultSet.findColumn(MonitoringEntry.TIMESTAMP);

        Map<Integer, KS_ID> ks_ids = new HashMap<>();
        KS_ID ks_id;
        VehicleTN vehicleTN = new VehicleTN();
        int tmpVehicle, stopID, tmpTransType;
        List<PositionRelToRoutes<VehicleTN, KR_ID>> positions = null;
        while (resultSet.next()) {
            tmpVehicle = resultSet.getInt(transIdIndex);
            tmpTransType = resultSet.getInt(transTypeIndex);
            if (vehicleTN.id != tmpVehicle || vehicleTN.transType != tmpTransType) {
                vehicleTN = new VehicleTN(tmpVehicle, tmpTransType);
                result.put(vehicleTN, positions = new ArrayList<>());
            }
            stopID = resultSet.getInt(nextStopIndex);
            ks_id = ks_ids.get(stopID);
            if (ks_id == null) {
                ks_ids.put(stopID, new KS_ID(stopID));
            }
            positions.add(new PositionRelToRoutes<>(kr_id, vehicleTN, ks_id,
                                                    resultSet.getDouble(distIndex),
                                                    resultSet.getInt(timeIndex),
                                                    resultSet.getDouble(xIndex),
                                                    resultSet.getDouble(yIndex),
                                                    resultSet.getDouble(latIndex),
                                                    resultSet.getDouble(lngIndex)
            ));
        }
        return result;
    }

    public static Map<VehicleTN, List<? extends PositionRelToRoutes>> resultSetToAllDirectionsMonitoring(
            ResultSet resultSet)
            throws SQLException {
        Map<VehicleTN, List<? extends PositionRelToRoutes>> result = new HashMap<>();
        if (resultSet.isClosed()) {
            System.out.println("monitoring result closed");
            return result;
        }
        int transTypeIndex = resultSet.findColumn(MonitoringEntry.TRANS_TYPE);
        int transIdIndex = resultSet.findColumn(MonitoringEntry.VEHICLE_TN);
        int xIndex = resultSet.findColumn(MonitoringEntry.X);
        int yIndex = resultSet.findColumn(MonitoringEntry.Y);
        int latIndex = resultSet.findColumn(MonitoringEntry.LAT);
        int lngIndex = resultSet.findColumn(MonitoringEntry.LNG);
        int timeIndex = resultSet.findColumn(MonitoringEntry.TIMESTAMP);
        int routeIndex = resultSet.findColumn(MonitoringEntry.KR_ID);
        int stopIndex = resultSet.findColumn(MonitoringEntry.NEXT_STOP);
        int stopDistIndex = resultSet.findColumn(MonitoringEntry.STOP_DIST);

        VehicleTN tn_id = new VehicleTN(0, 0);
        KR_ID kr_id = new KR_ID();
        KS_ID ks_id = new KS_ID();
        Map<Integer, KR_ID> kr_ids = new HashMap<>();
        Map<Integer, KS_ID> ks_ids = new HashMap<>();
        int tmpVehicle, tmpTransType;
        List<PositionRelToRoutes> positions = null;
        while (resultSet.next()) {
            tmpVehicle = resultSet.getInt(transIdIndex);
            tmpTransType = resultSet.getInt(transTypeIndex);
            if (tn_id.id != tmpVehicle || tn_id.transType != tmpTransType) {
                tn_id = new VehicleTN(tmpVehicle, tmpTransType);
                result.put(tn_id, positions = new ArrayList<>());
            }
            tmpVehicle = resultSet.getInt(routeIndex);
            if (tmpVehicle != kr_id.id) {
                kr_id = kr_ids.get(tmpVehicle);
                if (kr_id == null) {
                    kr_ids.put(tmpVehicle, kr_id = new KR_ID(tmpVehicle));
                }
            }
            tmpVehicle = resultSet.getInt(stopIndex);
            if (tmpVehicle != ks_id.id) {
                ks_id = ks_ids.get(tmpVehicle);
                if (ks_id == null) {
                    ks_ids.put(tmpVehicle, ks_id = new KS_ID(tmpVehicle));
                }
            }
            positions.add(new PositionRelToRoutes<>(kr_id, null, ks_id,
                                                    resultSet.getDouble(stopDistIndex),
                                                    resultSet.getInt(timeIndex),
                                                    resultSet.getDouble(xIndex),
                                                    resultSet.getDouble(yIndex),
                                                    resultSet.getDouble(latIndex),
                                                    resultSet.getDouble(lngIndex)
            ));
        }
        return result;
    }

    public static Vehicle resultSetToVehicle(ResultSet resultSet) throws SQLException {

        return resultSet.next() ? new Vehicle(
                resultSet.getInt(
                        resultSet.findColumn(VehicleEntry.VEHICLE_TN)),
                new KT_ID(resultSet.getInt(
                        resultSet.findColumn(VehicleEntry.KT_ID))),
                resultSet.getInt(
                        resultSet.findColumn(VehicleEntry.TRANS_TYPE)),
                resultSet.getString(
                        resultSet.findColumn(VehicleEntry.BOARD_NUMBER))
        ) : null;
    }

    public static List<Vehicle> resultSetToVehicles(ResultSet resultSet) throws SQLException {
        List<Vehicle> result = new ArrayList<>();
        int tnIndex = resultSet.findColumn(VehicleEntry.VEHICLE_TN),
                ktIdIndex = resultSet.findColumn(VehicleEntry.KT_ID),
                transTypeIndex = resultSet.findColumn(VehicleEntry.TRANS_TYPE),
                boardNumberIndex = resultSet.findColumn(VehicleEntry.BOARD_NUMBER);

        while (resultSet.next()) {
            result.add(new Vehicle(
                    resultSet.getInt(tnIndex),
                    new KT_ID(resultSet.getInt(ktIdIndex)),
                    resultSet.getInt(transTypeIndex),
                    resultSet.getString(boardNumberIndex)
            ));
        }
        return result;
    }

    public static Table<KT_ID, CP_ID, List<Integer>> resultSetToRouteScheduleByVehicle(
            ResultSet resultSet)
            throws SQLException {
        Table<KT_ID, CP_ID, List<Integer>> result = HashBasedTable.create();
        int cpIndex = resultSet.findColumn(DbContract.ScheduleEntry.CONTROL_POINT_ID);
        int timeIndex = resultSet.findColumn(DbContract.ScheduleEntry.TIME);
        int transIndex = resultSet.findColumn(DbContract.ScheduleEntry.KT_ID);

        KT_ID kt_id = new KT_ID();
        CP_ID cp_id = new CP_ID();
        int tmpKT_ID, tmpCP_ID;
        List<Integer> times = null;
        while (resultSet.next()) {
            tmpKT_ID = resultSet.getInt(transIndex);
            if (tmpKT_ID != kt_id.id) {
                kt_id = new KT_ID();
                kt_id.id = tmpKT_ID;
                cp_id = new CP_ID();
            }
            tmpCP_ID = resultSet.getInt(cpIndex);
            if (cp_id.id != tmpCP_ID) {
                if (cp_id.id != 0) {
                    cp_id = new CP_ID();
                }
                cp_id.id = tmpCP_ID;
                result.put(kt_id, cp_id, times = new ArrayList<>());
            }
            times.add(resultSet.getInt(timeIndex));
        }
        return result;
    }

    public static Map<CP_ID, List<Deviation>> resultSetToRouteDeviations(ResultSet resultSet)
            throws SQLException {
        Map<CP_ID, List<Deviation>> deviations = new HashMap<>();
        int cpIndex = resultSet.findColumn(DbContract.DeviationEntry.CP_ID);
        int devIndex = resultSet.findColumn(DbContract.DeviationEntry.DEVIATION);
        int dirIndex = resultSet.findColumn(DbContract.DeviationEntry.DIRECTION_TN);
        int planKTindex = resultSet.findColumn(DbContract.DeviationEntry.KT_ID_PLAN);
        int realKTindex = resultSet.findColumn(DbContract.DeviationEntry.KT_ID_REAL);
        int timeIndex = resultSet.findColumn(DbContract.DeviationEntry.PLAN_TIME);

        Map<Integer, KT_ID> kt_ids = new HashMap<>();
        CP_ID cp_id = new CP_ID();
        int tmpCP_ID;
        List<Deviation> times = null;
        KT_ID realKT, planKT;
        int realID, planID;
        while (resultSet.next()) {
            tmpCP_ID = resultSet.getInt(cpIndex);
            if (cp_id.id != tmpCP_ID) {
                if (cp_id.id != 0) {
                    cp_id = new CP_ID();
                }
                cp_id.id = tmpCP_ID;
                deviations.put(cp_id, times = new ArrayList<>());
            }
            realID = resultSet.getInt(realKTindex);
            realKT = kt_ids.get(realID);
            if (realKT == null && realID > 0) {
                kt_ids.put(realID, realKT = new KT_ID(realID));
            }
            planID = resultSet.getInt(planKTindex);
            planKT = kt_ids.get(planID);
            if (planKT == null && planID > 0) {
                kt_ids.put(planID, planKT = new KT_ID(planID));
            }
            times.add(new Deviation(
                    resultSet.getInt(timeIndex),
                    resultSet.getInt(devIndex),
                    resultSet.getString(dirIndex),
                    planKT,
                    realKT
            ));
        }
        return deviations;
    }

    public static Table<KR_ID, KS_ID, Double> resulSetToCircuitsWithDists(ResultSet resultSet)
            throws SQLException {
        Table<KR_ID, KS_ID, Double> result = HashBasedTable.create();

        int krIndex = resultSet.findColumn(DbContract.ControlPointEntry.KR_ID);
        int ksIndex = resultSet.findColumn(StopEntry.KS_ID);
        int distIndex = resultSet.findColumn(CircuitEntry.DIST);

        Map<KS_ID, Double> routeCircuit = null;

        int tmpKR = 0;
        KR_ID kr_id = new KR_ID(0);
        while (resultSet.next()) {
            if ((tmpKR = resultSet.getInt(krIndex)) != kr_id.id) {
                kr_id = new KR_ID(tmpKR);
                routeCircuit = result.row(kr_id);
            }
            routeCircuit.put(
                    new KS_ID(resultSet.getInt(ksIndex)),
                    resultSet.getDouble(distIndex)
            );
        }

        return result;
    }

    public static Map<VehicleTN, List<Accident<ID, VehicleTN>>> resultSetToAccidents(ResultSet resultSet)
            throws SQLException {
        Map<VehicleTN, List<Accident<ID, VehicleTN>>> accidents = new HashMap<>();
        int krIndex = resultSet.findColumn(DbContract.AccidentEntry.KR_ID);
        int tnIndex = resultSet.findColumn(DbContract.AccidentEntry.ROUTE_TN);
        int vehicleIndex = resultSet.findColumn(DbContract.AccidentEntry.VEHICLE_TN);
        int transTypeIndex = resultSet.findColumn(DbContract.AccidentEntry.TRANS_TYPE);
        int startIndex = resultSet.findColumn(DbContract.AccidentEntry.START_TIME);
        int endIndex = resultSet.findColumn(DbContract.AccidentEntry.END_TIME);
        int xIndex = resultSet.findColumn(DbContract.AccidentEntry.X);
        int yIndex = resultSet.findColumn(DbContract.AccidentEntry.Y);

        List<Accident<ID, VehicleTN>> vehicleAccidents = null;
        Accident<ID, VehicleTN> accident;
        int tmpTN;
        Table<Integer, Integer, VehicleTN> vehicleTNs = HashBasedTable.create();
        VehicleTN vehicleTN;
        Map<Integer, ID> routes_ids = new HashMap<>();
        double x, y;
        ID routeID;
        int tmpRouteID, tmpTypeID, count = 0;
        while (resultSet.next()) {
            tmpTypeID = resultSet.getInt(transTypeIndex);
            tmpTN = resultSet.getInt(vehicleIndex);

            vehicleTN = vehicleTNs.get(tmpTypeID, tmpTN);
            if (vehicleTN == null) {
                vehicleTNs.put(tmpTypeID, tmpTN, vehicleTN = new VehicleTN(tmpTN, tmpTypeID));
            }

            vehicleAccidents = accidents.get(vehicleTN);
            if (vehicleAccidents == null) {
                accidents.put(vehicleTN, vehicleAccidents = new ArrayList<>());
            }

            x = resultSet.getDouble(xIndex);
            y = resultSet.getDouble(yIndex);
            tmpRouteID = resultSet.getInt(krIndex);
            if (tmpRouteID > 0) {
                routeID = routes_ids.get(tmpRouteID);
                if (routeID == null) {
                    routes_ids.put(tmpRouteID, routeID = new KR_ID(tmpRouteID));
                }
            } else {
                tmpRouteID = resultSet.getInt(tnIndex);
                routeID = routes_ids.get(tmpRouteID);
                if (routeID == null) {
                    routes_ids.put(tmpRouteID, routeID = new MR_ID(tmpRouteID, tmpTypeID));
                }
            }

            accident = new Accident<>(
                    new TransportPosition<>(vehicleTN, resultSet.getInt(startIndex), x, y),
                    new TransportPosition<>(vehicleTN, resultSet.getInt(endIndex), x, y),
                    routeID);
            vehicleAccidents.add(accident);
            count++;
        }
        System.out.println("count=" + count);
        return accidents;
    }

    public static List<List<Accident<ID, VehicleTN>>> resultSetToAccidentsList(ResultSet resultSet)
            throws SQLException {
        List<List<Accident<ID, VehicleTN>>> accidents = new ArrayList<>();
        List<Accident<ID, VehicleTN>> cluster = null;
        int kr_idIndex = resultSet.findColumn(DbContract.AccidentEntry.KR_ID);
        int route_tnIndex = resultSet.findColumn(DbContract.AccidentEntry.ROUTE_TN);
        int vehicleIndex = resultSet.findColumn(DbContract.AccidentEntry.VEHICLE_TN);
        int transTypeIndex = resultSet.findColumn(DbContract.AccidentEntry.TRANS_TYPE);
        int startIndex = resultSet.findColumn(DbContract.AccidentEntry.START_TIME);
        int endIndex = resultSet.findColumn(DbContract.AccidentEntry.END_TIME);
        int xIndex = resultSet.findColumn(DbContract.AccidentEntry.X);
        int yIndex = resultSet.findColumn(DbContract.AccidentEntry.Y);
        int numberIndex = resultSet.findColumn(DbContract.AccidentEntry.NUMBER);

        Accident<ID, VehicleTN> accident;
        int tmpVehicleTN;
        Table<Integer, Integer, VehicleTN> vehicleTNs = HashBasedTable.create();
        VehicleTN vehicleTN;
        Map<Integer, KR_ID> kr_ids = new HashMap<>();
        Table<Integer, Integer, MR_ID> tn_ids = HashBasedTable.create();
        double x, y;
        ID routeID;
        int tmpRouteID, tmpTypeID, tmpNumber = -1;
        while (resultSet.next()) {
            if (tmpNumber < (tmpNumber = resultSet.getInt(numberIndex))) {
                accidents.add(cluster = new ArrayList<>());
            }
            tmpTypeID = resultSet.getInt(transTypeIndex);
            tmpVehicleTN = resultSet.getInt(vehicleIndex);
            vehicleTN = vehicleTNs.get(tmpTypeID, tmpVehicleTN);

            if (vehicleTN == null) {
                vehicleTNs.put(tmpTypeID, tmpVehicleTN,
                               vehicleTN = new VehicleTN(tmpVehicleTN, tmpTypeID));
            }
            x = resultSet.getDouble(xIndex);
            y = resultSet.getDouble(yIndex);
            tmpRouteID = resultSet.getInt(kr_idIndex);
            if (tmpRouteID != 0) {
                routeID = kr_ids.get(tmpRouteID);
                if (routeID == null) {
                    kr_ids.put(tmpRouteID, (KR_ID) (routeID = new KR_ID(tmpRouteID)));
                }
            } else {
                tmpRouteID = resultSet.getInt(route_tnIndex);
                routeID = tn_ids.get(tmpTypeID, tmpRouteID);
                if (routeID == null) {
                    tn_ids.put(tmpTypeID, tmpRouteID,
                               (MR_ID) (routeID = new MR_ID(tmpRouteID, tmpTypeID)));
                }
            }

            accident = new Accident<>(
                    new TransportPosition<>(vehicleTN, resultSet.getInt(startIndex), x, y),
                    new TransportPosition<>(vehicleTN, resultSet.getInt(endIndex), x, y),
                    routeID);
            cluster.add(accident);
        }
        return accidents;
    }

    public static Map<CP_ID, List<ScheduleTime>> resultSetToRouteSchedule(ResultSet resultSet)
            throws SQLException {
        Map<CP_ID, List<ScheduleTime>> result = new HashMap<>();
        int cpIndex = resultSet.findColumn(DbContract.ScheduleEntry.CONTROL_POINT_ID);
        int timeIndex = resultSet.findColumn(DbContract.ScheduleEntry.TIME);
        int transIndex = resultSet.findColumn(DbContract.ScheduleEntry.KT_ID);
        int directionIndex = resultSet.findColumn(DbContract.ScheduleEntry.DIRECTION_TN);

        CP_ID cp_id = new CP_ID();
        int tmpCP_ID;
        List<ScheduleTime> times = null;
        while (resultSet.next()) {
            tmpCP_ID = resultSet.getInt(cpIndex);
            if (cp_id.id != tmpCP_ID) {
                if (cp_id.id != 0) {
                    cp_id = new CP_ID();
                }
                cp_id.id = tmpCP_ID;
                result.put(cp_id, times = new ArrayList<>());
            }
            times.add(new ScheduleTime(resultSet.getInt(transIndex), resultSet.getInt(timeIndex),
                                       resultSet.getString(directionIndex)));
        }
        return result;
    }

    public static Table<KR_ID, CP_ID, ControlPoint> resultSetToControlPointsTable(ResultSet resultSet,
                                                                                  boolean copyCoords)
            throws SQLException {
        Table<KR_ID, CP_ID, ControlPoint> result = HashBasedTable.create();
        ControlPoint controlPoint;

        int idIndex = resultSet.findColumn(ControlPointEntry.CP_ID);
        int nameIndex = resultSet.findColumn(ControlPointEntry.NAME);
        int stopIndex =
                copyCoords ? resultSet.findColumn(StopEntry.KS_ID) :
                resultSet.findColumn(ControlPointEntry.KS_ID);
        int routeIndex = resultSet.findColumn(ControlPointEntry.KR_ID);
        int xIndex = 0;
        int yIndex = 0;
        int distIndex = 0;
        if (copyCoords) {
            xIndex = resultSet.findColumn(StopEntry.X);
            yIndex = resultSet.findColumn(StopEntry.Y);
            distIndex = resultSet.findColumn(CircuitEntry.DIST);
        }

        if (copyCoords) {
            while (resultSet.next()) {
                controlPoint = new ControlPoint(
                        resultSet.getInt(idIndex),
                        resultSet.getInt(stopIndex),
                        resultSet.getInt(routeIndex),
                        resultSet.getString(nameIndex),
                        resultSet.getDouble(xIndex),
                        resultSet.getDouble(yIndex),
                        resultSet.getDouble(distIndex)
                );

                result.put(controlPoint.kr_id, controlPoint.cp_id, controlPoint);
            }
        } else {
            while (resultSet.next()) {
                controlPoint = new ControlPoint(
                        resultSet.getInt(idIndex),
                        resultSet.getInt(stopIndex),
                        resultSet.getInt(routeIndex),
                        resultSet.getString(nameIndex)
                );

                result.put(controlPoint.kr_id, controlPoint.cp_id, controlPoint);
            }
        }

        return result;
    }

    public static Table<CP_ID, KR_ID, Integer> resultSetToControlPointsOrderedTable(ResultSet resultSet)
            throws SQLException {
        Table<CP_ID, KR_ID, Integer> result = HashBasedTable.create();

        int numberIndex = resultSet.findColumn(CircuitEntry.NUMBER);
        int routeIndex = resultSet.findColumn(ControlPointEntry.KR_ID);
        int cpIndex = resultSet.findColumn(ControlPointEntry.CP_ID);
        int number, lastRoute = -1, tmpRoute;
        KR_ID kr_id = null;
        CP_ID cp_id;

        while (resultSet.next()) {
            tmpRoute = resultSet.getInt(routeIndex);
            if (tmpRoute != lastRoute) {
                lastRoute = tmpRoute;
                kr_id = new KR_ID(lastRoute);
            }
            number = resultSet.getInt(numberIndex);
            cp_id = new CP_ID(resultSet.getInt(cpIndex));

            result.put(cp_id, kr_id, number);
        }


        return result;
    }

    public static Map<KR_ID, List<ControlPoint>> resultSetToControlPointsMap(ResultSet resultSet,
                                                                             boolean copyCoords)
            throws SQLException {
        Map<KR_ID, List<ControlPoint>> result = new HashMap<>();
        ControlPoint controlPoint;

        int idIndex = resultSet.findColumn(ControlPointEntry.CP_ID);
        int nameIndex = resultSet.findColumn(ControlPointEntry.NAME);
        int stopIndex =
                copyCoords ? resultSet.findColumn(StopEntry.KS_ID) :
                resultSet.findColumn(ControlPointEntry.KS_ID);
        int routeIndex = resultSet.findColumn(ControlPointEntry.KR_ID);
        int xIndex = 0;
        int yIndex = 0;
        int distIndex = 0;
        if (copyCoords) {
            xIndex = resultSet.findColumn(StopEntry.X);
            yIndex = resultSet.findColumn(StopEntry.Y);
            distIndex = resultSet.findColumn(CircuitEntry.DIST);
        }

        KR_ID kr_id = new KR_ID();
        int tmpID;
        List<ControlPoint> controlPoints = null;
        if (copyCoords) {
            while (resultSet.next()) {
                controlPoint = new ControlPoint(
                        resultSet.getInt(idIndex),
                        resultSet.getInt(stopIndex),
                        tmpID = resultSet.getInt(routeIndex),
                        resultSet.getString(nameIndex),
                        resultSet.getDouble(xIndex),
                        resultSet.getDouble(yIndex),
                        resultSet.getDouble(distIndex)
                );

                if (tmpID != kr_id.id) {
                    kr_id = new KR_ID(tmpID);
                    controlPoints = result.get(kr_id);
                    if ((controlPoints == null)) {
                        result.put(kr_id, controlPoints = new ArrayList<>());
                    }
                }

                controlPoints.add(controlPoint);
            }
        } else {
            while (resultSet.next()) {
                controlPoint = new ControlPoint(
                        resultSet.getInt(idIndex),
                        resultSet.getInt(stopIndex),
                        tmpID = resultSet.getInt(routeIndex),
                        resultSet.getString(nameIndex)
                );

                if (tmpID != kr_id.id) {
                    kr_id = new KR_ID(tmpID);
                    controlPoints = result.get(kr_id);
                    if ((controlPoints == null)) {
                        result.put(kr_id, controlPoints = new ArrayList<>());
                    }
                }

                controlPoints.add(controlPoint);
            }
        }

        return result;
    }

    public static List<ControlPoint> resultSetToControlPointsList(ResultSet resultSet, KR_ID kr_id,
                                                                  boolean copyCoords)
            throws SQLException {
        List<ControlPoint> result = new ArrayList<>();
        ControlPoint controlPoint;

        int idIndex = resultSet.findColumn(ControlPointEntry.CP_ID);
        int nameIndex = resultSet.findColumn(ControlPointEntry.NAME);
        int stopIndex =
                copyCoords ? resultSet.findColumn(StopEntry.TABLE_NAME + "." + StopEntry.KS_ID) :
                resultSet.findColumn(ControlPointEntry.KS_ID);
        int xIndex = 0;
        int yIndex = 0;
        int distIndex = 0;
        if (copyCoords) {
            xIndex = resultSet.findColumn(StopEntry.X);
            yIndex = resultSet.findColumn(StopEntry.Y);
            distIndex = resultSet.findColumn(CircuitEntry.DIST);
        }

        if (copyCoords) {
            while (resultSet.next()) {
                controlPoint = new ControlPoint(
                        resultSet.getInt(idIndex),
                        resultSet.getInt(stopIndex),
                        kr_id,
                        resultSet.getString(nameIndex),
                        resultSet.getDouble(xIndex),
                        resultSet.getDouble(yIndex),
                        resultSet.getDouble(distIndex)
                );


                result.add(controlPoint);
            }
        } else {
            while (resultSet.next()) {
                controlPoint = new ControlPoint(
                        resultSet.getInt(idIndex),
                        resultSet.getInt(stopIndex),
                        kr_id,
                        resultSet.getString(nameIndex)
                );


                result.add(controlPoint);
            }
        }

        return result;
    }

    public static Map<KR_ID, Route> resultSetToRoutes(ResultSet resultSet) throws SQLException {
        Map<KR_ID, Route> result = new HashMap<>();
        Route route;

        int idIndex = resultSet.findColumn(RouteEntry.KR_ID);
        int transTypeIndex = resultSet.findColumn(RouteEntry.TRANSPORT_TYPE_ID);
        int affiliationIndex = resultSet.findColumn(RouteEntry.AFFILIATION_ID);
        int directionIndex = resultSet.findColumn(RouteEntry.DIRECTION);
        int geoportalIndex = resultSet.findColumn(RouteEntry.GEOPORTAL_ID);
        int layerIndex = resultSet.findColumn(RouteEntry.LAYER_NAME);
        int numberIndex = resultSet.findColumn(RouteEntry.NUMBER);

        while (resultSet.next()) {
            route = new Route(
                    resultSet.getInt(transTypeIndex),
                    resultSet.getInt(affiliationIndex),
                    resultSet.getString(directionIndex),
                    resultSet.getInt(geoportalIndex),
                    resultSet.getString(layerIndex),
                    new KR_ID(resultSet.getInt(idIndex)),
                    resultSet.getString(numberIndex)
            );

            result.put(route.kr_id, route);
        }

        return result;
    }

    public static Route resultSetToRoute(ResultSet resultSet) throws SQLException {
        int idIndex = resultSet.findColumn(RouteEntry.KR_ID);
        int transTypeIndex = resultSet.findColumn(RouteEntry.TRANSPORT_TYPE_ID);
        int affiliationIndex = resultSet.findColumn(RouteEntry.AFFILIATION_ID);
        int directionIndex = resultSet.findColumn(RouteEntry.DIRECTION);
        int geoportalIndex = resultSet.findColumn(RouteEntry.GEOPORTAL_ID);
        int layerIndex = resultSet.findColumn(RouteEntry.LAYER_NAME);
        int numberIndex = resultSet.findColumn(RouteEntry.NUMBER);

        if (resultSet.next()) {
            return new Route(
                    resultSet.getInt(transTypeIndex),
                    resultSet.getInt(affiliationIndex),
                    resultSet.getString(directionIndex),
                    resultSet.getInt(geoportalIndex),
                    resultSet.getString(layerIndex),
                    new KR_ID(resultSet.getInt(idIndex)),
                    resultSet.getString(numberIndex)
            );

        }
        return null;
    }

    public static List<Route> resultSetToRouteDirections(ResultSet resultSet) throws SQLException {
        List<Route> routes = new ArrayList<>();
        int idIndex = resultSet.findColumn(RouteEntry.KR_ID);
        int transTypeIndex = resultSet.findColumn(RouteEntry.TRANSPORT_TYPE_ID);
        int affiliationIndex = resultSet.findColumn(RouteEntry.AFFILIATION_ID);
        int directionIndex = resultSet.findColumn(RouteEntry.DIRECTION);
        int geoportalIndex = resultSet.findColumn(RouteEntry.GEOPORTAL_ID);
        int layerIndex = resultSet.findColumn(RouteEntry.LAYER_NAME);
        int numberIndex = resultSet.findColumn(RouteEntry.NUMBER);

        if (resultSet.next()) {
            routes.add(new Route(
                    resultSet.getInt(transTypeIndex),
                    resultSet.getInt(affiliationIndex),
                    resultSet.getString(directionIndex),
                    resultSet.getInt(geoportalIndex),
                    resultSet.getString(layerIndex),
                    new KR_ID(resultSet.getInt(idIndex)),
                    resultSet.getString(numberIndex)
            ));
        }
        return routes;
    }

    public static Map<KS_ID, Stop> resultSetToStops(ResultSet resultSet) throws SQLException {
        Map<KS_ID, Stop> result = new HashMap<>();
        Stop stop;

        int KS_IDindex = resultSet.findColumn(StopEntry.KS_ID);
        int geoportalIDindex = resultSet.findColumn(StopEntry.GEO_ID);
        int titleIndex = resultSet.findColumn(StopEntry.TITLE);
        int streetIndex = resultSet.findColumn(StopEntry.ADJACENT_STREET);
        int dirIndex = resultSet.findColumn(StopEntry.DIRECTION);
        int munIndex = resultSet.findColumn(StopEntry.BUSES_MUNICIPAL);
        int commIndex = resultSet.findColumn(StopEntry.BUSES_COMMERCIAL);
        int prigIndex = resultSet.findColumn(StopEntry.BUSES_PRIGOROD);
        int seasonIndex = resultSet.findColumn(StopEntry.BUSES_SEASON);
        int specIndex = resultSet.findColumn(StopEntry.BUSES_SPECIAL);
        int tramsIndex = resultSet.findColumn(StopEntry.TRAMS);
        int trollsIndex = resultSet.findColumn(StopEntry.TROLLEYBUSES);
        int latIndex = resultSet.findColumn(StopEntry.LATITUDE);
        int lngIndex = resultSet.findColumn(StopEntry.LONGITUDE);
        int xIndex = resultSet.findColumn(StopEntry.X);
        int yIndex = resultSet.findColumn(StopEntry.Y);


        while (resultSet.next()) {
            stop = new Stop(
                    new KS_ID(resultSet.getInt(KS_IDindex)),
                    resultSet.getInt(geoportalIDindex),
                    resultSet.getString(titleIndex),
                    resultSet.getString(streetIndex),
                    resultSet.getString(dirIndex),
                    resultSet.getString(munIndex),
                    resultSet.getString(commIndex),
                    resultSet.getString(prigIndex),
                    resultSet.getString(seasonIndex),
                    resultSet.getString(specIndex),
                    resultSet.getString(tramsIndex),
                    resultSet.getString(trollsIndex),
                    resultSet.getDouble(latIndex),
                    resultSet.getDouble(lngIndex),
                    resultSet.getDouble(xIndex),
                    resultSet.getDouble(yIndex)
            );

            result.put(stop.ks_id, stop);
        }
        return result;
    }

    public static Stop resultSetToStop(ResultSet resultSet) throws SQLException {

        int KS_IDindex = resultSet.findColumn(StopEntry.KS_ID);
        int geoportalIDindex = resultSet.findColumn(StopEntry.GEO_ID);
        int titleIndex = resultSet.findColumn(StopEntry.TITLE);
        int streetIndex = resultSet.findColumn(StopEntry.ADJACENT_STREET);
        int dirIndex = resultSet.findColumn(StopEntry.DIRECTION);
        int munIndex = resultSet.findColumn(StopEntry.BUSES_MUNICIPAL);
        int commIndex = resultSet.findColumn(StopEntry.BUSES_COMMERCIAL);
        int prigIndex = resultSet.findColumn(StopEntry.BUSES_PRIGOROD);
        int seasonIndex = resultSet.findColumn(StopEntry.BUSES_SEASON);
        int specIndex = resultSet.findColumn(StopEntry.BUSES_SPECIAL);
        int tramsIndex = resultSet.findColumn(StopEntry.TRAMS);
        int trollsIndex = resultSet.findColumn(StopEntry.TROLLEYBUSES);
        int latIndex = resultSet.findColumn(StopEntry.LATITUDE);
        int lngIndex = resultSet.findColumn(StopEntry.LONGITUDE);
        int xIndex = resultSet.findColumn(StopEntry.X);
        int yIndex = resultSet.findColumn(StopEntry.Y);

        if (resultSet.next()) {
            return new Stop(
                    new KS_ID(resultSet.getInt(KS_IDindex)),
                    resultSet.getInt(geoportalIDindex),
                    resultSet.getString(titleIndex),
                    resultSet.getString(streetIndex),
                    resultSet.getString(dirIndex),
                    resultSet.getString(munIndex),
                    resultSet.getString(commIndex),
                    resultSet.getString(prigIndex),
                    resultSet.getString(seasonIndex),
                    resultSet.getString(specIndex),
                    resultSet.getString(tramsIndex),
                    resultSet.getString(trollsIndex),
                    resultSet.getDouble(latIndex),
                    resultSet.getDouble(lngIndex),
                    resultSet.getDouble(xIndex),
                    resultSet.getDouble(yIndex)
            );

        }
        return null;
    }

    static void fillMonitoringStmt(PreparedStatement statement, PositionRelToRoutes position,
                                   MR_ID mr_id,
                                   VehicleTN vehicleTN,
                                   int transType) throws SQLException {
        statement.setInt(1, mr_id.id);
        statement.setInt(2, position.routeID != null ? position.routeID.id : 0);
        statement.setInt(3, vehicleTN.id);
        statement.setInt(4, transType);
        statement.setInt(5, position.nextStopID != null ? position.nextStopID.id : 0);
        statement.setDouble(6, position.nextStopDist);
        statement.setDouble(7, position.lat);
        statement.setDouble(8, position.lng);
        statement.setDouble(9, position.x);
        statement.setDouble(10, position.y);
        statement.setInt(11, position.timestamp);
    }

    static void fillStopStmt(PreparedStatement statement, Stop stop)
            throws SQLException {
        statement.setInt(1, stop.ks_id.id);
        statement.setString(2, stop.title);
        statement.setString(3, stop.adjacentStreet);
        statement.setString(4, stop.direction);
        statement.setString(5, stop.busesMunicipal);
        statement.setString(6, stop.busesCommercial);
        statement.setString(7, stop.busesPrigorod);
        statement.setString(8, stop.busesSeason);
        statement.setString(9, stop.busesSpecial);
        statement.setString(10, stop.trams);
        statement.setString(11, stop.trolleybuses);
        statement.setDouble(12, stop.lat);
        statement.setDouble(13, stop.lng);
        statement.setInt(14, stop.geoportalID);
        statement.setDouble(15, stop.x);
        statement.setDouble(16, stop.y);
    }

    static void fillCircuitStmt(PreparedStatement statement, KR_ID kr_id, KS_ID ks_id, int number,
                                PeriodID periodID)
            throws SQLException {
        statement.setInt(1, kr_id.id);
        statement.setInt(2, ks_id.id);
        statement.setInt(3, number);
        statement.setInt(4, periodID.id);
        statement.setInt(5, 0);
    }

    static void fillRouteStmt(PreparedStatement statement, Route route)
            throws SQLException {
        statement.setInt(1, route.kr_id.id);
        ByteArrayInputStream in = new ByteArrayInputStream(route.getNumber().getBytes());
        statement.setUnicodeStream(2, in, in.available());
        statement.setInt(3, route.affiliationID);
        statement.setInt(4, route.transportTypeID);
        in = new ByteArrayInputStream(route.direction.getBytes());
        statement.setUnicodeStream(5, in, in.available());
        statement.setInt(6, route.geoportalId);
        statement.setString(7, route.layerName);
    }

    static void fillMonitoringStmt(PreparedStatement statement,
                                   MonitoringLogEntry position)
            throws SQLException {
        statement.setInt(1, position.routeTN);
        statement.setInt(2, position.routeID != null ? position.routeID.id : 0);
        statement.setInt(3, position.vehicleID.id);
        statement.setInt(4, position.vehicleID.transType);
        statement.setInt(5, position.nextStopID != null ? position.nextStopID.id : 0);
        statement.setDouble(6, position.nextStopDist);
//            statement.setInt(6, position.status);
        statement.setDouble(7, position.lat);
        statement.setDouble(8, position.lng);
        statement.setDouble(9, position.x);
        statement.setDouble(10, position.y);
        statement.setInt(11, position.timestamp);
    }

    static void fillScheduleStmt(PreparedStatement statement, ScheduleRecord race) throws SQLException {
        statement.setInt(1, race.kt_id.id);
        statement.setInt(2, race.kr_id.id);
        statement.setInt(3, race.raceNumber);
        statement.setInt(4, race.controlPointId);
        statement.setInt(5, race.time);
//            statement.setInt(6, position.status);
        statement.setString(6, race.direction);
        statement.setString(7, race.directionTN);
        statement.setInt(8, race.routeIdTN);
        statement.setInt(9, race.transportTypeIdTN);
    }
}
