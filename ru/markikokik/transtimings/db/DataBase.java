package ru.markikokik.transtimings.db;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipFile;

import ru.markikokik.transtimings.Printer;
import ru.markikokik.transtimings.Utils;
import ru.markikokik.transtimings.XmlParser;
import ru.markikokik.transtimings.db.connectors.Connector;
import ru.markikokik.transtimings.db.errorcodes.ErrorCodes;
import ru.markikokik.transtimings.entities.Accident;
import ru.markikokik.transtimings.entities.ControlPoint;
import ru.markikokik.transtimings.entities.ControlPointBinding;
import ru.markikokik.transtimings.entities.Deviation;
import ru.markikokik.transtimings.entities.Interval;
import ru.markikokik.transtimings.entities.MonitoringLogEntry;
import ru.markikokik.transtimings.entities.Period;
import ru.markikokik.transtimings.entities.PositionRelToRoutes;
import ru.markikokik.transtimings.entities.Race;
import ru.markikokik.transtimings.entities.RaceTimings;
import ru.markikokik.transtimings.entities.Route;
import ru.markikokik.transtimings.entities.RouteBinding;
import ru.markikokik.transtimings.entities.RouteGeoData;
import ru.markikokik.transtimings.entities.RouteInfo;
import ru.markikokik.transtimings.entities.ScheduleRecord;
import ru.markikokik.transtimings.entities.ScheduleTime;
import ru.markikokik.transtimings.entities.Stop;
import ru.markikokik.transtimings.entities.StopDistance;
import ru.markikokik.transtimings.entities.TransportPosition;
import ru.markikokik.transtimings.entities.Vehicle;
import ru.markikokik.transtimings.entities.ids.CP_ID;
import ru.markikokik.transtimings.entities.ids.ID;
import ru.markikokik.transtimings.entities.ids.KR_ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;
import ru.markikokik.transtimings.entities.ids.KT_ID;
import ru.markikokik.transtimings.entities.ids.MR_ID;
import ru.markikokik.transtimings.entities.ids.PeriodID;
import ru.markikokik.transtimings.entities.ids.RaceID;
import ru.markikokik.transtimings.entities.ids.RouteIDs;
import ru.markikokik.transtimings.entities.ids.TransTypeID;
import ru.markikokik.transtimings.entities.ids.VehicleTN;

/**
 * Created by markikokik on 25.03.16.
 */
public class DataBase implements DbContract, AutoCloseable {
    private Connection c;
    private ErrorCodes errorCodes;
    private static final Period ALL_TIME_PERIOD = new Period(0, Utils.allTime[0], Utils.allTime[1]);

    public Period getRoutePeriodForTime(KR_ID kr_id, int time) throws SQLException {
        try (PreparedStatement statement = c.prepareStatement(SELECT_SUITABLE_ROUTE_PERIOD_SQL)) {
            statement.setInt(1, kr_id.id);
            statement.setInt(2, time);
            statement.setInt(3, time);
            try (ResultSet resultSet = statement.executeQuery()) {
                int idIndex = resultSet.findColumn(PeriodEntry.ID);
                int startIndex = resultSet.findColumn(PeriodEntry.START);
                int endIndex = resultSet.findColumn(PeriodEntry.END);
                while (resultSet.next()) {
                    return new Period(resultSet.getInt(idIndex),
                                      resultSet.getInt(startIndex),
                                      resultSet.getInt(endIndex)
                    );
                }
            }
        }
        return ALL_TIME_PERIOD;
    }

    public List<Period> getAllRoutePeriods(KR_ID kr_id) throws SQLException {
        List<Period> periods = new ArrayList<>();
        try (PreparedStatement statement = c.prepareStatement(SELECT_ALL_ROUTE_PERIODS_SQL)) {
            statement.setInt(1, kr_id.id);
            try (ResultSet resultSet = statement.executeQuery()) {
                int idIndex = resultSet.findColumn(PeriodEntry.ID);
                int startIndex = resultSet.findColumn(PeriodEntry.START);
                int endIndex = resultSet.findColumn(PeriodEntry.END);
                while (resultSet.next()) {
                    periods.add(new Period(resultSet.getInt(idIndex),
                                           resultSet.getInt(startIndex),
                                           resultSet.getInt(endIndex)
                    ));
                }
            }
        }
        return periods;
    }

    public void createAccidentsTable() throws SQLException {
        Statement statement = c.createStatement();
        statement.execute(AccidentEntry.CREATE_ACCIDENTS_SQL);
        statement.close();
        c.commit();
    }

    public void createStaysTable() throws SQLException {
        Statement statement = c.createStatement();
        statement.execute(StationStayEntry.CREATE_STAYS_SQL);
        statement.close();
        c.commit();
    }

    public void createDeviationsTable() throws SQLException {
        Statement statement = c.createStatement();
        statement.execute(DeviationEntry.CREATE_DEVIATIONS_SQL);
        statement.close();
        c.commit();
    }

    public void dropDeviations() throws SQLException {
        Statement statement = c.createStatement();
        statement.execute(DeviationEntry.DROP_DEVIATIONS_SQL);
        statement.close();
        c.commit();
    }

    public void dropAccidents() throws SQLException {
        Statement statement = c.createStatement();
        statement.execute(AccidentEntry.DROP_ACCIDENTS_SQL);
        statement.close();
        c.commit();
    }

    public void insertDistrib(Table<MR_ID, VehicleTN, List<RaceTimings>> distrib) throws SQLException {
        PreparedStatement statement = c.prepareStatement(DistribEntry.INSERT_DISTRIB_SQL);
        int i = 1;
        for (Table.Cell<MR_ID, VehicleTN, List<RaceTimings>> cell : distrib.cellSet()) {
            try {
                statement.setInt(1, cell.getColumnKey().id);
                statement.setInt(2, cell.getColumnKey().transType);
                statement.setInt(3, cell.getRowKey().id);
            } catch (SQLException e) {
                statement = c.prepareStatement(DistribEntry.INSERT_DISTRIB_SQL);
                statement.setInt(1, cell.getColumnKey().id);
                statement.setInt(2, cell.getColumnKey().transType);
                statement.setInt(3, cell.getRowKey().id);
            }
            for (RaceTimings raceTimings : cell.getValue()) {
                try {
                    statement.setInt(4, raceTimings.startTime);
                    statement.setInt(5, raceTimings.endTime);
                } catch (SQLException e) {
                    statement = c.prepareStatement(DistribEntry.INSERT_DISTRIB_SQL);
                    statement.setInt(1, cell.getColumnKey().id);
                    statement.setInt(2, cell.getColumnKey().transType);
                    statement.setInt(3, cell.getRowKey().id);
                    statement.setInt(4, raceTimings.startTime);
                    statement.setInt(5, raceTimings.endTime);
                }
                if (i % 100 == 0) {
                    statement.executeBatch();
                } else {
                    statement.addBatch();
                }
                i++;
            }
        }
        statement.close();
        c.commit();
    }

    public void createDistribTable() throws SQLException {
        Statement statement = c.createStatement();
        statement.execute(DistribEntry.CREATE_DISTRIB_SQL);
        statement.close();
        c.commit();
    }

    public void insertDeviations(KR_ID kr_id, Map<CP_ID, List<Deviation>> deviations)
            throws SQLException {
        PreparedStatement statement = c.prepareStatement(DeviationEntry.INSERT_DEVIATION_SQL);
        CP_ID cp_id;
        statement.setInt(1, kr_id.id);
        for (Map.Entry<CP_ID, List<Deviation>> cpDeviationsEntry : deviations.entrySet()) {
            cp_id = cpDeviationsEntry.getKey();
            try {
                statement.setInt(2, cp_id.id);
            } catch (SQLException e) {
                statement = c.prepareStatement(DeviationEntry.INSERT_DEVIATION_SQL);
                statement.setInt(1, kr_id.id);
                statement.setInt(2, cp_id.id);
            }
            for (Deviation deviation : cpDeviationsEntry.getValue()) {
                try {
                    statement.setString(3, deviation.direction);
                    statement.setInt(4, deviation.planTime);
                    statement.setInt(5, deviation.deviation);
                    statement.setInt(6, deviation.planVehicle != null ? deviation.planVehicle.id : 0);
                    statement.setInt(7, deviation.realVehicle != null ? deviation.realVehicle.id : 0);
                } catch (SQLException e) {
                    statement = c.prepareStatement(DeviationEntry.INSERT_DEVIATION_SQL);
                    statement.setInt(1, kr_id.id);
                    statement.setInt(2, cp_id.id);
                    statement.setString(3, deviation.direction);
                    statement.setInt(4, deviation.planTime);
                    statement.setInt(5, deviation.deviation);
                    statement.setInt(6, deviation.planVehicle != null ? deviation.planVehicle.id : 0);
                    statement.setInt(7, deviation.realVehicle != null ? deviation.realVehicle.id : 0);
                }
                try {
                    statement.execute();
                } catch (SQLException e) {
                    System.out.print("double ");
                    System.out.print(kr_id);
                    System.out.print(" ");
                    System.out.print(cp_id);
                    System.out.print(" ");
                    Printer.printTimeHM(deviation.planTime);
                    System.out.println();
                }
            }
        }
        statement.close();
        c.commit();
    }

    /**
     * @param kr_id
     * @return all related kr_ids and tn_ids
     * @throws SQLException
     */
    public RouteIDs getRelatedRouteIDs(KR_ID kr_id, int time) throws SQLException {
        RouteIDs result = new RouteIDs();
        try (PreparedStatement statement = c.prepareStatement(SELECT_RELATED_ROUTE_IDS_BY_KR_ID_SQL)) {
            statement.setInt(1, kr_id.id);
            statement.setInt(2, time);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    result.transType =
                            resultSet.getInt(resultSet.findColumn("r." + RouteBindEntry.TRANS_TYPE));
                    int krIndex = resultSet.findColumn(RouteBindEntry.KR_ID);
                    int tnIndex = resultSet.findColumn("r." + RouteBindEntry.ROUTE_TN);

                    int lastKR = -1, lastTN = -1, tmpKR, tmpTN;
                    do {
                        tmpKR = resultSet.getInt(krIndex);
                        tmpTN = resultSet.getInt(tnIndex);
                        if (tmpKR != lastKR) {
                            result.kr_ids.add(new KR_ID(lastKR = tmpKR));
                        }
                        if (tmpTN != lastTN) {
                            result.tn_ids.add(new MR_ID(lastTN = tmpTN));
                        }
                    } while (resultSet.next());
                }
            }
        }
        return result;
    }

    /**
     * @param mr_id
     * @return all related kr_ids and tn_ids
     * @throws SQLException
     */
    public RouteIDs getRelatedRouteIDs(MR_ID mr_id, int time) throws SQLException {
        RouteIDs result = new RouteIDs();
        try (PreparedStatement statement = c.prepareStatement(SELECT_RELATED_ROUTE_IDS_BY_TN_ID_SQL)) {
            statement.setInt(1, mr_id.id);
            statement.setInt(2, mr_id.transType);
            statement.setInt(3, time);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    result.transType =
                            resultSet.getInt(resultSet.findColumn("r." + RouteBindEntry.TRANS_TYPE));
                    int krIndex = resultSet.findColumn(RouteBindEntry.KR_ID);
                    int tnIndex = resultSet.findColumn("r." + RouteBindEntry.ROUTE_TN);

                    int lastKR = -1, lastTN = -1, tmpKR, tmpTN;
                    do {
                        tmpKR = resultSet.getInt(krIndex);
                        tmpTN = resultSet.getInt(tnIndex);
                        if (tmpKR != lastKR) {
                            result.kr_ids.add(new KR_ID(lastKR = tmpKR));
                        }
                        if (tmpTN != lastTN) {
                            result.tn_ids.add(new MR_ID(lastTN = tmpTN));
                        }
                    } while (resultSet.next());
                }
            }
        }
        return result;
    }

    /**
     * @param ids
     * @param periodID
     * @return geometries and stop mileages for all route's directions
     * @throws SQLException
     */
    public RouteInfo getRelatedRoutesInfo(RouteIDs ids, PeriodID periodID) throws SQLException {
        RouteInfo result = new RouteInfo(ids);
        int i = 0;
        result.geometries = new double[ids.kr_ids.size()][];
        result.stopMileages = new StopDistance[result.geometries.length][];
        for (Iterator<KR_ID> iterator = result.kr_ids.iterator(); iterator.hasNext(); i++) {
            KR_ID kr_id = iterator.next();
            result.geometries[i] = getRouteGeometry(kr_id, periodID);
            result.stopMileages[i] = getRouteCircuitWithDistsList(kr_id, periodID);
        }
        return result;
    }

    /**
     * @param kr_id
     * @param periodID
     * @return geometries and stop mileages for all route's directions
     * @throws SQLException
     */
    public RouteInfo getRelatedRoutesInfo(KR_ID kr_id, PeriodID periodID, int time) throws SQLException {
        RouteIDs ids = getRelatedRouteIDs(kr_id, time);
        return getRelatedRoutesInfo(ids, periodID);
    }

    public void removeVehicleDuplicates() throws SQLException {
        List<Vehicle> vehicles = null;
        Map<VehicleTN, List<Vehicle>> result = new HashMap<>();
        try (Statement statement = c.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SELECT_DOUBLES_IN_TRANSPORT_SQL)) {
                int tnIndex = resultSet.findColumn(VehicleEntry.VEHICLE_TN),
                        ktIdIndex = resultSet.findColumn(VehicleEntry.KT_ID),
                        transTypeIndex = resultSet.findColumn(VehicleEntry.TRANS_TYPE),
                        boardNumberIndex = resultSet.findColumn(VehicleEntry.BOARD_NUMBER),
                        tmpType;

                int tmpTN_id;
                VehicleTN tn_id = new VehicleTN(0, 0);

                while (resultSet.next()) {
                    tmpTN_id = resultSet.getInt(tnIndex);
                    tmpType = resultSet.getInt(transTypeIndex);
                    if (tmpTN_id != tn_id.id || tmpType != tn_id.transType) {
                        tn_id = new VehicleTN(tmpTN_id, tmpType);
                        result.put(tn_id, vehicles = new ArrayList<>(2));
                    }
                    vehicles.add(new Vehicle(
                            tmpTN_id,
                            new KT_ID(resultSet.getInt(ktIdIndex)),
                            tmpType,
                            resultSet.getString(boardNumberIndex)
                    ));
                }
            }
        }


        PreparedStatement statement =
                c.prepareStatement(ScheduleEntry.UPDATE_SCHEDULES_WITH_NEW_KT_IDS_SQL);
//        PreparedStatement statement2 = c.prepareStatement(UPDATE_REAL_SCHEDULE_WITH_NEW_KT_IDS_SQL);
        PreparedStatement statement2 = c.prepareStatement(VehicleEntry.DELETE_DOUBLES_IN_TRANSPORT);
        for (Map.Entry<VehicleTN, List<Vehicle>> transport : result.entrySet()) {
            vehicles = transport.getValue();
            try {
                statement.setInt(1, vehicles.get(0).kt_id.id);
                statement.setInt(2, vehicles.get(1).kt_id.id);
            } catch (SQLException e) {
                statement = c.prepareStatement(ScheduleEntry.UPDATE_SCHEDULES_WITH_NEW_KT_IDS_SQL);
                statement.setInt(1, vehicles.get(0).kt_id.id);
                statement.setInt(2, vehicles.get(1).kt_id.id);
            }
            statement.executeUpdate();

//            try {
//                statement2.setInt(1, vehicles.get(0).vehicleID.id);
//                statement2.setInt(2, vehicles.get(1).vehicleID.id);
//            } catch (SQLException e) {
//                statement2 = c.prepareStatement(UPDATE_REAL_SCHEDULE_WITH_NEW_KT_IDS_SQL);
//                statement2.setInt(1, vehicles.get(0).vehicleID.id);
//                statement2.setInt(2, vehicles.get(1).vehicleID.id);
//            }

            try {
                statement2.setInt(1, vehicles.get(1).kt_id.id);
            } catch (SQLException e) {
                statement2 = c.prepareStatement(VehicleEntry.DELETE_DOUBLES_IN_TRANSPORT);
                statement2.setInt(1, vehicles.get(1).kt_id.id);
            }
            statement2.executeUpdate();

        }
        c.commit();
        statement.close();
        statement2.close();

        c.commit();
    }

    public Vehicle getVehicle(KT_ID kt_id) throws SQLException {
        Vehicle result;
        try (PreparedStatement statement = c.prepareStatement(SELECT_TRANSPORT_BY_KT_ID_SQL)) {
            statement.setInt(1, kt_id.id);
            try (ResultSet resultSet = statement.executeQuery()) {
                result = DbUtils.resultSetToVehicle(resultSet);
            }
        }
        return result;
    }

    public void insertAccidents(
            Map<VehicleTN, ? extends List<? extends Accident<? extends ID, ? extends ID>>> accidents
    )
            throws SQLException {
        List<? extends Accident<? extends ID, ? extends ID>> vehicleAccidents;
        PreparedStatement statement = c.prepareStatement(AccidentEntry.INSERT_ACCIDENT_SQL);
        for (Map.Entry<VehicleTN, ? extends List<? extends Accident<? extends ID, ? extends ID>>> vehicleAccidentsEntry : accidents
                .entrySet()) {
            vehicleAccidents = vehicleAccidentsEntry.getValue();
            statement.setInt(1, 0);
            statement.setInt(4, vehicleAccidentsEntry.getKey().id);
            statement.setInt(5, vehicleAccidentsEntry.getKey().transType);
            for (Accident accident : vehicleAccidents) {
                try {
                    if (accident.routeID instanceof KR_ID) {
                        statement.setInt(3, accident.routeID.id);
                        statement.setInt(2, 0);
                    } else {
                        statement.setInt(3, 0);
                        statement.setInt(2, accident.routeID.id);
                    }
                    statement.setInt(6, accident.start.timestamp);
                    statement.setInt(7, accident.end.timestamp);
                    statement.setDouble(8, accident.start.x);
                    statement.setDouble(9, accident.start.y);
                } catch (SQLException e) {
                    statement = c.prepareStatement(AccidentEntry.INSERT_ACCIDENT_SQL);
                    statement.setInt(1, 0);
                    if (accident.routeID instanceof KR_ID) {
                        statement.setInt(3, accident.routeID.id);
                        statement.setInt(2, 0);
                    } else {
                        statement.setInt(3, 0);
                        statement.setInt(2, accident.routeID.id);
                    }
                    statement.setInt(4, vehicleAccidentsEntry.getKey().id);
                    statement.setInt(5, vehicleAccidentsEntry.getKey().transType);
                    statement.setInt(6, accident.start.timestamp);
                    statement.setInt(7, accident.end.timestamp);
                    statement.setDouble(8, accident.start.x);
                    statement.setDouble(9, accident.start.y);
                }
                statement.execute();
            }
        }
        statement.close();
        c.commit();
    }

    public void insertAccidents(List<Accident<ID, ID>> accidents) throws SQLException {
        List<Accident<ID, VehicleTN>> vehicleAccidents;
        PreparedStatement statement = c.prepareStatement(AccidentEntry.INSERT_ACCIDENT_SQL);
        VehicleTN vehicleTN;
        for (Accident<ID, ID> accident : accidents) {
            if (accident.start.vehicleID instanceof VehicleTN) {
                vehicleTN = (VehicleTN) accident.start.vehicleID;
            } else {
                vehicleTN = null;
            }
            statement.setInt(1, 0);
            statement.setInt(4, accident.start.vehicleID.id);
            if (vehicleTN != null) {
                statement.setInt(5, vehicleTN.transType);
            }
            try {
                if (accident.routeID instanceof KR_ID) {
                    statement.setInt(3, accident.routeID.id);
                    statement.setInt(2, 0);
                } else {
                    statement.setInt(3, 0);
                    statement.setInt(2, accident.routeID.id);
                }
                statement.setInt(6, accident.start.timestamp);
                statement.setInt(7, accident.end.timestamp);
                statement.setDouble(8, accident.start.x);
                statement.setDouble(9, accident.start.y);
            } catch (SQLException e) {
                statement = c.prepareStatement(AccidentEntry.INSERT_ACCIDENT_SQL);
                statement.setInt(1, 0);
                if (accident.routeID instanceof KR_ID) {
                    statement.setInt(3, accident.routeID.id);
                    statement.setInt(2, 0);
                } else {
                    statement.setInt(3, 0);
                    statement.setInt(2, accident.routeID.id);
                }
                statement.setInt(4, accident.start.vehicleID.id);
                if (vehicleTN != null) {
                    statement.setInt(5, vehicleTN.transType);
                }
                statement.setInt(6, accident.start.timestamp);
                statement.setInt(7, accident.end.timestamp);
                statement.setDouble(8, accident.start.x);
                statement.setDouble(9, accident.start.y);
            }
            statement.execute();
        }
        statement.close();
        c.commit();
    }

    public List<VehicleTN> getMU() throws SQLException {
        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery(DbContract.SELECT_MU);
        List<VehicleTN> list = new ArrayList<>();
        while (resultSet.next()) {
            list.add(new VehicleTN(
                    resultSet.getInt(1),
                    3
            ));
        }
        resultSet.close();
        statement.close();
        return list;
    }

    public Table<MR_ID, VehicleTN, Deque<Interval<MR_ID, VehicleTN>>>
    getStays(int stationID, Set<Integer> routeTNs, int timeFrom, int timeTo) throws SQLException {
        Statement statement = c.createStatement();
        ResultSet resultSet =
                statement.executeQuery(
                        DbUtils.constructStaysRequest(stationID, routeTNs, timeFrom, timeTo));

        Table<MR_ID, VehicleTN, Deque<Interval<MR_ID, VehicleTN>>> table =
                DbUtils.resultSetToStays(resultSet);
        resultSet.close();
        return table;
    }

    public void insertStays(Table<MR_ID, VehicleTN, Deque<Interval<MR_ID, VehicleTN>>> stays,
                            int stationID) throws SQLException {
        PreparedStatement statement = c.prepareStatement(StationStayEntry.INSERT_STAY_SQL);
        for (Table.Cell<MR_ID, VehicleTN, Deque<Interval<MR_ID, VehicleTN>>> cell :
                stays.cellSet()) {
            statement.clearBatch();
            statement.setInt(1, stationID);
            statement.setInt(2, cell.getRowKey().id);
            statement.setInt(3, cell.getColumnKey().id);
            statement.setInt(4, cell.getColumnKey().transType);


            for (Interval<MR_ID, VehicleTN> interval : cell.getValue()) {
                statement.setInt(5, interval.start.timestamp);
                statement.setInt(6, interval.end.timestamp);
                if (interval.end.timestamp < interval.start.timestamp) {
                    System.err.println("INTERVAL ERROR station=" + stationID + " route=" +
                                       interval.routeID.id + " start=" +
                                       Printer.getFormattedDate(Printer.fullDateFormat,
                                                                interval.start.timestamp) +
                                       " end=" +
                                       Printer.getFormattedDate(Printer.fullDateFormat,
                                                                interval.end.timestamp)
                    );
                }
                statement.addBatch();
            }
            statement.executeBatch();
        }
        statement.close();
        c.commit();
    }

    public void insertAccidentsByClusters(List<List<Accident<ID, VehicleTN>>> accidents)
            throws SQLException {
        PreparedStatement statement = c.prepareStatement(AccidentEntry.INSERT_ACCIDENT_SQL);
        int i = 0;
        for (List<Accident<ID, VehicleTN>> accidentList : accidents) {
            for (Accident<ID, VehicleTN> accident : accidentList) {
                try {
                    statement.setInt(1, i);
                    if (accident.routeID instanceof KR_ID) {
                        statement.setInt(3, accident.routeID.id);
                        statement.setInt(2, 0);
                    } else {
                        statement.setInt(3, 0);
                        statement.setInt(2, accident.routeID.id);
                    }
                    statement.setInt(4, accident.start.vehicleID.id);
                    statement.setInt(5, accident.start.vehicleID.transType);
                    statement.setInt(6, accident.start.timestamp);
                    statement.setInt(7, accident.end.timestamp);
                    statement.setDouble(8, accident.start.x);
                    statement.setDouble(9, accident.start.y);
                } catch (SQLException e) {
                    statement.close();
                    statement = c.prepareStatement(AccidentEntry.INSERT_ACCIDENT_SQL);
                    statement.setInt(1, i);
                    if (accident.routeID instanceof KR_ID) {
                        statement.setInt(3, accident.routeID.id);
                        statement.setInt(2, 0);
                    } else {
                        statement.setInt(3, 0);
                        statement.setInt(2, accident.routeID.id);
                    }
                    statement.setInt(4, accident.start.vehicleID.id);
                    statement.setInt(5, accident.start.vehicleID.transType);
                    statement.setInt(6, accident.start.timestamp);
                    statement.setInt(7, accident.end.timestamp);
                    statement.setDouble(8, accident.start.x);
                    statement.setDouble(9, accident.start.y);
                }
                try {
                    statement.execute();
                } catch (SQLException e) {
                    if (e.getErrorCode() != errorCodes.DUPLICATE) {
                        System.out.println("Accident insert error ");
                        e.printStackTrace();
                    }
                }
            }
            i++;
        }
        statement.close();
//        c.commit();
    }


    public Map<VehicleTN, List<Accident<ID, VehicleTN>>> getAccidentsByVehicle(int[] limits)
            throws SQLException {
        Map<VehicleTN, List<Accident<ID, VehicleTN>>> accidents;
        try (PreparedStatement statement = c.prepareStatement(SELECT_ACCIDENTS_SQL)) {
            statement.setInt(1, limits[0]);
            statement.setInt(2, limits[1]);
            try (ResultSet resultSet = statement.executeQuery()) {
                accidents = DbUtils.resultSetToAccidents(resultSet);
            }
        }
        return accidents;
    }

    public List<List<Accident<ID, VehicleTN>>> getAccidentsAsList(int[] limits) throws SQLException {
        List<List<Accident<ID, VehicleTN>>> accidents;
        try (PreparedStatement statement = c.prepareStatement(SELECT_ACCIDENTS_SQL)) {
            statement.setInt(1, limits[0]);
            statement.setInt(2, limits[1]);
            try (ResultSet resultSet = statement.executeQuery()) {
                accidents = DbUtils.resultSetToAccidentsList(resultSet);
            }
        }
        return accidents;
    }

    public Map<CP_ID, List<Deviation>> getDeviations(KR_ID kr_id, int[] timeLimits) throws SQLException {
        Map<CP_ID, List<Deviation>> deviations;
        try (PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_DEVIATIONS)) {
            statement.setInt(1, kr_id.id);
            statement.setInt(2, timeLimits[0]);
            statement.setInt(3, timeLimits[1]);
            try (ResultSet resultSet = statement.executeQuery()) {
                deviations = DbUtils.resultSetToRouteDeviations(resultSet);
            }
        }
        return deviations;
    }

    public List<Vehicle> searchVehicles(String boardNumber) throws SQLException {
        List<Vehicle> result;
        try (PreparedStatement statement = c.prepareStatement(SELECT_TRANSPORT_BY_KT_ID_SQL)) {
            statement.setString(1, boardNumber);
            try (ResultSet resultSet = statement.executeQuery()) {
                result = DbUtils.resultSetToVehicles(resultSet);
            }
        }
        return result;
    }

    public Vehicle getVehicle(VehicleTN tn_id) throws SQLException {
        Vehicle result;
        try (PreparedStatement statement = c.prepareStatement(SELECT_TRANSPORT_BY_TN_ID_SQL)) {
            statement.setInt(1, tn_id.id);
            statement.setInt(2, tn_id.transType);
            try (ResultSet resultSet = statement.executeQuery()) {
                result = DbUtils.resultSetToVehicle(resultSet);
            }
        }
        return result;
    }

    public Table<KT_ID, CP_ID, List<Integer>> getRouteScheduleByVehicles(KR_ID kr_id, int[] timeLimits)
            throws SQLException {
        Table<KT_ID, CP_ID, List<Integer>> result;
        try (PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_SCHEDULE_BY_VEHICLE_SQL)) {
            statement.setInt(1, kr_id.id);
            statement.setInt(2, timeLimits[0]);
            statement.setInt(3, timeLimits[1]);
            try (ResultSet resultSet = statement.executeQuery()) {
                result = DbUtils.resultSetToRouteScheduleByVehicle(resultSet);
            }
        }
        return result;
    }

    public Map<CP_ID, List<ScheduleTime>> getRouteSchedule(KR_ID kr_id, int[] timeLimits)
            throws SQLException {
        Map<CP_ID, List<ScheduleTime>> result;
        try (PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_SCHEDULE_SQL)) {
            statement.setInt(1, kr_id.id);
            statement.setInt(2, timeLimits[0]);
            statement.setInt(3, timeLimits[1]);
            try (ResultSet resultSet = statement.executeQuery()) {
                result = DbUtils.resultSetToRouteSchedule(resultSet);
            }
        }
        return result;
    }

    public Map<KT_ID, List<TransportPosition<KT_ID>>> getRouteMonitoringByKTid(KR_ID kr_id,
                                                                               int[] timeLimits)
            throws SQLException {
        Map<KT_ID, List<TransportPosition<KT_ID>>> result;
        try (PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_MONITORING_BY_KT_ID_SQL)) {
            statement.setInt(1, kr_id.id);
            statement.setInt(2, timeLimits[0]);
            statement.setInt(3, timeLimits[1]);
            try (ResultSet resultSet = statement.executeQuery()) {
                result = DbUtils.resultSetToRouteMonitoringByKTid(resultSet);
            }
        }
        return result;
    }

    public Map<VehicleTN, List<PositionRelToRoutes<VehicleTN, KR_ID>>> getRouteMonitoringByTNid(
            KR_ID kr_id,
            int[] timeLimits)
            throws SQLException {
        Map<VehicleTN, List<PositionRelToRoutes<VehicleTN, KR_ID>>> result;
        try (PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_MONITORING_BY_KT_ID_SQL)) {
            statement.setInt(1, kr_id.id);
            statement.setInt(2, timeLimits[0]);
            statement.setInt(3, timeLimits[1]);
            try (ResultSet resultSet = statement.executeQuery()) {
                result = DbUtils.resultSetToRouteMonitoringByTNid(resultSet, kr_id);
            }
        }
        return result;
    }

    public Map<VehicleTN, List<? extends PositionRelToRoutes>> getRouteMonitoringByTNid(
            RouteIDs routeIDs,
            int[] timeLimits)
            throws SQLException {
        Map<VehicleTN, List<? extends PositionRelToRoutes>> result;
        String tnEnum = ROUTE_TN_ENUM_ELEMENT;
        int size = routeIDs.tn_ids.size();
        for (int i = 1; i < size; i++) {
            tnEnum += " or " + ROUTE_TN_ENUM_ELEMENT;
        }

        try (PreparedStatement statement = c.prepareStatement(
                SELECT_ROUTE_ALL_DIRECTIONS_MONITORING_SQL.replaceAll(ROUTE_TN_ENUM_MARKER, tnEnum))) {
            int i = 1;
            for (MR_ID tn_id : routeIDs.tn_ids) {
                statement.setInt(i++, tn_id.id);
            }
            statement.setInt(i++, routeIDs.transType);
            statement.setInt(i++, timeLimits[0]);
            statement.setInt(i, timeLimits[1]);

            try (ResultSet resultSet = statement.executeQuery()) {
                result = DbUtils.resultSetToAllDirectionsMonitoring(resultSet);
            }
        }
        return result;
    }

    @Deprecated
    public Table<KT_ID, RaceID, List<TransportPosition<KT_ID>>> getRouteMonitoringByRace(KR_ID kr_id)
            throws SQLException {
        Table<KT_ID, RaceID, List<TransportPosition<KT_ID>>> result;
        try (PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_MONITORING_BY_KT_ID_SQL)) {
            statement.setInt(1, kr_id.id);
            try (ResultSet resultSet = statement.executeQuery()) {
                result = DbUtils.resultSetToRouteMonitoringByRace(resultSet);
            }
        }
        return result;
    }

    public Table<KR_ID, PeriodID, KS_ID[]> getAllRouteCircuits() throws SQLException {
        Table<KR_ID, PeriodID, KS_ID[]> result = HashBasedTable.create();
        Map<Integer, PeriodID> periodIDs = new HashMap<>();
        PeriodID periodID = null;
        int tmpID;
        try (PreparedStatement statement = c.prepareStatement(SELECT_ALL_ROUTE_CIRCUITS_COUNTS_SQL)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    tmpID = resultSet.getInt(2);
                    periodID = periodIDs.get(tmpID);
                    if (periodID == null) {
                        periodIDs.put(tmpID, periodID = new PeriodID(tmpID));
                    }
                    result.put(new KR_ID(resultSet.getInt(1)), periodID,
                               new KS_ID[resultSet.getInt(3)]);
                }
            }
        }

        try (PreparedStatement statement = c.prepareStatement(SELECT_ALL_ROUTE_CIRCUITS_SQL)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                KS_ID[] circuit = null;
                int i = 0;
                KR_ID KR_ID = new KR_ID();
                while (resultSet.next()) {
                    tmpID = resultSet.getInt(1);
                    if (KR_ID.id != tmpID) {
                        KR_ID = new KR_ID(tmpID);
                        i = 0;
                        circuit = null;
                    }
                    tmpID = resultSet.getInt(2);
                    if (periodID.id != tmpID) {
                        periodID = periodIDs.get(tmpID);
                        if (periodID == null) {
                            periodIDs.put(tmpID, periodID = new PeriodID(tmpID));
                        }
                        i = 0;
                        circuit = null;
                    }
                    if (circuit == null) {
                        circuit = result.get(KR_ID, periodID);
                    }
                    circuit[i++] = new KS_ID(resultSet.getInt(3));
                }
            }
        }

        return result;
    }

    public Map<KS_ID, Double> getRouteCircuitWithDistsMap(KR_ID kr_id, PeriodID periodID)
            throws SQLException {
        Map<KS_ID, Double> result;
        try (PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_CIRCUIT_DISTS_SQL)) {
            statement.setInt(1, kr_id.id);
            statement.setInt(2, periodID.id);

            result = new HashMap<>();
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    result.put(new KS_ID(resultSet.getInt(1)), resultSet.getDouble(2));
                }
            }
        }
        return result;
    }

    public Table<KR_ID, KS_ID, Double> getAllRouteCircuitsWithDists(PeriodID periodID)
            throws SQLException {
        Table<KR_ID, KS_ID, Double> result;
        try (PreparedStatement statement = c
                .prepareStatement(SELECT_DISTS_ON_ROUTES_WITH_SCHEDULE_SQL)) {
            statement.setInt(1, periodID.id);

            try (ResultSet resultSet = statement.executeQuery()) {
                result = DbUtils.resulSetToCircuitsWithDists(resultSet);
            }
        }
        return result;
    }

    public StopDistance[] getRouteCircuitWithDistsList(KR_ID kr_id, PeriodID periodID)
            throws SQLException {
        StopDistance[] result;
        int count = -1;
        try (PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_CIRCUIT_COUNT_SQL)) {
            statement.setInt(1, kr_id.id);
            statement.setInt(2, periodID.id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    count = resultSet.getInt(1);
                }
            }
        }

        if (count < 0) {
            return new StopDistance[0];
        }

        try (PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_CIRCUIT_DISTS_SQL)) {
            statement.setInt(1, kr_id.id);
            statement.setInt(2, periodID.id);

            result = new StopDistance[count];
            int i = 0;
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    result[i++] = new StopDistance(resultSet.getInt(1), resultSet.getDouble(2),
                                                   resultSet.getDouble(3),
                                                   resultSet.getDouble(4));
                }
            }
        }
        return result;
    }


    public int[] getRouteCircuit(KR_ID kr_id, PeriodID periodID) throws SQLException {
        PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_CIRCUIT_COUNT_SQL);
        statement.setInt(1, kr_id.id);
        statement.setInt(2, periodID.id);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        int count = resultSet.getInt(1);
        resultSet.close();
        statement.close();

        statement = c.prepareStatement(SELECT_ROUTE_CIRCUIT_SQL);
        statement.setInt(1, kr_id.id);
        statement.setInt(2, periodID.id);
        int[] result = new int[count];
        resultSet = statement.executeQuery();

        for (int i = 0; resultSet.next(); i++) {
            result[i] = resultSet.getInt(1);
        }
        resultSet.close();
        statement.close();

        return result;
    }

    public List<KS_ID> getRouteCircuitList(KR_ID kr_id, PeriodID periodID) throws SQLException {
        PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_CIRCUIT_COUNT_SQL);
        statement.setInt(1, kr_id.id);
        statement.setInt(2, periodID.id);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        int count = resultSet.getInt(1);
        resultSet.close();
        statement.close();

        statement = c.prepareStatement(SELECT_ROUTE_CIRCUIT_SQL);
        statement.setInt(1, kr_id.id);
        statement.setInt(2, periodID.id);
        List<KS_ID> result = new ArrayList<>(count);
        resultSet = statement.executeQuery();

        while (resultSet.next()) {
            result.add(new KS_ID(resultSet.getInt(1)));
        }
        resultSet.close();
        statement.close();

        return result;
    }

    public double[] getRouteGeometry(KR_ID kr_id, PeriodID periodID) throws SQLException {
        PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_GEOMETRY_COUNT_SQL);
        statement.setInt(1, kr_id.id);
        statement.setInt(2, periodID.id);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        int count = resultSet.getInt(1);
        resultSet.close();

        statement = c.prepareStatement(SELECT_ROUTE_GEOMETRY_SQL);
        statement.setInt(1, kr_id.id);
        statement.setInt(2, periodID.id);
        double[] result = new double[count * 2];
        resultSet = statement.executeQuery();
        int i = 0;
        int xIndex = resultSet.findColumn(GeometryEntry.X);
        int yIndex = resultSet.findColumn(GeometryEntry.Y);
        while (resultSet.next()) {
            result[i++] = resultSet.getDouble(xIndex);
            result[i++] = resultSet.getDouble(yIndex);
        }
        resultSet.close();
        statement.close();

        return result;
    }

    public Table<VehicleTN, CP_ID, List<RaceTimings>> getRouteRealScheduleByVehicle(KR_ID kr_id,
                                                                                    int[] timeLimits)
            throws SQLException {
        try (PreparedStatement statement = c
                .prepareStatement(SELECT_ROUTE_REAL_SCHEDULE_BY_VEHICLE_SQL)) {
            statement.setInt(1, kr_id.id);
            statement.setInt(2, timeLimits[0]);
            statement.setInt(3, timeLimits[1]);
            try (ResultSet resultSet = statement.executeQuery()) {
                return DbUtils.resultSetToRouteRealScheduleByVehicle(resultSet);
            }
        }
    }

    public Map<CP_ID, List<Race>> getRouteRealSchedule(KR_ID kr_id, int[] timeLimits)
            throws SQLException {
        try (PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_REAL_SCHEDULE_SQL)) {
            statement.setInt(1, kr_id.id);
            statement.setInt(2, timeLimits[0]);
            statement.setInt(3, timeLimits[1]);
            try (ResultSet resultSet = statement.executeQuery()) {
                return DbUtils.resultSetToRouteRealSchedule(resultSet);
            }
        }
    }

    public void insertRouteRealSchedule(KR_ID kr_id,
                                        Table<VehicleTN, CP_ID, List<RaceTimings>> realSchedule)
            throws SQLException {
        Statement create = c.createStatement();
        create.execute(RealScheduleEntry.CREATE_REAL_SCHEDULE_SQL);
        create.close();
        PreparedStatement statement = c.prepareStatement(RealScheduleEntry.INSERT_REAL_SCHEDULE_SQL);
        statement.setInt(1, kr_id.id);

        Map<CP_ID, List<RaceTimings>> transMovement;
        for (VehicleTN vehicleTN : realSchedule.rowKeySet()) {
            statement.setInt(2, vehicleTN.id);
            statement.setInt(3, vehicleTN.transType);
            transMovement = realSchedule.row(vehicleTN);
            for (CP_ID cp_id : transMovement.keySet()) {
                statement.setInt(4, cp_id.id);
                for (RaceTimings timings : transMovement.get(cp_id)) {
                    statement.setInt(5, timings.startTime);
                    statement.setInt(6, timings.endTime);
                    statement.executeUpdate();
                }
            }
        }

        statement.close();
        c.commit();
    }

    public void insertRouteGeometry(KR_ID kr_id, double[] coords, PeriodID periodID)
            throws SQLException {
        PreparedStatement statement = c.prepareStatement(GeometryEntry.INSERT_GEOMETRY_SQL);
        statement.setInt(1, kr_id.id);
        statement.setInt(3, periodID.id);
        int length = coords.length / 2;
        for (int i = 0; i < length; i++) {
            try {
                statement.setInt(2, i);
                statement.setDouble(4, coords[i * 2]);
                statement.setDouble(5, coords[i * 2 + 1]);
                statement.executeUpdate();
            } catch (SQLException e) {
                statement = c.prepareStatement(GeometryEntry.INSERT_GEOMETRY_SQL);
                statement.setInt(1, kr_id.id);
                statement.setInt(3, periodID.id);
                statement.setInt(2, i);
                statement.setDouble(4, coords[i * 2]);
                statement.setDouble(5, coords[i * 2 + 1]);
            }
        }
        statement.close();
    }

    public void insertStops(Collection<Stop> stops) throws SQLException {
        PreparedStatement statement = c.prepareStatement(StopEntry.INSERT_STOP_SQL);
        for (Stop stop : stops) {
            try {
                DbUtils.fillStopStmt(statement, stop);

                statement.executeUpdate();
            } catch (SQLException e) {
                if (e.getErrorCode() == errorCodes.DUPLICATE) {
                    continue;
                }
                statement = c.prepareStatement(StopEntry.INSERT_STOP_SQL);
                DbUtils.fillStopStmt(statement, stop);
                statement.executeUpdate();
            }
        }
        statement.close();
    }

    public void insertRoutes(Collection<Route> routes) throws SQLException {
        PreparedStatement statement = c.prepareStatement(RouteEntry.INSERT_ROUTE_SQL);

        for (Route route : routes) {
            try {
                DbUtils.fillRouteStmt(statement, route);

                statement.executeUpdate();
            } catch (SQLException e) {
                if (e.getErrorCode() == errorCodes.DUPLICATE) {
                    continue;
                }
                statement = c.prepareStatement(RouteEntry.INSERT_ROUTE_SQL);
                DbUtils.fillRouteStmt(statement, route);
                statement.executeUpdate();
            }
        }
        statement.close();
    }

    public void insertCircuit(KR_ID kr_id, List<KS_ID> circuit, PeriodID periodID) throws SQLException {
        PreparedStatement statement = c.prepareStatement(CircuitEntry.INSERT_CIRCUIT_SQL);

        for (int i = 0; i < circuit.size(); i++) {
            KS_ID ks_id = circuit.get(i);
            try {
                DbUtils.fillCircuitStmt(statement, kr_id, ks_id, i, periodID);

                statement.executeUpdate();
            } catch (SQLException e) {
                if (e.getErrorCode() == errorCodes.DUPLICATE) {
                    continue;
                }
                statement = c.prepareStatement(CircuitEntry.INSERT_CIRCUIT_SQL);
                DbUtils.fillCircuitStmt(statement, kr_id, ks_id, i, periodID);
                statement.executeUpdate();
            }
        }
        statement.close();
    }

    public void updateStopWithGeoIDAndXY(Stop stop) throws SQLException {
        PreparedStatement statement = c.prepareStatement(StopEntry.UPDATE_STOP_GEOID_XY_SQL);
        if (statement.isClosed()) {
            statement = c.prepareStatement(StopEntry.UPDATE_STOP_GEOID_XY_SQL);
        }
        statement.setInt(1, stop.geoportalID);
        statement.setDouble(2, stop.x);
        statement.setDouble(3, stop.y);
        statement.setInt(4, stop.ks_id.id);
        statement.executeUpdate();

        statement.close();
//        c.commit();
    }

    public void updateControlPointsWithStopID(List<ControlPointBinding> updatedPoints)
            throws SQLException {
        PreparedStatement statement =
                c.prepareStatement(ControlPointEntry.UPDATE_CONTROL_POINT_KS_ID_SQL);
        for (ControlPointBinding point : updatedPoints) {
            if (statement.isClosed()) {
                statement = c.prepareStatement(ControlPointEntry.UPDATE_CONTROL_POINT_KS_ID_SQL);
            }
            statement.setInt(1, point.ks_id.id);
            statement.setInt(2, point.cp_id.id);
            statement.setInt(3, point.kr_id.id);
            statement.executeUpdate();
        }
        statement.close();
        c.commit();
    }

    public void updateMonitoringWithKR_ID(RouteIDs routeIDs,
                                          Map<VehicleTN, List<? extends PositionRelToRoutes>> routeMonitoring)
            throws SQLException {
        String tnEnum = ROUTE_TN_ENUM_ELEMENT;
        int size = routeIDs.tn_ids.size();
        for (int i = 1; i < size; i++) {
            tnEnum += " or " + ROUTE_TN_ENUM_ELEMENT;
        }

        try (PreparedStatement statement =
                     c.prepareStatement(
                             MonitoringEntry.DELETE_ROUTE_MONITORING_SQL
                                     .replace(ROUTE_TN_ENUM_MARKER, tnEnum))) {
            statement.setInt(1, routeIDs.transType);
            int i = 2;
            for (MR_ID tn_id : routeIDs.tn_ids) {
                statement.setInt(i++, tn_id.id);
            }

            statement.executeUpdate();
        }
//        c.commit();

        MR_ID mr_id = routeIDs.tn_ids.iterator().next();
        VehicleTN vehicleTN;
        PreparedStatement statement = c.prepareStatement(MonitoringEntry.INSERT_MONITORING_SQL);
        for (Map.Entry<VehicleTN, List<? extends PositionRelToRoutes>> vehicleMonitoringEntry : routeMonitoring
                .entrySet()) {
            vehicleTN = vehicleMonitoringEntry.getKey();
            for (PositionRelToRoutes position : vehicleMonitoringEntry.getValue()) {
                try {
                    DbUtils.fillMonitoringStmt(statement, position, mr_id, vehicleTN,
                                               routeIDs.transType);
                } catch (SQLException e) {
                    statement.close();
                    statement = c.prepareStatement(MonitoringEntry.INSERT_MONITORING_SQL);
                    DbUtils.fillMonitoringStmt(statement, position, mr_id, vehicleTN,
                                               routeIDs.transType);
                }

                try {
                    statement.execute();
                } catch (SQLException ex) {
                    System.out
                            .println(vehicleTN.id + " " + routeIDs.transType + " " + position.timestamp);
                }
            }
        }

        statement.close();
        c.commit();
    }

    public void updateMonitoringWithKR_ID(RouteIDs routeIDs, VehicleTN vehicleTN,
                                          List<PositionRelToRoutes> vehicleMonitoring)
            throws SQLException {
        String tns = "";
        for (MR_ID tn_id : routeIDs.tn_ids) {
            tns += "," + tn_id.id;
        }
        tns = "(" + tns.substring(1) + ")";

        try (PreparedStatement statement =
                     c.prepareStatement(
                             MonitoringEntry.DELETE_VEHICLE_ON_ROUTE_MONITORING_SQL
                                     .replace(ROUTE_TN_ENUM_MARKER, tns))) {
            statement.setInt(1, vehicleTN.id);
            statement.setInt(2, routeIDs.transType);

            statement.executeUpdate();
        }
//        c.commit();

        MR_ID mr_id = routeIDs.tn_ids.iterator().next();
        PreparedStatement statement = c.prepareStatement(MonitoringEntry.INSERT_MONITORING_SQL);
        for (PositionRelToRoutes position : vehicleMonitoring) {

            try {
                DbUtils.fillMonitoringStmt(statement, position, mr_id, vehicleTN, routeIDs.transType);
            } catch (SQLException e) {
                statement.close();
                statement = c.prepareStatement(MonitoringEntry.INSERT_MONITORING_SQL);
                DbUtils.fillMonitoringStmt(statement, position, mr_id, vehicleTN, routeIDs.transType);
            }

            try {
                statement.execute();
            } catch (SQLException ex) {
                System.out.println(vehicleTN.id + " " + routeIDs.transType + " " + position.timestamp);
            }
        }

        statement.close();
        c.commit();
    }

    public void updateCircuitsWithMileage(Table<KR_ID, PeriodID, double[]> distances)
            throws SQLException {
        PreparedStatement statement = c.prepareStatement(CircuitEntry.UPDATE_CIRCUIT_WITH_MILEAGE_SQL);
        double[] dist;
        for (Table.Cell<KR_ID, PeriodID, double[]> distancesCell : distances.cellSet()) {
            dist = distancesCell.getValue();
            try {
                statement.setInt(2, distancesCell.getRowKey().id);
                statement.setDouble(3, distancesCell.getColumnKey().id);
            } catch (SQLException e) {
                statement.close();
                statement = c.prepareStatement(CircuitEntry.UPDATE_CIRCUIT_WITH_MILEAGE_SQL);
                statement.setInt(2, distancesCell.getRowKey().id);
                statement.setDouble(3, distancesCell.getColumnKey().id);
            }

            for (int i = 0; i < dist.length; i++) {
                try {
                    statement.setDouble(1, dist[i]);
                    statement.setInt(4, i);
                } catch (SQLException e) {
                    statement.close();
                    statement = c.prepareStatement(CircuitEntry.UPDATE_CIRCUIT_WITH_MILEAGE_SQL);
                    statement.setInt(2, distancesCell.getRowKey().id);
                    statement.setDouble(3, distancesCell.getColumnKey().id);
                    statement.setDouble(1, dist[i]);
                    statement.setInt(4, i);
                }
                statement.executeUpdate();
            }
        }

        statement.close();
        c.commit();
    }

    public synchronized void updateFromZip(File classifiersZip)
            throws IOException, XmlPullParserException, SQLException {
        ZipFile zf = new ZipFile(classifiersZip);

        boolean autoCommit = c.getAutoCommit();
        c.setAutoCommit(false);
        try {

            List<Stop> stops =
                    XmlParser.parseStopsFullDB(zf.getInputStream(zf.getEntry("stopsFullDB.xml")));

            insertStops(stops);
            Complementator.stopsXY(this);
            stops = null;
//            c.commit();

            List<RouteGeoData> geoDatas =
                    XmlParser.parseRoutesGeoData(
                            zf.getInputStream(zf.getEntry("routesAndStopsCorrespondence.xml"))
                    );
            Map<KR_ID, PeriodID> kr_ids = new HashMap<>();
            kr_ids.put(new KR_ID(114), new PeriodID(2));
            kr_ids.put(new KR_ID(115), new PeriodID(2));
            kr_ids.put(new KR_ID(95), new PeriodID(2));
            kr_ids.put(new KR_ID(96), new PeriodID(2));
            kr_ids.put(new KR_ID(97), new PeriodID(3));
            kr_ids.put(new KR_ID(98), new PeriodID(3));
            kr_ids.put(new KR_ID(103), new PeriodID(3));
            kr_ids.put(new KR_ID(104), new PeriodID(3));
            kr_ids.put(new KR_ID(612), new PeriodID(3));
            kr_ids.put(new KR_ID(613), new PeriodID(3));
            kr_ids.put(new KR_ID(612), new PeriodID(4));
            kr_ids.put(new KR_ID(613), new PeriodID(4));
            kr_ids.put(new KR_ID(614), new PeriodID(3));
            kr_ids.put(new KR_ID(615), new PeriodID(3));
            kr_ids.put(new KR_ID(120), new PeriodID(3));
            kr_ids.put(new KR_ID(120), new PeriodID(3));

            for (RouteGeoData geoData : geoDatas) {
                if (kr_ids.containsKey(geoData.kr_id)) {
//                    insertRouteGeometry(geoData.kr_id, geoData.geometry, new PeriodID(0));
                    insertCircuit(geoData.kr_id, geoData.getCircuit(), kr_ids.get(geoData.kr_id));

                }
            }
            geoDatas = null;

            Map<KR_ID, Route> routes = new HashMap<>();
            XmlParser.parseRoutes(
                    zf.getInputStream(zf.getEntry("routes.xml")),
                    routes
            );

            routes = XmlParser.parseGeoportalRoutes
                    (zf.getInputStream(zf.getEntry("GeoportalRoutesCorrespondence.xml")), routes);

            insertRoutes(routes.values());
            Complementator.routesXY(this, kr_ids);
            routes = null;

            zf.close();


            Complementator.measureStopsDist(this, kr_ids.keySet());

            c.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            c.rollback();

        } finally {
            c.setAutoCommit(autoCommit);
        }
    }

    public List<CP_ID> getControlPointCandidatesIDs(int controlPointID) throws SQLException {
        PreparedStatement statement = c.prepareStatement(SELECT_CP_CANDIDATES_SQL);
        statement.setInt(1, controlPointID);
        ResultSet resultSet = statement.executeQuery();
        List<CP_ID> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(new CP_ID(resultSet.getInt(1)));
        }
        resultSet.close();
        statement.close();
        return result;
    }

    public Set<KR_ID> getRoutesWithSchedule() throws SQLException {
        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ROUTES_WITH_SCHEDULE_SQL);
        Set<KR_ID> result = new HashSet<>();
        while (resultSet.next()) {
            result.add(new KR_ID(resultSet.getInt(1)));
        }
        resultSet.close();
        statement.close();
        return result;
    }

    public void executeSQL(String sql) throws SQLException {
        try (Statement statement = c.createStatement();) {
            statement.execute(sql);
        }

        c.commit();
    }

    public Table<KR_ID, CP_ID, ControlPoint> getAllCPsTableWithCoords() throws SQLException {
        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL_CONTROL_POINTS_WITH_COORDS_SQL);
        Table<KR_ID, CP_ID, ControlPoint> result =
                DbUtils.resultSetToControlPointsTable(resultSet, true);
        resultSet.close();
        statement.close();
        return result;
    }

    public Map<KR_ID, List<ControlPoint>> getAllCPsListWithCoords() throws SQLException {
        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL_CONTROL_POINTS_WITH_COORDS_SQL);
        Map<KR_ID, List<ControlPoint>> result = DbUtils.resultSetToControlPointsMap(resultSet, true);
        resultSet.close();
        statement.close();
        return result;
    }

    public List<KS_ID> getStopsThroughCP(CP_ID cp_id) throws SQLException {
        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_CONTROL_POINT_STOPS_SQL);
        List<KS_ID> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(new KS_ID(resultSet.getInt(0)));
        }
        resultSet.close();
        statement.close();
        return result;
    }

    public List<KR_ID> getRoutesThroughCP(CP_ID cp_id, PeriodID periodID) throws SQLException {
        PreparedStatement statement = c.prepareStatement(SELECT_CONTROL_POINT_ROUTES_SQL);
        statement.setInt(1, cp_id.id);
        statement.setInt(2, periodID.id);
        statement.setInt(3, periodID.id);
        ResultSet resultSet = statement.executeQuery();
        List<KR_ID> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(new KR_ID(resultSet.getInt(1)));
        }
        resultSet.close();
        statement.close();
        return result;
    }

    public Table<KR_ID, CP_ID, ControlPoint> getAllControlPoints() throws SQLException {
        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL_CONTROL_POINTS_SQL);
        Table<KR_ID, CP_ID, ControlPoint> result =
                DbUtils.resultSetToControlPointsTable(resultSet, false);
        resultSet.close();
        statement.close();
        return result;
    }

    public Table<CP_ID, KR_ID, Integer> getAllControlPointsOrdered(PeriodID periodID)
            throws SQLException {
        PreparedStatement statement = c.prepareStatement(SELECT_ALL_CONTROL_POINTS_ORDERED_SQL);
        statement.setInt(1, periodID.id);
        ResultSet resultSet = statement.executeQuery();
        Table<CP_ID, KR_ID, Integer> result =
                DbUtils.resultSetToControlPointsOrderedTable(resultSet);
        resultSet.close();
        statement.close();
        return result;
    }

    public void removeCPduplicates() throws SQLException {
        int count = 0;
        try (Statement statement = c.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SELECT_CONTROL_POINTS_DUPLICATES_SQL)) {
                int lastID = -1, lastKR_ID = -1, lastKS_ID = -1, id, KS_ID, KR_ID;
                boolean changed;

                PreparedStatement deleteStatement =
                        c.prepareStatement(ControlPointEntry.DELETE_CONTROL_POINT_SQL);
                PreparedStatement updateStatement =
                        c.prepareStatement(ScheduleEntry.UPDATE_SCHEDULE_CP_SQL);
                while (resultSet.next()) {
                    id = resultSet.getInt(1);
                    KR_ID = resultSet.getInt(2);
                    KS_ID = resultSet.getInt(3);
                    changed = false;
                    if (lastKR_ID != KR_ID) {
                        lastID = id;
                        lastKR_ID = KR_ID;
                        changed = true;
                    }
                    if (lastKS_ID != KS_ID) {
                        lastID = id;
                        lastKS_ID = KS_ID;
                        changed = true;
                    }
                    if (!changed) {
                        try {
                            updateStatement.setInt(1, lastID);
                            updateStatement.setInt(2, id);
                            updateStatement.setInt(3, KR_ID);
                        } catch (SQLException e) {
                            updateStatement = c.prepareStatement(ScheduleEntry.UPDATE_SCHEDULE_CP_SQL);
                            updateStatement.setInt(1, lastID);
                            updateStatement.setInt(2, id);
                            updateStatement.setInt(3, KR_ID);
                        }

                        try {
                            deleteStatement.setInt(1, id);
                            deleteStatement.setInt(2, lastKR_ID);
                        } catch (SQLException e) {
                            deleteStatement =
                                    c.prepareStatement(ControlPointEntry.DELETE_CONTROL_POINT_SQL);
                            deleteStatement.setInt(1, id);
                            deleteStatement.setInt(2, lastKR_ID);
                        }

                        try {
                            updateStatement.executeUpdate();
                        } catch (SQLException e) {
                            System.out
                                    .println("duplicate: " + id + " -> " + lastID + ", KR_ID=" + KR_ID);
                            count++;
                        }
                        try {
                            deleteStatement.executeUpdate();
                        } catch (SQLException e) {
                            System.out.println("delete error " + id + ", KR_ID=" + KR_ID);
                        }

                    }

                }

            }
        }
        System.out.println("duplicate races " + count);
        c.commit();
    }

    public List<ControlPoint> getRouteControlPoints(KR_ID kr_id, PeriodID periodID) throws SQLException {
        PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_CONTROL_POINTS_SQL);
        statement.setInt(1, kr_id.id);
        statement.setInt(2, periodID.id);
        ResultSet resultSet = statement.executeQuery();
        List<ControlPoint> result = DbUtils.resultSetToControlPointsList(resultSet, kr_id, false);
        resultSet.close();
        statement.close();
        return result;
    }

    public List<ControlPoint> getRouteControlPointsWithCoords(KR_ID kr_id, PeriodID periodID)
            throws SQLException {
        PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_CONTROL_POINTS_WITH_COORDS_SQL);
        statement.setInt(1, kr_id.id);
        statement.setInt(2, periodID.id);
        ResultSet resultSet = statement.executeQuery();
        List<ControlPoint> result = DbUtils.resultSetToControlPointsList(resultSet, kr_id, true);
        resultSet.close();
        statement.close();
        return result;
    }

    public Map<KS_ID, Stop> getAllStops() throws SQLException {
        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL_STOPS_SQL);
        Map<KS_ID, Stop> result = DbUtils.resultSetToStops(resultSet);
        resultSet.close();
        statement.close();
        return result;
    }

    public Map<KS_ID, Stop> getStopsWithAbsentSpatialData() throws SQLException {
        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_STOPS_WITH_NULL_COORDS_SQL);
        Map<KS_ID, Stop> result = DbUtils.resultSetToStops(resultSet);
        resultSet.close();
        statement.close();
        return result;
    }

    public Stop getStop(KS_ID ks_id) throws SQLException {
        if (ks_id == null) {
            return null;
        }
        Stop stop;
        try (PreparedStatement statement = c.prepareStatement(SELECT_STOP_BY_ID_SQL)) {
            statement.setInt(1, ks_id.id);
            try (ResultSet resultSet = statement.executeQuery()) {
                stop = DbUtils.resultSetToStop(resultSet);
            }
        }
        return stop;
    }

    public Map<KR_ID, Route> getAllRoutes() throws SQLException {
        Statement statement = c.createStatement();
        ResultSet resultSet = statement.executeQuery(SELECT_ALL_ROUTES_SQL);
        Map<KR_ID, Route> result = DbUtils.resultSetToRoutes(resultSet);
        resultSet.close();
        statement.close();
        return result;
    }

    public Set<KR_ID> getAllRoutesIds() throws SQLException {
        Set<KR_ID> list = new HashSet<>();
        try (Statement statement = c.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SELECT_ROUTES_IDS_SQL)) {
                while (resultSet.next()) {
                    list.add(new KR_ID(resultSet.getInt(1)));
                }
            }
        }
        return list;
    }

    public Map<TransTypeID, Set<KR_ID>> getElectrotransportRoutesIds() throws SQLException {
        Map<TransTypeID, Set<KR_ID>> result = new HashMap<>();
        int tmpID;
        TransTypeID transTypeID = new TransTypeID(0);
        Set<KR_ID> transTypeIds = null;
        try (Statement statement = c.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(SELECT_ELECTRO_ROUTES_IDS_SQL)) {
                while (resultSet.next()) {
                    tmpID = resultSet.getInt(2);
                    if (tmpID != transTypeID.id) {
                        transTypeID = new TransTypeID(tmpID);
                        result.put(transTypeID, transTypeIds = new HashSet<>());
                    }
                    transTypeIds.add(new KR_ID(resultSet.getInt(1)));
                }
            }
        }
        return result;
    }

    public Map<KR_ID, Route> getRoutes(Set<KR_ID> kr_ids) throws SQLException {
        Map<KR_ID, Route> result;
        String ids = "";
        for (KR_ID kr_id : kr_ids) {
            ids += "," + kr_id.id;
        }
        ids = ids.substring(1);
        try (PreparedStatement statement = c
                .prepareStatement(SELECT_ROUTES_BY_IDS_SQL.replace(KR_IDS_ENUM_MARKER, ids))) {
            try (ResultSet resultSet = statement.executeQuery()) {
                result = DbUtils.resultSetToRoutes(resultSet);
            }
        }
        return result;
    }

    public List<KS_ID> getRouteStopsIDs(KR_ID kr_id) throws SQLException {
        PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_STOPS_SQL);
        statement.setInt(1, kr_id.id);
        ResultSet resultSet = statement.executeQuery();
        List<KS_ID> result = new ArrayList<>();
        while (resultSet.next()) {
            result.add(new KS_ID(resultSet.getInt(1)));
        }
        resultSet.close();
        statement.close();
        return result;
    }

    public Route getRoute(KR_ID kr_id) throws SQLException {
        Route result;
        try (PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_BY_KR_ID_SQL)) {
            statement.setInt(1, kr_id.id);
            try (ResultSet resultSet = statement.executeQuery()) {
                result = DbUtils.resultSetToRoute(resultSet);
            } catch (SQLException e) {
                return null;
            }
        }
        return result;
    }

    public List<Route> getRouteByTN(MR_ID mr_id) throws SQLException {
        List<Route> result;
        try (PreparedStatement statement = c.prepareStatement(SELECT_ROUTE_BY_TN_ID_SQL)) {
            statement.setInt(1, mr_id.id);
            statement.setInt(2, mr_id.transType);
            try (ResultSet resultSet = statement.executeQuery()) {
                result = DbUtils.resultSetToRouteDirections(resultSet);
            } catch (SQLException e) {
                return null;
            }
        }
        return result;
    }

    public DataBase(Connector connector) throws ClassNotFoundException, SQLException {
        c = connector.connect();
        errorCodes = connector.getErrorCodes();
        System.out.println("Opened database successfully");
    }

    public void createMonitoringTables() throws SQLException {
        Statement statement = c.createStatement();

        statement.executeUpdate(MonitoringEntry.CREATE_MONITORING_SQL);
        statement.executeUpdate(VehicleEntry.CREATE_TRANSPORT_SQL);
        statement.executeUpdate(RouteBindEntry.CREATE_ROUTES_BINDINGS_SQL);

        statement.close();
        c.commit();
    }

    public void createTimingsTables() throws SQLException {
        Statement statement = c.createStatement();

        statement.executeUpdate(ScheduleEntry.CREATE_SCHEDULE_SQL);
        statement.executeUpdate(ControlPointEntry.CREATE_CONTROL_POINTS_SQL);

        statement.close();
        c.commit();
    }

    public void createRealScheduleTable() throws SQLException {
        Statement statement = c.createStatement();

        statement.executeUpdate(RealScheduleEntry.CREATE_REAL_SCHEDULE_SQL);

        statement.close();
        c.commit();
    }

    public void insertScheduleData(List<ScheduleRecord> races) throws SQLException {
        PreparedStatement statement;
        int size = races.size();
        ScheduleRecord race;
        Table<Integer, Integer, String> controlPoints = HashBasedTable.create();
        Map<Integer, String> routeCP;

        statement = c.prepareStatement(ScheduleEntry.INSERT_TIMING_SQL);
        for (int i = 0; i < size; i++) {
            race = races.get(i);
            if (race.kr_id.id > 0) {
                controlPoints.put(race.controlPointId, race.kr_id.id, race.controlPointName);
            }
            try {
                DbUtils.fillScheduleStmt(statement, race);
            } catch (SQLException e) {
                statement = c.prepareStatement(ScheduleEntry.INSERT_TIMING_SQL);
                DbUtils.fillScheduleStmt(statement, race);
            }

            statement.execute();
        }
        statement.close();

        statement = c.prepareStatement(ControlPointEntry.INSERT_CONTROL_POINT_SQL);
        for (Integer cpID : controlPoints.rowKeySet()) {
            routeCP = controlPoints.row(cpID);
            for (Integer KR_ID : routeCP.keySet()) {
                statement.setInt(1, cpID);
                statement.setInt(2, KR_ID);
                statement.setString(3, routeCP.get(KR_ID));

                statement.execute();
            }
        }
        statement.close();

        System.out.println(controlPoints.size() + " control points added");
        c.commit();
    }

    private static final int BATCH_SIZE = 1000;

    public void insertMonitoringData(Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positions)
            throws SQLException {
        PreparedStatement statement = c.prepareStatement(MonitoringEntry.INSERT_MONITORING_SQL);
        int i = 0;
        for (Table.Cell<MR_ID, VehicleTN, List<MonitoringLogEntry>> transTypeMonitoringEntry :
                positions.cellSet()) {
            for (MonitoringLogEntry position : transTypeMonitoringEntry.getValue()) {
                try {
                    DbUtils.fillMonitoringStmt(statement, position);
                    statement.addBatch();
                } catch (SQLException e) {
                    statement.executeBatch();
                    statement.close();
                    statement = c.prepareStatement(MonitoringEntry.INSERT_MONITORING_SQL);
                    DbUtils.fillMonitoringStmt(statement, position);
                    statement.addBatch();
                }

                if (i % BATCH_SIZE == 0) {
                    try {
                        statement.executeBatch();
                        statement.clearBatch();
                    } catch (SQLException ex) {
//                ex.printStackTrace();

                        System.out.println(
                                (position.vehicleID + " " + position.vehicleID.transType) + " " +
                                Printer.getFormattedDate(Printer.fullDateFormat,
                                                         position.timestamp));
                    }
                }
                if (i % 100000 == 0) {
                    System.out.println(i);
                }
                i++;
            }
        }
        c.commit();
    }

    public void insertRawMonitoringData(List<MonitoringLogEntry> positions) throws SQLException {
        Map<VehicleTN, Vehicle> transport = new HashMap<>();
        Map<KR_ID, RouteBinding> routes = new HashMap<>();
        RouteBinding routeBinding;
//        MonitoringLogEntry position;
        int i = 0;

        PreparedStatement statement = c.prepareStatement(MonitoringEntry.INSERT_MONITORING_SQL);
        for (MonitoringLogEntry position : positions) {
            try {
                DbUtils.fillMonitoringStmt(statement, position);
            } catch (SQLException e) {
                statement.close();
                statement = c.prepareStatement(MonitoringEntry.INSERT_MONITORING_SQL);
                DbUtils.fillMonitoringStmt(statement, position);
            }

            try {
                statement.execute();
            } catch (SQLException ex) {
//                ex.printStackTrace();
                System.out.println(
                        "#" + i + " " + position.vehicleID.id + " " + position.vehicleID.transType +
                        " " +
                        position.timestamp);
            }

            routeBinding = routes.get(position.routeID);
            if (routeBinding == null) {
                routes.put(position.routeID,
                           new RouteBinding(position.routeID, position.routeTN,
                                            position.vehicleID.transType));
            } else {
                routeBinding.TN_IDs.add(position.routeTN);
            }
            if (position.vehicleID.id >= 0 && !transport.containsKey(position.vehicleID)) {
                transport.put(position.vehicleID,
                              new Vehicle(position.vehicleID, new KT_ID(position.kt_id),
                                          position.vehicleID.transType,
                                          position.boardNum));
            }

            if (i % 100000 == 0) {
                System.out.println(i);
            }
            i++;
        }
        statement.close();
        c.commit();

        insertVehicles(transport);
        insertRouteBindings(routes);
    }

    public void insertRouteBindingsTable(Table<MR_ID, KR_ID, Integer> routeIds) throws SQLException {
        int i = 0;
        MR_ID mr_id;
        PreparedStatement statement = c.prepareStatement(RouteBindEntry.INSERT_ROUTE_BINDING_SQL);
        for (Table.Cell<MR_ID, KR_ID, Integer> transTypeBindings : routeIds.cellSet()) {
            try {
                statement.setInt(1, transTypeBindings.getColumnKey().id);
                statement.setInt(2, (mr_id = transTypeBindings.getRowKey()).id);
                statement.setInt(3, mr_id.transType);
                statement.setInt(4, transTypeBindings.getValue());
            } catch (SQLException e) {
                statement = c.prepareStatement(RouteBindEntry.INSERT_ROUTE_BINDING_SQL);
                statement.setInt(1, transTypeBindings.getColumnKey().id);
                statement.setInt(2, (mr_id = transTypeBindings.getRowKey()).id);
                statement.setInt(3, mr_id.transType);
                statement.setInt(4, transTypeBindings.getValue());
            }

            try {
                statement.execute();
            } catch (SQLException e) {
                if (e.getErrorCode() != errorCodes.DUPLICATE) {
                    System.out.println("Binding insert error ");
                    e.printStackTrace();
                }
                i--;
            }

            i++;
        }
        statement.close();
        c.commit();
        System.out.println(i + " routes added");

    }

    public void insertRouteBindings(Map<KR_ID, RouteBinding> routes) throws SQLException {
        int i = 0;
        PreparedStatement statement = c.prepareStatement(RouteBindEntry.INSERT_ROUTE_BINDING_SQL);
        for (RouteBinding routeBinding : routes.values()) {
            statement.setInt(1, routeBinding.kr_id.id);
            statement.setInt(3, routeBinding.transType);
            for (Integer TN_ID : routeBinding.TN_IDs) {
                try {
                    statement = c.prepareStatement(RouteBindEntry.INSERT_ROUTE_BINDING_SQL);
                    statement.setInt(2, TN_ID);
                } catch (SQLException e) {
                    statement.setInt(2, TN_ID);
                }
                try {
                    statement.execute();
                } catch (SQLException e) {
                }
            }
            i += routeBinding.TN_IDs.size();
        }
        statement.close();
        c.commit();
        System.out.println(i + " routes added");
    }

    public void insertVehiclesTable(Table<VehicleTN, KT_ID, Vehicle> vehicles) throws SQLException {
        PreparedStatement statement = c.prepareStatement(VehicleEntry.INSERT_VEHICLES_SQL);
        int i = 0;
        VehicleTN vehicleTN;
        for (Map.Entry<VehicleTN, Map<KT_ID, Vehicle>> vehicleEntry : vehicles.rowMap().entrySet()) {
            try {
                statement.setInt(2, (vehicleTN = vehicleEntry.getKey()).id);
                statement.setInt(3, vehicleTN.transType);
            } catch (SQLException e) {
                statement = c.prepareStatement(VehicleEntry.INSERT_VEHICLES_SQL);
                statement.setInt(2, (vehicleTN = vehicleEntry.getKey()).id);
                statement.setInt(3, vehicleTN.transType);
            }
            for (Vehicle vehicle : vehicleEntry.getValue().values()) {
                try {
                    statement.setInt(1, vehicle.kt_id.id);
                    statement.setString(4, vehicle.boardNum);
                } catch (SQLException e) {
                    statement = c.prepareStatement(VehicleEntry.INSERT_VEHICLES_SQL);
                    statement.setInt(1, vehicle.kt_id.id);
                    statement.setInt(2, (vehicleTN = vehicleEntry.getKey()).id);
                    statement.setInt(3, vehicleTN.transType);
                    statement.setString(4, vehicle.boardNum);
                }

                try {
                    statement.execute();
                } catch (SQLException e) {
                    i--;
                    if (e.getErrorCode() != errorCodes.DUPLICATE) {
                        System.out.println("Vehicle insert error ");
                        e.printStackTrace();
                    }
                }
            }
            i += vehicleEntry.getValue().size();
        }


        statement.close();
        c.commit();
        System.out.println(i + " vehicles added");
    }

    public void insertVehicles(Map<VehicleTN, Vehicle> transport) throws SQLException {
        PreparedStatement statement = c.prepareStatement(VehicleEntry.INSERT_VEHICLES_SQL);
        for (Vehicle vehicle : transport.values()) {
            try {
                statement.setInt(1, vehicle.kt_id.id);
                statement.setInt(2, vehicle.vehicleTN.id);
                statement.setInt(3, vehicle.transType);
                statement.setString(4, vehicle.boardNum);
            } catch (SQLException e) {
                statement = c.prepareStatement(VehicleEntry.INSERT_VEHICLES_SQL);
                statement.setInt(1, vehicle.kt_id.id);
                statement.setInt(2, vehicle.vehicleTN.id);
                statement.setInt(3, vehicle.transType);
                statement.setString(4, vehicle.boardNum);
            }

            try {
                statement.execute();
            } catch (SQLException e) {
            }
        }
        statement.close();
        c.commit();
        System.out.println(transport.size() + " vehicles added");
    }

    public void dropMonitoring() throws SQLException {
        Statement statement = c.createStatement();

        statement.execute(MonitoringEntry.DROP_MONITORING_SQL);

        statement.close();
        c.commit();
    }

    public void dropRoutesBindings() throws SQLException {
        Statement statement = c.createStatement();

        statement.execute(RouteBindEntry.DROP_ROUTES_BINDINGS_SQL);

        statement.close();
        c.commit();
    }

    public void dropVehicles() throws SQLException {
        Statement statement = c.createStatement();

        statement.execute(VehicleEntry.DROP_VEHICLES_SQL);

        statement.close();
        c.commit();
    }

    public void dropTimings() throws SQLException {
        Statement statement = c.createStatement();

        statement.execute(ScheduleEntry.DROP_SCHEDULES_SQL);
        statement.execute(ControlPointEntry.DROP_CONTROL_POINTS_SQL);

        statement.close();
        c.commit();
    }

    public void dropRealSchedules() throws SQLException {
        Statement statement = c.createStatement();

        statement.execute(RealScheduleEntry.DROP_REAL_SCHEDULES_SQL);

        statement.close();
        c.commit();
    }

    public void close() throws SQLException {
        c.commit();
        c.close();
    }
}
