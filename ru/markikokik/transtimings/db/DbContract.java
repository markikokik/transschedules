package ru.markikokik.transtimings.db;


import ru.markikokik.transtimings.entities.TransType;

/**
 * Created by markikokik on 29.03.16.
 */
public interface DbContract {
    String ROUTE_TN_ENUM_ELEMENT = MonitoringEntry.ROUTE_TN + "=?";
    String ROUTE_TN_ENUM = "(%route_tn%)";
    String ROUTE_TN_ENUM_MARKER = "%route_tn%";


    String SELECT_ROUTE_STOPS_SQL =
            "select " + CircuitEntry.KS_ID + " from " + CircuitEntry.TABLE_NAME + " where " +
            CircuitEntry.KR_ID + "=? order by " + CircuitEntry.NUMBER;

//    String SELECT_ROUTE_CONTROL_POINTS_SQL = "select * from " + ControlPointEntry.TABLE_NAME +
//                                             " where " + ControlPointEntry.KR_ID + "=? and " +
//                                             ControlPointEntry.PERIOD_ID + "=?";

    String SELECT_ROUTE_CONTROL_POINTS_SQL =
            "SELECT " + ControlPointEntry.CP_ID + ", cp." + ControlPointEntry.KS_ID + " as " +
            ControlPointEntry.KS_ID + "," + ControlPointEntry.NAME + " FROM " +
            ControlPointEntry.TABLE_NAME + " " + "cp join " + CircuitEntry.TABLE_NAME + " c on cp." +
            ControlPointEntry.KS_ID + "=c." + CircuitEntry.KS_ID + " WHERE c." + CircuitEntry.KR_ID +
            "=? and c." + CircuitEntry.PERIOD_ID + "=? ORDER BY c." + CircuitEntry.NUMBER;

    String SELECT_ALL_STOPS_SQL = "select * from " + StopEntry.TABLE_NAME;

    String SELECT_STOPS_WITH_NULL_COORDS_SQL =
            "select * from " + StopEntry.TABLE_NAME + " where " + StopEntry.GEO_ID + " is null or " +
            StopEntry.X +
            " is null or " + StopEntry.Y + " is null or " + StopEntry.GEO_ID + "=0 or " + StopEntry.X +
            "=0 or " +
            StopEntry.Y + "=0";

    String SELECT_STOP_SQL =
            "select * from " + StopEntry.TABLE_NAME + " where " + StopEntry.KS_ID + "=?";

    String SELECT_ALL_ROUTES_SQL = "select * from " + RouteEntry.TABLE_NAME;

    String SELECT_ROUTE_BY_KR_ID_SQL =
            "select * from " + RouteEntry.TABLE_NAME + " where " + RouteEntry.KR_ID + "=? and " +
            RouteEntry.KR_ID + ">0";

    String SELECT_ROUTE_BY_TN_ID_SQL =
            "select * from " + RouteEntry.TABLE_NAME + " join " + RouteBindEntry.TABLE_NAME + " on " +
            RouteEntry.TABLE_NAME + "." + RouteEntry.KR_ID + "=" + RouteBindEntry.TABLE_NAME + "." +
            RouteBindEntry.KR_ID + " and " + RouteEntry.TABLE_NAME + "." + RouteEntry.KR_ID + ">0 and " +
            RouteBindEntry.TABLE_NAME + "." + RouteBindEntry.ROUTE_TN + "=? and " +
            RouteBindEntry.TABLE_NAME + "." + RouteBindEntry.TRANS_TYPE + "=?";

    String SELECT_ROUTES_BY_IDS_SQL =
            "select * from " + RouteEntry.TABLE_NAME + " where " + RouteEntry.KR_ID + " in (%kr_ids%)";
    String KR_IDS_ENUM_MARKER = "%kr_ids%";

    String SELECT_STOP_BY_ID_SQL = SELECT_ALL_STOPS_SQL + " where " + StopEntry.KS_ID + "=?";

    String SELECT_ALL_CONTROL_POINTS_SQL = "select * from " + ControlPointEntry.TABLE_NAME;

    String SELECT_ALL_CONTROL_POINTS_ORDERED_SQL =
            "SELECT " + CircuitEntry.KR_ID + "," + CircuitEntry.NUMBER + "," +
            ControlPointEntry.CP_ID + " FROM " + CircuitEntry.TABLE_NAME + " c left JOIN " +
            ControlPointEntry.TABLE_NAME + " cp on c." + CircuitEntry.KS_ID + "=cp." +
            ControlPointEntry.KS_ID + " WHERE " + CircuitEntry.PERIOD_ID + "=? " + "ORDER BY " +
            CircuitEntry.KR_ID + "," +
            CircuitEntry.NUMBER;

    String SELECT_CONTROL_POINT_ROUTES_SQL = "select " + CircuitEntry.KR_ID + " from "
                                             + CircuitEntry.TABLE_NAME + " WHERE " + CircuitEntry.KS_ID
                                             + " in (select " + ControlPointEntry.KS_ID
                                             + " from " + ControlPointEntry.TABLE_NAME
                                             + " WHERE " + ControlPointEntry.CP_ID + "=? and "
                                             + ControlPointEntry.PERIOD_ID + "=?) and "
                                             + CircuitEntry.PERIOD_ID + "=? GROUP BY " +
                                             CircuitEntry.KR_ID;


    String SELECT_CONTROL_POINT_STOPS_SQL = "select " + ControlPointEntry.KS_ID + " from "
                                            + ControlPointEntry.TABLE_NAME + " where "
                                            + ControlPointEntry.CP_ID + " = ? and "
                                            + ControlPointEntry.PERIOD_ID + " = ?";

    String SELECT_ACCIDENTS_SQL =
            "select * from " + AccidentEntry.TABLE_NAME + " where " + AccidentEntry.START_TIME +
            ">? and " + AccidentEntry.START_TIME + "<? order by " + AccidentEntry.NUMBER + "," +
            AccidentEntry.TRANS_TYPE + "," + AccidentEntry.VEHICLE_TN + "," + AccidentEntry.ROUTE_TN +
            "," +
            AccidentEntry.START_TIME + "," + AccidentEntry.KR_ID;

//    String SELECT_CONTROL_POINTS_DUPLICATES_SQL = "select * from (select " + ControlPointEntry.CP_ID +
//                                                  "," + ControlPointEntry.KS_ID + "," + ControlPointEntry.KR_ID +
//                                                  ",count(" + ControlPointEntry.KR_ID + ") as cnt from " +
//                                                  ControlPointEntry.TABLE_NAME + " group by " +
//                                                  ControlPointEntry.KR_ID + "," + ControlPointEntry.KS_ID +
//                                                  ") where cnt>1";

    String SELECT_ROUTE_DEVIATIONS =
            "select * from " + DeviationEntry.TABLE_NAME + " where " + DeviationEntry.KR_ID + "=? and " +
            DeviationEntry.PLAN_TIME + ">? and " + DeviationEntry.PLAN_TIME + "<? order by "
            + DeviationEntry.CP_ID + "," + DeviationEntry.PLAN_TIME;

    String SELECT_CONTROL_POINTS_DUPLICATES_SQL = "select " + ControlPointEntry.CP_ID +
                                                  ",s1." + ControlPointEntry.KR_ID + " as " +
                                                  ControlPointEntry.KR_ID +
                                                  ",s1." + ControlPointEntry.KS_ID + " as " +
                                                  ControlPointEntry.KS_ID +
                                                  " from " +
                                                  ControlPointEntry.TABLE_NAME +
                                                  " join (select " + ControlPointEntry.KS_ID + "," +
                                                  ControlPointEntry.KR_ID + ",count(" +
                                                  ControlPointEntry.KR_ID +
                                                  ") as cnt from " + ControlPointEntry.TABLE_NAME +
                                                  " group by " + ControlPointEntry.KR_ID + "," +
                                                  ControlPointEntry.KS_ID +
                                                  ") as s1 on cnt>1 and " +
                                                  ControlPointEntry.TABLE_NAME +
                                                  "." + ControlPointEntry.KS_ID + "=s1." +
                                                  ControlPointEntry.KS_ID +
                                                  " and " + ControlPointEntry.TABLE_NAME + "." +
                                                  ControlPointEntry.KR_ID +
                                                  "=s1." + ControlPointEntry.KR_ID + " order by s1." +
                                                  ControlPointEntry.KR_ID + ",s1." +
                                                  ControlPointEntry.KS_ID;

    String SELECT_ALL_CONTROL_POINTS_WITH_COORDS_SQL = "select " + ControlPointEntry.CP_ID + "," +
                                                       CircuitEntry.TABLE_NAME + "." +
                                                       CircuitEntry.KR_ID + "," +
                                                       StopEntry.TABLE_NAME + "." + StopEntry.KS_ID +
                                                       "," +
                                                       ControlPointEntry.NAME + "," + StopEntry.X + "," +
                                                       StopEntry.Y +
                                                       "," + CircuitEntry.DIST + " from (" +
                                                       ControlPointEntry.TABLE_NAME + " join " +
                                                       StopEntry.TABLE_NAME +
                                                       " join " + CircuitEntry.TABLE_NAME + " on " +
                                                       ControlPointEntry.TABLE_NAME + "." +
                                                       ControlPointEntry.KS_ID +
                                                       "=" + CircuitEntry.TABLE_NAME + "." +
                                                       CircuitEntry.KS_ID +
                                                       " and " + ControlPointEntry.TABLE_NAME + "." +
                                                       ControlPointEntry.KR_ID + "=" +
                                                       CircuitEntry.TABLE_NAME +
                                                       "." + CircuitEntry.KR_ID + " and " +
                                                       ControlPointEntry.TABLE_NAME + "." +
                                                       ControlPointEntry.KS_ID +
                                                       "=" + StopEntry.TABLE_NAME + "." +
                                                       StopEntry.KS_ID +
                                                       ")" + " group by " + ControlPointEntry.CP_ID +
                                                       "," +
                                                       CircuitEntry.TABLE_NAME + "." +
                                                       CircuitEntry.KR_ID + "," +
                                                       StopEntry.TABLE_NAME + "." + StopEntry.KS_ID
                                                       + " order by " + CircuitEntry.DIST;


    String SELECT_ROUTE_CONTROL_POINTS_WITH_COORDS_SQL = "select " + ControlPointEntry.CP_ID + "," +
                                                         CircuitEntry.TABLE_NAME + "." +
                                                         CircuitEntry.KR_ID + "," +
                                                         StopEntry.TABLE_NAME + "." +
                                                         ControlPointEntry.KS_ID + "," +
                                                         ControlPointEntry.NAME + "," + StopEntry.X +
                                                         "," +
                                                         StopEntry.Y + "," + CircuitEntry.DIST +
                                                         " from ((select * from " +
                                                         ControlPointEntry.TABLE_NAME +
                                                         " where " + ControlPointEntry.KR_ID +
                                                         "=? and " +
                                                         ControlPointEntry.PERIOD_ID +
                                                         "=?) as s1 join " +
                                                         StopEntry.TABLE_NAME + " join " +
                                                         CircuitEntry.TABLE_NAME +
                                                         " on s1." + ControlPointEntry.KS_ID + "=" +
                                                         CircuitEntry.TABLE_NAME + "." +
                                                         CircuitEntry.KS_ID +
                                                         " and s1." + ControlPointEntry.KR_ID + "=" +
                                                         CircuitEntry.TABLE_NAME + "." +
                                                         CircuitEntry.KR_ID +
                                                         " and s1." + ControlPointEntry.KS_ID + "=" +
                                                         StopEntry.TABLE_NAME + "." + StopEntry.KS_ID +
                                                         ") group by " +
                                                         ControlPointEntry.CP_ID + "," +
                                                         CircuitEntry.TABLE_NAME + "." +
                                                         CircuitEntry.KR_ID + "," +
                                                         StopEntry.TABLE_NAME + "." +
                                                         StopEntry.KS_ID + " order by " +
                                                         CircuitEntry.DIST;

    String SELECT_ROUTES_WITH_SCHEDULE_SQL = "select " + ControlPointEntry.KR_ID +
                                             " from " + ControlPointEntry.TABLE_NAME + " where " +
                                             ControlPointEntry.KR_ID + ">0 group by " +
                                             ControlPointEntry.KR_ID;

    String SELECT_DISTS_ON_ROUTES_WITH_SCHEDULE_SQL = "select cp." + ControlPointEntry.KR_ID + ",s." +
                                                      StopEntry.KS_ID + ",c." + CircuitEntry.DIST + "," +
                                                      StopEntry.X + "," + StopEntry.Y + " from " +
                                                      ControlPointEntry.TABLE_NAME + " as cp join " +
                                                      StopEntry.TABLE_NAME + " as s join " +
                                                      CircuitEntry.TABLE_NAME +
                                                      " as c on cp." + ControlPointEntry.KR_ID +
                                                      " >0 and c." +
                                                      CircuitEntry.KS_ID + "=s." + StopEntry.KS_ID +
                                                      " and c." +
                                                      CircuitEntry.KR_ID + "=cp." +
                                                      ControlPointEntry.KR_ID +
                                                      " and c." + CircuitEntry.PERIOD_ID + "=? and cp." +
                                                      ControlPointEntry.PERIOD_ID + "=c." +
                                                      CircuitEntry.PERIOD_ID +
                                                      " group by " + ControlPointEntry.KR_ID + "," +
                                                      StopEntry.KS_ID +
                                                      " order by cp." + ControlPointEntry.KR_ID;

    String SELECT_ROUTES_IDS_SQL = "select " + RouteEntry.KR_ID + " from " + RouteEntry.TABLE_NAME +
                                   " where " + RouteEntry.KR_ID + ">0 group by " + RouteEntry.KR_ID;

    String SELECT_ELECTRO_ROUTES_IDS_SQL =
            "select " + RouteEntry.KR_ID + "," + RouteEntry.TRANSPORT_TYPE_ID + " from " +
            RouteEntry.TABLE_NAME +
            " where " + RouteEntry.KR_ID + ">0 and " + RouteEntry.TRANSPORT_TYPE_ID +
            " in (" + TransType.TRAM + "," + TransType.TROLLEYBUS + ") group by " + RouteEntry.KR_ID
            + " order by " + RouteEntry.TRANSPORT_TYPE_ID;

    String SELECT_TRANSPORT_BY_KT_ID_SQL = "select * from " + VehicleEntry.TABLE_NAME + " where " +
                                           VehicleEntry.KT_ID + "=? and " +
                                           VehicleEntry.KT_ID + ">0";

    String SELECT_TRANSPORT_BY_TN_ID_SQL = "select * from " + VehicleEntry.TABLE_NAME + " where " +
                                           VehicleEntry.VEHICLE_TN + "=? and " +
                                           VehicleEntry.TRANS_TYPE + "=?";

    String SEARCH_TRANSPORT_BY_BOARD_NUM_SQL = "select * from " + VehicleEntry.TABLE_NAME + " where " +
                                               VehicleEntry.BOARD_NUMBER + " like '?'";

    String SELECT_CP_CANDIDATES_SQL = "select " + CircuitEntry.KS_ID + " from (" +
                                      "select " + CircuitEntry.KS_ID + ",count(" + CircuitEntry.KS_ID +
                                      ") as cnt,max(" + CircuitEntry.NUMBER + ") as num from " +
                                      CircuitEntry.TABLE_NAME +
                                      " where " + CircuitEntry.KR_ID +
                                      " in (select " + ScheduleEntry.KR_ID + " from " +
                                      ScheduleEntry.TABLE_NAME +
                                      " where " + ScheduleEntry.CONTROL_POINT_ID + "=? group by " +
                                      ScheduleEntry.KR_ID + ") group by " + CircuitEntry.KS_ID +
                                      ") where cnt>1 order by num";

    String SELECT_ALL_ROUTE_PERIODS_SQL =
            "select * from " + PeriodEntry.TABLE_NAME + " where " + PeriodEntry.KR_ID + "=? order by " +
            PeriodEntry.START;

    String SELECT_SUITABLE_ROUTE_PERIOD_SQL =
            "select * from " + PeriodEntry.TABLE_NAME + " where " + PeriodEntry.KR_ID + "=? and " +
            PeriodEntry.START +
            "<=? and " + PeriodEntry.END + ">=?";

    String SELECT_ALL_ROUTE_CIRCUITS_SQL =
            "select " + CircuitEntry.KR_ID + "," + CircuitEntry.PERIOD_ID + "," + CircuitEntry.KS_ID +
            " from " +
            CircuitEntry.TABLE_NAME + " where " + CircuitEntry.KR_ID + ">0 order by " +
            CircuitEntry.KR_ID + "," +
            CircuitEntry.PERIOD_ID + "," +
            CircuitEntry.NUMBER;

    String SELECT_ALL_ROUTE_CIRCUITS_COUNTS_SQL =
            "select " + CircuitEntry.KR_ID + "," + CircuitEntry.PERIOD_ID + ",count(" +
            CircuitEntry.KR_ID +
            ") from " + CircuitEntry.TABLE_NAME + " where " + CircuitEntry.KR_ID + ">0 group by " +
            CircuitEntry.KR_ID +
            "," + CircuitEntry.PERIOD_ID;

    String SELECT_ROUTE_CIRCUIT_SQL =
            "select " + CircuitEntry.KS_ID + " from " + CircuitEntry.TABLE_NAME +
            " where " + CircuitEntry.KR_ID + "=? and " + CircuitEntry.KR_ID + ">0 and " +
            CircuitEntry.PERIOD_ID + "=? order by " + CircuitEntry.NUMBER;

    String SELECT_RELATED_ROUTE_IDS_BY_KR_ID_SQL =
            "select " + RouteBindEntry.KR_ID + ",r." + RouteBindEntry.ROUTE_TN + ",r." +
            RouteBindEntry.TRANS_TYPE + " from " + RouteBindEntry.TABLE_NAME + " as r join (select "
            + RouteBindEntry.ROUTE_TN + "," + RouteBindEntry.TRANS_TYPE + " from " +
            RouteBindEntry.TABLE_NAME +
            " where " + RouteBindEntry.KR_ID + "=?) as s1 on r." + RouteBindEntry.ROUTE_TN + "=s1." +
            RouteBindEntry.ROUTE_TN + " and r." + RouteBindEntry.TRANS_TYPE + "=s1." +
            RouteBindEntry.TRANS_TYPE +
            " and r." + RouteBindEntry.KR_ID + ">0 and " + RouteBindEntry.TIME + "=?";

    String SELECT_RELATED_ROUTE_IDS_BY_TN_ID_SQL =
            "select r." + RouteBindEntry.KR_ID + ",r." + RouteBindEntry.ROUTE_TN + ",r." +
            RouteBindEntry.TIME + ",r." +
            RouteBindEntry.TRANS_TYPE + " from " + RouteBindEntry.TABLE_NAME + " as r join (select " +
            RouteBindEntry.KR_ID + " from " + RouteBindEntry.TABLE_NAME + " where " +
            RouteBindEntry.ROUTE_TN +
            "=? and " + RouteBindEntry.TRANS_TYPE + "=?) as s1 on r." + RouteBindEntry.KR_ID + "=s1." +
            RouteBindEntry.KR_ID + " and r." + RouteBindEntry.KR_ID + ">0 and r." + RouteBindEntry.TIME +
            "=?";

    String SELECT_ROUTE_REAL_SCHEDULE_BY_VEHICLE_SQL = "select " + RealScheduleEntry.VEHICLE_TN + "," +
                                                       RealScheduleEntry.TRANS_TYPE + "," +
                                                       RealScheduleEntry.CP_ID + "," +
                                                       RealScheduleEntry.TIME_ARR +
                                                       "," + RealScheduleEntry.TIME_DEP + " from " +
                                                       RealScheduleEntry.TABLE_NAME + " where " +
                                                       RealScheduleEntry.KR_ID + "=? and " +
                                                       RealScheduleEntry.TIME_ARR + ">? and " +
                                                       RealScheduleEntry.TIME_ARR + "<? order by " +
                                                       RealScheduleEntry.VEHICLE_TN + "," +
                                                       RealScheduleEntry.CP_ID + "," +
                                                       RealScheduleEntry.TIME_ARR;

    String SELECT_ROUTE_REAL_SCHEDULE_SQL = "select " + RealScheduleEntry.VEHICLE_TN + "," +
                                            RealScheduleEntry.TRANS_TYPE + "," +
                                            RealScheduleEntry.CP_ID + "," +
                                            RealScheduleEntry.TIME_ARR + "," +
                                            RealScheduleEntry.TIME_DEP + " from " +
                                            RealScheduleEntry.TABLE_NAME + " where " +
                                            RealScheduleEntry.KR_ID +
                                            "=? and " + RealScheduleEntry.TIME_ARR + ">? and " +
                                            RealScheduleEntry.TIME_ARR + "<? order by " +
                                            RealScheduleEntry.CP_ID +
                                            "," + RealScheduleEntry.TIME_ARR;

    String SELECT_ROUTE_CIRCUIT_DISTS_SQL = "select " + StopEntry.TABLE_NAME + "." + StopEntry.KS_ID +
                                            "," + CircuitEntry.DIST
                                            + "," + StopEntry.X + "," + StopEntry.Y
                                            + " from " +
                                            CircuitEntry.TABLE_NAME +
                                            " join " + StopEntry.TABLE_NAME + " on " +
                                            CircuitEntry.TABLE_NAME + "." +
                                            CircuitEntry.KS_ID + "=" + StopEntry.TABLE_NAME + "." +
                                            StopEntry.KS_ID +
                                            " and " + CircuitEntry.TABLE_NAME + "." +
                                            CircuitEntry.KR_ID + "=? and " +
                                            CircuitEntry.PERIOD_ID + "=?";

    String SELECT_ROUTE_CIRCUIT_DISTS_WITH_ORDER_SQL =
            "select " + CircuitEntry.KS_ID + "," + CircuitEntry.DIST + " from " +
            CircuitEntry.TABLE_NAME + " where " + CircuitEntry.KR_ID + "=? and " +
            CircuitEntry.PERIOD_ID + "=? order by " + CircuitEntry.NUMBER;

    String SELECT_ROUTE_CIRCUIT_COUNT_SQL = "select count(" + CircuitEntry.NUMBER + ") from " +
                                            CircuitEntry.TABLE_NAME + " where " + CircuitEntry.KR_ID +
                                            "=? and " +
                                            CircuitEntry.PERIOD_ID + "=?";

    String SELECT_ROUTE_GEOMETRY_SQL = "select " + GeometryEntry.X + "," + GeometryEntry.Y + " from "
                                       + GeometryEntry.TABLE_NAME + " where " + GeometryEntry.KR_ID +
                                       "=? and " + GeometryEntry.PERIOD_ID + "=? order by " +
                                       GeometryEntry.NUMBER;

    String SELECT_ROUTE_GEOMETRY_COUNT_SQL = "select count(" + GeometryEntry.NUMBER + ") from " +
                                             GeometryEntry.TABLE_NAME + " where " + GeometryEntry.KR_ID +
                                             "=? and " +
                                             GeometryEntry.PERIOD_ID + "=?";

    String SELECT_ROUTE_SCHEDULE_BY_VEHICLE_SQL =
            "select " + ScheduleEntry.KT_ID + "," + ScheduleEntry.CONTROL_POINT_ID +
            "," + ScheduleEntry.TIME + " from " + ScheduleEntry.TABLE_NAME + " where " +
            ScheduleEntry.KR_ID + "=? and " + ScheduleEntry.TIME + ">? and " + ScheduleEntry.TIME +
            "<? order by " +
            ScheduleEntry.KT_ID + "," + ScheduleEntry.CONTROL_POINT_ID + "," + ScheduleEntry.TIME;

    String SELECT_ROUTE_SCHEDULE_SQL =
            "select " + ScheduleEntry.KT_ID + "," + ScheduleEntry.CONTROL_POINT_ID +
            "," + ScheduleEntry.TIME + "," + ScheduleEntry.DIRECTION_TN + " from " +
            ScheduleEntry.TABLE_NAME + " where " + ScheduleEntry.KR_ID + "=? and " +
            ScheduleEntry.TIME + ">? and " + ScheduleEntry.TIME + "<? order by " +
            ScheduleEntry.CONTROL_POINT_ID + "," + ScheduleEntry.TIME;

    String SELECT_ROUTE_MONITORING_BY_KT_ID_SQL =
            "select " + MonitoringEntry.NEXT_STOP + "," + MonitoringEntry.STOP_DIST + "," +
            MonitoringEntry.X + "," +
            MonitoringEntry.Y + "," + MonitoringEntry.LAT + "," + MonitoringEntry.LNG + "," +
            MonitoringEntry.TIMESTAMP + "," + VehicleEntry.KT_ID + " from (select " +
            MonitoringEntry.NEXT_STOP + "," +
            MonitoringEntry.STOP_DIST + "," + MonitoringEntry.X + "," + MonitoringEntry.Y + "," +
            MonitoringEntry.LAT +
            "," + MonitoringEntry.LNG + "," + MonitoringEntry.TIMESTAMP + "," +
            MonitoringEntry.VEHICLE_TN + "," +
            MonitoringEntry.TRANS_TYPE + " from " + MonitoringEntry.TABLE_NAME + " where " +
            MonitoringEntry.KR_ID +
            "=? and " + MonitoringEntry.TIMESTAMP + ">? and " + MonitoringEntry.TIMESTAMP +
            "<?) as s1 join (select " +
            VehicleEntry.VEHICLE_TN + "," + VehicleEntry.KT_ID + "," + VehicleEntry.TRANS_TYPE +
            " from " +
            VehicleEntry.TABLE_NAME + ") as s2 on s1." + MonitoringEntry.VEHICLE_TN + "=s2." +
            VehicleEntry.VEHICLE_TN +
            " and s1." + MonitoringEntry.TRANS_TYPE + "=s2." + VehicleEntry.TRANS_TYPE + " order by " +
            VehicleEntry.KT_ID + "," + MonitoringEntry.TIMESTAMP;

    String SELECT_ROUTE_MONITORING_BY_TN_ID_SQL =
            "select " + MonitoringEntry.NEXT_STOP + "," + MonitoringEntry.STOP_DIST + "," +
            MonitoringEntry.X + "," +
            MonitoringEntry.Y + "," + MonitoringEntry.LAT + "," + MonitoringEntry.LNG + "," +
            MonitoringEntry.TIMESTAMP + "," + MonitoringEntry.VEHICLE_TN + " from " +
            MonitoringEntry.TABLE_NAME +
            " where " + MonitoringEntry.KR_ID + "=? and " + MonitoringEntry.TIMESTAMP + ">? and " +
            MonitoringEntry.TIMESTAMP + "<? order by " + MonitoringEntry.VEHICLE_TN + "," +
            MonitoringEntry.TIMESTAMP;


//    String SELECT_ROUTE_ALL_DIRECTIONS_MONITORING_SQL =
//            "select " + MonitoringEntry.X + "," + MonitoringEntry.Y + "," + MonitoringEntry.LAT + "," +
//            MonitoringEntry.LNG + "," + MonitoringEntry.TIMESTAMP + "," + MonitoringEntry.VEHICLE_TN + " from " +
//            MonitoringEntry.TABLE_NAME + " where " + ROUTE_TN_ENUM + " and " + MonitoringEntry.TRANS_TYPE +
//            "=? and " + MonitoringEntry.TIMESTAMP + ">? and " + MonitoringEntry.TIMESTAMP + "<? order by " +
//            MonitoringEntry.VEHICLE_TN + "," + MonitoringEntry.TIMESTAMP;

    String SELECT_ROUTE_ALL_DIRECTIONS_MONITORING_SQL =
            "select " + MonitoringEntry.STOP_DIST + "," + MonitoringEntry.NEXT_STOP + "," +
            MonitoringEntry.KR_ID +
            "," + MonitoringEntry.X + "," + MonitoringEntry.Y + "," + MonitoringEntry.LAT + "," +
            MonitoringEntry.LNG +
            "," + MonitoringEntry.TIMESTAMP + "," + MonitoringEntry.VEHICLE_TN + "," +
            MonitoringEntry.TRANS_TYPE +
            " from " + MonitoringEntry.TABLE_NAME + " where " + ROUTE_TN_ENUM + " and " +
            MonitoringEntry.TRANS_TYPE +
            "=? and " + MonitoringEntry.TIMESTAMP + ">? and " + MonitoringEntry.TIMESTAMP +
            "<? order by " +
            MonitoringEntry.VEHICLE_TN + "," + MonitoringEntry.TIMESTAMP;

    String SELECT_DOUBLES_IN_TRANSPORT_SQL = "select * from " + VehicleEntry.TABLE_NAME +
                                             " where " + VehicleEntry.VEHICLE_TN + " in (select " +
                                             VehicleEntry.VEHICLE_TN + " from (select count(" +
                                             VehicleEntry.BOARD_NUMBER + ") as cnt," +
                                             VehicleEntry.VEHICLE_TN +
                                             " from " + VehicleEntry.TABLE_NAME + " group by " +
                                             VehicleEntry.VEHICLE_TN + ") where cnt>1)";
    String SELECT_ROUTE_CP_SCHEDULE =
            "select " + ScheduleEntry.CONTROL_POINT_ID + " from " + ScheduleEntry.TABLE_NAME +
            " where " + ScheduleEntry.CONTROL_POINT_ID + "=? and " + ScheduleEntry.KR_ID +
            "=?";

    String SELECT_MU =
            "SELECT " + VehicleEntry.VEHICLE_TN + " FROM " + VehicleEntry.TABLE_NAME +
            " WHERE " + VehicleEntry.TRANS_TYPE +
            "=3 and " + VehicleEntry.BOARD_NUMBER +
            " in (744,759,763,779,771,773,777,781,783,786,790,793,761,767" +
            "800,802,812,814,827,838,832,840,846,848,870,872,876,880,882,878,887,893,895,897,899," +
            "917,907,905,903,909,911,915,919,921,923,926,939,941,943,945,947,949,951,953,955," +
            "1003,1005,1007,1009,1011,1013,1015,1017,1019,1021,1023,1025,1027,1029,1031,1033,1035," +
            "1041,1043,1045,1047,1049,1051,1053,1055,1057,1059,1061,1063,1065,1067,1069," +
            "1071,1073,1075,1077,1079,1081,1083," +
            "1163,1165,1169,1171,1175,1183,1185,1189,1193,1195,1197,1199," +
            "1201,1203,1215," +
            "2002,2027,2029,2037,2047,2051,2053,2055,2061,2063,2071,2081,2085,2089,2097," +
            "2103,2123, 2125,2127,2129,2135,2137,2141,2143,2145,2149,2152,2155,2166,2169,2175,2177," +
            "2180,2182, 2184,2186,2188,2190,2194,2196,2198,2200,2202,2205,2207,2209,2211,2214,2192)" +
            " GROUP BY " + VehicleEntry.VEHICLE_TN;

    interface MonitoringEntry {
        String TABLE_NAME = "MONITORING",
                ROUTE_TN = "ROUTE_TN",
                KR_ID = "KR_ID",
                VEHICLE_TN = "VEHICLE_TN",
                TRANS_TYPE = "TRANS_TYPE",
                NEXT_STOP = "NEXT_STOP",
                LAT = "LAT",
                LNG = "LNG",
                X = "X",
                Y = "Y",
                STOP_DIST = "STOP_DIST",
                TIMESTAMP = "TIMESTAMP",

        INSERT_MONITORING_SQL = "insert into " + TABLE_NAME + "(" +
                                ROUTE_TN + "," +
                                KR_ID + "," +
                                VEHICLE_TN + "," +
                                TRANS_TYPE + "," +
                                NEXT_STOP + "," +
                                STOP_DIST + "," +
                                LAT + "," +
                                LNG + "," +
                                X + "," +
                                Y + "," +
                                TIMESTAMP +
                                //                                   MonitoringEntry.STATUS + "," +
                                ") values (?,?,?,?,?,?,?,?,?,?,?)",

        UPDATE_MONITORING_WITH_NEW_KR_IDS_SQL = "update " + TABLE_NAME + " set " +
                                                MonitoringEntry.KR_ID + "=?," +
                                                MonitoringEntry.NEXT_STOP +
                                                "=?," +
                                                MonitoringEntry.STOP_DIST + "=? where " +
                                                MonitoringEntry.VEHICLE_TN + "=? and " +
                                                MonitoringEntry.TRANS_TYPE +
                                                "=? " + MonitoringEntry.TIMESTAMP + "=?",

        CREATE_MONITORING_SQL = "create table if not exists " + TABLE_NAME + "(" +
                                MonitoringEntry.ROUTE_TN + " int," +
                                MonitoringEntry.KR_ID + " int," +
                                MonitoringEntry.VEHICLE_TN + " int," +
                                MonitoringEntry.TRANS_TYPE + " int," +
                                MonitoringEntry.NEXT_STOP + " int," +
                                MonitoringEntry.STOP_DIST + " float," +
                                MonitoringEntry.LAT + " float," +
                                MonitoringEntry.LNG + " float," +
                                MonitoringEntry.X + " float," +
                                MonitoringEntry.Y + " float," +
                                MonitoringEntry.TIMESTAMP + " int," +
                                "primary key (" + MonitoringEntry.VEHICLE_TN + "," +
                                MonitoringEntry.TIMESTAMP +
                                "," +
                                MonitoringEntry.TRANS_TYPE + "))",

        DELETE_ROUTE_MONITORING_SQL =
                "delete from " + TABLE_NAME + " where " + MonitoringEntry.TRANS_TYPE + "=? and " +
                ROUTE_TN_ENUM,

        DELETE_VEHICLE_ON_ROUTE_MONITORING_SQL = "delete from " + TABLE_NAME + " where " +
                                                 MonitoringEntry.VEHICLE_TN + "=? and " +
                                                 MonitoringEntry.TRANS_TYPE +
                                                 "=? and " + MonitoringEntry.ROUTE_TN + " in " +
                                                 ROUTE_TN_ENUM,

        DROP_MONITORING_SQL = "drop table if exists " + TABLE_NAME;

    }

    interface VehicleEntry {
        String TABLE_NAME = "TRANSPORT",
                VEHICLE_TN = "VEHICLE_TN",
                KT_ID = "KT_ID",
                TRANS_TYPE = "TRANS_TYPE",
                BOARD_NUMBER = "BOARD_NUMBER",

        DELETE_DOUBLES_IN_TRANSPORT =
                "delete from " + TABLE_NAME + " where " + VehicleEntry.KT_ID + "=?",

        DROP_VEHICLES_SQL = "drop table if exists " + TABLE_NAME,

        INSERT_VEHICLES_SQL = "insert into " + TABLE_NAME + "(" +
                              KT_ID + "," +
                              VEHICLE_TN + "," +
                              TRANS_TYPE + "," +
                              BOARD_NUMBER +
                              ") values (?,?,?,?)",

        CREATE_TRANSPORT_SQL = "create table if not exists " + TABLE_NAME + "(" +
                               KT_ID + " int," +
                               VEHICLE_TN + " int," +
                               TRANS_TYPE + " int," +
                               BOARD_NUMBER + " varchar(10)," +
                               "primary key (" + VEHICLE_TN + "," + KT_ID + "))";
    }

    interface GeometryEntry {
        String TABLE_NAME = "route_geometry",
                KR_ID = "KR_ID",
                NUMBER = "NUMBER",
                PERIOD_ID = "PERIOD_ID",
                X = "X",
                Y = "Y",

        INSERT_GEOMETRY_SQL = "insert into " + TABLE_NAME + "(" +
                              KR_ID + "," +
                              NUMBER + "," +
                              PERIOD_ID + "," +
                              X + "," +
                              Y +
                              ") values (?,?,?,?,?)",

        CREATE_ROUTE_GEOMETRY_SQL = "create table if not exists " + TABLE_NAME + "(" +
                                    KR_ID + " int," +
                                    NUMBER + " int," +
                                    X + " real," +
                                    Y + " real," +
                                    PERIOD_ID + " tinyint(3)," +
                                    "primary key (" + KR_ID + "," + NUMBER + "," +
                                    PERIOD_ID + "))";
    }


    interface PeriodEntry {
        String TABLE_NAME = "periods",
                ID = "ID",
                KR_ID = "KR_ID",
                START = "STARTTIME",
                END = "ENDTIME",
                COMMENT = "COMMENT";
    }

    interface ControlPointEntry {
        String TABLE_NAME = "control_points_old",
                CP_ID = "CP_ID",
                KS_ID = "KS_ID",
                KR_ID = "KR_ID",
                NAME = "NAME",
                PERIOD_ID = "PERIOD_ID",

        DELETE_CONTROL_POINT_SQL =
                "delete from " + TABLE_NAME + " where " + ControlPointEntry.CP_ID + "=? and " +
                ControlPointEntry.KR_ID + "=?",

        DROP_CONTROL_POINTS_SQL = "drop table if exists " + TABLE_NAME,

        UPDATE_CONTROL_POINT_KS_ID_SQL = "update " + TABLE_NAME + " set " +
                                         KS_ID + "=? where " + CP_ID + "=? and " +
                                         KR_ID + "=? and " + PERIOD_ID +
                                         "=?",

        INSERT_CONTROL_POINT_SQL = "insert into " + TABLE_NAME + "(" +
                                   CP_ID + "," +
                                   KR_ID + "," +
                                   NAME + "," +
                                   PERIOD_ID +
                                   ") values (?,?,?,?)",

        CREATE_CONTROL_POINTS_SQL = "create table if not exists " + TABLE_NAME + "(" +
                                    CP_ID + " int," +
                                    KS_ID + " int," +
                                    KR_ID + " int," +
                                    NAME + " int," +
                                    PERIOD_ID + " tinyint(3)," +
                                    "primary key (" + CP_ID + "," + KR_ID +
                                    "," + PERIOD_ID + "))";

    }

    interface CircuitEntry {
        String TABLE_NAME = "circuits",
                KR_ID = "KR_ID",
                KS_ID = "KS_ID",
                NUMBER = "NUMBER",
                PERIOD_ID = "PERIOD_ID",
                DIST = "DIST",

        INSERT_CIRCUIT_SQL = "insert into " + TABLE_NAME + "(" +
                             KR_ID + "," +
                             KS_ID + "," +
                             NUMBER + "," +
                             PERIOD_ID + "," +
                             DIST +
                             ") values (?,?,?,?,?)",

        UPDATE_CIRCUIT_WITH_MILEAGE_SQL = "update " + TABLE_NAME + " set " +
                                          DIST + "=? where " + KR_ID + "=? and " +
                                          PERIOD_ID + "=? and " + NUMBER + "=?",

        ALTER_CIRCUITS_WITH_DIST_SQL = "alter table " + TABLE_NAME + " add " + DIST +
                                       " real";

    }

    interface AccidentEntry {
        String TABLE_NAME = "accidents",
                NUMBER = "NUMBER",
                KR_ID = "KR_ID",
                ROUTE_TN = "ROUTE_TN",
                VEHICLE_TN = "VEHICLE_TN",
                TRANS_TYPE = "TRANS_TYPE",
                START_TIME = "START_TIME",
                END_TIME = "END_TIME",
                X = "X",
                Y = "Y",

        DROP_ACCIDENTS_SQL = "drop table if exists " + TABLE_NAME,

        CREATE_ACCIDENTS_SQL = "create table if not exists " + TABLE_NAME + "(" +
                               NUMBER + " int," +
                               KR_ID + " int," +
                               ROUTE_TN + " int," +
                               VEHICLE_TN + " int," +
                               TRANS_TYPE + " int," +
                               START_TIME + " int," +
                               END_TIME + " int," +
                               X + " float," +
                               Y + " float," +
                               "primary key (" + VEHICLE_TN + "," + TRANS_TYPE + "," +
                               START_TIME + "))",

        INSERT_ACCIDENT_SQL = "insert into " + TABLE_NAME + "(" +
                              NUMBER + "," +
                              ROUTE_TN + "," +
                              KR_ID + "," +
                              VEHICLE_TN + "," +
                              TRANS_TYPE + "," +
                              START_TIME + "," +
                              END_TIME + "," +
                              X + "," +
                              Y +
                              ") values (?,?,?,?,?,?,?,?,?)";
    }

    interface StationStayEntry {
        String TABLE_NAME = "stays",
                STATION_ID = "STATION",
                ROUTE_TN = "ROUTE_TN",
                VEHICLE_TN = "VEHICLE_TN",
                TRANS_TYPE = "TRANS_TYPE",
                ARRIVE_TIME = "ARRIVE",
                DEPART_TIME = "DEPART",

        INSERT_STAY_SQL = "insert into " + TABLE_NAME + "(" +
                          STATION_ID + "," +
                          ROUTE_TN + "," +
                          VEHICLE_TN + "," +
                          TRANS_TYPE + "," +
                          ARRIVE_TIME + "," +
                          DEPART_TIME +
                          ") values (?,?,?,?,?,?)",

        SELECT_STAYS_SQL = "select * from " + TABLE_NAME + " ? order by " + STATION_ID + "," +
                           ROUTE_TN + "," + VEHICLE_TN,

        CREATE_STAYS_SQL = "create table if not exists " + TABLE_NAME + "(" +
                           STATION_ID + " int," +
                           ROUTE_TN + " int," +
                           VEHICLE_TN + " int," +
                           TRANS_TYPE + " int," +
                           ARRIVE_TIME + " int," +
                           DEPART_TIME + " int," +
                           "primary key (" + VEHICLE_TN + "," + TRANS_TYPE + "," +
                           ARRIVE_TIME + "))";
    }

    interface DistribEntry {
        String TABLE_NAME = "distribution",
                VEHICLE_TN = "VEHICLE_TN",
                TRANS_TYPE = "TRANS_TYPE",
                ROUTE_TN = "ROUTE_TN",
                START_TIME = "START_TIME",
                END_TIME = "END_TIME",

        INSERT_DISTRIB_SQL = "insert into " + TABLE_NAME + "(" +
                             VEHICLE_TN + "," +
                             TRANS_TYPE + "," +
                             ROUTE_TN + "," +
                             START_TIME + "," +
                             END_TIME +
                             ") values (?,?,?,?,?)",

        CREATE_DISTRIB_SQL = "create table if not exists " + TABLE_NAME + "(" +
                             VEHICLE_TN + " int," +
                             TRANS_TYPE + " int," +
                             ROUTE_TN + " int," +
                             START_TIME + " int," +
                             END_TIME + " int," +
                             "primary key (" + VEHICLE_TN + "," + TRANS_TYPE + "," +
                             START_TIME + "))";
    }

    interface StopEntry {
        String TABLE_NAME = "stops",
                KS_ID = "KS_ID",
                TITLE = "title",
                ADJACENT_STREET = "adjacentStreet",
                DIRECTION = "direction",
                BUSES_MUNICIPAL = "busesMunicipal",
                BUSES_COMMERCIAL = "busesCommercial",
                BUSES_PRIGOROD = "busesPrigorod",
                BUSES_SEASON = "busesSeason",
                BUSES_SPECIAL = "busesSpecial",
                TRAMS = "trams",
                TROLLEYBUSES = "trolleybuses",
                LATITUDE = "latitude",
                LONGITUDE = "longitude",
                GEO_ID = "Geoportal_ID",
                X = "X",
                Y = "Y",

        UPDATE_STOP_GEOID_XY_SQL = "update " + TABLE_NAME + " set " +
                                   GEO_ID + "=?, " + X + "=?, " + Y + "=? where " +
                                   KS_ID + "=?",

        INSERT_STOP_SQL = "insert into " + TABLE_NAME + "(" +
                          KS_ID + "," +
                          TITLE + "," +
                          ADJACENT_STREET + "," +
                          DIRECTION + "," +
                          BUSES_MUNICIPAL + "," +
                          BUSES_COMMERCIAL + "," +
                          BUSES_PRIGOROD + "," +
                          BUSES_SEASON + "," +
                          BUSES_SPECIAL + "," +
                          TRAMS + "," +
                          TROLLEYBUSES + "," +
                          LATITUDE + "," +
                          LONGITUDE + "," +
                          GEO_ID + "," +
                          X + "," +
                          Y +
                          ") values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    }

    interface RouteEntry {
        String TABLE_NAME = "routes",
                KR_ID = "KR_ID",
                NUMBER = "number",
                AFFILIATION_ID = "affiliationID",
                TRANSPORT_TYPE_ID = "transportTypeID",
                DIRECTION = "direction",
                GEOPORTAL_ID = "geoportalID",
                LAYER_NAME = "layerName",

        INSERT_ROUTE_SQL = "insert into " + TABLE_NAME + "(" +
                           KR_ID + "," +
                           NUMBER + "," +
                           AFFILIATION_ID + "," +
                           TRANSPORT_TYPE_ID + "," +
                           DIRECTION + "," +
                           GEOPORTAL_ID + "," +
                           LAYER_NAME +
                           ") values (?,?,?,?,?,?,?)";
    }

    interface ScheduleEntry {
        String TABLE_NAME = "schedule",
                KR_ID = "KR_ID",
                ROUTE_ID_TN = "ROUTE_ID_TN",
                KT_ID = "KT_ID",
                RACE_NUMBER = "RACE_NUMBER",
                CONTROL_POINT_ID = "CONTROL_POINT_ID",
                DIRECTION = "DIRECTION",
                DIRECTION_TN = "DIRECTION_TN",
                TIME = "TIME",
                TRANS_TYPE_TN = "TRANS_TYPE_TN",

        UPDATE_SCHEDULE_CP_SQL =
                "update " + TABLE_NAME + " set " + ScheduleEntry.CONTROL_POINT_ID + "=? where " +
                ScheduleEntry.CONTROL_POINT_ID + "=? and " + ScheduleEntry.KR_ID + "=?",

        UPDATE_SCHEDULES_WITH_NEW_KT_IDS_SQL = "update " + TABLE_NAME + " set " +
                                               ScheduleEntry.KT_ID + "=? where " + ScheduleEntry.KT_ID +
                                               "=?",

        DELETE_SCHEDULE_CP_SQL =
                "delete from " + TABLE_NAME + " set " + ScheduleEntry.CONTROL_POINT_ID + "=? where " +
                ScheduleEntry.CONTROL_POINT_ID + "=? and " + ScheduleEntry.KR_ID + "=?",

        DROP_SCHEDULES_SQL = "drop table if exists " + TABLE_NAME,

        INSERT_TIMING_SQL = "insert into " + TABLE_NAME + "(" +
                            KT_ID + "," +
                            KR_ID + "," +
                            RACE_NUMBER + "," +
                            CONTROL_POINT_ID + "," +
                            TIME + "," +
                            DIRECTION + "," +
                            DIRECTION_TN + "," +
                            ROUTE_ID_TN + "," +
                            TRANS_TYPE_TN +
                            ") values (?,?,?,?,?,?,?,?,?)",

        CREATE_SCHEDULE_SQL = "create table if not exists " + TABLE_NAME + "(" +
                              KT_ID + " int," +
                              KR_ID + " int," +
                              RACE_NUMBER + " int," +
                              CONTROL_POINT_ID + " int," +
                              TIME + " int," +
                              DIRECTION + " varchar(1)," +
                              DIRECTION_TN + " varchar(1)," +
                              ROUTE_ID_TN + " int," +
                              TRANS_TYPE_TN + " int," +
                              "primary key (" +
                              KT_ID + "," +
                              KR_ID + "," +
                              RACE_NUMBER + "," +
                              CONTROL_POINT_ID + "," +
                              TIME + "," +
                              DIRECTION + "," +
                              DIRECTION_TN + "," +
                              ROUTE_ID_TN + "," +
                              TRANS_TYPE_TN +
                              "))";

    }

    interface DeviationEntry {
        String TABLE_NAME = "deviations",
                KR_ID = "KR_ID",
                CP_ID = "CP_ID",
                DIRECTION_TN = "DIRECTION_TN",
                KT_ID_PLAN = "KT_ID_PLAN",
                KT_ID_REAL = "KT_ID_REAL",
                PLAN_TIME = "PLAN_TIME",
                DEVIATION = "DEVIATION",

        DROP_DEVIATIONS_SQL = "drop table if exists " + TABLE_NAME,

        INSERT_DEVIATION_SQL = "insert into " + TABLE_NAME + "(" +
                               KR_ID + "," +
                               CP_ID + "," +
                               DIRECTION_TN + "," +
                               PLAN_TIME + "," +
                               DEVIATION + "," +
                               KT_ID_PLAN + "," +
                               KT_ID_REAL +
                               ") values (?,?,?,?,?,?,?)",

        CREATE_DEVIATIONS_SQL = "create table if not exists " + TABLE_NAME + "(" +
                                KR_ID + " int," +
                                CP_ID + " int," +
                                DIRECTION_TN + " varchar(1)," +
                                PLAN_TIME + " int," +
                                DEVIATION + " int," +
                                KT_ID_PLAN + " int," +
                                KT_ID_REAL + " int," +
                                "primary key (" +
                                KR_ID + "," +
                                CP_ID + "," +
                                PLAN_TIME + "," +
                                KT_ID_PLAN + "," +
                                DIRECTION_TN + "))";
    }

    interface RealScheduleEntry {
        String TABLE_NAME = "real_schedule",
                KR_ID = "KR_ID",
                VEHICLE_TN = "VEHICLE_TN",
                TRANS_TYPE = "TRANS_TYPE",
                CP_ID = "CP_ID",
                TIME_ARR = "arrival",
                TIME_DEP = "depart",

        CREATE_REAL_SCHEDULE_SQL = "create table if not exists " + TABLE_NAME + "(" +
                                   KR_ID + " int," +
                                   VEHICLE_TN + " int," +
                                   TRANS_TYPE + " int," +
                                   CP_ID + " int," +
                                   TIME_ARR + " int," +
                                   TIME_DEP + " int," +
                                   "primary key (" +
                                   KR_ID + "," +
                                   VEHICLE_TN + "," +
                                   TRANS_TYPE + "," +
                                   CP_ID + "," +
                                   TIME_ARR + "," +
                                   TIME_DEP +
                                   "))",

        INSERT_REAL_SCHEDULE_SQL = "insert into " + TABLE_NAME + "(" +
                                   KR_ID + "," +
                                   VEHICLE_TN + "," +
                                   TRANS_TYPE + "," +
                                   CP_ID + "," +
                                   TIME_ARR + "," +
                                   TIME_DEP +
                                   ") values (?,?,?,?,?,?)",

//        UPDATE_REAL_SCHEDULE_WITH_NEW_KT_IDS_SQL = "update " + TABLE_NAME + " set " +
//                                                   RealScheduleEntry.KT_ID + "=? where " +
//                                                   RealScheduleEntry.KT_ID +
//                                                   "=?",
//
//        DELETE_DOUBLES_IN_REAL_SCHEDULE =
//                "delete from " + TABLE_NAME + " where " + RealScheduleEntry.KT_ID + "=?",

        DROP_REAL_SCHEDULES_SQL = "drop table if exists " + TABLE_NAME;

    }

    interface RouteBindEntry {
        String TABLE_NAME = "ROUTES_BIND",
                ROUTE_TN = "ROUTE_TN",
                KR_ID = "KR_ID",
                TRANS_TYPE = "TRANS_TYPE",
                TIME = "TIME",

        INSERT_ROUTE_BINDING_SQL = "insert into " + TABLE_NAME + "(" +
                                   RouteBindEntry.KR_ID + "," +
                                   ROUTE_TN + "," +
                                   TRANS_TYPE + "," +
                                   TIME +
                                   ") values (?,?,?,?)",

        CREATE_ROUTES_BINDINGS_SQL = "create table if not exists " + TABLE_NAME + "(" +
                                     KR_ID + " int," +
                                     RouteBindEntry.ROUTE_TN + " int," +
                                     RouteBindEntry.TRANS_TYPE + " int," +
                                     RouteBindEntry.TIME + " int," +
                                     "primary key (" + KR_ID + "," + RouteBindEntry.ROUTE_TN + "," +
                                     RouteBindEntry.TIME +
                                     "))",

        DROP_ROUTES_BINDINGS_SQL = "drop table if exists " + TABLE_NAME;
    }


}
