package ru.markikokik.transtimings.db;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ru.markikokik.transtimings.Utils;
import ru.markikokik.transtimings.db.connectors.Connector;
import ru.markikokik.transtimings.entities.ControlPoint;
import ru.markikokik.transtimings.entities.ControlPointBinding;
import ru.markikokik.transtimings.entities.Period;
import ru.markikokik.transtimings.entities.Route;
import ru.markikokik.transtimings.entities.Stop;
import ru.markikokik.transtimings.entities.TransType;
import ru.markikokik.transtimings.entities.ids.CP_ID;
import ru.markikokik.transtimings.entities.ids.KR_ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;
import ru.markikokik.transtimings.entities.ids.PeriodID;
import ru.markikokik.transtimings.entities.ids.TransTypeID;

/**
 * Created by markikokik on 20.05.16.
 */
public class Complementator {
    private static String regex = "[^0-9А-ЯA-Z]";

    private static String[][] replaceBeforeRegex = new String[][]{{"Ё", "Е"},
                                                                  {"ИМ\\.", ""},
                                                                  {"БТЭЦ", "БезымянскаяТЭЦ"},
                                                                  {"УЛИЦА", "УЛ"},
                                                                  {"ПЕРЕУЛОК", "ПЕР"},
                                                                  {"ПРОСПЕКТ", "ПР"},
                                                                  {"ПЛОЩАДЬ", "ПЛ"},
                                                                  {"XXII", "22"},
                                                                  {"МИКРОРАЙОН", "МКР"},
                                                                  {"СТД", "Северноетрамвайноедепо"},
                                                                  {"Поликлиника№6".toUpperCase(),
                                                                   "Железнодорожный вокзал"},
                                                                  {"/ул.Физкультурная".toUpperCase(),
                                                                   ""},
                                                                  {"ТТЦ", ""},
                                                                  {"ТЦ", ""},
                                                                  {"КРЦ", ""},
                                                                  {"ТРК", ""},
                                                                  {"-Й", ""},
                                                                  {"АВТОСТАНЦИЯ", "а/с"},
                                                                  {"Мегамебель-1".toUpperCase(),
                                                                   "Мегамебель"},
                                                                  {"(стоянка)".toUpperCase(), ""},
                                                                  {"(ул.Поселковая)".toUpperCase(), ""},
                                                                  {"(Железнодорожный вокзал)".toUpperCase(),
                                                                   ""},
                                                                  {"Поликлиника/".toUpperCase(), ""},
                                                                  {"ПОСЕЛОК", "ПОС"}
    };

    private static String[][] replaceAfterRegex =
            new String[][]{{"Станция1".toUpperCase(), "улТухачевского"},
                           {"Станция6".toUpperCase(), "БарбошинаПоляна"},
                           {"Станция2".toUpperCase(), "ПостниковОвраг"},
                           {"оврагПодпольщиков".toUpperCase(), "ПостниковОвраг"},
                           {"Педагогическийуниверситет".toUpperCase(),
                            "Педуниверситет"},
                           {"МагазинМебель".toUpperCase(), "МебельныйМагазин"},
                           {"Станцияметро".toUpperCase(), "стм"},
                           {"Ильинскаяпл".toUpperCase(),
                            "улБратьевКоростелевых"},
                           {"МОСКОВСКИЙ", "18КМ"},
                           {"ПроходнаяОАОМоторостроитель".toUpperCase(),
                            "заводскоеШоссе"},
                           {"ОАОМоторостроитель".toUpperCase(),
                            "заводскоеШоссе"}};

    private static int[][] manualBindings = {
            //KR_ID, CP_ID, KS_ID
            //Овраг для 23
            {107, 10200, 161},
            {107, 10513, 169},
            {107, 10355, 796},
            //Моторостроитель для 2, 3, 12, 19
            //Б
            {98, 10092, 1171},
            {115, 10092, 1171},
            {115, 10096, 1171},
//            {96, 10092, 1171},
            {612, 10092, 1171},
            //А
            {97, 10096, 244},
            {114, 10092, 244},
            {613, 10096, 244},
            //Ставропольская/НВ 7, 19 А
            {95, 10105, 217},
            {119, 10105, 217},
            //15 мкр Б для 5
            {117, 10550, 261},
            //Ставропольская нуль для кировских 13
            {86, 10103, 272}
    };

    public static String reduceStopName(String name) {
        for (int length = 0; length != name.length();
             name = name.replaceAll(" ", "")) {
            length = name.length();
        }

        name = name.toUpperCase();
        for (int i = 0; i < replaceBeforeRegex.length; i++) {
            name = name.replaceAll(replaceBeforeRegex[i][0], replaceBeforeRegex[i][1]);
        }

        name = name.toUpperCase().replaceAll(regex, "");

        for (int i = 0; i < replaceAfterRegex.length; i++) {
            name = name.replaceAll(replaceAfterRegex[i][0], replaceAfterRegex[i][1]);
        }
        return name.toUpperCase();
    }

    public static void bindControlPoints(Connector connector) {
        try (DataBase db = new DataBase(connector)) {
            Set<KR_ID> routesWithSchedule = db.getRoutesWithSchedule();
            Table<KR_ID, CP_ID, ControlPoint> controlPoints = db.getAllControlPoints();
            Map<KS_ID, Stop> stops = db.getAllStops();
            Map<KR_ID, Route> routes = db.getAllRoutes();
            List<KS_ID> routeStops;
            Map<CP_ID, ControlPoint> routeCPs;

            ControlPoint point;
            Route route;
            Stop stop;
            int count = 0;
            List<ControlPointBinding> updatedPoints = new ArrayList<>();

            for (Stop tempStop : stops.values()) {
                tempStop.title = reduceStopName(tempStop.title);
            }

            for (ControlPoint controlPoint : controlPoints.values()) {
                controlPoint.name = reduceStopName(controlPoint.name);
                controlPoint.name = controlPoint.name.replaceAll("Поликлиника".toUpperCase(),
                                                                 "Железнодорожныйвокзал".toUpperCase());
            }

            int[] binding;
            for (int i = 0; i < manualBindings.length; i++) {
                binding = manualBindings[i];
                point = controlPoints.get(new KR_ID(binding[0]), new CP_ID(binding[1]));
                if (point == null) {
                    continue;
                }
                point.ks_id = new KS_ID(binding[2]);
                updatedPoints.add(point);
                count++;
            }

            for (KR_ID kr_id : routesWithSchedule) {
                routeStops = db.getRouteStopsIDs(kr_id);
                routeCPs = controlPoints.row(kr_id);
                for (Map.Entry<CP_ID, ControlPoint> entry : routeCPs.entrySet()) {
                    point = entry.getValue();
                    if (point.ks_id != null && point.ks_id.id != 0) {
                        continue;
                    }

                    for (KS_ID ks_id : routeStops) {
                        stop = stops.get(ks_id);
                        if (point.name.equals(stop.title)) {
                            route = routes.get(point.kr_id);
                            System.out.println(
                                    "#" + point.cp_id.id + " " + point.name + " (" + route.getNumber() +
                                    " " + TransType.getTransTypeName(route.transportTypeID,
                                                                     route.affiliationID)
                                    + " " + route.direction
                                    + ") -> #" + ks_id + ": " +
                                    stop.title + " " +
                                    stop.adjacentStreet + " " + stop.direction);
                            point.ks_id = ks_id;
                            updatedPoints.add(point);
                            count++;
                            break;
                        }
                    }
                }
            }


            System.out.println(count + " points from " + controlPoints.size() + " recognized");
            db.updateControlPointsWithStopID(updatedPoints);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void createRoutesSegments(Connector connector) {

    }

    /**
     * loads geoportal ID, x, y from TOS
     *
     * @param connector
     */
    public static void stopsXY(Connector connector) {
        try (DataBase db = new DataBase(connector)) {
            stopsXY(db);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * loads geoportal ID, x, y from TOS
     *
     * @param db
     */
    public static void stopsXY(DataBase db) throws IOException, SQLException {
        Collection<Stop> stops = db.getStopsWithAbsentSpatialData().values();
        int i = 0;
        for (Stop stop : stops) {
            Utils.loadStopSpatialData(stop);
            db.updateStopWithGeoIDAndXY(stop);
            if (++i % 10 == 0) {
                System.out.println(i);
            }
        }
    }

    /**
     * loads route x, y coords from geoportal
     *
     * @param db
     * @param allowed
     */
    public static void routesXY(DataBase db, Map<KR_ID, PeriodID> allowed)
            throws SQLException, IOException {
        Collection<Route> routes = db.getAllRoutes().values();
        int i = 0;
        double[] coords;
        for (Route route : routes) {
            if (allowed != null && !allowed.containsKey(route.kr_id)) {
                continue;
            }
            coords = Utils.loadRouteCoords(route);
            PeriodID periodID = (allowed != null && allowed.get(route.kr_id) != null) ?
                                allowed.get(route.kr_id) : new PeriodID(0);
            db.insertRouteGeometry(route.kr_id, coords, periodID);
            if (++i % 10 == 0) {
                System.out.println(i);
            }
        }
    }

    /**
     * loads route x, y coords from geoportal
     *
     * @param connector
     * @param periodID
     */
    public static void routesXY(Connector connector, Map<KR_ID, PeriodID> allowed) {
        try (DataBase db = new DataBase(connector)) {
            routesXY(db, allowed);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void measureStopsDist(DataBase db, Collection<KR_ID> filter) throws SQLException {
        try {
            db.executeSQL(DbContract.CircuitEntry.ALTER_CIRCUITS_WITH_DIST_SQL);
        } catch (SQLException ignored) {
//                ignored.printStackTrace();
        }
        Map<KS_ID, Stop> stops = db.getAllStops();
        Table<KR_ID, PeriodID, KS_ID[]> circuits = db.getAllRouteCircuits();
        Table<KR_ID, PeriodID, double[]> distances = HashBasedTable.create(circuits.rowMap().size(),
                                                                           circuits.columnMap()
                                                                                   .size());
        double[] track;
        KS_ID[] circuit;
        int count;
        double[] dist;
        Stop stop;
        KR_ID kr_id;
        PeriodID periodID;

        for (Table.Cell<KR_ID, PeriodID, KS_ID[]> routePeriodCircuitEntry : circuits.cellSet()) {
            kr_id = routePeriodCircuitEntry.getRowKey();
            if (filter != null && !filter.contains(kr_id)) {
                continue;
            }
            periodID = routePeriodCircuitEntry.getColumnKey();

            circuit = routePeriodCircuitEntry.getValue();
            count = circuit.length;
            distances.put(kr_id, periodID, dist = new double[count]);
            track = db.getRouteGeometry(kr_id, periodID);
            for (int i = 0; i < count; i++) {
                stop = stops.get(circuit[i]);
                if (stop == null) {
                    System.out.print(circuit[i].id + ", ");
                    continue;
                }
                dist[i] = Utils.getStopMileage(track, stop);
            }
        }


        db.updateCircuitsWithMileage(distances);
    }

    public static void measureStopsDist(Connector connector, Collection<KR_ID> filter) {
        try (DataBase db = new DataBase(connector)) {
            measureStopsDist(db, filter);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static class GraphPoint {
        private static int lastId;
        int id = lastId++, in = -1, out = -1;
        int[] inElse, outElse;
        double lat, lng;

        void addIn(int in) {
            add(in, true);
        }

        void add(int value, boolean isIn) {
            if (isIn && this.in < 0) {
                this.in = value;
            } else if (!isIn && this.out < 0) {
                this.out = value;
            } else {
                if (isIn && in == value) {
                    return;
                }
                if (!isIn && out == value) {
                    return;
                }
                int[] array = isIn ? inElse : outElse;
                if (array == null) {
                    array = new int[]{value};
                } else {
                    for (int i : array) {
                        if (i == value) {
                            return;
                        }
                    }
                    int len = array.length + 1;
                    System.arraycopy(array, 0, array = new int[len], 0, len - 1);
                    array[len - 1] = value;
                }
                if (isIn) {
                    inElse = array;
                } else {
                    outElse = array;
                }
            }
        }

        void addOut(int out) {
            add(out, false);
        }

    }

    public static void buildTramGraph(Connector connector) {
        try (DataBase db = new DataBase(connector)) {
            Set<KR_ID> tramIds = db.getElectrotransportRoutesIds().get(new TransTypeID(TransType.TRAM));
            Table<Double, Double, GraphPoint> coordsToPoints = HashBasedTable.create(1200, 1200);
            List<GraphPoint> points = new ArrayList<>(25000);
            Set<Integer> startPoints = new HashSet<>();
            Set<Integer> endPoints = new HashSet<>();
            for (KR_ID kr_id : tramIds) {
                List<Period> periods = db.getAllRoutePeriods(kr_id);
                for (Period period : periods) {
                    double[] geometry = db.getRouteGeometry(kr_id, period.id);
                    GraphPoint pointPrev, pointNext;
                    if (geometry.length < 2) {
                        continue;
                    }
                    pointPrev = coordsToPoints.get(geometry[0], geometry[1]);
                    if (pointPrev == null) {
                        coordsToPoints.put(geometry[0], geometry[1], pointPrev = new GraphPoint());
                        pointPrev.lat = geometry[0];
                        pointPrev.lng = geometry[1];
                        points.add(pointPrev);
                    }
                    startPoints.add(pointPrev.id);
                    for (int i = 2; i < geometry.length; i += 2) {
                        pointNext = coordsToPoints.get(geometry[i], geometry[i + 1]);
                        if (pointNext == null) {
                            coordsToPoints
                                    .put(geometry[i], geometry[i + 1], pointNext = new GraphPoint());
                            pointNext.lat = geometry[i];
                            pointNext.lng = geometry[i + 1];
                            points.add(pointNext);
                        }
                        pointPrev.addOut(pointNext.id);
                        pointNext.addIn(pointPrev.id);

                        if (pointPrev.outElse != null) {
                            startPoints.add(pointPrev.id);
                        }
                        if (pointNext.inElse != null) {
                            endPoints.add(pointNext.id);
                        }
                        pointPrev = pointNext;
                    }
                    endPoints.add(pointPrev.id);
                }
            }

            startPoints.size();


        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
