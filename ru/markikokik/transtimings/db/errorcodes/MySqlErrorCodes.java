package ru.markikokik.transtimings.db.errorcodes;

import com.mysql.jdbc.MysqlErrorNumbers;

/**
 * Created by markikokik on 03.06.16.
 */
public class MySqlErrorCodes extends ErrorCodes {
    private static MySqlErrorCodes instance;

    private MySqlErrorCodes() {
        DUPLICATE = MysqlErrorNumbers.ER_DUP_ENTRY;
    }

    public static MySqlErrorCodes getInstance() {
        return instance != null ? instance : (instance = new MySqlErrorCodes());
    }
}
