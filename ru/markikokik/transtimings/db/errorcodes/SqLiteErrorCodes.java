package ru.markikokik.transtimings.db.errorcodes;

/**
 * Created by markikokik on 03.06.16.
 */
public class SqLiteErrorCodes extends ErrorCodes {
    private static SqLiteErrorCodes instance;

    private SqLiteErrorCodes() {
        DUPLICATE = 19;
    }

    public static SqLiteErrorCodes getInstance() {
        return instance != null ? instance : (instance = new SqLiteErrorCodes());
    }
}
