package ru.markikokik.transtimings.db.errorcodes;

/**
 * Created by markikokik on 03.06.16.
 */
public class SqlServerErrorCodes extends ErrorCodes {
    private static SqlServerErrorCodes instance;

    private SqlServerErrorCodes() {
        DUPLICATE = 2627;
    }

    public static SqlServerErrorCodes getInstance() {
        return instance != null ? instance : (instance = new SqlServerErrorCodes());
    }
}
