package ru.markikokik.transtimings.db.connectors;

import java.sql.Connection;
import java.sql.SQLException;

import ru.markikokik.transtimings.db.errorcodes.ErrorCodes;

/**
 * Created by markikokik on 02.06.16.
 */
public interface Connector {
    Connection connect() throws ClassNotFoundException, SQLException;

    ErrorCodes getErrorCodes();
}
