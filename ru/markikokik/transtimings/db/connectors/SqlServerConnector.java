package ru.markikokik.transtimings.db.connectors;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import ru.markikokik.transtimings.db.errorcodes.SqlServerErrorCodes;

/**
 * Created by markikokik on 09.06.16.
 */
public class SqlServerConnector implements Connector {
    private String host, dbName, user, pass;

    public SqlServerConnector(String host, String dbName, String user, String pass) {
        this.host = host;
        this.dbName = dbName;
        this.user = user;
        this.pass = pass;
    }

    public SqlServerConnector() {
        this.host = "192.168.12.10";
        this.dbName = "samara_transport_duty";
        this.user = "rgis";
        this.pass = "rgis";
    }

    @Override
    public Connection connect() throws ClassNotFoundException, SQLException {
        Properties properties = new Properties();
        properties.setProperty("databaseName", dbName);
        properties.setProperty("password", pass);
        properties.setProperty("user", user);
        Connection c = DriverManager.getConnection("jdbc:sqlserver://" + host, properties);
        c.setAutoCommit(false);
        return c;
    }

    @Override
    public SqlServerErrorCodes getErrorCodes() {
        return SqlServerErrorCodes.getInstance();
    }
}
