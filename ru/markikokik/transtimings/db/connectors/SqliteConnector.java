package ru.markikokik.transtimings.db.connectors;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import ru.markikokik.transtimings.db.errorcodes.ErrorCodes;
import ru.markikokik.transtimings.db.errorcodes.SqLiteErrorCodes;

/**
 * Created by markikokik on 02.06.16.
 */
public class SqliteConnector implements Connector {
    private String path;

    public SqliteConnector(String path) {
        this.path = path;
    }

    @Override
    public Connection connect() throws ClassNotFoundException, SQLException {
        Class.forName("org.sqlite.JDBC");
        Connection c = DriverManager.getConnection("jdbc:sqlite:" + path);
        c.setAutoCommit(false);
        return c;
    }

    @Override
    public ErrorCodes getErrorCodes() {
        return SqLiteErrorCodes.getInstance();
    }
}
