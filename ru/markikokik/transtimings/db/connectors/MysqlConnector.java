package ru.markikokik.transtimings.db.connectors;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import ru.markikokik.transtimings.db.errorcodes.MySqlErrorCodes;

/**
 * Created by markikokik on 02.06.16.
 */
public class MysqlConnector implements Connector {
    private String host, dbName, user, pass;

    public MysqlConnector(String host, String dbName, String user, String pass) {
        this.host = host;
        this.dbName = dbName;
        this.user = user;
        this.pass = pass;
    }

    @Override
    public Connection connect() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection c = DriverManager
                .getConnection("jdbc:mysql://" + host + "/" + dbName + "?user=" + user + "&password=" + pass);
        c.setAutoCommit(false);
        return c;
    }

    @Override
    public MySqlErrorCodes getErrorCodes() {
        return MySqlErrorCodes.getInstance();
    }
}
