package ru.markikokik.transtimings;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import ru.markikokik.transtimings.db.DataBase;
import ru.markikokik.transtimings.db.connectors.Connector;
import ru.markikokik.transtimings.entities.Accident;
import ru.markikokik.transtimings.entities.ControlPoint;
import ru.markikokik.transtimings.entities.Deviation;
import ru.markikokik.transtimings.entities.Interval;
import ru.markikokik.transtimings.entities.Matching;
import ru.markikokik.transtimings.entities.MonitoringLogEntry;
import ru.markikokik.transtimings.entities.Period;
import ru.markikokik.transtimings.entities.PositionRelToRoutes;
import ru.markikokik.transtimings.entities.Race;
import ru.markikokik.transtimings.entities.RaceTimings;
import ru.markikokik.transtimings.entities.ScheduleTime;
import ru.markikokik.transtimings.entities.TransportPosition;
import ru.markikokik.transtimings.entities.ids.CP_ID;
import ru.markikokik.transtimings.entities.ids.ID;
import ru.markikokik.transtimings.entities.ids.KR_ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;
import ru.markikokik.transtimings.entities.ids.MR_ID;
import ru.markikokik.transtimings.entities.ids.PeriodID;
import ru.markikokik.transtimings.entities.ids.TransTypeID;
import ru.markikokik.transtimings.entities.ids.VehicleTN;

import static ru.markikokik.transtimings.Utils.getDistBetween;
import static ru.markikokik.transtimings.Utils.getSpeedBetweenLatLng;

/**
 * Здесь все вычисления
 */
public class Calculator {
    private static final int TYPE_FIRST = 0, TYPE_LAST = 2, TYPE_INTER = 1;
    //остановка первая, конечная, промежуточная
    public static final int AREA = 250;
    private static final int AREA_FIRST = 1000;
    private static final int AREA_NEAR = 50;
    private static final double MOVING_SPEED = 30 / 3.6;

    public static final int NEW_RACE_MOVE_TRESHOLD = 1000;
    public static final int NEW_RACE_TIME_TRESHOLD = 600;

    private static final int ACCIDENTS_INTERSECTION_TIME = 120;
    public static final int ACCIDENTS_AREA = 500;


    //координаты конечных станций
    public static final double[][] stationsLatLng = new double[][]{
            //Около стадиона
            {
                    53.266822, 50.241535,
                    53.275899, 50.258877,
                    53.263808, 50.264908
            },
            //Около Оврага
            {
                    53.220434, 50.165544,
                    53.220501, 50.171142,
                    53.217101, 50.170831,
                    53.217287, 50.164855
            },
            //Около КТД
            {
                    53.205835, 50.231739,
                    53.207657, 50.238776,
                    53.204509, 50.241076,
                    53.201886, 50.235740
            },
            //Дом печати
            {
                    53.213054, 50.187937,
                    53.211096, 50.188645,
                    53.211199, 50.191735,
                    53.213543, 50.191907
            },
            //Тарасова
            {
                    53.237990, 50.190042,
                    53.235426, 50.191067,
                    53.236088, 50.195012,
                    53.238547, 50.193426
            },
            //Елизарова
            {
                    53.229616, 50.281941,
                    53.228026, 50.282466,
                    53.227904, 50.284140,
                    53.229809, 50.284644
            },
            //Металлург трамвай
            {
                    53.241939, 50.274353,
                    53.241708, 50.277164,
                    53.239803, 50.276295,
                    53.240041, 50.272808
            },
            //Стадион
            {
                    53.273566, 50.250463,
                    53.275279, 50.254786,
                    53.272208, 50.258271,
                    53.270118, 50.253182
            },
            //Хлебная
            {
                    53.179340, 50.079369,
                    53.179292, 50.082850,
                    53.177729, 50.082882,
                    53.177980, 50.079219
            },
            //Чапаевская
            {
                    53.181948, 50.088442,
                    53.184751, 50.090286,
                    53.184065, 50.092661,
                    53.181397, 50.090902
            },
            //ГТД и ТД1
            {
                    53.200199, 50.133231,
                    53.198350, 50.135581,
                    53.197209, 50.133124,
                    53.195744, 50.124772,
                    53.197254, 50.122889,
                    53.199935, 50.128323,
                    53.198875, 50.129707
            },
            //ТД1
//            {
//                    53.196520, 50.128722,
//                    53.198736, 50.125953,
//                    53.197931, 50.124156,
//                    53.196059, 50.126452
//            },
            //Тухачевского
            {
                    53.195935, 50.158639,
                    53.196531, 50.157137,
                    53.195384, 50.155549,
                    53.194379, 50.157609
            },
            //Аврора
            {
                    53.190482, 50.190383,
                    53.190430, 50.187347,
                    53.191306, 50.187755,
                    53.191429, 50.189783
            },
            //КТД и ТД2
            {
                    53.202065, 50.231785,
                    53.203347, 50.231656,
                    53.205315, 50.236270,
                    53.203756, 50.238367,
                    53.203504, 50.238276,
                    53.202187, 50.235326
            },
            //Кабельная
            {
                    53.202438, 50.265457,
                    53.202941, 50.266701,
                    53.202464, 50.267249,
                    53.201897, 50.265988
            },
            //Юнгородок и Костромской
            {
                    53.213791, 50.281082,
                    53.212933, 50.189916,
                    53.214040, 50.298836,
                    53.208407, 50.288211
            },
            //Костромской
            {
                    53.215432, 50.287055,
                    53.217631, 50.292251,
                    53.215645, 50.295807,
                    53.213298, 50.289434
            },
            //Металлург тролл
            {
                    53.240183, 50.279402,
                    53.239829, 50.281076,
                    53.241010, 50.282170,
                    53.241496, 50.279874
            },
            //15 мкр
            {
                    53.243658, 50.251211,
                    53.244597, 50.253603,
                    53.246187, 50.251683,
                    53.245209, 50.249022
            },
            //15А мкр
            {
                    53.251672, 50.265271,
                    53.249542, 50.267792,
                    53.247657, 50.264294,
                    53.250179, 50.261301
            },
            //Поляна
            {
                    53.260602, 50.215070,
                    53.259219, 50.217720,
                    53.257679, 50.215780,
                    53.258361, 50.212304
            },
            //СТД и ТД3
            {
                    53.230312, 50.206948,
                    53.232050, 50.203504,
                    53.233569, 50.200221,
                    53.236492, 50.204974,
                    53.234838, 50.207431,
                    53.234780, 50.211122,
                    53.232726, 50.213858
            },

            //Овраг
            {
                    53.221355, 50.164641,
                    53.218876, 50.164094,
                    53.218573, 50.167570,
                    53.220479, 50.168096
            },
            //Губернский
            {
                    53.195570, 50.123070,
                    53.193457, 50.121568,
                    53.193077, 50.123574,
                    53.195068, 50.125387
            },
            //Звезда
            {
                    53.215665, 50.149488,
                    53.216125, 50.150544,
                    53.216692, 50.149852,
                    53.216222, 50.148951
            },
            //БТЭЦ
            {
                    53.193549, 50.294351,
                    53.192973, 50.292442,
                    53.192142, 50.293477,
                    53.192725, 50.295451
            },
            //Экран
            {
                    53.195051, 50.290363,
                    53.197113, 50.287638,
                    53.198099, 50.289815,
                    53.195451, 50.292530
            }
    };

    public static final double[][] stationsXY = new double[][]{
            //Cтадион
            {
                    9923.055735215055, 8695.462422261015,
                    10211.038306205452, 8886.721631893888,
                    10444.2788586728, 8545.456044944003,
                    10105.269905341454, 8312.116025583819
            },
            //Хлебная
            {
                    -1493.962106496109, -1801.4227444361895,
                    -1369.1387100765648, -1808.690341629088,
                    -1377.7930466961561, -1922.6487525533885,
                    -1504.0341508825613, -1952.7738611968234
            },
            //Чапаевская,
            {
                    -887.307288910335, -1511.312705481425,
                    -763.9863816629288, -1199.3852098276839,
                    -605.2243363768855, -1275.748179109767,
                    -722.8522810198558, -1572.6563145872205
            },
            //ГТД и ТД1,
            {
                    2106.0942718293363, 520.306947673671,
                    2263.239353322052, 314.60786044970155,
                    2099.0994573641565, 187.5463247243315,
                    1540.9846000862385, 24.27802253421396,
                    1415.083094564372, 192.2834515767172,
                    1778.122579878719, 490.783753214404,
                    1870.6602712602776, 372.85421405453235
            },
            //Тухачевского,
            {
                    3804.407808924427, 46.86420595832169,
                    3703.9714893372216, 113.11136949434876,
                    3597.941973158156, -14.62231901846826,
                    3735.710122800787, -126.35924168676138
            },
            //Аврора,
            {
                    5926.721161607706, -557.7806912539527,
                    5723.7977985760035, -563.8207091577351,
                    5750.949112402694, -466.2972830468789,
                    5886.483610252345, -452.4398472364992
            },
            //КТД и ТД2,
            {
                    8709, 738,
                    8692, 868,
                    9000, 1091,
                    9137, 911,
                    8921, 479
            },
            //Кабельная,
            {
                    10941.686751100453, 781.7945912331343,
                    11024.683553793939, 837.9668008461595,
                    11061.426318049802, 784.9670942882076,
                    10977.309578218763, 721.6690517459065
            },
            //Юнгородок,
            {
                    12462.073501344681, 1887.3384225564077,
                    12325.031541324935, 2074.059187538922,
                    12162.581471657166, 1988.3895127167925,
                    12325.778190154257, 1787.7096026279032
            },
            // Костромской,
            {
                    12341.758100535655, 2217.8907465394586,
                    12612.788776008652, 2314.2051016213372,
                    12797.233589503905, 2213.648501995951,
                    12560.506317287201, 2028.8271171282977
            },
            //Елизарова,
            {
                    12035.486050484904, 3809.1196539225057,
                    12070.99801631135, 3632.2572807939723,
                    12182.827631262968, 3618.9668634599075,
                    12215.938031237372, 3831.062129176222
            },
            //Металлург трамвай,
            {
                    11525.406321194472, 5179.289435070939,
                    11713.136065226017, 5154.043374728411,
                    11655.643827373962, 4941.89094051905,
                    11422.77154966065, 4967.808818955906
            },
            //Металлург тролл,
            {
                    11862.975136476245, 4984.698229933158,
                    11974.838008521163, 4945.583521994762,
                    12047.543013792729, 5077.203659395687,
                    11894.119532972198, 5130.902221490629
            },
            //15 мкр,
            {
                    9980.011260887835, 5367.074346801266,
                    10139.472927749273, 5471.917479569092,
                    10010.923022434396, 5648.595712744631,
                    9833.51613069474, 5539.378960636444,
                    },
            //15А мкр,
            {
                    10916.57189303664, 6261.040582717396,
                    11085.396249705014, 6024.383627613075,
                    10852.389643526101, 5814.057071194984,
                    10651.965317035707, 6094.276901103556
            },
            //Поляна,
            {
                    7564.244010144044, 7248.285896678455,
                    7741.338681041892, 7094.6589587638155,
                    7582.615415972143, 6988.450091637671,
                    7489.452470634008, 7029.811974585988
            },
            //СТД и ТД3,
            {
                    7027.3016888281845, 3876.440857072361,
                    6797.032550001145, 4069.524530608207,
                    6577.566907879228, 4238.26179189235,
                    6894.466438397407, 4564.0212479503825,
                    7058.797210475428, 4380.190803301521,
                    7305.264112729913, 4374.113656267524,
                    7488.318546957436, 4145.8108331840485
            },
            //Тарасова,
            {
                    6076.247947662078, 4733.161693193018,
                    6168.145778305206, 4562.0053969928995,
                    6050.805230147683, 4478.82888743002,
                    5903.694362776478, 4648.3591777887195
            },
            //Овраг,
            {
                    4202.969757672312, 2876.2092754589394,
                    4166.682264416889, 2600.287212252617,
                    4398.901457889368, 2566.7809767173603,
                    4433.834135861949, 2778.9340225579217
            },
            //Дом печати,
            {
                    5892.354705965965, 1940.7534538609907,
                    5889.5854487669085, 1858.7288905186579,
                    5956.304281397306, 1823.6456982335076,
                    5998.124915660511, 1928.757393446751
            },
            //Губернский,
            {
                    1427.2407840301717, 4.875002954155207,
                    1326.9288517822138, -230.31251059565693,
                    1461.0175674482134, -272.55977702792734,
                    1582.114076950179, -50.93941804114729
            },
            //Звезда,
            {
                    3191.2907078210096, 2242.154622618109,
                    3261.7974576170077, 2293.3978451872244,
                    3215.5265382168973, 2356.466839621775,
                    3155.3752010962776, 2304.1185131203383
            },
            //БТЭЦ,
            {
                    12875.15403229051, -202.59742797818035,
                    12747.735777097892, -267.0457656206563,
                    12817.162990156201, -359.34070825204253,
                    12948.925112210021, -294.09961976483464
            },
            //Экран,
            {
                    12608.164474811045, -36.15786923188716,
                    12425.438458260927, 192.83991694170982,
                    12570.637940367382, 302.95613480266184,
                    12752.874206141085, 8.746212104335427
            }
    };

    public static final double[][] K1latlng, d1latlng;
    // коэффициенты для алгоритма вхождения точки в многоугольник
    public static final double[][] K1XY, d1XY; // коэффициенты для алгоритма вхождения точки в
    // многоугольник

    static {
        K1latlng = new double[stationsLatLng.length][];
        d1latlng = new double[stationsLatLng.length][];
        double x11, x12, y11, y12;
        int count;
        for (int i = 0; i < stationsLatLng.length; i++) {
            int nextIndex;
            count = stationsLatLng[i].length / 2;
            K1latlng[i] = new double[count];
            d1latlng[i] = new double[count];
            for (int j = 0; j < count; j++) {
                nextIndex = (j + 1) % count;
                x11 = stationsLatLng[i][j * 2];
                x12 = stationsLatLng[i][nextIndex * 2];
                y11 = stationsLatLng[i][j * 2 + 1];
                y12 = stationsLatLng[i][nextIndex * 2 + 1];
                K1latlng[i][j] = (y12 - y11) / (x12 - x11);
                d1latlng[i][j] = (x12 * y11 - x11 * y12) / (x12 - x11);
            }
        }

        K1XY = new double[stationsXY.length][];
        d1XY = new double[stationsXY.length][];
        for (int i = 0; i < stationsXY.length; i++) {
            int nextIndex;
            count = stationsXY[i].length / 2;
            K1XY[i] = new double[count];
            d1XY[i] = new double[count];
            for (int j = 0; j < count; j++) {
                nextIndex = (j + 1) % count;
                x11 = stationsXY[i][j * 2];
                x12 = stationsXY[i][nextIndex * 2];
                y11 = stationsXY[i][j * 2 + 1];
                y12 = stationsXY[i][nextIndex * 2 + 1];
                K1XY[i][j] = (y12 - y11) / (x12 - x11);
                d1XY[i][j] = (x12 * y11 - x11 * y12) / (x12 - x11);
            }
        }
    }

    private static class Position {
        int lastStopNumber, matchCount, absentPointsPenalty;
    }

    public static KR_ID detectRaceKR_IDs(
            List<? extends CP_ID> seq,
            Collection<KR_ID> allowedRoutes,
            Table<CP_ID, KR_ID, Integer> routesThruCPs // CP - route - seq number
    ) {
        Map<KR_ID, Position> positions = new HashMap<>();
        Position pos;
        for (KR_ID kr_id : allowedRoutes) {
            positions.put(kr_id, pos = new Position());
            pos.lastStopNumber = -1; //последовательный номер остановки на маршруте, к
            // которой привязана последняя попавшаяся точка
            pos.matchCount = 0; //число совпадений точек
//            pos[1] = 0; //длина совпавшего участка
            pos.absentPointsPenalty = 0; //признак, что в маршруте не нашлась последняя точка
        }

        Iterator<? extends CP_ID> iterator = seq.iterator();
        for (int i = 0, seqSize = seq.size(); i < seqSize; i++) {
            CP_ID cp_id = iterator.next();
            Map<KR_ID, Integer> routes = routesThruCPs.row(cp_id);

            for (Map.Entry<KR_ID, Position> posEntry : positions.entrySet()) {
                if (!allowedRoutes.contains(posEntry.getKey())) {
                    continue;
                }
                pos = posEntry.getValue();
                Integer number = routes.get(posEntry.getKey());
                if (number != null) {
                    if (pos.lastStopNumber > number) {
                        //если поехали в обратную сторону
                        pos.lastStopNumber = Integer.MAX_VALUE;
                    } else if (pos.lastStopNumber < number) {
                        pos.matchCount++;
                        pos.lastStopNumber = number;
                    }
                } else {
                    if (i == 0 || i == seqSize - 1) {
                        posEntry.getValue().absentPointsPenalty += 1;
                    } else {
                        posEntry.getValue().absentPointsPenalty += 10;
                    }
                }
            }


//            for (Map.Entry<KR_ID, Integer> routeEntry : routes.entrySet()) {
//                pos = positions.get(routeEntry.getKey());
//
//                if (pos.lastStopNumber > routeEntry.getValue()) {
//                    //если поехали в обратную сторону
//                    pos.lastStopNumber = Integer.MAX_VALUE;
//                } else if (pos.lastStopNumber < routeEntry.getValue()) {
//                    pos.matchCount++;
//                    pos.lastStopNumber = routeEntry.getValue();
//                }
//            }
//
//            for (Map.Entry<KR_ID, Position> posEntry : positions.entrySet()) {
//                if (!routes.containsKey(posEntry.getKey())) {
//                    if (i == 0 || i == seqSize - 1) {
//                        posEntry.getValue().absentPointsPenalty += 1;
//                    } else {
//                        posEntry.getValue().absentPointsPenalty += 10;
//                    }
//                }
//            }

        }

        KR_ID kr_id = new KR_ID();
        double percentage;
        double tmpPerc;
        int seqSize = seq.size(), processed = 0, curSkip = 0, routesCount = positions.size();
        while (kr_id.id == 0 && processed < routesCount) {
            percentage = 0;
            for (Map.Entry<KR_ID, Position> positionEntry : positions.entrySet()) {
                pos = positionEntry.getValue();
                if (pos.absentPointsPenalty != curSkip) {
                    continue;
                }
                if (pos.lastStopNumber < 0 || pos.lastStopNumber == Integer.MAX_VALUE ||
                    pos.matchCount == 1) {
                    processed++;
                    continue;
                }


                int size = routesThruCPs.column(positionEntry.getKey()).size();
                tmpPerc = 1.0 * pos.matchCount / (size + seqSize);
                if (kr_id.id == 0 || tmpPerc > percentage) {
                    kr_id.id = positionEntry.getKey().id;
                    percentage = tmpPerc;
                }
                processed++;
            }
            curSkip++;
        }

        return kr_id;
    }

    /**
     * развернуть во временной ряд инфу, разбитую по транспортам
     *
     * @param stays стоянки транспорта вблизи контрольных точек
     * @param mu
     * @return
     */
    public static Matching<TransportPosition<VehicleTN>, Integer>[]
    staysTimeline(Collection<? extends Collection<Interval<MR_ID, VehicleTN>>> stays,
                  Collection<VehicleTN> mu) {
        int count = 0;
        for (Collection<Interval<MR_ID, VehicleTN>> intervals : stays) {
            count += intervals.size();
        }

        Matching<TransportPosition<VehicleTN>, Integer>[] timeline = new Matching[count * 2];
        int i = 0;
        Matching<TransportPosition<VehicleTN>, Integer> event;
        for (Collection<Interval<MR_ID, VehicleTN>> intervals : stays) {
            for (Interval<MR_ID, VehicleTN> interval : intervals) {
                event = timeline[i++] = new Matching<>();
                event.first = interval.start;
                event.second = mu.contains(interval.start.vehicleID) ? 2 : 1;

                event = timeline[i++] = new Matching<>();
                event.first = interval.end;
                event.second = mu.contains(interval.end.vehicleID) ? -2 : -1;
            }
        }

        Arrays.sort(timeline, new Comparator<Matching<TransportPosition<VehicleTN>, Integer>>() {
            @Override
            public int compare(Matching<TransportPosition<VehicleTN>, Integer> o1,
                               Matching<TransportPosition<VehicleTN>, Integer> o2) {
                return Integer.compare(o1.first.timestamp, o2.first.timestamp);
            }
        });

        int timelineLength = timeline.length;
//        int posCount = 0, negCount = 0;
//        for (i = 0; i < timelineLength; i++) {
//            if (timeline[i].second > 0) {
//                posCount++;
//            } else {
//                negCount++;
//            }
//        }
        for (i = 1; i < timelineLength; i++) {
            timeline[i].second += timeline[i - 1].second;
        }

        return timeline;
    }

    /**
     * максимальная загрузка станции по часам суток
     *
     * @param timeline
     * @return
     */
    public static int[] stationMaxLoadByHours(
            Matching<TransportPosition<VehicleTN>, Integer>[] timeline) {
        int hourStart, hourEnd;
        int[] hourLoads = new int[24];
        for (int i = 0; i < 24; i++) {
            hourLoads[i] = 0;
        }

        Matching<TransportPosition<VehicleTN>, Integer> eventStart;
        Matching<TransportPosition<VehicleTN>, Integer> eventEnd = timeline[0];
        for (int i = 1, timelineLength = timeline.length; i < timelineLength; i++) {
            eventStart = eventEnd;
            eventEnd = timeline[i];
            hourStart = Utils.getHourFromTimestamp(eventStart.first.timestamp);
            hourEnd = Utils.getHourFromTimestamp(eventEnd.first.timestamp);
            if (hourLoads[hourEnd] < eventEnd.second) {
                hourLoads[hourEnd] = eventEnd.second;
            }
            if (hourStart != hourEnd) {
                if (hourLoads[hourStart] < eventStart.second) {
                    hourLoads[hourStart] = eventStart.second;
                }
            }
        }

        return hourLoads;
    }

    /**
     * средняя загрузка станции по часам суток
     *
     * @param timeline
     * @return
     */
    public static double[] stationAvgLoadByHours(
            Matching<TransportPosition<VehicleTN>, Integer>[] timeline) {
        int hourStart, hourEnd, timeSpent;
        int[] astroHours = new int[24], vehicleHours = new int[24];
        for (int i = 0; i < 24; i++) {
            astroHours[i] = vehicleHours[i] = 0;
        }

        Matching<TransportPosition<VehicleTN>, Integer> eventStart;
        Matching<TransportPosition<VehicleTN>, Integer> eventEnd = timeline[0];
        for (int i = 1, timelineLength = timeline.length; i < timelineLength; i++) {
            eventStart = eventEnd;
            eventEnd = timeline[i];
            hourStart = Utils.getHourFromTimestamp(eventStart.first.timestamp);
            hourEnd = Utils.getHourFromTimestamp(eventEnd.first.timestamp);

            if (hourStart == hourEnd) {
                timeSpent = eventEnd.first.timestamp - eventStart.first.timestamp;
                vehicleHours[hourStart] += timeSpent * eventStart.second;
                astroHours[hourStart] += timeSpent;
            } else {
                timeSpent = Utils.getHourTimestampOfDay(eventStart.first.timestamp, hourStart + 1) -
                            eventStart.first.timestamp;
                vehicleHours[hourStart] += timeSpent * eventStart.second;
                astroHours[hourStart] += timeSpent;

                timeSpent = eventEnd.first.timestamp -
                            Utils.getHourTimestampOfDay(eventEnd.first.timestamp, hourEnd);
                vehicleHours[hourStart] += timeSpent * eventStart.second;
                astroHours[hourEnd] += timeSpent;
            }
        }

        double[] avgVehicles = new double[24];
        for (int i = 0; i < 24; i++) {
            avgVehicles[i] = 1d * vehicleHours[i] / astroHours[i];
        }

        return avgVehicles;
    }

    /**
     * среднее время отстоя по часам
     *
     * @param stays
     * @return
     */
    public static int[] stayAvgTimeByHours(
            Collection<? extends Collection<Interval<MR_ID, VehicleTN>>> stays) {
        int hourStart, hourEnd;
        int[] avgTimes = new int[24],
                hourCounts = new int[24];
        for (int i = 0; i < 24; i++) {
            avgTimes[i] = hourCounts[i] = 0;
        }

        for (Collection<Interval<MR_ID, VehicleTN>> vehicleStays : stays) {
            for (Interval<MR_ID, VehicleTN> stay : vehicleStays) {
                hourStart = Utils.getHourFromTimestamp(stay.start.timestamp);
                hourEnd = Utils.getHourFromTimestamp(stay.end.timestamp);
                if (hourStart == hourEnd) {
                    avgTimes[hourStart] += stay.end.timestamp - stay.start.timestamp;
                    hourCounts[hourStart]++;
                } else {
                    avgTimes[hourStart] +=
                            Utils.getHourTimestampOfDay(stay.start.timestamp, hourStart + 1)
                            - stay.start.timestamp;
                    hourCounts[hourStart]++;

                    avgTimes[hourEnd] += stay.end.timestamp -
                                         Utils.getHourTimestampOfDay(stay.start.timestamp, hourEnd);
                    hourCounts[hourEnd]++;
                }
            }
        }

        for (int i = 0; i < 24; i++) {
            if (hourCounts[i] > 0) {
                avgTimes[i] /= hourCounts[i];
            }
        }


        return avgTimes;
    }

    /**
     * среднее время отстоя
     *
     * @param stays
     * @return
     */
    public static int stayAvgTime(
            Collection<? extends Collection<Interval<MR_ID, VehicleTN>>> stays) {
        int avgTime = 0, count = 0;

        for (Collection<Interval<MR_ID, VehicleTN>> vehicleStays : stays) {
            for (Interval<MR_ID, VehicleTN> stay : vehicleStays) {
                avgTime += stay.end.timestamp - stay.start.timestamp;
            }
            count += vehicleStays.size();
        }

        avgTime /= count;
        return avgTime;
    }

    /**
     * посчитать и напечатать метрик станций
     *
     * @param stays
     * @param mu
     */
    public static void stationStats(Table<MR_ID, VehicleTN, Deque<Interval<MR_ID, VehicleTN>>> stays,
                                    Collection<VehicleTN> mu) {
        Matching<TransportPosition<VehicleTN>, Integer>[] timelime = staysTimeline(stays.values(), mu);
        try (FileWriter out = new FileWriter("/media/Data/Documents/TransportStateLogs/stats.txt")) {
            Printer.printStaysTimeline(out, timelime);
        } catch (IOException e) {
            e.printStackTrace();
        }

        int stayAvgTime = stayAvgTime(stays.values());
        int[] maxLoadByHours = stationMaxLoadByHours(timelime);
        double[] avgLoadByHours = stationAvgLoadByHours(timelime);
        int[] avgTimeByHours = stayAvgTimeByHours(stays.values());

        for (int i = 0; i < 24; i++) {
            System.out.println(i + "\t" + maxLoadByHours[i]);
        }
        System.out.println();
        for (int i = 0; i < 24; i++) {
            System.out.println(i + "\t" + avgLoadByHours[i]);
        }
        System.out.println();
        for (int i = 0; i < 24; i++) {
            System.out.println(i + "\t" + avgTimeByHours[i]);
        }

        return;
    }

    /**
     * расчёт метрик станций за некоторые дни
     *
     * @param c
     * @param stationID
     */
    public static void stationStats(Connector c, int stationID) {
        try {
            DataBase db = new DataBase(c);
            long time = -System.nanoTime();
            int[] dayLimits;

//            Utils.getDayLimits("20170310");
            Table<MR_ID, VehicleTN, Deque<Interval<MR_ID, VehicleTN>>> week =
                    HashBasedTable.create();
            Table<MR_ID, VehicleTN, Deque<Interval<MR_ID, VehicleTN>>> hol =
                    HashBasedTable.create();

            List<VehicleTN> mu = db.getMU();

            dayLimits =
                    new int[]{
                            Utils.getDayBeginning(2017, 02, 1),
                            Utils.getDayEnd(2017, 02, 3)
                    };

            mergeStays(db.getStays(stationID, null, dayLimits[0], dayLimits[1]), week);

            dayLimits =
                    new int[]{
                            Utils.getDayBeginning(2017, 02, 6),
                            Utils.getDayEnd(2017, 02, 10)
                    };

            mergeStays(db.getStays(stationID, null, dayLimits[0], dayLimits[1]), week);
            dayLimits =
                    new int[]{
                            Utils.getDayBeginning(2017, 02, 13),
                            Utils.getDayEnd(2017, 02, 17)
                    };

            mergeStays(db.getStays(stationID, null, dayLimits[0], dayLimits[1]), week);
            dayLimits =
                    new int[]{
                            Utils.getDayBeginning(2017, 02, 20),
                            Utils.getDayEnd(2017, 02, 22)
                    };

            mergeStays(db.getStays(stationID, null, dayLimits[0], dayLimits[1]), week);
            dayLimits =
                    new int[]{
                            Utils.getDayBeginning(2017, 02, 27),
                            Utils.getDayEnd(2017, 03, 3)
                    };

            mergeStays(db.getStays(stationID, null, dayLimits[0], dayLimits[1]), week);


            dayLimits =
                    new int[]{
                            Utils.getDayBeginning(2017, 02, 4),
                            Utils.getDayEnd(2017, 02, 5)
                    };

            mergeStays(db.getStays(stationID, null, dayLimits[0], dayLimits[1]), hol);
            dayLimits =
                    new int[]{
                            Utils.getDayBeginning(2017, 02, 11),
                            Utils.getDayEnd(2017, 02, 12)
                    };

            mergeStays(db.getStays(stationID, null, dayLimits[0], dayLimits[1]), hol);
            dayLimits =
                    new int[]{
                            Utils.getDayBeginning(2017, 02, 18),
                            Utils.getDayEnd(2017, 02, 19)
                    };

            mergeStays(db.getStays(stationID, null, dayLimits[0], dayLimits[1]), hol);
            dayLimits =
                    new int[]{
                            Utils.getDayBeginning(2017, 02, 23),
                            Utils.getDayEnd(2017, 02, 26)
                    };

            mergeStays(db.getStays(stationID, null, dayLimits[0], dayLimits[1]), hol);

            time += System.nanoTime();
            System.out.println("select stays " + time / 1e6);

            db.close();
            stationStats(week, mu);
            System.out.println();
            stationStats(hol, mu);
            System.out.println();
            mergeStays(week, hol);
            stationStats(hol, mu);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * объединяем две таблицы отстоев
     *
     * @param from
     * @param to
     */
    public static void mergeStays(Table<MR_ID, VehicleTN, Deque<Interval<MR_ID, VehicleTN>>> from,
                                  Table<MR_ID, VehicleTN, Deque<Interval<MR_ID, VehicleTN>>> to) {
        Deque<Interval<MR_ID, VehicleTN>> deque;
        MR_ID mr_id;
        VehicleTN vehicleTN;
        for (Table.Cell<MR_ID, VehicleTN, Deque<Interval<MR_ID, VehicleTN>>> cell : from
                .cellSet()) {
            mr_id = cell.getRowKey();
            vehicleTN = cell.getColumnKey();
            deque = to.get(mr_id, vehicleTN);
            if (deque == null) {
                to.put(mr_id, vehicleTN, cell.getValue());
            } else {
                deque.addAll(cell.getValue());
            }
        }
    }

    /**
     * отстои на станции
     *
     * @param positions
     * @param stationIndex
     * @param routes
     * @return
     */
    public static Table<MR_ID, VehicleTN, Deque<Interval<MR_ID, VehicleTN>>>
    trafficThroughStation(Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positions,
                          int stationIndex,
                          Set<MR_ID> routes) {
        Table<MR_ID, VehicleTN, Deque<Interval<MR_ID, VehicleTN>>> positionsFiltered =
                HashBasedTable.create();
        List<MonitoringLogEntry> vehicleMonitoring;
        Deque<Interval<MR_ID, VehicleTN>> deque;
        Interval<MR_ID, VehicleTN> last;

        for (MR_ID route : routes) {
            for (Map.Entry<VehicleTN, List<MonitoringLogEntry>> entry : positions.row(route)
                                                                                 .entrySet()) {
                vehicleMonitoring = entry.getValue();
                deque = null;
                for (MonitoringLogEntry sample : vehicleMonitoring) {
                    if (Utils.pointOnStationLatLng(sample, stationIndex)) {
                        if (deque == null) {
                            positionsFiltered.put(route, entry.getKey(), deque = new ArrayDeque<>());
                        }
                        last = deque.peekLast();
                        if (last != null &&
                            sample.timestamp - last.end.timestamp < NEW_RACE_TIME_TRESHOLD) {
                            last.end = sample;
                        } else {
                            deque.add(new Interval<>(sample, sample, route));
                        }
                    }
                }
            }
        }

        int count = 0;
        for (Deque<Interval<MR_ID, VehicleTN>> intervals : positionsFiltered.values()) {
            count += intervals.size();
        }
        System.out.println(count + " races thru station #" + stationIndex);

        return positionsFiltered;
    }

    /**
     * соответствия айдишников маршрута
     *
     * @param positions
     * @return
     */
    public static Table<MR_ID, KR_ID, Object> getRoutesBindings(
            Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positions) {
//        long time = -System.nanoTime();
        Table<MR_ID, KR_ID, Object> bindings = HashBasedTable.create();
        Object value = new Object();
        Map<KR_ID, Object> kr_ids;
        for (Map.Entry<MR_ID, Map<VehicleTN, List<MonitoringLogEntry>>> routeEntry : positions.rowMap()
                                                                                              .entrySet()) {
            kr_ids = bindings.row(routeEntry.getKey());
            for (List<MonitoringLogEntry> monitoring : routeEntry.getValue().values()) {
                for (MonitoringLogEntry sample : monitoring) {
                    if (sample.routeID != null && sample.routeID.id > 0) {
                        kr_ids.put(sample.routeID, value);
                    }
                }
            }
        }

        Table<MR_ID, KR_ID, Object> lost = HashBasedTable.create();
        do {
            lost.clear();

            for (MR_ID mr_id : bindings.rowKeySet()) { //28
                for (KR_ID kr_id : bindings.row(mr_id).keySet()) { //135
                    for (MR_ID mr_id1 : bindings.column(kr_id).keySet()) { //29
                        for (KR_ID kr_id1 : bindings.row(mr_id1).keySet()) { //136
                            if (!bindings.contains(mr_id, kr_id1)) {
                                lost.put(mr_id, kr_id1, value);
                            }
                        }
                    }
                }
            }

            bindings.putAll(lost);
        } while (lost.size() > 0);

//        time += System.nanoTime();
//        System.out.println("bindings " + time / 1e6);

        return bindings;
    }

    /**
     * fill 'dist' field with distance from route beginning
     *
     * @param monitoring
     */
    public static void calcDists(DataBase db,
                                 Table<KR_ID, KS_ID, Double> stopsDists,
                                 List<? extends PositionRelToRoutes<? extends ID, KR_ID>> monitoring
    ) throws SQLException {
        Double dist;
        if (stopsDists == null) {
            stopsDists = HashBasedTable.create();
        }
        Map<KS_ID, Double> dists = null;
        KR_ID lastKR_ID = null;
        for (PositionRelToRoutes<? extends ID, KR_ID> position : monitoring) {
            if (position.routeID == null) {
                continue;
            }
            if (!position.routeID.equals(lastKR_ID)) {
                lastKR_ID = position.routeID;
                dists = stopsDists.row(lastKR_ID);
                if (dists.isEmpty()) {
                    dists.putAll(db.getRouteCircuitWithDistsMap(
                            lastKR_ID,
                            db.getRoutePeriodForTime(lastKR_ID, position.timestamp).id
                    ));
                }
            }
            dist = dists.get(position.nextStopID);
            if (dist != null) {
                position.dist = dist - position.nextStopDist;
            }
        }
    }

    /**
     * fill 'dist' field with distance from route beginning
     *
     * @param monitoring
     */
    public static void calcDists(DataBase db,
                                 Map<VehicleTN, ? extends List<? extends PositionRelToRoutes<VehicleTN, KR_ID>>> monitoring
    ) throws SQLException {
        Table<KR_ID, KS_ID, Double> stopsDists = HashBasedTable.create();
        for (List<? extends PositionRelToRoutes<VehicleTN, KR_ID>> vehicleMonitoring : monitoring
                .values()) {
            calcDists(db, stopsDists, vehicleMonitoring);
        }
    }

    /**
     * Объединить задержки отдельных ТС по времени и месту
     *
     * @param accidents
     * @return
     */
    public static List<List<Accident<ID, VehicleTN>>> clusterizeAccidents(
            Collection<? extends Accident<ID, VehicleTN>> accidents) {
        ArrayList<Accident<ID, VehicleTN>> accidentsCopy = new ArrayList<>(accidents);
        List<List<Accident<ID, VehicleTN>>> result = new ArrayList<>();
        Accident<ID, VehicleTN> currentAccident;
        Accident<ID, VehicleTN> accident;
        List<Accident<ID, VehicleTN>> cluster;
        ListIterator<Accident<ID, VehicleTN>> iterator;

        while (accidentsCopy.size() > 0) {
            cluster = new ArrayList<>();
            currentAccident = accidentsCopy.get(0);
            cluster.add(currentAccident);
            accidentsCopy.remove(0);
            int size;

            do {
                size = cluster.size();
                for (int i = 0; i < cluster.size(); i++) {
                    currentAccident = cluster.get(i);
                    for (iterator = accidentsCopy.listIterator(0); iterator.hasNext(); ) {
                        accident = iterator.next();

                        if (currentAccident.start.timestamp + ACCIDENTS_INTERSECTION_TIME >
                            accident.end.timestamp) {
                            continue;
                        }

                        if (currentAccident.end.timestamp - ACCIDENTS_INTERSECTION_TIME <
                            accident.start.timestamp) {
                            continue;
                        }

                        if (Utils.getDistBetweenLatLng(currentAccident.start, accident.start) >
                            ACCIDENTS_AREA) {
                            continue;
                        }

                        cluster.add(accident);
                        iterator.remove();
                    }
                }
            } while (size != cluster.size());
            if (cluster.size() >= MIN_ACCIDENT_CLUSTER_SIZE) {
                result.add(cluster);
            }
        }

        return result;
    }

    public static final int MIN_ACCIDENT_CLUSTER_SIZE = 2;

    /**
     * предполагаемая привязка планового рейса к фактическому
     */
    private static class CandidateForRealRace {
        Deviation deviation;
        ScheduleTime scheduleTime;
    }

    /**
     * предполагаемая привязка фактического рейса к плановому
     */
    private static class CandidateForPlanRace {
        Deviation deviation;
        Race race;
    }

//    private static boolean isNearStation(Point point) {
//        for (int i = 0; i < stationsLatLng.length; i += 2) {
//            if (getDistBetweenLatLng(point.lat, point.lng, stationsLatLng[i], stationsLatLng[i + 1]) < 100) {
//                return true;
//            }
//        }
//        return false;
//    }

    public static final int ACCIDENT_MIN_TIME = 300; //минимальная длительность задержки
    public static final double ACCIDENT_MAX_SPEED = 1.0;

    private static final int SHIFTS_MIN_INTERVAL = 60 * 60;

    /**
     * распределение, какой транспорт и когда находился на каком маршруте
     *
     * @param positionsToday
     * @return
     */
    public static Table<MR_ID, VehicleTN, List<RaceTimings>> calcVehiclesDistrib(
            Table<MR_ID, VehicleTN, ? extends List<? extends PositionRelToRoutes<VehicleTN, KR_ID>>> positionsToday) {
        Table<MR_ID, VehicleTN, List<RaceTimings>> result = HashBasedTable.create();
        List<RaceTimings> vehicleTimes;
        Iterator<? extends PositionRelToRoutes<VehicleTN, KR_ID>> iterator;
        PositionRelToRoutes<VehicleTN, KR_ID> sample = null;
        VehicleTN vehicleTN;
        MR_ID mr_id;
        boolean found;
        RaceTimings lastShift;
        for (Table.Cell<MR_ID, VehicleTN, ? extends List<? extends PositionRelToRoutes<VehicleTN, KR_ID>>> posCell
                : positionsToday.cellSet()) {
            iterator = posCell.getValue().iterator();
            found = false;
            for (; iterator.hasNext(); ) {
                sample = iterator.next();
                if (sample.routeID != null && sample.routeID.id > 0) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                continue;
            }

            vehicleTN = posCell.getColumnKey();
            mr_id = posCell.getRowKey();
            vehicleTimes = result.get(mr_id, vehicleTN);
            if (vehicleTimes == null) {
                result.put(mr_id, vehicleTN, vehicleTimes = new ArrayList<>());
            }
            vehicleTimes.add(lastShift = new RaceTimings(sample.timestamp, sample.timestamp));

            for (; iterator.hasNext(); ) {
                sample = iterator.next();
                if (sample.timestamp - lastShift.endTime < SHIFTS_MIN_INTERVAL) {
                    if (sample.routeID != null && sample.routeID.id > 0) {
                        lastShift.endTime = sample.timestamp;
                    }
                } else {
                    vehicleTimes.add(lastShift = new RaceTimings(sample.timestamp, sample.timestamp));
                }
            }
        }

        return result;
    }

    /**
     * выявление задержек
     *
     * @param db
     * @param positions
     * @return
     * @throws SQLException
     */
    public static Map<VehicleTN, List<Accident<ID, VehicleTN>>> detectAccidentsByVehicle(
            DataBase db,
            Table<MR_ID, VehicleTN, ? extends List<? extends PositionRelToRoutes<VehicleTN, KR_ID>>> positions)
            throws SQLException {
        List<Integer> typeIds = Arrays.asList(3, 4);
        Map<VehicleTN, List<Accident<ID, VehicleTN>>> accidents = new HashMap<>();
        List<Accident<ID, VehicleTN>> vehicleAccidents;
        List<Accident<ID, VehicleTN>> existsAccidents;
        VehicleTN vehicleTN;
        Table<KR_ID, PeriodID, KS_ID[]> circuits = db.getAllRouteCircuits();

        for (Table.Cell<MR_ID, VehicleTN, ? extends List<? extends PositionRelToRoutes<VehicleTN, KR_ID>>> monitoringCell :
                positions.cellSet()) {
            if (!typeIds.contains(monitoringCell.getColumnKey().transType)) {
                continue;
            }

            vehicleAccidents = detectVehicleAccidents(db, monitoringCell.getValue(), circuits);
            if (vehicleAccidents.size() > 0) {
                vehicleTN = monitoringCell.getColumnKey();
                existsAccidents = accidents.get(vehicleTN);
                if (existsAccidents == null) {
                    accidents.put(vehicleTN, vehicleAccidents);
                } else {
                    existsAccidents.addAll(vehicleAccidents);
                }
            }
        }

        return accidents;
    }

    /**
     * * выявление задержек
     *
     * @param db
     * @param positions
     * @return
     * @throws SQLException
     */
    public static List<Accident<ID, VehicleTN>>
    detectAccidentsAsList(
            DataBase db,
            Table<MR_ID, VehicleTN, ? extends List<? extends PositionRelToRoutes<VehicleTN, KR_ID>>> positions)
            throws SQLException {
//        List<Integer> typeIds = Arrays.asList(3, 4);
        List<Integer> typeIds = Arrays.asList(3);
        List<Accident<ID, VehicleTN>> accidents = new ArrayList<>();
        Table<KR_ID, PeriodID, KS_ID[]> circuits = db.getAllRouteCircuits();

        for (Map.Entry<MR_ID, ? extends Map<VehicleTN, ? extends List<? extends PositionRelToRoutes<VehicleTN, KR_ID>>>> routeMonitoringEntry : positions
                .rowMap().entrySet()) {
            if (!typeIds.contains(routeMonitoringEntry.getKey().transType)) {
                continue;
            }
            calcDists(db, routeMonitoringEntry.getValue());

            for (List<? extends PositionRelToRoutes<VehicleTN, KR_ID>> vehicleMonitoring : routeMonitoringEntry
                    .getValue().values()) {
                accidents.addAll(detectVehicleAccidents(db, vehicleMonitoring, circuits));
            }
        }

        return accidents;
    }

    /**
     * * выявление задержек и запись их в БД
     *
     * @param connector
     * @param timeLimits
     */
    public static void detectAccidentsByVehicle(Connector connector, int[] timeLimits) {
        try (DataBase db = new DataBase(connector)) {
            db.createAccidentsTable();
            Map<TransTypeID, Set<KR_ID>> electrotransportRoutesIds = db.getElectrotransportRoutesIds();
            Map<VehicleTN, List<PositionRelToRoutes<VehicleTN, KR_ID>>> routeMonitoring;
            Map<VehicleTN, List<Accident<ID, VehicleTN>>> accidents = new HashMap<>();
            Map<KS_ID, Double> stopsDists;
            List<Accident<ID, VehicleTN>> vehicleAccidents;
            List<Accident<ID, VehicleTN>> existsAccidents;
            VehicleTN vehicleTN;
            Table<KR_ID, PeriodID, KS_ID[]> circuits = db.getAllRouteCircuits();
            for (Map.Entry<TransTypeID, Set<KR_ID>> transTypeIDsEntry : electrotransportRoutesIds
                    .entrySet()) {
                for (KR_ID kr_id : transTypeIDsEntry.getValue()) {
                    routeMonitoring = db.getRouteMonitoringByTNid(kr_id, timeLimits);
                    calcDists(db, routeMonitoring);

                    for (Map.Entry<VehicleTN, List<PositionRelToRoutes<VehicleTN, KR_ID>>> vehicleMonitoringEntry : routeMonitoring
                            .entrySet()) {

                        vehicleAccidents =
                                detectVehicleAccidents(db, vehicleMonitoringEntry.getValue(), circuits);
                        if (vehicleAccidents.size() > 0) {
                            vehicleTN = vehicleMonitoringEntry.getKey();
                            existsAccidents = accidents.get(vehicleTN);
                            if (existsAccidents == null) {
                                accidents.put(vehicleTN, vehicleAccidents);
                            } else {
                                existsAccidents.addAll(vehicleAccidents);
                            }
                        }
                    }
                }

                db.insertAccidents(accidents);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /**
     * * выявление задержек для одного ТС
     *
     * @param db
     * @param vehicleMonitoring
     * @param circuits
     * @return
     * @throws SQLException
     */
    public static List<Accident<ID, VehicleTN>> detectVehicleAccidents(
            DataBase db,
            List<? extends PositionRelToRoutes<VehicleTN, KR_ID>> vehicleMonitoring,
            Table<KR_ID, PeriodID, KS_ID[]> circuits) throws SQLException {
        PositionRelToRoutes<VehicleTN, KR_ID> position, prevPos, accStart;
        Iterator<? extends PositionRelToRoutes<VehicleTN, KR_ID>> iterator1;
        Iterator<? extends PositionRelToRoutes<VehicleTN, KR_ID>> iterator2;
        List<Accident<ID, VehicleTN>> vehicleAccidents;
        double dist;
        MonitoringLogEntry monitoringLogEntry;
        KR_ID kr_id;
        Period period;
        KS_ID[] circuit;
//        boolean directionChanged = false;

        iterator1 = vehicleMonitoring.iterator();
        if (!iterator1.hasNext()) {
            return new ArrayList<>();
        }
        vehicleAccidents = new ArrayList<>();
        iterator2 = vehicleMonitoring.iterator();
        dist = 0;
        prevPos = iterator1.next();
        accStart = iterator2.next();

        while (iterator1.hasNext()) {
            position = iterator1.next();
            if (position.timestamp - prevPos.timestamp > NEW_RACE_TIME_TRESHOLD) {
                prevPos = position;
                while (accStart != position) {
                    accStart = iterator2.next();
                }
                dist = 0;
                continue;
            }

            dist += getDistBetween(prevPos, position);
//            double speed = getDistBetween(prevPos, position) / (position.timestamp - prevPos.timestamp);

            if (dist > Corrector.ROUTE_LEAVE_TRESHOLD) {
//            if (speed > ACCIDENT_MAX_SPEED) {
                if (position.timestamp - accStart.timestamp >= ACCIDENT_MIN_TIME &&
                    !Utils.pointOnStationLatLng(position) && !Utils.pointOnStationLatLng(accStart)
                    && Math.abs(getSpeedBetweenLatLng(prevPos, position)) < Corrector.MAX_SPEED) {

                    kr_id = accStart.routeID != null ? accStart.routeID : position.routeID;
                    circuit = null;
                    if (kr_id != null && circuits != null) {
                        period = db.getRoutePeriodForTime(kr_id, accStart.timestamp);
                        circuit = circuits.get(kr_id, period.id);
                    }

                    if (circuit == null || accStart.routeID == null
                        || accStart.nextStopID.id != circuit[0].id
                           && (accStart.nextStopID.id != circuit[1].id || accStart.dist > 30)) {

                        ID routeID;
                        if (accStart.routeID != null) {
                            routeID = accStart.routeID;
                        } else {
                            if (position.routeID != null) {
                                routeID = position.routeID;
                            } else {
                                if (accStart instanceof MonitoringLogEntry) {
                                    monitoringLogEntry = (MonitoringLogEntry) accStart;
                                    routeID = new MR_ID(
                                            monitoringLogEntry.routeTN,
                                            monitoringLogEntry.vehicleID !=
                                            null ?
                                            monitoringLogEntry.vehicleID.transType :
                                            0);
                                } else {
                                    routeID = new MR_ID(0, 0);
                                }
                            }
                        }

                        vehicleAccidents.add(
                                new Accident<>(accStart = iterator2.next(),
                                               position,
                                               routeID
                                )
                        );
                    }
                }
                while (iterator2.hasNext() && accStart != position) {
                    accStart = iterator2.next();
                }
                dist = 0;
            }

            prevPos = position;
        }
        return vehicleAccidents;
    }

    public static TimeBetween[][] calcTimeMich(DataBase db,
                                               Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positionsToday)
            throws SQLException {
        List<KR_ID[]> incedentalRoutes = new ArrayList<>();
        incedentalRoutes.add(new KR_ID[]{new KR_ID(114), new KR_ID(94)});
        incedentalRoutes.add(new KR_ID[]{new KR_ID(115), new KR_ID(93)});
        Map<KR_ID, CP_ID[]> cps = new HashMap<>();
        cps.put(new KR_ID(114), new CP_ID[]{new CP_ID(10498), new CP_ID(10206)});//aquarium, arts
        cps.put(new KR_ID(94), new CP_ID[]{new CP_ID(10499), new CP_ID(10206)});//aquarium, arts
        cps.put(new KR_ID(115), new CP_ID[]{new CP_ID(20000), new CP_ID(10499)});//square mich,
        cps.put(new KR_ID(93), new CP_ID[]{new CP_ID(20000), new CP_ID(10499)});//square mich,
        // aquarium

        TimeBetween[][] times = calcTimesBetweenCPs(db, positionsToday, incedentalRoutes, cps);

        return times;
    }

    public static TimeBetween[][] calcTimesBetweenCPs(DataBase db,
                                                      Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positionsToday,
                                                      List<KR_ID[]> incedentalRoutes,
                                                      Map<KR_ID, CP_ID[]> pointsFromToForRoutes)
            throws SQLException {
        Table<MR_ID, KR_ID, Object> routeIds = Calculator.getRoutesBindings(positionsToday);
        Set<KR_ID> routeSet = new HashSet<>();
        Set<MR_ID> routeTNset = new HashSet<>();
        for (KR_ID[] kr_ids : incedentalRoutes) {
            for (KR_ID kr_id : kr_ids) {
                routeTNset.addAll(routeIds.column(kr_id).keySet());
                routeSet.add(kr_id);
            }
        }
        Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positions = HashBasedTable.create();
        for (MR_ID mr_id : routeTNset) {
            positions.row(mr_id).putAll(positionsToday.row(mr_id));
        }

        Map<KR_ID, Table<VehicleTN, CP_ID, List<RaceTimings>>> realSchedule =
                Calculator.calcRealSchedule(
                        db, positions, routeSet,
                        db.getAllCPsListWithCoords(), routeIds
                );

        TimeBetween[][] times = new TimeBetween[incedentalRoutes.size()][];
        for (int i = 0; i < incedentalRoutes.size(); i++) {
            KR_ID[] kr_ids = incedentalRoutes.get(i);

            for (KR_ID kr_id : kr_ids) {
                CP_ID[] cp_ids = pointsFromToForRoutes.get(kr_id);
                TimeBetween[] routeTimes = Calculator.avgTimeBetweenCPsByHours(
                        realSchedule.get(kr_id), cp_ids[0], cp_ids[1], 20 * 60
                );
                if (times[i] == null) {
                    times[i] = routeTimes;
                } else {
                    for (int j = 0; j < routeTimes.length; j++) {
                        times[i][j].count += routeTimes[j].count;
                        times[i][j].totalTime += routeTimes[j].totalTime;
                    }
                }
            }
        }

        return times;
    }

    /**
     * отклонения от графика для контрольной точки
     *
     * @param connector
     * @param timeLimits
     * @return
     */
    public static Map<CP_ID, List<Deviation>> calcScheduleDeviation(Connector connector,
                                                                    int[] timeLimits) {
        try (DataBase db = new DataBase(connector)) {
            try {
                db.dropDeviations();
                db.createDeviationsTable();
            } catch (SQLException e) {
            }
            Set<KR_ID> routesWithSchedule = db.getRoutesWithSchedule();
            Map<CP_ID, List<Race>> real;
            Map<CP_ID, List<ScheduleTime>> plan;
            List<ControlPoint> controlPoints;
            Map<CP_ID, List<Deviation>> deviations = new HashMap<>();
            List<Deviation> cpDeviations;
            List<Race> realCPtimes;
            Map<Race, CandidateForRealRace> realRaceCandidates = new HashMap<>();
            Map<ScheduleTime, CandidateForPlanRace> planRaceCandidates = new HashMap<>();
            int deviation;
            Deviation minPlanRaceDeviation, minRealRaceDeviation;
            int firstId;
            int lastId;
            int type;
            CP_ID cp_id;
            Race realRace;
            ScheduleTime scheduleTime;
            CandidateForRealRace candidateForRealRace;
            CandidateForPlanRace candidateForPlanRace;
            List<ScheduleTime> schedule;
            int racesLeft;
            int[] timeEdges = Utils.splitByDays(timeLimits);
            int periodsCount = timeEdges.length - 1;
            Period period;

            for (KR_ID kr_id : routesWithSchedule) {
                for (int i = 0; i < periodsCount; i++) {
                    System.arraycopy(timeEdges, i, timeLimits, 0, 2);
                    period = db.getRoutePeriodForTime(kr_id, timeEdges[i]);
                    real = db.getRouteRealSchedule(kr_id, timeLimits);
                    plan = db.getRouteSchedule(kr_id, timeLimits);
                    controlPoints = db.getRouteControlPointsWithCoords(kr_id, period.id);
                    deviations.clear();
                    firstId = controlPoints.get(0).cp_id.id;
                    lastId = controlPoints.get(controlPoints.size() - 1).cp_id.id;

                    realRaceCandidates.clear();
                    planRaceCandidates.clear();

                    for (Map.Entry<CP_ID, List<ScheduleTime>> vehicleCPschedule : plan.entrySet()) {
                        cp_id = vehicleCPschedule.getKey();
                        realCPtimes = real.get(cp_id);
                        if (realCPtimes == null) {
                            realCPtimes = new ArrayList<>();
                        }
                        deviations.put(cp_id, cpDeviations = new ArrayList<>());
                        if (cp_id.id == firstId) {
                            type = TYPE_FIRST;
                        } else if (cp_id.id == lastId) {
                            type = TYPE_LAST;
                        } else {
                            type = TYPE_INTER;
                        }

                        schedule = vehicleCPschedule.getValue();

                        do {
                            realRaceCandidates.clear();
                            planRaceCandidates.clear();
                            racesLeft = schedule.size();

                            for (ListIterator<ScheduleTime> scheduleIterator = schedule.listIterator();
                                 scheduleIterator.hasNext(); ) {
                                scheduleTime = scheduleIterator.next();
                                planRaceCandidates.put(scheduleTime, candidateForPlanRace =
                                        new CandidateForPlanRace());
                                candidateForPlanRace.deviation =
                                minPlanRaceDeviation =
                                        new Deviation(scheduleTime.time, Integer.MAX_VALUE,
                                                      scheduleTime.kt_id,
                                                      scheduleTime.direction);


                                for (Iterator<Race> realRacesIterator = realCPtimes.iterator();
                                     realRacesIterator.hasNext(); ) {
                                    realRace = realRacesIterator.next();
                                    deviation = calcDeviation(scheduleTime.time, realRace, type);

                                    candidateForRealRace = realRaceCandidates.get(realRace);
                                    if (candidateForRealRace == null) {
                                        realRaceCandidates
                                                .put(realRace,
                                                     candidateForRealRace = new CandidateForRealRace());
                                        candidateForRealRace.deviation =
                                        minRealRaceDeviation = new Deviation(Integer.MAX_VALUE);
                                        minRealRaceDeviation.realVehicle = realRace.kt_id;
                                    } else {
                                        minRealRaceDeviation = candidateForRealRace.deviation;
                                    }

                                    if (Math.abs(deviation) > 600) {
                                        continue;
                                    }

                                    if (realRace.kt_id.equals(scheduleTime.kt_id)) {
                                        minRealRaceDeviation.deviation = deviation;
                                        minRealRaceDeviation.planTime = scheduleTime.time;
                                        minRealRaceDeviation.planVehicle = scheduleTime.kt_id;
                                        minRealRaceDeviation.direction = scheduleTime.direction;
                                        cpDeviations.add(minRealRaceDeviation);
                                        scheduleIterator.remove();
                                        realRacesIterator.remove();
                                        realRaceCandidates.remove(realRace);
                                        planRaceCandidates.remove(scheduleTime);
                                        break;
                                    }

                                    if (Math.abs(deviation) > 300) {
                                        continue;
                                    }

                                    if (Math.abs(minRealRaceDeviation.deviation) > Math.abs(deviation)) {
                                        minRealRaceDeviation.deviation = deviation;
//                                    minRealRaceDeviation.direction = scheduleRace.direction;
                                        minRealRaceDeviation.planTime = scheduleTime.time;
                                        minRealRaceDeviation.planVehicle = scheduleTime.kt_id;
                                        candidateForRealRace.scheduleTime = scheduleTime;
                                    }

                                    if (Math.abs(minPlanRaceDeviation.deviation) > Math.abs(deviation)) {
                                        minPlanRaceDeviation.deviation = deviation;
                                        minPlanRaceDeviation.realVehicle = realRace.kt_id;
                                        candidateForPlanRace.race = realRace;
                                    }
                                }
                            }

                            for (Map.Entry<ScheduleTime, CandidateForPlanRace> planRaceCandidate : planRaceCandidates
                                    .entrySet()) {
                                realRace = planRaceCandidate.getValue().race;
                                if (realRace == null) {
                                    continue;
                                }
                                candidateForRealRace = realRaceCandidates.get(realRace);
                                if (candidateForRealRace != null &&
                                    candidateForRealRace.scheduleTime == planRaceCandidate.getKey()) {
                                    planRaceCandidate.getValue().deviation.direction =
                                            candidateForRealRace.scheduleTime.direction;
                                    cpDeviations.add(planRaceCandidate.getValue().deviation);
                                    schedule.remove(candidateForRealRace.scheduleTime);
                                    realCPtimes.remove(realRace);
                                }
                            }
                        } while (racesLeft > schedule.size());

                        for (Iterator<ScheduleTime> iterator = schedule.iterator();
                             iterator.hasNext(); ) {
                            scheduleTime = iterator.next();
                            minPlanRaceDeviation =
                                    new Deviation(scheduleTime.time, Integer.MAX_VALUE,
                                                  scheduleTime.kt_id,
                                                  scheduleTime.direction);
                            cpDeviations.add(minPlanRaceDeviation);
                        }

//                    Collections.sort(cpDeviations, Deviation.planTimeComparator);
                    }

                    db.insertDeviations(kr_id, deviations);
//                printDeviations(db, deviations, controlPoints);

                    System.out.println(kr_id.id);
                }
            }
            return deviations;
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * посчитать отклонение от планового времени
     *
     * @param planTime
     * @param raceTimings
     * @param type
     * @return
     */
    private static int calcDeviation(int planTime, RaceTimings raceTimings, int type) {
        if (raceTimings.endTime < planTime || type == TYPE_FIRST) {
            return raceTimings.endTime - planTime;
        }
        if (raceTimings.startTime > planTime || type == TYPE_LAST) {
            return raceTimings.startTime - planTime;
        }
        return 0;
    }

    static class TimeBetween {
        int totalTime = 0;
        int count = 0;

        public TimeBetween() {
        }

        public TimeBetween(int totalTime, int count) {
            this.totalTime = totalTime;
            this.count = count;
        }
    }

    /**
     * сколько в среднем занимает пробег между контрольными точками по часам суток
     *
     * @param realSchedule
     * @param firstCP
     * @param secondCP
     * @param maxTime
     * @return
     */
    public static TimeBetween[] avgTimeBetweenCPsByHours(
            Table<VehicleTN, CP_ID, List<RaceTimings>> realSchedule,
            CP_ID firstCP, CP_ID secondCP, int maxTime) {
        int time = 0;
        for (List<RaceTimings> list : realSchedule.values()) {
            for (RaceTimings raceTimings : list) {
                time = Utils.getHourTimestampOfDay(raceTimings.startTime, 5);
                break;
            }
            if (time > 0) {
                break;
            }
        }

        TimeBetween[] result = new TimeBetween[20];
        int[] limits = new int[2];
        for (int i = 0; i < 20; i++) {
            limits[0] = time;
            time += 3600;
            limits[1] = time;
            result[i] = avgTimeBetweenCPs(realSchedule, firstCP, secondCP, maxTime, limits);
        }
        return result;
    }

    /**
     * среднее время пробега между контрольными точками
     *
     * @param realSchedule
     * @param firstCP
     * @param secondCP
     * @param maxTime
     * @param timeLimits
     * @return
     */
    public static TimeBetween avgTimeBetweenCPs(
            Table<VehicleTN, CP_ID, List<RaceTimings>> realSchedule,
            CP_ID firstCP, CP_ID secondCP, int maxTime,
            int[] timeLimits) {
        List<RaceTimings> firstTimings;
        List<RaceTimings> secondTimings;
        int timeAll = 0, count = 0;
        for (Map.Entry<VehicleTN, Map<CP_ID, List<RaceTimings>>> vehicleEntry : realSchedule.rowMap()
                                                                                            .entrySet()) {
            firstTimings = vehicleEntry.getValue().get(firstCP);
            secondTimings = vehicleEntry.getValue().get(secondCP);

            if (firstTimings == null || secondTimings == null) {
                continue;
            }

            for (RaceTimings firstTiming : firstTimings) {
                for (RaceTimings secondTiming : secondTimings) {
                    if (firstTiming.endTime < timeLimits[0] ||
                        firstTiming.endTime > timeLimits[1] ||
                        secondTiming.startTime - firstTiming.endTime > maxTime) {
                        break;
                    }
                    if (secondTiming.startTime - firstTiming.endTime > 0) {
                        timeAll += secondTiming.startTime - firstTiming.endTime;
                        count++;
                        break;
                    }
                }
            }
        }

//        if (count > 0) {
//            timeAll /= count;
//        }
        return new TimeBetween(timeAll, count);
    }

    /**
     * Не доделано, должен свалить в базу фактический график
     *
     * @param connector
     * @param timeLimits
     */
    public static void calcRealSchedule(Connector connector, int[] timeLimits) {
        try (DataBase db = new DataBase(connector)) {
            Map<KR_ID, List<ControlPoint>> allCPs = db.getAllCPsListWithCoords(); //KR_ID - id - CP

            Set<KR_ID> routesWithSchedule = db.getRoutesWithSchedule();
//            db.getRouteCircuitWithDistsMap(kr_id);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * фактический график для маршрута
     *
     * @param routeMonitoring
     * @param kr_id
     * @param controlPoints
     * @return
     */
    public static Table<VehicleTN, CP_ID, List<RaceTimings>> calcRouteRealSchedule(
            DataBase db,
            Map<VehicleTN, List<MonitoringLogEntry>> routeMonitoring,
            KR_ID kr_id,
            List<ControlPoint> controlPoints) throws SQLException {

        Table<KR_ID, VehicleTN, Map<CP_ID, List<TransportPosition>>> nearCPs = HashBasedTable.create();
        Map<CP_ID, List<TransportPosition>> vehicleMovement;

        Table<VehicleTN, CP_ID, List<RaceTimings>> realSchedule;
        Map<VehicleTN, Map<CP_ID, List<TransportPosition>>> routeMovement;
        ListIterator<TransportPosition> timesIterator;

        Collections.sort(controlPoints);
        calcDists(db, routeMonitoring);

        int size;
        int area;
        List<TransportPosition> cpTimes;

        for (Map.Entry<VehicleTN, List<MonitoringLogEntry>> entry : routeMonitoring.entrySet()) {
            nearCPs.put(kr_id, entry.getKey(), vehicleMovement = new HashMap<>());

            area = AREA_FIRST;

            for (ControlPoint point : controlPoints) {
                vehicleMovement.put(point.cp_id, cpTimes = new ArrayList<>());
                for (MonitoringLogEntry position : entry.getValue()) {
                    if (position.routeID != null && position.routeID.id == kr_id.id &&
                        Math.abs(point.dist - position.dist) <= area) {
                        cpTimes.add(position);
                    }
                }
                area = AREA;
            }
        }


        TransportPosition position, prevPosition;
        int raceStartIndex;

        realSchedule = HashBasedTable.create();
        List<RaceTimings> races;
        routeMovement = nearCPs.row(kr_id);
        for (Map.Entry<VehicleTN, Map<CP_ID, List<TransportPosition>>> entry : routeMovement
                .entrySet()) {
            for (ControlPoint point : controlPoints) {
                realSchedule.put(entry.getKey(), point.cp_id, races = new ArrayList<>());

                cpTimes = entry.getValue().get(point.cp_id);
                size = cpTimes.size();

                timesIterator = cpTimes.listIterator();

                if (!timesIterator.hasNext()) {
                    continue;
                }
                raceStartIndex = 0;
                prevPosition = timesIterator.next();
                while (timesIterator.hasNext()) {
                    position = timesIterator.next();
                    if (position.timestamp - prevPosition.timestamp > NEW_RACE_TIME_TRESHOLD) {
                        races.add(calcCPtime(point, cpTimes, raceStartIndex,
                                             raceStartIndex = timesIterator.previousIndex()));
                    }
                    prevPosition = position;
                }
                races.add(calcCPtime(point, cpTimes, raceStartIndex, size));
            }
        }

        return realSchedule;
    }

    /**
     * посчитать фактический график для всех маршрутов
     *
     * @param positionsToday
     * @param routesWithSchedule
     * @param allCPs
     * @param routeIds
     * @return
     */
    public static Map<KR_ID, Table<VehicleTN, CP_ID, List<RaceTimings>>> calcRealSchedule(
            DataBase db,
            Table<MR_ID, VehicleTN, List<MonitoringLogEntry>> positionsToday,
            Set<KR_ID> routesWithSchedule,
            Map<KR_ID, List<ControlPoint>> allCPs,
            Table<MR_ID, KR_ID, Object> routeIds) throws SQLException {

        Map<KR_ID, Table<VehicleTN, CP_ID, List<RaceTimings>>> realSchedule = new HashMap<>();

//        db.dropRealSchedules();
//        db.createRealScheduleTable();
        int cnt = 0;

        MR_ID mr_id;
        for (KR_ID kr_id : routesWithSchedule) {
            mr_id = routeIds.column(kr_id).keySet().iterator().next();
            Table<VehicleTN, CP_ID, List<RaceTimings>> routeRealSchedule =
                    calcRouteRealSchedule(db,
                                          positionsToday.row(mr_id),
                                          kr_id,
                                          allCPs.get(kr_id)
                    );


            realSchedule.put(kr_id, routeRealSchedule);

            cnt++;
            System.out.println(cnt + ": " + kr_id.id);
        }

        return realSchedule;
    }


    /**
     * посчитать интервал нахождения транспорта вблизи контрольной точки
     *
     * @param controlPoint
     * @param nearCP
     * @param raceStart
     * @param raceEnd
     * @return
     */
    private static RaceTimings calcCPtime(ControlPoint controlPoint, List<TransportPosition> nearCP,
                                          int raceStart, int raceEnd) {
//        List<TransportPosition> race = nearCP.subList(raceStart, raceEnd);
//        int lastIndex = raceEnd - raceStart - 1;
        RaceTimings result = new RaceTimings();
        double cpDist = controlPoint.dist;
        TransportPosition position, prevPos;

        double dist, avgSpeedBefore = 0, avgSpeedAfter = 0;
        int lastSpeedIndex, i;
        boolean monotonic;

        ListIterator<TransportPosition> iterator = nearCP.listIterator(raceStart);
        double distTreshold = cpDist - AREA_NEAR;
        prevPos = iterator.next();
        result.startTime = prevPos.timestamp;
        lastSpeedIndex = raceStart;
        i = raceStart + 1;
        double minDistBefore = -prevPos.dist;
        monotonic = true;
        while (iterator.hasNext() && i < raceEnd) {
            position = iterator.next();
            if (distTreshold < position.dist) {
                break;
            }
            if (monotonic && (monotonic = (dist = position.dist - prevPos.dist) > 0)) {
                avgSpeedBefore += dist;
                lastSpeedIndex++;
            }
            result.startTime = position.timestamp;
            minDistBefore = -position.dist;

            prevPos = position;
            i++;
        }
        minDistBefore += cpDist;


        if (lastSpeedIndex > raceStart) {
            avgSpeedBefore /= nearCP.get(lastSpeedIndex).timestamp - nearCP.get(raceStart).timestamp;
        } else {
            avgSpeedBefore = MOVING_SPEED;
        }

        iterator = nearCP.listIterator(raceEnd);
        distTreshold = AREA_NEAR + cpDist;
        position = iterator.previous();
        result.endTime = position.timestamp;
        lastSpeedIndex = raceEnd - 1;
        i = raceEnd - 2;
        double minDistAfter = position.dist;
        monotonic = true;
        while (iterator.hasPrevious() && i >= raceStart) {
            prevPos = iterator.previous();
            if (prevPos.dist < distTreshold) {
                break;
            }
            if (monotonic && (monotonic = (dist = position.dist - prevPos.dist) > 0)) {
                avgSpeedAfter += dist;
                lastSpeedIndex--;
            }
            result.endTime = prevPos.timestamp;
            minDistAfter = prevPos.dist;

            position = prevPos;
            i--;
        }
        minDistAfter -= cpDist;

        if (lastSpeedIndex < raceEnd - 1) {
            avgSpeedAfter /= nearCP.get(raceEnd - 1).timestamp - nearCP.get(lastSpeedIndex).timestamp;
        } else {
            avgSpeedAfter = MOVING_SPEED;
        }


        RaceTimings corr = new RaceTimings();

        corr.startTime = (int) (result.startTime + minDistBefore / avgSpeedBefore);
        corr.endTime = (int) (result.endTime - minDistAfter / avgSpeedAfter);
        if (corr.endTime >= corr.startTime) {
//            System.out.println((int) (minDistBefore / avgSpeedBefore) + "  " + (int) (minDistAfter / avgSpeedAfter));
            result = corr;
        }

//        printDate(hmDateFormat, result.startTime);
//        if (result.endTime - result.startTime >= 60) {
//            System.out.print("-");
//            printDate(hmDateFormat, result.endTime);
//        }
//        System.out.println();

        return result;
    }
}
