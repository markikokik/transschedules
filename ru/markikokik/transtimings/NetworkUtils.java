package ru.markikokik.transtimings;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.zip.GZIPInputStream;

public class NetworkUtils {
    public static String CONTENT_TYPE_FORM_URLENCODED = "application/x-www-form-urlencoded";
    public static String GOOGLE_URL = "http://www.google.com/", API_URL = "http://tosamara.ru/api/xml";

    public static boolean checkApiAvailability() {
        return checkUrlAvailability(API_URL);
    }

    public static boolean checkInternetAvailability() {
        return checkUrlAvailability(GOOGLE_URL);
    }

    public static boolean checkUrlAvailability(String address) {
        try {
            // тест доступности внешнего ресурса
            URL url = new URL(address);
            HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
            urlc.setRequestProperty("User-Agent", "test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(1000); // Timeout в секундах
            urlc.connect();
            // статус ресурса OK
            return urlc.getResponseCode() != -1;
        } catch (IOException e) {
            return false;
        }
    }

    public static String downloadUrlToString(String address, String message, String method,
                                             String contentType,
                                             int timeout)
            throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        downloadUrlToStream(address, message, method, contentType, out, timeout);
        out.close();

        return new String(out.toByteArray());
    }

    public static String downloadUrlToString(String address, String message, String method, int timeout)
            throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        downloadUrlToStream(address, message, method, CONTENT_TYPE_FORM_URLENCODED, out, timeout);
        out.close();

        return new String(out.toByteArray());
    }

    public static String downloadUrlToString(String address, int timeout) throws IOException {
        return downloadUrlToString(address, null, "GET", timeout);
    }

//    public static void downloadUrlToStream(String address, String message, String method, String contentType,
//                                           OutputStream out, AsyncStatusReporter callback,
//                                           AsyncInterruptReporter interruptReporter) {
//        try {
//            downloadUrlToStream(address, message, method, contentType, out, 1024, 10, callback, interruptReporter);
//        } catch (IOException e) {
//            e.printStackTrace();
//            callback.onError(e);
//        }
//    }
//
//    public static void downloadUrlToStream(String address, String message, String method, String contentType,
//                                           OutputStream out, int bufferSize,
//                                           int progressReportFrequency) throws IOException {
//        downloadUrlToStream(address, message, method, contentType, out, bufferSize, progressReportFrequency,
//                            null, null);
//    }

    public static void downloadUrlToStream(String address, String message, String method,
                                           String contentType,
                                           OutputStream out, int timeout) throws IOException {
        downloadUrlToStream(address, message, method, contentType, out, 1024, timeout, 10, null, null);
    }

//    public static void downloadUrlToStream(String address, String method, String contentType, OutputStream out,
//                                           AsyncStatusReporter callback,
//                                           AsyncInterruptReporter interruptReporter) {
//        try {
//            downloadUrlToStream(address, null, method, contentType, out, 1024, 10, callback, interruptReporter);
//        } catch (IOException e) {
//            e.printStackTrace();
//            callback.onError(e);
//        }
//    }
//
//    public static void downloadUrlToStream(String address, String method, String contentType, OutputStream out,
//                                           int bufferSize,
//                                           int progressReportFrequency) throws IOException {
//        downloadUrlToStream(address, null, method, contentType, out, bufferSize, progressReportFrequency, null,
//                            null);
//    }
//
//    public static void downloadUrlToStream(String address, String method, String contentType, OutputStream out)
//            throws IOException {
//        downloadUrlToStream(address, null, method, contentType, out, 1024, 10, null, null);
//    }

    public static void downloadUrlToStream(String address, OutputStream out, int timeout,
                                           AsyncStatusReporter callback,
                                           AsyncInterruptReporter interruptReporter) throws IOException {
        downloadUrlToStream(address, null, "GET", null, out, 1024, timeout, 10, callback,
                            interruptReporter);
    }

//    public static void downloadUrlToStream(String address, OutputStream out, int bufferSize,
//                                           int progressReportFrequency) throws IOException {
//        downloadUrlToStream(address, null, "GET", null, out, bufferSize,
//                            progressReportFrequency, null, null);
//    }
//
//    public static void downloadUrlToStream(String address, OutputStream out) throws IOException {
//        downloadUrlToStream(address, null, "GET", null, out, 1024, 10, null, null);
//    }

    public static void downloadUrlToStream(String address, String message, String method,
                                           String contentType,
                                           OutputStream out, int bufferSize, int timeout,
                                           int progressReportFrequency, AsyncStatusReporter callback,
                                           AsyncInterruptReporter interruptReporter) throws IOException {
        URLConnection connection;
        URL url;
        HttpURLConnection httpConnection;

        try {
            url = new URL(address);
            connection = url.openConnection();
            httpConnection = (HttpURLConnection) connection;
            httpConnection.setRequestMethod(method);

            if (message != null) {
                httpConnection.setRequestProperty("Content-Type", contentType);
                httpConnection
                        .setRequestProperty("Content-Length", String.valueOf(message.getBytes().length));
                httpConnection.setDoOutput(true);
            } else {
                httpConnection.setDoOutput(false);
            }

            httpConnection.setRequestProperty("Accept-Encoding", "gzip");

            httpConnection.setConnectTimeout(timeout);
            httpConnection.setReadTimeout(timeout);

            httpConnection.connect();

            if (message != null) {
                OutputStream os = httpConnection.getOutputStream();
                os.write(message.getBytes());

                os.flush();
                os.close();
            }

            int responseCode = httpConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream in = new MonitoredInputStream(httpConnection.getInputStream(), callback,
                                                          progressReportFrequency * bufferSize,
                                                          httpConnection.getContentLength());

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                byte[] buffer = new byte[bufferSize];
                int c;
                while ((c = in.read(buffer)) > -1) {
                    byteArrayOutputStream.write(buffer, 0, c);
                    if (interruptReporter != null && interruptReporter.interrupted()) {
                        in.close();
                        httpConnection.disconnect();
                        if (callback != null) {
                            callback.onError(new InterruptedException());
                        }
                        break;
                    }
//                    try {
//                        Thread.sleep(100);
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
                }
                if (interruptReporter != null && interruptReporter.interrupted()) {
                    return;
                }
                in.close();
                in = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());

                if ("gzip".equals(httpConnection.getContentEncoding())) {
//                    Log.e("downloadUrlToStream", "gzipped input");
                    in = new GZIPInputStream(in);
                }

                while ((c = in.read(buffer)) > -1) {
                    out.write(buffer, 0, c);
                }

                in.close();
                httpConnection.disconnect();
                if (callback != null) {
                    callback.onSuccess();
                }
            } else if (responseCode == HttpURLConnection.HTTP_UNAVAILABLE) {
                try {
                    Thread.sleep(15000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
                downloadUrlToStream(address, message, method, contentType, out, bufferSize, timeout,
                                    progressReportFrequency, callback, interruptReporter);
            } else {
                throw new IOException("HTTP Error: code " + responseCode);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Exception throwable = ex;
            if (!checkInternetAvailability()) {
                throwable = new InternetDownException();
            }
            if (callback != null) {
                callback.onError(throwable);
            } else {
                throw ex;
            }
        }
    }


    public static class InternetDownException extends IOException {
        public InternetDownException() {
            super("google.com unavailable");
        }
    }

    private static class MonitoredInputStream extends InputStream {
        private final InputStream in;
        private final AsyncStatusReporter callback;
        private int read = 0;
        private final int length;
        private final int progressReportFrequency;
        private int progress = 0;

        public MonitoredInputStream(InputStream in, AsyncStatusReporter callback,
                                    int progressReportFrequency,
                                    int length) {
            this.in = in;
            this.callback = callback;
            this.length = length;
            this.progressReportFrequency = progressReportFrequency;
        }

        @Override
        public int read(byte[] buffer) throws IOException {
            return read(buffer, 0, buffer.length);
        }

        @Override
        public int read(byte[] buffer, int byteOffset, int byteCount) throws IOException {
            int count = in.read(buffer, byteOffset, byteCount);
            read += count;
            int newProgress = read / progressReportFrequency;
            if (callback != null && newProgress - progress > 0) {
                callback.onProgressChanged(read, length);
                progress = newProgress;
            }
            return count;
        }

        @Override
        public int read() throws IOException {
            int data = in.read();
            read += 1;
            int newProgress = read / progressReportFrequency;
            if (newProgress - progress > 0) {
                callback.onProgressChanged(read, length);
                progress = newProgress;
            }
            return data;
        }

        @Override
        public void close() throws IOException {
            super.close();
            in.close();
        }
    }
}
