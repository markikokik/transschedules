package ru.markikokik.transtimings;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import ru.markikokik.transtimings.entities.MonitoringLogEntry;
import ru.markikokik.transtimings.entities.Period;
import ru.markikokik.transtimings.entities.PlanTimingRecord;
import ru.markikokik.transtimings.entities.Point;
import ru.markikokik.transtimings.entities.PositionRelToRoutes;
import ru.markikokik.transtimings.entities.Route;
import ru.markikokik.transtimings.entities.RouteInfo;
import ru.markikokik.transtimings.entities.Stop;
import ru.markikokik.transtimings.entities.StopDistance;
import ru.markikokik.transtimings.entities.TransportPosition;
import ru.markikokik.transtimings.entities.ids.KR_ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;
import ru.markikokik.transtimings.entities.ids.KT_ID;
import ru.markikokik.transtimings.entities.ids.MR_ID;
import ru.markikokik.transtimings.entities.ids.PeriodID;
import ru.markikokik.transtimings.entities.ids.RaceID;

import static ru.markikokik.transtimings.Calculator.K1XY;
import static ru.markikokik.transtimings.Calculator.K1latlng;
import static ru.markikokik.transtimings.Calculator.d1XY;
import static ru.markikokik.transtimings.Calculator.d1latlng;
import static ru.markikokik.transtimings.Calculator.stationsLatLng;
import static ru.markikokik.transtimings.Calculator.stationsXY;
import static ru.markikokik.transtimings.Printer.hourFromDate;
import static ru.markikokik.transtimings.Templates.stopRequestTempl;

/**
 * Created by markikokik on 04.04.16.
 */
public class Utils {
    //                String regex = "[^0-9А-Яа-яёЁ]";
    private static final Calendar calendar = Calendar.getInstance();
    public static final int DAY_BOUND_HOUR = 3;
    public static final int DAY_SECS = 24 * 60 * 60;
    public static final long DAY_MILLIS = DAY_SECS * 1000l;
    public static final int DAY_OFFSET_SECS = DAY_BOUND_HOUR * 60 * 60;
    public static final long DAY_OFFSET_MILLIS = DAY_OFFSET_SECS * 1000l;

    private static final double LATITUDE_TO_M = 111111.11111;
    private static final double LONGITUDE_TO_M = 66684.132308;
    private static final double LONGITUDE_SECOND_TO_M = 30.922604938271604938271604938272;

    static {
        calendar.setTimeZone(TimeZone.getTimeZone("GMT+4:00"));
    }

    public static final int[] allTime = new int[]{1420070400, 1893456000};
    public static final Period allTimePeriod = new Period(0, allTime[0], allTime[1]);

    public static boolean pointNearRouteEnds(MonitoringLogEntry entry, KR_ID kr_id,
                                             Table<KR_ID, PeriodID, KS_ID[]> circuits,
                                             Map<KR_ID, Period> periods,
                                             Map<KS_ID, Stop> stops,
                                             GetDistBetween getDistBetweenProcedure) {
        KS_ID[] circuit = circuits.get(kr_id, periods.get(kr_id).id);
        Stop lastStop;
        Stop firstStop;
        lastStop = stops.get(circuit[circuit.length - 1]);
        firstStop = stops.get(circuit[0]);
        switch (kr_id.id) {
            case 114:
            case 115:
                if (getDistBetweenProcedure.getDistBetween(stops.get(new KS_ID(274)), entry) < 200) {
                    return true;
                }
                break;
            case 109: //24
                firstStop = stops.get(new KS_ID(185));
                break;
            case 108:
                lastStop = stops.get(new KS_ID(186));
                break;
            case 118: //5
            case 120: //7
                firstStop = stops.get(new KS_ID(195));
                break;
            case 117:
            case 119:
                lastStop = stops.get(new KS_ID(196));
                break;
            case 81: //1
            case 82:
            case 116: //4
            case 89: //16
            case 90:
            case 93: //18
            case 94:
            case 107: //23
                if (getDistBetweenProcedure.getDistBetween(stops.get(new KS_ID(752)), entry) <
                    Calculator.AREA) {
                    return true;
                }
                break;
            case 105: //22
            case 106: //22
                if (getDistBetweenProcedure.getDistBetween(stops.get(new KS_ID(197)), entry) < 400) {
                    return true;
                }
        }

        return getDistBetweenProcedure.getDistBetween(lastStop, entry) < 300 ||
               getDistBetweenProcedure.getDistBetween(firstStop, entry) < 300;
    }

    public static Table<MR_ID, KT_ID, Map<RaceID, List<PlanTimingRecord>>>
    splitPlanRacesByRaces(List<PlanTimingRecord> planTimingRecords) {
        Table<MR_ID, KT_ID, Map<RaceID, List<PlanTimingRecord>>> schedule
                = HashBasedTable.create();
        int lastRoute = -1, lastTransport = -1;
        RaceID lastRace = new RaceID(-1);
        Map<KT_ID, Map<RaceID, List<PlanTimingRecord>>> routeRaces = null;
        Map<RaceID, List<PlanTimingRecord>> transportRaces = null;
        List<PlanTimingRecord> raceTimings = null;
        for (PlanTimingRecord record : planTimingRecords) {
            if (lastRoute != record.mr_id) {
                routeRaces = schedule.row(new MR_ID(lastRoute = record.mr_id, record.transType));
                lastTransport = -1;
            }

            if (lastTransport != record.kt_id) {
                KT_ID kt_id = new KT_ID(lastTransport = record.kt_id);
                transportRaces = routeRaces.get(kt_id);
                if (transportRaces == null) {
                    routeRaces.put(kt_id, transportRaces = new TreeMap<>());
                }
                lastRace = new RaceID(-1);
            }

            if (lastRace.id != record.raceNumber) {
                raceTimings =
                        transportRaces.get(lastRace = new RaceID(record.raceNumber));
                if (raceTimings == null ||
                    record.time - raceTimings.get(raceTimings.size() - 1).time > 30) {
                    transportRaces.put(lastRace, raceTimings = new ArrayList<>());
                }
            }

            raceTimings.add(record);
        }

        return schedule;
    }

    public static double getDistBetween(PositionRelToRoutes pos1, PositionRelToRoutes pos2) {
        if (pos1.routeID != null && pos1.routeID instanceof KR_ID && pos1.routeID.id > 0 &&
            pos2.routeID != null && pos2.routeID instanceof KR_ID &&
            pos2.routeID.id == pos1.routeID.id) {
            double distBetweenSamples = getDistBetweenSamples(pos1, pos2);
            if (Math.abs(distBetweenSamples) < 500) {
                return distBetweenSamples;
            }
        }
        return getDistBetweenLatLng(pos1, pos2);
    }

    public static double getSampleMileage(PositionRelToRoutes pos, RouteInfo routeInfo) {
        int size = routeInfo.kr_ids.size();
        KR_ID kr_id;
        for (int i = 0; i < size; i++) {
            kr_id = routeInfo.kr_ids.get(i);
            if (pos.routeID.id == kr_id.id) {
                for (StopDistance stopDistance : routeInfo.stopMileages[i]) {
                    if (pos.nextStopID.id == stopDistance.ks_id.id) {
                        return stopDistance.dist - pos.nextStopDist;
                    }
                }

            }
        }
        return -1;
    }

    public static double getDistBetween(PositionRelToRoutes pos1, PositionRelToRoutes pos2,
                                        RouteInfo routeInfo) {
        if (pos1.routeID != null && pos1.routeID instanceof KR_ID && pos1.routeID.id > 0 &&
            pos2.routeID != null && pos2.routeID instanceof KR_ID &&
            pos2.routeID.id == pos1.routeID.id) {

            int size = routeInfo.kr_ids.size();
            KR_ID kr_id;
            for (int i = 0; i < size; i++) {
                kr_id = routeInfo.kr_ids.get(i);
                if (kr_id.id == pos1.routeID.id) {
                    if (pos1.dist == 0) {
                        pos1.dist = getSampleMileage(pos1, routeInfo);
                    }
                    if (pos2.dist == 0) {
                        pos2.dist = getSampleMileage(pos2, routeInfo);
                    }
                }
            }

            double distBetweenSamples = getDistBetweenSamples(pos1, pos2);
            if (Math.abs(distBetweenSamples) < 500) {
                return distBetweenSamples;
            }
        }
        return getDistBetweenLatLng(pos1, pos2);
    }


    /**
     * selects period matching given time
     *
     * @param periods
     * @param time
     * @return period matching given time
     */
    public static Period getPeriod(List<Period> periods, int time) {
        for (Period period : periods) {
            if (period.endTime >= time && period.startTime <= time) {
                return period;
            }
        }
        return allTimePeriod;
    }

    /**
     * @param dateYYYYMMDD
     * @return interval from 3:00 day matching given date to 3:00 of next day
     */
    public static int[] getDayLimits(String dateYYYYMMDD) {
        int year = Integer.parseInt(dateYYYYMMDD.substring(0, 4));
        int month = Integer.parseInt(dateYYYYMMDD.substring(4, 6));
        int day = Integer.parseInt(dateYYYYMMDD.substring(6, 8));
        return getDayLimits(year, month, day);
    }

    /**
     * @param limits
     * @return array of timestamps from limits[0] to limits[1] with intermediate values every 3:00
     */
    public static int[] splitByDays(int[] limits) {
        int firstIndex = 0, firstDayEnd = getDayEnd(limits[0]);
        if (firstDayEnd > limits[0]) {
            firstIndex++;
        }
        int rest = (limits[1] - firstDayEnd) % DAY_SECS;
        int count = (limits[1] - firstDayEnd - rest) / DAY_SECS + 1;
        if (rest > 0) {
            count++;
        }
        int[] result = new int[firstIndex + count];
        result[0] = limits[0];
        result[result.length - 1] = limits[1];
        for (int time = firstDayEnd; time < limits[1]; time += DAY_SECS) {
            result[firstIndex++] = time;
        }
        return result;
    }

    /**
     * @param year
     * @param month
     * @param day
     * @return timestamp matching 12:00 of given day
     */
    public static int getMiddayTimestamp(int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("GMT+4:00"));
        calendar.set(year, month, day, 12, 0);
        return (int) (calendar.getTimeInMillis() / 1000);
    }

    /**
     * @param year
     * @param month
     * @param day
     * @return timestamps from 3:00 day matching given date to 3:00 of next day
     */
    public static int[] getDayLimits(int year, int month, int day) {
        int[] result = new int[2];
        calendar.set(year, month - 1, day, DAY_BOUND_HOUR, 0, 0);
        result[0] = (int) (calendar.getTimeInMillis() / 1000);
        calendar.setTimeInMillis(calendar.getTimeInMillis() + DAY_MILLIS);
        result[1] = (int) (calendar.getTimeInMillis() / 1000);
        return result;
    }

    /**
     * @param time
     * @return timestamp matching next 3:00
     */
    public static int getDayEnd(int time) {
        return getDayBeginning(time) + DAY_SECS;
    }

    /**
     * @param time
     * @return timestamp matching next 3:00
     */
    public static int getDayEnd(int year, int month, int day) {
        return getDayBeginning(year, month, day) + DAY_SECS;
    }

    /**
     * @param time
     * @return timestamp matching 3:00 of given timestamp's day
     */
    public static int getDayBeginning(int time) {
        return getHourTimestampOfDay(time, DAY_BOUND_HOUR);
    }

    /**
     * @param year
     * @param month
     * @param day
     * @return timestamp matching 3:00
     */
    public static int getDayBeginning(int year, int month, int day) {
        calendar.set(year, month - 1, day, DAY_BOUND_HOUR, 0, 0);
        return (int) (calendar.getTimeInMillis() / 1000);
    }

    /**
     * @param time
     * @return timestamp of 12:00 of day matching given timestamp
     */
    public static int getMidday(int time) {
        return getHourTimestampOfDay(time, 12);
    }

    /**
     * @param time
     * @return timestamp of given hour of day matching given timestamp
     */
    public static int getHourTimestampOfDay(int time, int hour) {
        calendar.setTimeInMillis(time * 1000l - DAY_OFFSET_MILLIS);

        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return (int) (calendar.getTimeInMillis() / 1000);
    }

    public static int getHourFromTimestamp(int time) {
        return Integer.parseInt(Printer.getFormattedDate(hourFromDate, time));
    }

    public static double metersInLatitudeDegree() {
        return LATITUDE_TO_M;
    }

    public static double metersInLongitudeDegree(double latitude) {
        // return LATITUDE_SECOND_TO_M * Math.cos(getRadians(latitude)) * 3600;
        return kilometersInLongitudeDegree(latitude) * 1000;
    }

    public static double kilometersInLongitudeDegree(double latitude) {
        return LONGITUDE_SECOND_TO_M * Math.cos(getRadians(latitude)) * 3.6;
    }

    private static double getRadians(double degrees) {
        return degrees * Math.PI / 180;
    }

    public static String readStreamToString(InputStream in, String encoding) throws IOException {
        StringBuilder b = new StringBuilder();
        InputStreamReader r = new InputStreamReader(in, encoding);
        char[] buf = new char[1000];
        int c;
        while ((c = r.read(buf)) > 0) {
            b.append(buf, 0, c);
        }
        return b.toString();
    }

    /**
     * fills from geoportal and tosamara.ru geoportal ID and x, y of given stop object
     *
     * @param stop
     * @throws IOException
     */
    public static void loadStopSpatialData(Stop stop) throws IOException {
        String geoportalID, patternForGeoID = "baseStopId = \"", patternForXYstart = "<pos>",
                patternForXYend = "</pos>", content, message;
        int index;
        InputStream in;
        OutputStream os;
        HttpURLConnection httpConnection;
        String[] coords;

        in = new URL("http://tosamara.ru/spravka/ostanovki/" + stop.ks_id.id + "/").openStream();

        content = Utils.readStreamToString(in, "UTF-8");
        in.close();

        index = content.indexOf(patternForGeoID) + patternForGeoID.length();
        geoportalID = content.substring(index, content.indexOf("\"", index));

        message = stopRequestTempl.replace("%ID%", geoportalID);
        httpConnection = (HttpURLConnection) new URL("http://map.samadm.ru/wfs2").openConnection();
        httpConnection.setRequestProperty("Content-Type", "text/xml");
        httpConnection.setRequestProperty("Content-Length", String.valueOf(message.getBytes().length));
        httpConnection.setDoOutput(true);

        httpConnection.connect();

        os = httpConnection.getOutputStream();
        os.write(message.getBytes());
        os.flush();
        os.close();

        in = httpConnection.getInputStream();
        content = Utils.readStreamToString(in, "UTF-8");
        index = content.indexOf(patternForXYstart) + patternForXYstart.length();
        try {
            coords = content.substring(index, content.indexOf(patternForXYend, index)).split(" ");
            stop.x = Double.parseDouble(coords[0]);
            stop.y = Double.parseDouble(coords[1]);
            stop.geoportalID = Integer.parseInt(geoportalID);
        } catch (Exception ex) {
        }
    }


    public static double[] loadRouteCoords(Route route) throws IOException {
        String patternForXYstart = "<posList>", patternForXYend = "</posList>", content, message;
        int index;
        InputStream in;
        OutputStream os;
        HttpURLConnection httpConnection;

        message = stopRequestTempl.replace("%ID%", String.valueOf(route.geoportalId));
        httpConnection = (HttpURLConnection) new URL("http://map.samadm.ru/wfs2").openConnection();
        httpConnection.setRequestProperty("Content-Type", "text/xml");
        httpConnection.setRequestProperty("Content-Length", String.valueOf(message.getBytes().length));
        httpConnection.setDoOutput(true);

        httpConnection.connect();

        os = httpConnection.getOutputStream();
        os.write(message.getBytes());
        os.flush();
        os.close();

        in = httpConnection.getInputStream();
        content = Utils.readStreamToString(in, "UTF-8");
        index = content.indexOf(patternForXYstart) + patternForXYstart.length();
        try {
            String[] coords =
                    content.substring(index, content.indexOf(patternForXYend, index)).split(" ");
            double[] result = new double[coords.length];
            int i = 0;
            for (String coord : coords) {
                result[i++] = Double.parseDouble(coord);
            }
            return result;
        } catch (Exception ex) {
            return new double[0];
        }
    }


    public static double getAngleToLine(double[] line) {
        double A, B, x1, x2, y1, y2, cosa, sina;
        // lat *= LATITUDE_TO_M;
        // lng *= LONGITUDE_TO_M;
        x1 = line[0];
        x2 = line[2];
        y1 = line[1];
        y2 = line[3];
        A = 1 / (x2 - x1);
        B = 1 / (y1 - y2);
        cosa = -A / Math.sqrt(A * A + B * B);
        sina = -B / Math.sqrt(A * A + B * B);
        double result = Math.acos(cosa) * 180 * Math.signum(sina) / Math.PI;

        return result - 90 * Math.signum(cosa) * Math.signum(sina);
    }

    /**
     * @param routePoints attay of [x, y, x, y, ...]
     * @param x
     * @param y
     * @return
     */
    private static int getClosestEdgeID(double[] routePoints, double x, double y) {
        int result = -1;
        double minLength = 100000;
        double d;
        int length = routePoints.length - 2;
        for (int i = 0; i < length; i += 2) {
            d = getDistToSegment(routePoints, i, x, y);
            if (d < minLength) {
                minLength = d;
                result = i;
            }
        }
        return result / 2;
    }

    /**
     * calcs point of intersection for segments [x11, y11]-[x12, y12] and [x21, y21]-[x22, y22]
     *
     * @param x11
     * @param y11
     * @param x12
     * @param y12
     * @param x21
     * @param y21
     * @param x22
     * @param y22
     * @return
     */
    public static double[] segmentsIntersectPoint(double x11, double y11, double x12, double y12,
                                                  double x21, double y21, double x22, double y22) {
        double K1 = (y12 - y11) / (x12 - x11),
                d1 = (x12 * y11 - x11 * y12) / (x12 - x11),
                K2 = (y22 - y21) / (x22 - x21),
                d2 = (x22 * y21 - x21 * y22) / (x22 - x21),
                x = (d2 - d1) / (K1 - K2),
                y = K1 * x + d1;

        double tmp;
        if (x12 < x11) {
            tmp = x11;
            x11 = x12;
            x12 = tmp;
        }
        if (y12 < y11) {
            tmp = y11;
            y11 = y12;
            y12 = tmp;
        }
        if (x22 < x21) {
            tmp = x21;
            x21 = x22;
            x22 = tmp;
        }
        if (y22 < y21) {
            tmp = y21;
            y21 = y22;
            y22 = tmp;
        }

        if (x >= x11 && x <= x12 && x >= x21 && x <= x22 &&
            y >= y11 && y <= y12 && y >= y21 && y <= y22) {
            return new double[]{x, y};
        } else {
            return null;
        }
    }


    /**
     * determines if point lies on terminal station
     *
     * @param point
     * @return
     */
    public static boolean pointOnStationLatLng(Point point) {
        for (int i = 0; i < stationsLatLng.length; i++) {
            if (pointOnStationLatLng(point, i)) {
                return true;
            }
        }

        return false;
    }

    /**
     * determines if point lies on terminal station
     *
     * @param point
     * @return
     */
    public static boolean pointOnStationLatLng(Point point, int stationIndex) {
        return pointOnStationLatLng(point, stationsLatLng[stationIndex], d1latlng[stationIndex],
                                    K1latlng[stationIndex]);
    }

    /**
     * determines if point lies on terminal station
     *
     * @param point
     * @return
     */
    public static boolean pointOnStationLatLng(Point point, double[] coords, double[] d1, double[] K1) {
        double tmp;
        double x11, x12, y11, y12;
        double x;
        boolean intersect = false;
        int count;

        int nextIndex;
        count = coords.length / 2;
        for (int j = 0; j < count; j++) {
            nextIndex = (j + 1) % count;
            y11 = coords[j * 2 + 1];
            y12 = coords[nextIndex * 2 + 1];

            if (y12 < y11) {
                tmp = y11;
                y11 = y12;
                y12 = tmp;
            }

            if (point.lng < Math.min(y11, y12) || point.lng > Math.max(y11, y12)) {
                continue;
            }

            x11 = coords[j * 2];
            x12 = coords[nextIndex * 2];
            if (x12 < x11) {
                tmp = x11;
                x11 = x12;
                x12 = tmp;
            }

            x = (point.lng - d1[j]) / K1[j];

            if (x >= Math.min(x11, x12) && x <= Math.max(x11, x12) && x >= point.lat) {
                if (getDistSqBetweenLatLng(x, point.lng, x12, y12) >= 2) {
                    intersect = !intersect;
                }
            }
        }

        return intersect;
    }

    /**
     * determines if point lies on terminal station
     *
     * @param point
     * @return
     */
    public static boolean pointOnStationXY(Point point) {
        for (int i = 0; i < stationsXY.length; i++) {
            if (pointOnStationXY(point, i)) {
                return true;
            }
        }

        return false;
    }

    /**
     * determines if point lies on terminal station
     *
     * @param point
     * @return
     */
    public static boolean pointOnStationXY(Point point, int stationIndex) {
        return pointOnStationXY(point, stationsXY[stationIndex], d1XY[stationIndex],
                                K1XY[stationIndex]);
    }

    /**
     * determines if point lies on terminal station
     *
     * @param point
     * @return
     */
    public static boolean pointOnStationXY(Point point, double[] coords, double[] d1, double[] K1) {
        double tmp;
        double x11, x12, y11, y12;
        double x;
        boolean intersect = false;
        int count;

        int nextIndex;
        count = coords.length / 2;
        for (int j = 0; j < count; j++) {
            nextIndex = (j + 1) % count;
            y11 = coords[j * 2 + 1];
            y12 = coords[nextIndex * 2 + 1];

            if (y12 < y11) {
                tmp = y11;
                y11 = y12;
                y12 = tmp;
            }

            if (point.y < Math.min(y11, y12) || point.y > Math.max(y11, y12)) {
                continue;
            }

            x11 = coords[j * 2];
            x12 = coords[nextIndex * 2];
            if (x12 < x11) {
                tmp = x11;
                x11 = x12;
                x12 = tmp;
            }

            x = (point.y - d1[j]) / K1[j];

            if (x >= Math.min(x11, x12) && x <= Math.max(x11, x12) && x >= point.x) {
                if (getDistSqBetweenLatLng(x, point.y, x12, y12) >= 2) {
                    intersect = !intersect;
                }
            }
        }

        return intersect;
    }

    /**
     * determines if normal from (x,y) to line[fromIndex]-line[fromIndex+1] intersects specified segment
     *
     * @param line
     * @param fromIndex
     * @param x
     * @param y
     * @return
     */
    public static boolean isNearEdge(double[] line, int fromIndex, double x, double y) {
        double diff1 = line[fromIndex] - line[fromIndex + 2];
        double diff2 = line[fromIndex + 1] - line[fromIndex + 3];
        double lcos1 = diff1 * (x - line[fromIndex + 2]) + diff2 * (y - line[fromIndex + 3]);
        double lcos2 = diff1 * (x - line[fromIndex]) + diff2 * (y - line[fromIndex + 1]);

        return lcos1 >= 0 && lcos2 <= 0;
    }


    /**
     * determines speed based on 2D coords
     *
     * @param pos1
     * @param pos2
     * @return
     */
    public static double getSpeedBetween(TransportPosition pos1, TransportPosition pos2) {
        return Math.abs(getDistBetween(pos1, pos2) / (pos2.timestamp - pos1.timestamp));
//        int count = Math.min(pos1.mileages.length, pos2.mileages.length);
//        for (int i = 0; i < count; i++) {
//            if (pos1.mileages[i] >= 0 && pos2.mileages[i] >= 0) {
//                return Math.abs((pos1.mileages[i] - pos2.mileages[i]) / (pos2.timestamp - pos1.timestamp));
//            }
//        }
//        return Double.MAX_VALUE;
    }

    /**
     * determines distance based on distances difference
     *
     * @param pos1
     * @param pos2
     * @return
     */
    public static double getDistBetweenSamples(TransportPosition pos1, TransportPosition pos2) {
        return pos2.dist - pos1.dist;
    }

    /**
     * determines speed based on distances difference
     *
     * @param pos1
     * @param pos2
     * @return
     */
    public static double getSpeedBetweenSamples(TransportPosition pos1, TransportPosition pos2) {
        return (pos2.dist - pos1.dist) / (pos2.timestamp - pos1.timestamp);
    }

    /**
     * @param a
     * @param b
     * @return a*a+b*b
     */
    public static double sumSq(double a, double b) {
        return a * a + b * b;
    }

    public static double getDistBetween(double x1, double y1, double x2, double y2) {
        return Math.sqrt(getDistSqBetween(x1, y1, x2, y2));
    }

    public static double getDistSqBetween(double x1, double y1, double x2, double y2) {
        return sumSq(x2 - x1, y2 - y1);
    }

    public static double getDistBetween(Point pos1, Point pos2) {
        return Math.sqrt(getDistSqBetween(pos1, pos2));
    }

    public static double getDistSqBetween(Point pos1, Point pos2) {
        return sumSq(pos1.x - pos2.x, pos1.y - pos2.y);
    }

    public static double getDistBetween(double[] point, int fromIndex, double x2, double y2) {
        return Math.sqrt(getDistSqBetween(point, fromIndex, x2, y2));
    }

    public static double getDistSqBetween(double[] point, int fromIndex, double x2, double y2) {
        return getDistSqBetween(point[fromIndex], point[fromIndex + 1], x2, y2);
    }

    public static double getDistBetweenLatLng(double lat1, double lng1, double lat2, double lng2) {
        return Math.sqrt(getDistSqBetweenLatLng(lat1, lng1, lat2, lng2));
    }

    public static double getDistSqBetweenLatLng(double lat1, double lng1, double lat2, double lng2) {
        return sumSq((lat2 - lat1) * metersInLatitudeDegree(),
                     (lng2 - lng1) * metersInLongitudeDegree(lat1));
    }

    public static double getSpeedBetweenLatLng(TransportPosition pos1, TransportPosition pos2) {
        return Math.sqrt(getDistSqBetweenLatLng(pos1, pos2)) / Math.abs(pos2.timestamp - pos1.timestamp);
    }

    public static double getDistBetweenLatLng(Point pos1, Point pos2) {
        return Math.sqrt(getDistSqBetweenLatLng(pos1, pos2));
    }

    public static double getDistSqBetweenLatLng(Point pos1, Point pos2) {
        return getDistSqBetweenLatLng(pos1.lat, pos1.lng, pos2.lat, pos2.lng);
    }


    public static double getDistToLine(double[] line, int fromIndex, double x, double y) {
        return Math.sqrt(getDistSqToLine(line, fromIndex, x, y));
    }

    public static double getDistSqToLine(double[] line, int fromIndex, double x, double y) {
        double A = 1 / (line[fromIndex + 2] - line[fromIndex]),
                B = 1 / (line[fromIndex + 1] - line[fromIndex + 3]),
                C = A * (x - line[fromIndex]) + B * (y - line[fromIndex + 1]);

        return C * C / sumSq(A, B);
    }

    /**
     * @param segment
     * @param fromIndex
     * @param x
     * @param y
     * @return dist to line, if normal from point to segment intersects segment, otherwise min distance from point to
     * ends
     */
    public static double getDistToSegment(double[] segment, int fromIndex, double x, double y) {
        return isNearEdge(segment, fromIndex, x, y) ?
               getDistToLine(segment, fromIndex, x, y) :
               Math.min(getDistBetween(segment, fromIndex, x, y),
                        getDistBetween(segment, fromIndex + 2, x, y));
    }

    public static double getStopMileage(double[] routeTrack, Stop stop) {
        return getAnyPointMileage(routeTrack, stop.x, stop.y);
    }

    /**
     * @param routeTrack
     * @param x
     * @param y
     * @return mileage to point lies on route track
     */
    public static double getFixedPointMileage(double[] routeTrack, double x, double y) {
        int edgeIndex = getClosestEdgeID(routeTrack, x, y);
        return getFixedPointMileage(routeTrack, edgeIndex, x, y);
    }

    /**
     * @param routeTrack
     * @param pointEdgeIndex
     * @param x
     * @param y
     * @return mileage from track beginning to point lies on it edge with pointEdgeIndex
     */
    public static double getFixedPointMileage(double[] routeTrack, int pointEdgeIndex, double x,
                                              double y) {
        double result = calcRouteMileageToEdge(routeTrack, pointEdgeIndex);
        result += getDistBetween(routeTrack, 2 * pointEdgeIndex, x, y);
        return result;
    }

    /**
     * @param routeTrack
     * @param position
     * @return mileage to any point - along route track to nearest edge + dist from edge to point
     */
    public static double getAnyPointMileage(double[] routeTrack, Point position) {
        return getAnyPointMileage(routeTrack, position.x, position.y);
    }

    public static double getAnyPointMileage(double[] routeTrack, double x, double y) {
        int edgeIndex = getClosestEdgeID(routeTrack, x, y);
        return getAnyPointMileage(routeTrack, edgeIndex, x, y);
    }

    public static double getPointDistToRoute(double[] routeTrack, Point position) {
        return Math.sqrt(getPointDistSqToRoute(routeTrack, position));
    }

    public static double getPointDistSqToRoute(double[] routeTrack, Point position) {
        return getPointDistSqToRoute(routeTrack, position.x, position.y);
    }

    public static double getPointDistToRoute(double[] routeTrack, double x, double y) {
        return Math.sqrt(getPointDistSqToRoute(routeTrack, x, y));
    }

    public static double getPointDistSqToRoute(double[] routeTrack, double x, double y) {
        int length = routeTrack.length;
        double minDist = getDistSqBetween(x, y, routeTrack[0], routeTrack[1]), dist;
        for (int i = 2; i < length; i += 2) {
            if (isNearEdge(routeTrack, i - 2, x, y)) {
                dist = getDistSqToLine(routeTrack, i - 2, x, y);
            } else {
                dist = getDistSqBetween(x, y, routeTrack[i], routeTrack[i + 1]);
            }
            if (dist < minDist) {
                minDist = dist;
            }
        }
        return minDist;
    }

    /**
     * @param routeTrack
     * @param pointEdgeIndex
     * @param x
     * @param y
     * @return mileage along route track to edgeIndex + mileage to any point (x, y)
     */
    public static double getAnyPointMileage(double[] routeTrack, int pointEdgeIndex, double x,
                                            double y) {
        if (isNearEdge(routeTrack, 2 * pointEdgeIndex, x, y)) {
            double[] pointOnRoute = normalIntersectPoint(routeTrack, 2 * pointEdgeIndex, x, y);
            return getFixedPointMileage(routeTrack, pointEdgeIndex, pointOnRoute[0], pointOnRoute[1]);
        } else {
            return getDistBetween(routeTrack, 2 * pointEdgeIndex, x, y) <
                   getDistBetween(routeTrack, 2 * pointEdgeIndex + 2, x, y) ?

                   calcRouteMileageToEdge(routeTrack, pointEdgeIndex) :
                   calcRouteMileageToEdge(routeTrack, pointEdgeIndex + 1);
        }
    }

    public static double calcRouteMileageToEdge(double[] routeTrack, int edgeIndex) {
        double result = 0;
        edgeIndex *= 2;
        for (int i = 0; i < edgeIndex; i += 2) {
            result += getDistBetween(routeTrack[i], routeTrack[i + 1], routeTrack[i + 2],
                                     routeTrack[i + 3]);
        }
        return result;
    }

    /**
     * determines intersect point of normal from (x,y) to line with line
     *
     * @param line
     * @param fromIndex
     * @param x
     * @param y
     * @return
     */
    public static double[] normalIntersectPoint(double[] line, int fromIndex, double x, double y) {
        return normalIntersectPoint(line[fromIndex], line[fromIndex + 1], line[fromIndex + 2],
                                    line[fromIndex + 3],
                                    x, y);
    }

    public static double[] normalIntersectPoint(double xLine1, double yLine1, double xLine2,
                                                double yLine2,
                                                double x3, double y3) {
        double A = (yLine2 - yLine1) / (xLine2 - xLine1);
        double B = -A * xLine1 + yLine1;
        double C = 1 / A * x3 + y3;
        double[] result = new double[2];
        result[0] = (C - B) / (A + 1 / A);
        result[1] = A * result[0] + B;
        return result;
    }


    @FunctionalInterface
    public interface GetDistBetween {
        double getDistBetween(Point pos1, Point pos2);
    }
}
