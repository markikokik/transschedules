package ru.markikokik.transtimings;

/**
 * Created by markikokik on 24.05.16.
 */
public interface Templates {
    String STYLE_STUB = "%STYLE%";
    String GEOMETRY_STUB = "%GEOMETRY%";
    String TITLE_STUB = "%TITLE%";
    String DESC_STUB = "%DESC%";
    String X_STUB = "%x%";
    String Y_STUB = "%y%";
    String CREATE_MARKER_STUB = "%CREATE_MARKER%";

    String CREATE_MARKER =
            "\tgetMovie().createMarker(layerId, \"%STYLE%\", %GEOMETRY%, '%TITLE%', unescape(\"%DESC%\"));\n";
    String ACCIDENTS = "<html>\n" +
                       "<head>\n" +
                       "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />" +
                       "</head>\n" +
                       "<body>\n" +
                       "\n" +
                       "<div style=\"width: 100%; height: 100%;\" id=\"mapContainer\"></div>\n" +
                       "<script src=\"initApiSamples.js\"></script>\n" +
                       "\n" +
                       "\n" +
                       "<script>\n" +
                       "function imReady()\n" +
                       "{\n" +
                       "\tvar layerId=\"local\";\n" +
                       "\tgetMovie().createLocalLayer(layerId, 'задержки движения');\n" +
                       "\tgetMovie().clearLocalLayer(layerId);\t\t    \n" +
                       "\tgetMovie().turnLayer(layerId, true);\n" +
                       "\t\t    \n" +
                       "%CREATE_MARKER%" +
                       "} \n" +
                       "\n" +
                       "function $(name)\n" +
                       "{\n" +
                       "\treturn document.getElementById(name);\n" +
                       "}\n" +
                       " \n" +
                       "function getMovie() \n" +
                       "{\n" +
                       "\tif(typeof  window.gpClient != 'undefined'){\n" +
                       "\t\treturn window.gpClient.getMovieAPI();\n" +
                       "\t}else{\n" +
                       "\t\tvar M$ =  navigator.appName.indexOf(\"Microsoft\")!=-1;\n" +
                       "\t\tvar result= (M$ ? window : document)[\"GeoportalMap\"];\n" +
                       "\t\treturn result;\n" +
                       "\t}\n" +
                       "}\n" +
                       "</script>\n" +
                       "\n" +
                       "</body>\n" +
                       "</html>";
    String GEOMETRY = "JSON.stringify([{\"x\":%x%,\"y\":%y%}])";
    String MOVING_TRAM_STYLE = "moving_tram";
    String MOVING_TROLL_STYLE = "moving_trol";

    String stopRequestTempl =
            "<wfs:GetFeature service=\"WFS\" version=\"2.0.0\" outputFormat=\"application/gml+xml; version=3.2\" xsi:schemaLocation=\"http://www.opengis.net/wfs/2.0 http://schemas.opengis.net/wfs/2.0.0/wfs.xsd\" xmlns:geosmr=\"http://www.geosamara.ru/wfs/geosmr/namespace\" xmlns:wfs=\"http://www.opengis.net/wfs/2.0\" xmlns:fes=\"http://www.opengis.net/fes/2.0\" xmlns:gml=\"http://www.opengis.net/gml/3.2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
            "  <wfs:Query typeNames=\"geosmr:*\" useAreaRestriction=\"false\">\n" +
            "    <fes:Filter>\n" +
            "      <fes:Or>\n" +
            "        <fes:PropertyIsEqualTo>\n" +
            "          <fes:ValueReference>geosmr:id</fes:ValueReference>\n" +
            "          <fes:Literal>%ID%</fes:Literal>\n" +
            "        </fes:PropertyIsEqualTo>\n" +
            "      </fes:Or>\n" +
            "    </fes:Filter>\n" +
            "  </wfs:Query>\n" +
            "</wfs:GetFeature>";
}
