package ru.markikokik.transtimings;


import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import org.apache.commons.lang3.StringEscapeUtils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Writer;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import ru.markikokik.transtimings.Calculator.TimeBetween;
import ru.markikokik.transtimings.db.DataBase;
import ru.markikokik.transtimings.db.connectors.Connector;
import ru.markikokik.transtimings.entities.Accident;
import ru.markikokik.transtimings.entities.ControlPoint;
import ru.markikokik.transtimings.entities.Deviation;
import ru.markikokik.transtimings.entities.Matching;
import ru.markikokik.transtimings.entities.Period;
import ru.markikokik.transtimings.entities.PositionRelToRoutes;
import ru.markikokik.transtimings.entities.Race;
import ru.markikokik.transtimings.entities.RaceTimings;
import ru.markikokik.transtimings.entities.Route;
import ru.markikokik.transtimings.entities.Stop;
import ru.markikokik.transtimings.entities.StopDistance;
import ru.markikokik.transtimings.entities.TransportPosition;
import ru.markikokik.transtimings.entities.Vehicle;
import ru.markikokik.transtimings.entities.ids.CP_ID;
import ru.markikokik.transtimings.entities.ids.ID;
import ru.markikokik.transtimings.entities.ids.KR_ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;
import ru.markikokik.transtimings.entities.ids.KT_ID;
import ru.markikokik.transtimings.entities.ids.MR_ID;
import ru.markikokik.transtimings.entities.ids.PeriodID;
import ru.markikokik.transtimings.entities.ids.RouteIDs;
import ru.markikokik.transtimings.entities.ids.VehicleTN;

import static ru.markikokik.transtimings.Calculator.MIN_ACCIDENT_CLUSTER_SIZE;

/**
 * Здесь разнообразные представления посчитанного - печать в
 * удобной форме, формирование HTML или геопортальной страницы
 */
public class Printer implements Templates {
    //    private static final SimpleDateFormat hmDateFormat = new SimpleDateFormat("HH:mm:ss");
    private static final Date date = new Date();
    public static final SimpleDateFormat fullDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
    public static final SimpleDateFormat dateDateFormat = new SimpleDateFormat("yyyy.MM.dd");
    public static final SimpleDateFormat hmDateFormat = new SimpleDateFormat("HH:mm");
    public static final SimpleDateFormat hmsDateFormat = new SimpleDateFormat("HH:mm:ss");
    public static final SimpleDateFormat hmDurationDateFormat = new SimpleDateFormat("HH:mm");
    private static final SimpleDateFormat mSecFormat = new SimpleDateFormat("mm:ss");
    public static final SimpleDateFormat hourFromDate = new SimpleDateFormat("HH");
    private static final SimpleDateFormat minuteFromDate = new SimpleDateFormat("mm");

    static {
        hmDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+4:00"));
        hmDurationDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+0:00"));
        hourFromDate.setTimeZone(TimeZone.getTimeZone("GMT+4:00"));
        minuteFromDate.setTimeZone(TimeZone.getTimeZone("GMT+4:00"));
        mSecFormat.setTimeZone(TimeZone.getTimeZone("GMT+4:00"));
        dateDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+4:00"));
    }


    public static void printStaysTimeline(Writer out,
                                          Matching<TransportPosition<VehicleTN>, Integer>[] timeline)
            throws IOException {
        int count = 0;
        for (Matching<TransportPosition<VehicleTN>, Integer> matching : timeline) {
            out.write(Printer.getFormattedDate(Printer.fullDateFormat, matching.first.timestamp) + "\t" +
                      matching.first.vehicleID.id);
            if (matching.second > count) {
                out.write("\tarrived");
            } else {
                out.write("\tleft");
            }
            count = matching.second;

            out.write("\t" + matching.second + "\n");
        }
    }

    public static void printDistribution(Table<MR_ID, VehicleTN, List<RaceTimings>> distrib) {
        for (Map.Entry<MR_ID, Map<VehicleTN, List<RaceTimings>>> routeEntry : distrib.rowMap()
                                                                                     .entrySet()) {
            System.out.println(routeEntry.getKey());
            for (Map.Entry<VehicleTN, List<RaceTimings>> vehicleEntry : routeEntry.getValue()
                                                                                  .entrySet()) {
                System.out.print(vehicleEntry.getKey());
                System.out.print(": ");
                for (RaceTimings raceTimings : vehicleEntry.getValue()) {
                    printDate(raceTimings.startTime);
                    System.out.print("-");
                    printDate(raceTimings.endTime);
                    System.out.print(" ");
                }
                System.out.println();
                System.out.println();
            }
        }
    }

    public static void accidentsToCsv(
            Writer out,
            Collection<? extends List<? extends Accident<? extends ID, ? extends VehicleTN>>> accidents,
            int minDuration, int maxDuration
    ) throws IOException {
        for (List<? extends Accident<? extends ID, ? extends VehicleTN>> cluster : accidents) {
            if (cluster.size() < MIN_ACCIDENT_CLUSTER_SIZE) {
                continue;
            }
            int time = 0, minTime = Integer.MAX_VALUE;
            Accident<? extends ID, ? extends VehicleTN> firstAccident = null;
            for (Accident<? extends ID, ? extends VehicleTN> accident : cluster) {
                time += accident.end.timestamp - accident.start.timestamp;
                if (accident.start.timestamp < minTime) {
                    minTime = accident.start.timestamp;
                    firstAccident = accident;
                }
//                out.write(String.valueOf(accident.start.lat));
//                out.write(",");
//                out.write(String.valueOf(accident.start.lng));
//                out.write(",");
//                out.write(String.valueOf(accident.start.timestamp));
//                out.write(",");
//                out.write(String.valueOf(accident.end.timestamp - accident.start.timestamp));
//                out.write("\n");
            }
            int duration = firstAccident.end.timestamp - firstAccident.start.timestamp;
            if (duration < minDuration || duration > maxDuration) {
                continue;
            }
            out.write(String.valueOf(firstAccident.start.lat));
            out.write(",");
            out.write(String.valueOf(firstAccident.start.lng));
            out.write(",");
            out.write(String.valueOf(cluster.size()));
            out.write(",");
            out.write(String.valueOf(firstAccident.start.timestamp));
            out.write(",");
            out.write(String.valueOf(firstAccident.end.timestamp - firstAccident.start.timestamp));
            out.write(",");
            out.write(String.valueOf(time));
            out.write("\n");
        }
    }

    /**
     * вывод страницы с геопортальной картой, отображающей задержки движения
     */
    public static void visualizeAccidents(Connector connector, int[] timeLimits) {
        try (DataBase db = new DataBase(connector)) {
            try (FileWriter out = new FileWriter("/var/www/html/accidents.html")) {
                visualizeAccidents(out, db, db.getAccidentsByVehicle(timeLimits));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * вывод страницы с геопортальной картой, отображающей задержки движения
     */
    public static void visualizeAccidents(
            Writer out, DataBase db,
            Map<VehicleTN, ? extends List<? extends Accident<? extends ID, ? extends VehicleTN>>> accidents
    )
            throws SQLException, IOException {
        visualizeAccidents(out, db, accidents.values());
    }

    /**
     * вывод страницы с геопортальной картой, отображающей задержки движения
     */
    public static void visualizeAccidents(
            Writer out, DataBase db,
            Collection<? extends List<? extends Accident<? extends ID, ? extends VehicleTN>>> accidents
    )
            throws SQLException, IOException {
        Map<? extends ID, Route> routes = db.getAllRoutes();
        Vehicle vehicle;
        Route route;
        String style;
        String title;
        String desc;
        StringBuilder api = new StringBuilder();
        Stop stop;
        Map<KS_ID, Stop> stops = new HashMap<>();
        Map<MR_ID, RouteIDs> allRouteIDs = new HashMap<>();

        for (List<? extends Accident<? extends ID, ? extends VehicleTN>> cluster : accidents) {
            for (Accident<? extends ID, ? extends VehicleTN> accident : cluster) {
                vehicle = db.getVehicle(accident.start.vehicleID);

                style = vehicle.transType == 3 ? MOVING_TRAM_STYLE : MOVING_TROLL_STYLE;

                if (accident.routeID instanceof KR_ID) {
                    route = routes.get(accident.routeID);
                    title = " -> " + route.direction;
                } else {
                    RouteIDs routeIDs = allRouteIDs.get((MR_ID) accident.routeID);
                    if (routeIDs == null) {
                        allRouteIDs.put(
                                (MR_ID) accident.routeID,
                                routeIDs = db.getRelatedRouteIDs((MR_ID) accident.routeID,
                                                                 Utils.getDayBeginning(
                                                                         accident.start.timestamp))
                        );
                    }
                    route = routes.get(
                            routeIDs.kr_ids.size() > 0 ? routeIDs.kr_ids.get(0) : new KR_ID(0));
                    title = "";
                }
                title = vehicle.boardNum + " / " + route.getNumber() + title;

                desc = "Движение остановлено:\\t" +
                       getFormattedDate(hmDateFormat, accident.start.timestamp) +
                       "\\t";
                if (accident.start.nextStopID != null) {
                    try {
                        stop = stops.get(accident.start.nextStopID);
                        if (stop == null) {
                            stops.put(accident.start.nextStopID,
                                      stop = db.getStop(accident.start.nextStopID));
                        }
                        if (stop != null) {
                            desc += Math.round(accident.start.nextStopDist) + " м до " +
                                    StringEscapeUtils.escapeEcmaScript(stop.title) + " {" +
                                    stop.ks_id.id + "}";
                        }
                    } catch (SQLException e) {
                        desc += accident.start.dist + " м от начала маршрута";
                    }
                }

                desc += "\\n" + "Движение восстановлено:\\t" +
                        getFormattedDate(hmDateFormat, accident.end.timestamp) + " ";
//                try {
//                    stop = stops.get(accident.end.nextStopID);
//                    if (stop == null) {
//                        stops.put(accident.end.nextStopID, stop = db.getStop(accident.end.nextStopID));
//                    }
//                    desc += Math.round(accident.end.nextStopDist) + " м до " +
//                            StringEscapeUtils.escapeEcmaScript(stop.title) + " {" + stop.ks_id.id + "}";
//                } catch (SQLException e) {
//                    desc += accident.end.dist + " м от начала маршрута";
//                }
                desc += "\\n" + vehicle;
                api.append(Templates.CREATE_MARKER.replace(STYLE_STUB, style)
                                                  .replace(TITLE_STUB, title)
                                                  .replace(DESC_STUB, desc)
                                                  .replace(GEOMETRY_STUB,
                                                           GEOMETRY.replace(
                                                                   X_STUB,
                                                                   String.valueOf(accident.start.x)
                                                           ).replace(
                                                                   Y_STUB,
                                                                   String.valueOf(accident.start.y)
                                                           )
                                                  )
                );
            }
        }

        out.write(ACCIDENTS.replace(CREATE_MARKER_STUB, api.toString()));
    }

    /**
     * Печать информации о задержках
     *
     * @param connector
     * @param timeLimits
     */
    public static void printAccidents(Connector connector, int[] timeLimits) {
        try (DataBase db = new DataBase(connector)) {
            List<List<Accident<ID, VehicleTN>>> accidents = db.getAccidentsAsList(timeLimits);
            Table<KR_ID, PeriodID, double[]> tracks = HashBasedTable.create();
            Table<KR_ID, Period, StopDistance[]> distances = HashBasedTable.create();
            StopDistance[] routeDistances;
            Period period;
            KR_ID kr_id;
            double[] track;
            Map<KR_ID, Route> routes = db.getAllRoutes();
            Map<KS_ID, Stop> stops = db.getAllStops();
            Accident<ID, VehicleTN> vehicleAccident;
            KS_ID ks_id;
            int min, max, sum;
            double dist, minDist;
            Stop stop;

            for (List<Accident<ID, VehicleTN>> cluster : accidents) {
                ks_id = null;
                min = Integer.MAX_VALUE;
                max = 0;
                sum = 0;
                for (Accident<ID, VehicleTN> accident : cluster) {
                    if (accident.routeID instanceof KR_ID) {
                        kr_id = (KR_ID) accident.routeID;
                        period = db.getRoutePeriodForTime(kr_id, accident.start.timestamp);
                        track = tracks.get(kr_id, period.id);
                        if (track == null) {
                            tracks.put(kr_id, period.id, track = db.getRouteGeometry(kr_id, period.id));
                        }
                        routeDistances = distances.get(kr_id, period);
                        if (routeDistances == null) {
                            distances.put(kr_id, period,
                                          routeDistances =
                                                  db.getRouteCircuitWithDistsList(kr_id, period.id));
                        }
                        accident.start.dist =
                                Utils.getAnyPointMileage(track, accident.start.x, accident.start.y);
                        Corrector.determineNextStop(accident.start, routeDistances);
                        accident.end.dist =
                                Utils.getFixedPointMileage(track, accident.end.x, accident.end.y);
                        Corrector.determineNextStop(accident.end, routeDistances);
                        ks_id = accident.start.nextStopID;
                    }
                    if (min > accident.start.timestamp) {
                        min = accident.start.timestamp;
                    }
                    if (max < accident.end.timestamp) {
                        max = accident.end.timestamp;
                    }
                    sum += accident.end.timestamp - accident.start.timestamp;
                }

                if (ks_id == null) {
                    minDist = Double.MAX_VALUE;
                    vehicleAccident = cluster.get(0);
                    for (Map.Entry<KS_ID, Stop> stopEntry : stops.entrySet()) {
                        if (minDist > (dist = Utils.getDistSqBetween(stopEntry.getValue(),
                                                                     vehicleAccident.start))) {
                            minDist = dist;
                            ks_id = stopEntry.getKey();
                        }
                    }
                }

                stop = stops.get(ks_id);
                printAccidentsCluster(db, cluster, stop, stops, routes, min, max, sum);

            }
            try (FileWriter out = new FileWriter("/var/www/html/accidents.html")) {
                visualizeAccidents(out, db, accidents);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.nanoTime();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    /**
     * Печать информации о задержках, сгруппированных по месту и времени (т.е. один
     * простой нескольких трамваев)
     *
     * @param db
     * @param cluster
     * @param stopNear
     * @param stops
     * @param routes
     * @param startTime
     * @param endTime
     * @param sumDuration
     * @throws SQLException
     */
    public static void printAccidentsCluster(DataBase db,
                                             List<Accident<ID, VehicleTN>> cluster,
                                             Stop stopNear,
                                             Map<KS_ID, Stop> stops,
                                             Map<KR_ID, Route> routes,
                                             int startTime, int endTime, int sumDuration
    ) throws SQLException {
        Vehicle vehicle;
        Route route;
        RouteIDs routeIDs;
        System.out
                .print("Около ост. " + stopNear.title + " " + stopNear.adjacentStreet + " " +
                       stopNear.direction + " ");
        System.out.print(getFormattedDate(startTime));
        System.out.print(" - ");
        System.out.print(getFormattedDate(endTime));
        System.out.print(" (");
        System.out.print((endTime - startTime) / 60);
        System.out.print(" мин, суммарно ");
        System.out.print(getFormattedDate(hmDurationDateFormat, sumDuration));
        System.out.print(", ");
        System.out.print(cluster.size());
        System.out.print(" единиц");
        System.out.println(")");
        for (Accident<ID, VehicleTN> accident : cluster) {
            vehicle = db.getVehicle(accident.start.vehicleID);
            System.out.print(vehicle.transType == 3 ? "Трам" : "Тролл ");
            System.out.print(" ");
            System.out.print(vehicle.boardNum);
            System.out.print(": ");
            System.out.print(getFormattedDate(accident.start.timestamp));
            System.out.print(" - ");
            System.out.print(getFormattedDate(accident.end.timestamp));
            System.out.print(" (");
            System.out.print((accident.end.timestamp - accident.start.timestamp) / 60);
            System.out.print(" мин), мрш ");

            if (accident.routeID instanceof KR_ID) {
                route = routes.get(accident.routeID);
                System.out.print(route.getNumber());
                System.out.print(": >> ");
                System.out.print(route.direction);
            } else {
                routeIDs = db.getRelatedRouteIDs((MR_ID) accident.routeID,
                                                 Utils.getDayBeginning(accident.start.timestamp));
                if (routeIDs.kr_ids.size() > 0) {
                    route = routes.get(routeIDs.kr_ids.get(0));
                    System.out.print(route.getNumber());
                }
            }
            if (accident.start.nextStopID != null) {
                System.out.print(" (");
                System.out.print(Math.round(accident.start.nextStopDist));
                System.out.print(" м до ");
                System.out.print(stops.get(accident.start.nextStopID));
                System.out.print(")");
            } else if (accident.end.nextStopID != null) {
                System.out.print(" (");
                System.out.print(Math.round(accident.end.nextStopDist));
                System.out.print(" м до ");
                System.out.print(stops.get(accident.end.nextStopID));
                System.out.print(")");
            }

            System.out.println();
        }
        System.out.println();
    }

    public static void printAccidents(DataBase db, Map<KT_ID, List<Accident<KR_ID, KT_ID>>> accidents)
            throws SQLException {
        Map<KR_ID, Route> routes = db.getAllRoutes();
        Vehicle vehicle;
        Route route;
        Stop stop;
        Map<KS_ID, Stop> stops = new HashMap<>();

        for (Map.Entry<KT_ID, List<Accident<KR_ID, KT_ID>>> vehicleAccidentsEntry : accidents
                .entrySet()) {
            vehicle = db.getVehicle(vehicleAccidentsEntry.getKey());
            System.out.println(vehicle);
            for (Accident<KR_ID, KT_ID> accident : vehicleAccidentsEntry.getValue()) {
                route = routes.get(accident.routeID);
                System.out.print(route.getNumber());
                System.out.print(": -> ");
                System.out.print(route.direction);
                System.out.print(": ");
                try {
                    stop = stops.get(accident.start.nextStopID);
                    if (stop == null) {
                        stops.put(accident.start.nextStopID,
                                  stop = db.getStop(accident.start.nextStopID));
                    }
                    System.out.print(Math.round(accident.start.nextStopDist) + " м до " + stop.title +
                                     " {" +
                                     stop.ks_id.id + "}: ");
                } catch (SQLException e) {
                    System.out.print(accident.start.dist);
                }
                System.out.print(getFormattedDate(hmDateFormat, accident.start.timestamp));
                System.out.print("-");
                System.out.println(getFormattedDate(hmDateFormat, accident.end.timestamp));

            }
            System.out.println();
        }
    }

    /**
     * печать отклонений от графика
     *
     * @param connector
     * @param kr_id
     * @param timeLimits
     */
    public static void printDeviations(Connector connector, KR_ID kr_id, int[] timeLimits) {
        try (DataBase db = new DataBase(connector)) {
            int[] timeEdges = Utils.splitByDays(timeLimits);
            int periodsCount = timeEdges.length - 1;
            Period period;
            for (int i = 0; i < periodsCount; i++) {
                System.arraycopy(timeEdges, i, timeLimits, 0, 2);
                period = db.getRoutePeriodForTime(kr_id, timeEdges[i]);
                printDeviations(db, db.getDeviations(kr_id, timeLimits),
                                db.getRouteControlPoints(kr_id, period.id),
                                timeLimits);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * печать отклонений от графика
     *
     * @param db
     * @param deviations
     * @param controlPoints
     * @param timeLimits
     * @throws SQLException
     */
    public static void printDeviations(DataBase db, Map<CP_ID, List<Deviation>> deviations,
                                       List<ControlPoint> controlPoints, int[] timeLimits)
            throws SQLException {
        String title;
        int maxLength = 0;
        for (ControlPoint point : controlPoints) {
            title = point.name + " " + point.cp_id.id + "/" + point.ks_id.id;
            if (maxLength < title.length()) {
                maxLength = title.length();
            }
        }
        Map<CP_ID, List<Race>> real = db.getRouteRealSchedule(controlPoints.get(0).kr_id, timeLimits);

        String formatStr = "%1$30s";

        String label;
        int devTime, failCount, avgDeviationEarly, avgDeviationLate, early, late;
        Vehicle planVehicle, realVehicle = null;
        List<Deviation> cpDeviations;
        Race race = null;
        int hourPlan = 0;
        int hourReal = 0;
        int anotherVehicleCount = 0;
        ListIterator<Deviation> deviationsIterator;
        ListIterator<Race> raceIterator;
        Deviation dev = null;
        List<Race> pointRealSchedule;
        boolean nextReal, nextPlan, nextHour;

        System.out.print(String.format(formatStr, "TIME"));
        System.out.print("\t");
        System.out.print(String.format(formatStr, "REAL VEHICLE"));
        System.out.print("\t");
        System.out.print(String.format(formatStr, "PLAN VEHICLE IF NOT EQUALS"));
        System.out.print("\t");
        System.out.print(String.format(formatStr, "REAL TIMES"));
        System.out.print("\t");
        System.out.println(String.format(formatStr, "REAL VEHICLES"));
        System.out.println();

        for (ControlPoint point : controlPoints) {
            title = point.name + "\tID: " + point.cp_id.id + "\tKS_ID: " + point.ks_id.id;
            System.out.print(title + ":\t");
            cpDeviations = deviations.get(point.cp_id);
            if (cpDeviations == null || cpDeviations.size() == 0) {
                continue;
            }
            failCount = 0;
            anotherVehicleCount = 0;
            avgDeviationEarly = 0;
            avgDeviationLate = 0;
            early = 0;
            late = 0;


            for (Deviation deviation : cpDeviations) {
                devTime = Math.abs(deviation.deviation);
                if (deviation.realVehicle == null) {
                    failCount++;
                } else if (!deviation.realVehicle.equals(deviation.planVehicle)) {
                    anotherVehicleCount++;
                }
                if (deviation.deviation > 0) {
                    if (deviation.deviation < Integer.MAX_VALUE) {
                        avgDeviationLate += devTime;
                        late++;
                    }
                } else {
                    avgDeviationEarly += devTime;
                    early++;
                }
            }
//                if (cpDeviations.size() == failCount) {
//                    avgDeviation = 0;
//                } else {
//                    avgDeviation /= cpDeviations.size() - failCount;
//                }
            if (early > 0) {
                avgDeviationEarly /= early;
            }
            if (late > 0) {
                avgDeviationLate /= late;
            }
            System.out.print("fails: ");
            System.out.print(failCount);
            System.out.print("/");
            System.out.print(cpDeviations.size());
            System.out.print("\t");
            if (anotherVehicleCount > 0) {
                System.out.print("another vehicles: ");
                System.out.print(anotherVehicleCount);
            }
            System.out.print("\tavg deviation: ");
            System.out.print("-" + getFormattedDate(mSecFormat, avgDeviationEarly) + "/");
            printDate(mSecFormat, avgDeviationLate);
            System.out.println();


            deviationsIterator = cpDeviations.listIterator();
            pointRealSchedule = real.get(point.cp_id);
            if (pointRealSchedule == null) {
                pointRealSchedule = new ArrayList<>();
            }
            raceIterator = pointRealSchedule.listIterator();
            nextPlan = true;
            nextReal = true;
            nextHour = true;
            hourPlan = Integer.MAX_VALUE;
            hourReal = Integer.MAX_VALUE;

            do {
                if (deviationsIterator.hasNext()) {
                    if (nextPlan) {
                        dev = deviationsIterator.next();
                        nextHour &= (hourPlan <
                                     (hourPlan = Utils.getHourFromTimestamp(dev.planTime)));
                    } else {
                        nextHour = true;
                    }
                } else {
                    dev = null;
                    hourPlan = Integer.MAX_VALUE;
                }
                if (raceIterator.hasNext()) {
                    if (nextReal) {
                        race = raceIterator.next();
                        nextHour &= (hourReal <
                                     (hourReal = Utils.getHourFromTimestamp(race.startTime)));
                    } else {
                        nextHour = true;
                    }
                } else {
                    race = null;
                    hourReal = Integer.MAX_VALUE;
                }

                nextReal = hourPlan >= hourReal;
                nextPlan = hourPlan <= hourReal;

                if (nextHour) {
                    System.out.println();
                }

                if (dev != null && nextPlan) {
                    devTime = dev.deviation / 60;
                    label = getFormattedDate(hmDateFormat, dev.planTime);
                    if (devTime != 0) {
                        if (dev.deviation < Integer.MAX_VALUE) {
                            label += devTime > 0 ? "+" + devTime : devTime;
                        }
                    }
                    label += " " + dev.direction;
                    System.out.print(String.format(formatStr, label + "\t"));
                    if (dev.realVehicle != null) {
                        realVehicle = db.getVehicle(dev.realVehicle);
                        System.out.print(String.format(formatStr, realVehicle) + "\t");
                    } else {
                        System.out.print(String.format(formatStr, "FAIL\t"));
                    }
                    label = "\t";
                    if (dev.planVehicle != null) {
                        planVehicle = db.getVehicle(dev.planVehicle);
                        if (realVehicle == null || planVehicle.kt_id.id != realVehicle.kt_id.id) {
                            label = String.format(formatStr, planVehicle) + "\t";
                        }
                    }
                    System.out.print(String.format(formatStr, label));
                } else {
                    System.out.print(String.format(formatStr, "\t"));
                    System.out.print(String.format(formatStr, "\t"));
                    System.out.print(String.format(formatStr, "\t"));
                }

                if (race != null && nextReal) {
                    label = getFormattedDate(hmDateFormat, race.startTime);

                    if (race.endTime - race.startTime >= 60) {
                        label += "-";
                        if (Utils.getHourFromTimestamp(race.startTime) !=
                            Utils.getHourFromTimestamp(race.endTime)) {
                            label += getFormattedDate(hmDateFormat, race.endTime);
                        } else {
                            label += getFormattedDate(minuteFromDate, race.endTime);
                        }
                    }
                    System.out.print(String.format(formatStr, label + "\t"));
                    System.out.print(String.format(formatStr, db.getVehicle(race.kt_id)));
                }
                System.out.println();
            } while (dev != null || race != null);

            System.out.println();
        }
    }

    /**
     * Вывод HTML о направлениях всех ТС на маршруте
     *
     * @param connector
     * @param kr_id
     * @param timeLimits
     */
    public static void printDirections(Connector connector, KR_ID kr_id, int[] timeLimits) {
        try (DataBase db = new DataBase(connector)) {
            RouteIDs currentRouteIDs =
                    db.getRelatedRouteIDs(kr_id, Utils.getDayBeginning(timeLimits[0]));
            Map<VehicleTN, List<? extends PositionRelToRoutes>> routeMonitoring =
                    db.getRouteMonitoringByTNid(currentRouteIDs, timeLimits);
            printDirections(db, routeMonitoring);
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Вывод HTML о направлениях всех ТС на маршруте
     *
     * @param db
     * @param routeMonitoring
     */
    public static void printDirections(DataBase db,
                                       Map<VehicleTN, ? extends List<? extends PositionRelToRoutes>> routeMonitoring) {
        try (FileOutputStream fileOutputStream = new FileOutputStream(
                "/media/Data/Documents/TransportStateLogs/out.html")) {
            PrintStream out = new PrintStream(fileOutputStream, false, "windows-1251");
            for (Map.Entry<VehicleTN, ? extends List<? extends PositionRelToRoutes>> vehicleMonitoringEntry : routeMonitoring
                    .entrySet()) {
                printVehicleDirections(out, db, vehicleMonitoringEntry.getValue(),
                                       vehicleMonitoringEntry.getKey());
            }
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Вывод HTML о том, когда и к какому направлению маршрута привязано ТС
     *
     * @param out
     * @param db
     * @param vehicleMonitoring
     * @param vehicleTN
     * @throws SQLException
     */
    public static void printVehicleDirections(PrintStream out, DataBase db,
                                              List<? extends PositionRelToRoutes> vehicleMonitoring,
                                              VehicleTN vehicleTN) throws SQLException {
        Map<KS_ID, Stop> stops = new HashMap<>();
        Map<ID, Route> routes = new HashMap<>();
        Route route;
        Stop stop;
        PositionRelToRoutes prevPos = vehicleMonitoring.get(0);

        out.println(db.getVehicle(vehicleTN));

        out.print("<table>");
        out.print("<tr>");
        out.print("<td>");
        if (prevPos.routeID != null && prevPos.routeID.id > 0 &&
            prevPos.routeID.getClass() == KR_ID.class) {
            route = routes.get(prevPos.routeID);
            if (route == null) {
                routes.put(prevPos.routeID, route = db.getRoute((KR_ID) prevPos.routeID));
            }
            out.print(route.direction);
        } else {
            out.print("UNKNOWN");
        }
        out.print(": ");
        out.print("</td>");
        out.print("<td>");
        out.print("<a href=\"http://map.samadm.ru/transport/?defaultLayers=SM1,SM2,SM3,SM4&x=" +
                  Math.round(prevPos.x) + "&y=" + Math.round(prevPos.y) +
                  "&scale=0\" target=\"_blank\">");
        printTimeHMS(out, prevPos.timestamp);
        out.print("</a>");
        out.print("</td>");

        out.print("<td>");
        if (prevPos.nextStopID != null) {
            stop = stops.get(prevPos.nextStopID);
            if (stop == null) {
                stops.put(prevPos.nextStopID, stop = db.getStop(prevPos.nextStopID));
            }
            out.print(" <a href=\"http://tosamara.ru/spravka/ostanovki/");
            out.print(prevPos.nextStopID.id);
            out.print("/\" target=\"_blank\">");
            out.print(Math.round(prevPos.nextStopDist));
            out.print(" м до ост. ");
            out.print(stop.title);
            out.print("</a>");
        } else {
            out.print("&nbsp;");
        }
        out.print("</td>");

        for (PositionRelToRoutes position : vehicleMonitoring) {
            if (position.timestamp - prevPos.timestamp > 300 || position.routeID != prevPos.routeID ||
                position.routeID != null && prevPos.routeID != null &&
                position.routeID.id != prevPos.routeID.id) {

                out.print("<td>");
                out.print(" <a href=\"http://map.samadm.ru/transport/?defaultLayers=SM1,SM2,SM3,SM4&x=" +
                          Math.round(prevPos.x) + "&y=" + Math.round(prevPos.y) +
                          "&scale=0\" target=\"_blank\">");
                printTimeHMS(out, prevPos.timestamp);
                out.print("</a>");
                out.print("</td>");

                out.print("<td>");
                if (prevPos.nextStopID != null && prevPos.nextStopID.id > 0) {
                    stop = stops.get(prevPos.nextStopID);
                    if (stop == null) {
                        stops.put(prevPos.nextStopID, stop = db.getStop(prevPos.nextStopID));
                    }
                    out.print(" <a href=\"http://tosamara.ru/spravka/ostanovki/");
                    out.print(prevPos.nextStopID.id);
                    out.print("/\" target=\"_blank\">");
                    out.print(Math.round(prevPos.nextStopDist));
                    out.print(" м до ост. ");
                    out.print(stop.title);
                    out.print("</a>");
                } else {
                    out.print("&nbsp;");
                }
                out.print("</td>");
                out.print("</tr>");

                out.print("<tr>");
                out.print("<td>");
                if (position.routeID != null && position.routeID.id > 0 &&
                    position.routeID.getClass() == KR_ID.class) {
                    route = routes.get(position.routeID);
                    if (route == null) {
                        routes.put(position.routeID, route = db.getRoute((KR_ID) position.routeID));
                    }
                    out.print(route.direction);
                } else {
                    out.print("UNKNOWN");
                }
                out.print(": ");
                out.print("</td>");
                out.print("<td>");
                out.print(" <a href=\"http://map.samadm.ru/transport/?defaultLayers=SM1,SM2,SM3,SM4&x=" +
                          Math.round(position.x) + "&y=" + Math.round(position.y) +
                          "&scale=0\" target=\"_blank\">");
                printTimeHMS(out, position.timestamp);
                out.print("</a>");
                out.print("</td>");
                out.print("<td>");
                if (position.nextStopID != null && position.nextStopID.id > 0) {
                    stop = stops.get(position.nextStopID);
                    if (stop == null) {
                        stops.put(position.nextStopID, stop = db.getStop(position.nextStopID));
                    }
                    out.print(" <a href=\"http://tosamara.ru/spravka/ostanovki/");
                    out.print(position.nextStopID.id);
                    out.print("/\" target=\"_blank\">");
                    out.print(Math.round(position.nextStopDist));
                    out.print(" м до ост. ");
                    out.print(stop.title);
                    out.print("</a>");
                } else {
                    out.print("&nbsp;");
                }
                out.print("</td>");
            }
            prevPos = position;
        }
        out.print("<td>");
        out.print(" <a href=\"http://map.samadm.ru/transport/?defaultLayers=SM1,SM2,SM3,SM4&x=" +
                  Math.round(prevPos.x) + "&y=" + Math.round(prevPos.y) +
                  "&scale=0\" target=\"_blank\">");
        printTimeHMS(out, prevPos.timestamp);
        out.print("</a>");
        out.print("</td>");

        out.print("<td>");
        if (prevPos.nextStopID != null && prevPos.nextStopID.id > 0) {
            stop = stops.get(prevPos.nextStopID);
            if (stop == null) {
                stops.put(prevPos.nextStopID, stop = db.getStop(prevPos.nextStopID));
            }
            out.print(" <a href=\"http://tosamara.ru/spravka/ostanovki/");
            out.print(prevPos.nextStopID.id);
            out.print("/\" target=\"_blank\">");
            out.print(Math.round(prevPos.nextStopDist));
            out.print(" м до ост. ");
            out.print(stop.title);
            out.print("</a>");
        } else {
            out.print("&nbsp;");
        }
        out.print("</td>");
        out.print("</tr>");
        out.print("</table>");
        out.print("<br>");
        out.print("<br>");
    }

    /**
     * печать фактического графика движения
     *
     * @param db
     * @param todaySchedule
     * @param controlPoints
     * @throws SQLException
     */
    public static void printRealSchedule(DataBase db,
                                         Table<VehicleTN, CP_ID, List<RaceTimings>> todaySchedule,
                                         List<ControlPoint> controlPoints) throws SQLException {
        int hour, lastHour;
        boolean next;


        List<RaceTimings> cpTimes;
        int maxLength = 0;
        for (ControlPoint point : controlPoints) {
            if (maxLength < point.name.length()) {
                maxLength = point.name.length();
            }
        }
        String formatStr = "%1$" + maxLength + "s";
        for (VehicleTN vehicleTN : todaySchedule.rowKeySet()) {
            System.out.println(db.getVehicle(vehicleTN));
            for (ControlPoint point : controlPoints) {
                System.out.print(String.format(formatStr, point.name) + ":\t");

                lastHour = 5;

                next = false;
                cpTimes = todaySchedule.get(vehicleTN, point.cp_id);
                if (cpTimes != null) {
                    for (RaceTimings timings : cpTimes) {
                        hour = Utils.getHourFromTimestamp(timings.startTime);
                        if (hour == 0) {
                            hour = 24;
                        }
//                            minute = Integer.parseInt(getFormattedDate(minuteFromDate, timings.startTime));
                        for (; lastHour < hour; lastHour++) {
                            System.out.print(" \t");
                            next = false;
                        }

                        if (next) {
                            System.out.print("; ");
                        }
                        printTimeHM(timings.startTime);
                        if (timings.endTime - timings.startTime >= 60) {
                            System.out.print("-");
                            if (Utils.getHourFromTimestamp(timings.startTime) !=
                                Utils.getHourFromTimestamp(timings.endTime)) {
                                printTimeHM(timings.endTime);
                            } else {
                                printDate(minuteFromDate, timings.endTime);
                            }
//                                System.out.print((timings.endTime - timings.startTime) / 60);
//                                printTime( timings.endTime);
                        }
                        next = true;
                    }
                }

                System.out.println();
            }
            System.out.println();
        }
    }

    /**
     * печать фактического графика движения
     *
     * @param connector
     * @param kr_id
     * @param timeLimits
     */
    public static void printRealSchedule(Connector connector, KR_ID kr_id, int[] timeLimits) {
        try (DataBase db = new DataBase(connector)) {
            int[] timeEdges = Utils.splitByDays(timeLimits);
            int periodsCount = timeEdges.length - 1;
            Period period;
            for (int i = 0; i < periodsCount; i++) {
                System.arraycopy(timeEdges, i, timeLimits, 0, 2);
                period = db.getRoutePeriodForTime(kr_id, timeEdges[i]);
                Table<VehicleTN, CP_ID, List<RaceTimings>> schedule =
                        db.getRouteRealScheduleByVehicle(kr_id, timeLimits);
                List<ControlPoint> controlPoints = db.getRouteControlPointsWithCoords(kr_id, period.id);

                printRealSchedule(db, schedule, controlPoints);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static String getFormattedDate(DateFormat dateFormat, int time) {
        date.setTime(time * 1000l);
        return dateFormat.format(date);
    }

    public static String getFormattedDate(int time) {
        return getFormattedDate(hmDateFormat, time);
    }

    public static void printDate(int time) {
        printDate(System.out, hmDateFormat, time);
    }

    public static void printDate(DateFormat dateFormat, int time) {
        printDate(System.out, dateFormat, time);
    }

    public static void printDate(PrintStream out, DateFormat dateFormat, int time) {
        out.print(getFormattedDate(dateFormat, time));
    }

    public static void printTimeHM(int time) {
        printTimeHM(System.out, time);
    }

    public static void printTimeHM(PrintStream out, int time) {
        out.print(getFormattedDate(hmDateFormat, time));
    }

    public static void printTimeHMS(int time) {
        printTimeHMS(System.out, time);
    }

    public static void printTimeHMS(PrintStream out, int time) {
        out.print(getFormattedDate(hmsDateFormat, time));
    }

    public static void printlnDate(DateFormat dateFormat, int time) {
        System.out.println(getFormattedDate(dateFormat, time));
    }

    public static void printTimeBetweenCPs(String path, List<KR_ID[]> incedentalRoutes,
                                     TimeBetween[][] times) {
        for (int i = 0; i < incedentalRoutes.size(); i++) {
            KR_ID[] kr_ids = incedentalRoutes.get(i);
            StringBuilder fileName = new StringBuilder();
            for (KR_ID kr_id : kr_ids) {
                fileName.append("_").append(kr_id.id);
            }
            try (FileWriter writer = new FileWriter(
                    path + fileName.substring(1) + "_time.txt",
                    true
            )) {
                for (TimeBetween matching : times[i]) {
                    writer.write(String.valueOf(matching.totalTime));
                    writer.write("\t");
                }
                writer.write("\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
            try (FileWriter writer = new FileWriter(
                    path + fileName.substring(1) + "_count.txt",
                    true
            )) {
                for (TimeBetween matching : times[i]) {
                    writer.write(String.valueOf(matching.count));
                    writer.write("\t");
                }
                writer.write("\n");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void printStatsTram3_20(Connector connector, int[] timeLimits) {
        try (DataBase db = new DataBase(connector)) {
            Set<KR_ID> kr_ids = new HashSet<>();
            kr_ids.add(new KR_ID(114));
            kr_ids.add(new KR_ID(115));
            kr_ids.add(new KR_ID(100));
            kr_ids.add(new KR_ID(99));

            Map<KR_ID, Table<VehicleTN, CP_ID, List<RaceTimings>>> realSchedule = new HashMap<>();

//            int[] week = new int[]{1, 2, 3, 6, 7, 8, 9, 10, 13, 14, 15, 16, 17, 20, 21, 22, 27, 28};
            List<Integer> hol = Arrays.asList(4, 5, 11, 12, 18, 19, 23, 24, 25, 26);


            for (int i = 1; i <= 28; i++) {
                if (hol.contains(i)) {
                    continue;
                }
                realSchedule.clear();
                for (KR_ID kr_id : kr_ids) {
                    realSchedule.put(kr_id,
                                     db.getRouteRealScheduleByVehicle(
                                             kr_id,
                                             Utils.getDayLimits(2017, 02, i)
                                     )
                    );
                }
                printStatsTram3_20_diff(realSchedule);
            }

            for (int i = 1; i <= 28; i++) {
                if (!hol.contains(i)) {
                    continue;
                }
                realSchedule.clear();
                for (KR_ID kr_id : kr_ids) {
                    realSchedule.put(kr_id,
                                     db.getRouteRealScheduleByVehicle(
                                             kr_id,
                                             Utils.getDayLimits(2017, 02, i)
                                     )
                    );
                }
                printStatsTram3_20_diff(realSchedule);
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void printStatsTram3_20(
            Map<KR_ID, Table<VehicleTN, CP_ID, List<RaceTimings>>> realSchedule)
            throws SQLException {
        Map<KR_ID, CP_ID[]> routes = new HashMap<>();
        routes.put(new KR_ID(115),
                   new CP_ID[]{
                           new CP_ID(10487),
                           new CP_ID(10473)
                   }
        );
        routes.put(new KR_ID(114),
                   new CP_ID[]{
                           new CP_ID(10473),
                           new CP_ID(10487)
                   }
        );
        routes.put(new KR_ID(100),
                   new CP_ID[]{
                           new CP_ID(10496),
                           new CP_ID(10487)
                   }
        );
        routes.put(new KR_ID(99),
                   new CP_ID[]{
                           new CP_ID(10523),
                           new CP_ID(10549)
                   }
        );


        KR_ID kr_id = new KR_ID(114);
        CP_ID[] value = routes.get(kr_id);
        TimeBetween[] avgTime1 =
                Calculator.avgTimeBetweenCPsByHours(realSchedule.get(kr_id),
                                                    value[0],
                                                    value[1],
                                                    15 * 60);

        kr_id = new KR_ID(100);
        value = routes.get(kr_id);
        TimeBetween[] avgTime2 =
                Calculator.avgTimeBetweenCPsByHours(realSchedule.get(kr_id),
                                                    value[0],
                                                    value[1],
                                                    15 * 60);

        int[] avgTime = new int[20];
        for (int i = 0; i < 20; i++) {
            avgTime[i] = (avgTime1[i].count + avgTime2[i].count) > 0 ?
                         (avgTime1[i].totalTime + avgTime2[i].totalTime) /
                         (avgTime1[i].count + avgTime2[i].count) : 0;
        }

        try (FileOutputStream fos = new FileOutputStream(
                "/media/Data/Documents/TransportStateLogs/timeFrunze.txt",
                true)) {
            PrintStream out = new PrintStream(fos);

            for (int i = 0; i < avgTime.length; i++) {
                out.print(avgTime[i]);
                out.print("\t");
            }

            out.print("\t\t");

            kr_id = new KR_ID(115);
            value = routes.get(kr_id);
            avgTime1 = Calculator.avgTimeBetweenCPsByHours(realSchedule.get(kr_id),
                                                           value[0],
                                                           value[1],
                                                           15 * 60);

            kr_id = new KR_ID(99);
            value = routes.get(kr_id);
            avgTime2 = Calculator.avgTimeBetweenCPsByHours(realSchedule.get(kr_id),
                                                           value[0],
                                                           value[1],
                                                           15 * 60);

            avgTime = new int[20];
            for (int i = 0; i < 20; i++) {
                avgTime[i] = (avgTime1[i].count + avgTime2[i].count) > 0 ?
                             (avgTime1[i].totalTime + avgTime2[i].totalTime) /
                             (avgTime1[i].count + avgTime2[i].count) : 0;
            }

            for (int i = 0; i < avgTime.length; i++) {
                out.print(avgTime[i]);
                out.print("\t");
            }

            out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void printStatsTram3_20_diff(
            Map<KR_ID, Table<VehicleTN, CP_ID, List<RaceTimings>>> realSchedule
    ) {
        Map<KR_ID, CP_ID[]> routes = new HashMap<>();
        routes.put(new KR_ID(115),
                   new CP_ID[]{
                           new CP_ID(10487),
                           new CP_ID(10473)
                   }
        );
        routes.put(new KR_ID(114),
                   new CP_ID[]{
                           new CP_ID(10473),
                           new CP_ID(10487)
                   }
        );
        routes.put(new KR_ID(100),
                   new CP_ID[]{
                           new CP_ID(10496),
                           new CP_ID(10487)
                   }
        );
        routes.put(new KR_ID(99),
                   new CP_ID[]{
                           new CP_ID(10523),
                           new CP_ID(10549)
                   }
        );


        try (FileOutputStream fos = new FileOutputStream(
                "/media/Data/Documents/TransportStateLogs/timeFrunze.txt",
                true)) {
            PrintStream out = new PrintStream(fos);

            KR_ID kr_id = new KR_ID(114);
            CP_ID[] value = routes.get(kr_id);
            TimeBetween[] avgTime1 =
                    Calculator.avgTimeBetweenCPsByHours(realSchedule.get(kr_id),
                                                        value[0],
                                                        value[1],
                                                        15 * 60);
            for (int i = 0; i < avgTime1.length; i++) {
                out.print(avgTime1[i].totalTime / avgTime1[i].count);
                out.print("\t");
            }
            out.print("\t\t");

            kr_id = new KR_ID(100);
            value = routes.get(kr_id);
            avgTime1 = Calculator.avgTimeBetweenCPsByHours(realSchedule.get(kr_id),
                                                           value[0],
                                                           value[1],
                                                           15 * 60);

            for (int i = 0; i < avgTime1.length; i++) {
                out.print(avgTime1[i].totalTime / avgTime1[i].count);
                out.print("\t");
            }
            out.print("\t\t");

            kr_id = new KR_ID(115);
            value = routes.get(kr_id);
            avgTime1 = Calculator.avgTimeBetweenCPsByHours(realSchedule.get(kr_id),
                                                           value[0],
                                                           value[1],
                                                           15 * 60);
            for (int i = 0; i < avgTime1.length; i++) {
                out.print(avgTime1[i].totalTime / avgTime1[i].count);
                out.print("\t");
            }
            out.print("\t\t");

            kr_id = new KR_ID(99);
            value = routes.get(kr_id);
            avgTime1 = Calculator.avgTimeBetweenCPsByHours(realSchedule.get(kr_id),
                                                           value[0],
                                                           value[1],
                                                           15 * 60);

            for (int i = 0; i < avgTime1.length; i++) {
                out.print(avgTime1[i].totalTime / avgTime1[i].count);
                out.print("\t");
            }

            out.println();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
