package ru.markikokik.transtimings;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import ru.markikokik.transtimings.entities.Route;
import ru.markikokik.transtimings.entities.RouteGeoData;
import ru.markikokik.transtimings.entities.Stop;
import ru.markikokik.transtimings.entities.TransType;
import ru.markikokik.transtimings.entities.ids.KR_ID;
import ru.markikokik.transtimings.entities.ids.KS_ID;

public class XmlParser {
    public static Map<KR_ID, Route> parseGeoportalRoutes(InputStream in, Map<KR_ID, Route> routes)
            throws XmlPullParserException, IOException {
        XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
        parser.setInput(in, "UTF-8");
//        Map<Integer, GeoportalInfo> map = new HashMap<>();
        KR_ID kr_id = new KR_ID();
//        GeoportalInfo info;
        String layerName = null;
        int geoportalID = 0;
        Route route;
        while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
            if (parser.getEventType() == XmlPullParser.START_TAG
                && "route".equals(parser.getName())) {
                parser.next();
                if (parser.getEventType() == XmlPullParser.IGNORABLE_WHITESPACE) {
                    parser.next();
                }
//                info = new GeoportalInfo();
                while (!parser.getName().equals("route")) {
                    switch (parser.getName()) {
                        case "KR_ID":
                            kr_id = new KR_ID(Integer.parseInt(parser.nextText()));
                            break;
                        case "geoportalId":
                            geoportalID = Integer.parseInt(parser.nextText());
                            break;
                        case "layerName":
                            layerName = parser.nextText();
                            break;
                        default:
                            parser.nextText();
//                            parser.next();
                    }
                    parser.next();
                    if (parser.getEventType() == XmlPullParser.IGNORABLE_WHITESPACE) {
                        parser.next();
                    }
                }
                route = routes.get(kr_id);
                if (route != null) {
                    route.layerName = layerName;
                    route.geoportalId = geoportalID;
                }
            }
            parser.next();
            if (parser.getEventType() == XmlPullParser.IGNORABLE_WHITESPACE) {
                parser.next();
            }
        }

        return routes;
    }


    public static void parseRoutes(InputStream in, Map<KR_ID, Route> routes)
            throws XmlPullParserException, IOException {
        XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
        parser.setInput(in, "UTF-8");
        Route r;
        while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
            if (parser.getEventType() == XmlPullParser.START_TAG
                && "route".equals(parser.getName())) {
                r = new Route();
                parser.next();
                if (parser.getEventType() == XmlPullParser.IGNORABLE_WHITESPACE) {
                    parser.next();
                }
                while (!"route".equals(parser.getName())) {
                    switch (parser.getName()) {
                        case "KR_ID":
                            r.kr_id = new KR_ID(Integer.parseInt(parser.nextText()));
                            break;
                        case "number":
                            r.setNumber(parser.nextText());
                            break;
                        case "direction":
                            r.direction = parser.nextText();
                            break;
                        case "transportTypeID":
                            r.transportTypeID = TransType.getTransTypeMask(
                                    Integer.parseInt(parser.nextText())
                            );
//                            r.transportTypeID = Integer.parseInt(parser.nextText());
                            break;
                        case "affiliationID":
                            r.affiliationID = TransType.getAffiliationMask(
                                    Integer.parseInt(parser.nextText())
                            );
//                            r.affiliationID = Integer.parseInt(parser.nextText());
                            break;
                        default:
                            parser.nextText();
                    }
                    if (parser.getEventType() == XmlPullParser.IGNORABLE_WHITESPACE) {
                        parser.next();
                    }
                    parser.next();
                    if (parser.getEventType() == XmlPullParser.IGNORABLE_WHITESPACE) {
                        parser.next();
                    }
                }
                routes.put(r.kr_id, r);
            }
            parser.next();
            if (parser.getEventType() == XmlPullParser.IGNORABLE_WHITESPACE) {
                parser.next();
            }
        }
    }

    public static List<RouteGeoData> parseRoutesGeoData(InputStream in)
            throws XmlPullParserException, IOException {
        XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
        parser.setInput(in, "UTF-8");
        List<RouteGeoData> result = new ArrayList<>();
        RouteGeoData geoData;
        String[] geometry;
        while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
            if (parser.getEventType() == XmlPullParser.START_TAG
                && "route".equals(parser.getName())) {
                geoData = new RouteGeoData();
                parser.next();
                if (parser.getEventType() == XmlPullParser.IGNORABLE_WHITESPACE) {
                    parser.next();
                }
                while (!"route".equals(parser.getName())) {
                    switch (parser.getName()) {
                        case "KR_ID":
                            geoData.kr_id = new KR_ID(Integer.parseInt(parser.nextText()));
                            break;
                        case "geometry":
                            geometry = parser.nextText().trim().split("[ ,]");
                            geoData.geometry = new double[geometry.length];
                            for (int i = 0; i < geometry.length; i++) {
                                geoData.geometry[i] = Double.parseDouble(geometry[i]);
                            }
                            break;
                        case "performing":
                            parser.nextText();
//                            performing = "1".equals(parser.nextText());
                            break;
                        case "transportType":
                            parser.next();
                            while (!"transportType".equals(parser.getName())) {
                                parser.next();
                                while (parser.getEventType() != XmlPullParser.START_TAG &&
                                       parser.getEventType() != XmlPullParser.END_TAG) {
                                    parser.next();
                                }

                            }
                            break;
                        case "stop":
                            parser.next();
                            while (!"stop".equals(parser.getName())) {
                                if ("KS_ID".equals(parser.getName())) {
                                    geoData.addStop(new KS_ID(Integer.parseInt(parser.nextText())));
                                }
                                parser.next();
                                while (parser.getEventType() != XmlPullParser.START_TAG &&
                                       parser.getEventType() != XmlPullParser.END_TAG) {
                                    parser.next();
                                }
                            }
                            break;
                        default:
                            parser.nextText();
                    }
                    if (parser.getEventType() == XmlPullParser.IGNORABLE_WHITESPACE) {
                        parser.next();
                    }
                    parser.next();
                    if (parser.getEventType() == XmlPullParser.IGNORABLE_WHITESPACE) {
                        parser.next();
                    }
                }

                result.add(geoData);
            }
            parser.next();
            if (parser.getEventType() == XmlPullParser.IGNORABLE_WHITESPACE) {
                parser.next();
            }
        }

        return result;
    }

    public static List<Stop> parseStopsFullDB(InputStream in)
            throws XmlPullParserException, IOException {
        XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
        parser.setInput(in, "UTF-8");
        List<Stop> list = new ArrayList<>();
        Stop st;
        while (parser.getEventType() != XmlPullParser.END_DOCUMENT) {
            if (parser.getEventType() == XmlPullParser.START_TAG
                && "stop".equals(parser.getName())) {
                st = new Stop();
                parser.next();
                if (parser.getEventType() == XmlPullParser.IGNORABLE_WHITESPACE) {
                    parser.next();
                }
                while (!"stop".equals(parser.getName())) {
                    switch (parser.getName()) {
                        case "KS_ID":
                            st.ks_id = new KS_ID(Integer.parseInt(parser.nextText()));
                            break;
                        case "title":
                            st.title = parser.nextText().trim();
                            break;
                        case "adjacentStreet":
                            st.adjacentStreet = parser.nextText().trim();
                            break;
                        case "direction":
                            st.direction = parser.nextText();
                            break;
                        case "busesMunicipal":
                            st.busesMunicipal = parser.nextText();
                            break;
                        case "busesCommercial":
                            st.busesCommercial = parser.nextText();
                            break;
                        case "busesPrigorod":
                            st.busesPrigorod = parser.nextText();
                            break;
                        case "busesSeason":
                            st.busesSeason = parser.nextText();
                            break;
                        case "busesSpecial":
                            st.busesSpecial = parser.nextText();
                            break;
                        case "trams":
                            st.trams = parser.nextText();
                            break;
                        case "trolleybuses":
                            st.trolleybuses = parser.nextText();
                            break;
                        case "latitude":
                            st.lat = Double.parseDouble(parser.nextText());
                            break;
                        case "longitude":
                            st.lng = Double.parseDouble(parser.nextText());
                            break;
                        default:
                            parser.nextText();
//                            parser.next();
                    }
                    parser.next();
                    if (parser.getEventType() == XmlPullParser.IGNORABLE_WHITESPACE) {
                        parser.next();
                    }
                }
                list.add(st);
            }
            parser.next();
            if (parser.getEventType() == XmlPullParser.IGNORABLE_WHITESPACE) {
                parser.next();
            }
        }

        return list;
    }
}
